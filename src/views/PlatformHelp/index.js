import React from "react";
import helpPic from "@/assets/image/help.png";
import { Breadcrumb } from "antd";
import style from "./index.module.css";
function PlatformHelp(props){

    return(
        <div className="container">
            <Breadcrumb className="breadcrumb">
                <Breadcrumb.Item className="breadcrumb-item">平台帮助</Breadcrumb.Item>
            </Breadcrumb>
            <div className={style.box}>
                <p style={{float:"right"}}>
                    <img className={style.image} src={helpPic} alt="平台帮助" />
                </p>
                <p>本平台致力于：</p>
                <ol>
                    <li className={style.listItem}>收集、整理互联网中广泛分布的<b>文本、图表、图像、地图、音频、视频</b>六大类型的数据；</li>
                    <li className={style.listItem}>分析、挖掘这六类数据中所包含的<b>时间、地点、人物、事物、事件、现象、场景</b>七类要素信息；</li>
                    <li className={style.listItem}>构建不同主题的场景对信息进行有序聚合，并从<b>语义、概念、位置、几何、关系、过程</b>等方面进行更新。</li>
                </ol>
                <p className="textIndent">
                    基于此目的，收集散布于互联网中的文本、图表、图像、地图、音频、视频数据（“原数据”），
                    提取其类型、结构、内容等信息（“标签”），并挖掘这些标签关联的高层信息（“七要素”）。
                    为此，客户端提供了数据资源、信息标签、信息检索、信息溯源模块。在这些模块中，用户可以从不同的角度查询、
                    检索原数据、标签、语义等内容。
                </p>
                <p className="textIndent">
                    另外，平台还提供了位置聚合、动态更新模块。通过这两个模块，用户可以根据其所关心的某一主题，检索到与该主题相关的时间、
                    地点等要素信息以及文本、视频等原始数据。如：在位置聚合下的事件聚合模块中，搜索关键字“利奇马”，可以查询到台风“利奇马”相关的时间、
                    地点、人物、事物等碎片化的标签信息，以及这些标签信息的来源（文本、地图、视频等）。
                </p>
            </div>
        </div>
    );
}

export default PlatformHelp;