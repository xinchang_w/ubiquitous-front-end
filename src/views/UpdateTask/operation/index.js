import React, {Component} from "react";
import {Card, Drawer, Button, Input, DatePicker, Table, Space, Modal, message} from "antd";
import _MAP from "@/assets/image/地图.png";
import _Text from "@/assets/image/文本.png";
import _VIDEO from "@/assets/image/视频.png";
import _AUDIO from "@/assets/image/音频.png";
import {CheckOutlined, MenuUnfoldOutlined, RollbackOutlined, SaveOutlined} from "@ant-design/icons";
import Draggable from "react-draggable";
import moment from "moment";
import {v4 as uuidv4} from "uuid";
import DraggableCard from "@/components/customizeCard/index";
import axios from "axios";
import style from "@/views/SubjectCus/operation/index.module.css";
import {TableRendering} from "@/components";
import {stopPropagation} from "@/utility";
import audioSvg from "@/assets/image/data-query-audio.svg";
import videoSvg from "@/assets/image/data-query-video.svg";
import {NODE_PATH} from "@/assets/global";
import ALLMAP from "@/components/aggregation/customizeMap_allmap/index";
import BUSLINECOMPARE from "@/components/aggregation/customizeMap_busLineCompare/index";
import DYNAMICMAP from "@/components/aggregation/customizeMap_dynamicMap/index";


const ITEMS = {
    "text": {
        "title": "文本",
        "url": "aggregation/text/query",
        "searchCons": [
            {
                "label": "内容",
                "value": "content"
            },
            {
                "label": "来源",
                "value": "author"
            },
            {
                "label": "发布日期",
                "value": "time"
            }
        ],
        "cols": [
            {
                title: '发布日期',
                dataIndex: 'time',
                width: "100px",
                onCell: stopPropagation
            }, {
                title: "内容",
                dataIndex: "content",

                ellipsis: {
                    showTitle: false
                },
                className: "pointer"
            }, {
                title: '来源',
                dataIndex: 'author',
                width: "100px",
                onCell: stopPropagation
            }
        ]
    },
    "audio": {
        "title": "音频",
        "url": "aggregation/audio/query",
        "searchCons": [
            {
                "label": "节目名",
                "value": "programName"
            },
            {
                "label": "播出时间",
                "value": "date"
            }
        ],
        "cols": [
            {
                "title": "播出时间",
                "dataIndex": "time",
                "width": "200px",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "dataIndex": "url",
                "render": () => <img src={audioSvg} width="20" height="20" alt="音频"/>,
                "className": "pointer"
            },
        ]
    },
    "video": {
        "title": "视频",
        "url": "aggregation/video/query",
        "searchCons": [
            {
                "label": "拍摄时间",
                "value": "date"
            },
            {
                "label": "摄像头位置",
                "value": "location"
            }
        ],
        "cols": [
            {
                "title": "摄像头位置",
                "dataIndex": "location",
                "width": "150px",
                "ellipsis": true,
                "className": "pointer"
            },
            {
                "title": "内容",
                "dataIndex": "video",
                "ellipsis": true,
                "render": () => <img src={videoSvg} width="20" height="20" alt="视频"/>,
                "className": "pointer"
            },
            {
                "title": "拍摄时间",
                "dataIndex": "time",
                "width": "160px",
                "ellipsis": true,
                "onCell": stopPropagation
            },
        ]
    }
};

class Operation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //组件信息存储
            "componentsInfo": [],
            //组件宽高配置
            "widthValue": 400,
            "heightValue": 220,
            "key": 'tab1',

        }

        //数据配置面板append jsx dom
        this.dataConfigDom = [];
        //记录哪个类型组件被点击
        this.dataConfigtype = "";
        //此可视化案例唯一标识
        this.uid = window.location.search.slice(4);

        this.dataIds = [];

        this.dataUrl = "";
        this.textUrl = "http://94.191.49.160:8081/api/aggregation/text/query";
        this.videoUrl = "http://94.191.49.160:8081/api/aggregation/video/query";
        this.audioUrl = "http://94.191.49.160:8081/api/aggregation/audio/query";
        this.textCols = ITEMS["text"].cols;
        this.videoCols = ITEMS["video"].cols;
        this.audioCols = ITEMS["audio"].cols;
        this.pageSize = 2;

        this.dateTime = "";
        this.textMomentTime = moment('2021/06/26 17:00:00', 'YYYY/MM/DD HH:mm:ss');
        this.videoMomentTime = moment('2021/06/29 18:37:21', 'YYYY/MM/DD HH:mm:ss');
        this.audioMomentTime = moment('2021/06/29 17:04:00', 'YYYY/MM/DD HH:mm:ss');
        this.momentTime = null;
        this.weiboData = [];
        this.videoData = "";
        this.audioData = "";

        //组件容器的ref
        this.componentParentRef = React.createRef();

        //可拖动模态框的ref
        this.draggleRef = React.createRef();

        this.uidGenerate = this.uidGenerate.bind(this);
        this.addComponentInfo = this.addComponentInfo.bind(this);
        this.deleteComponentInfo = this.deleteComponentInfo.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);

        this.textTableRef = React.createRef();
        this.videoTableRef = React.createRef();
        this.audioTableRef = React.createRef();


        this.selectRow = this.selectRow.bind(this);
        this.selectRecord = {};
    }


    state = {
        //设置面板默认key

        //组件唯一标识
        componentKey: uuidv4(),
        //左侧抽屉参数
        visible: false,
        //模态框参数
        disabled: true,
        visibleModal: false,
        bounds: {left: 0, top: 0, bottom: 0, right: 0},

        //数据配置
        dataConfigDom: this.dataConfigDom,
    };

    componentDidMount() {
        this.getComponentInfo();
    }


    //组件面板（抽屉）的关闭与显示
    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    //获取时间设置面板所选的时间
    onChange(value, dateString) {
        this.dateTime = dateString;
        console.log(value, dateString, this.dateTime);
    }

    //模态框的显示与隐藏
    showModal() {
        this.setState({
            visibleModal: true,
            dataConfigDom: [],
        });
    }

    hideModal() {
        this.setState({
            visibleModal: false,
        });
    }

    onStart = (event, uiData) => {
        const {clientWidth, clientHeight} = window?.document?.documentElement;
        const targetRect = this.draggleRef?.current?.getBoundingClientRect();
        this.setState({
            bounds: {
                left: -targetRect?.left + uiData?.x,
                right: clientWidth - (targetRect?.right - uiData?.x),
                top: -targetRect?.top + uiData?.y,
                bottom: clientHeight - (targetRect?.bottom - uiData?.y),
            },
        });
    };

    //初始加载已有的组件数据
    getComponentInfo = () => {
        let comInfo = this.state.componentsInfo;
        axios
            .get("http://94.191.49.160:8081/api/aggregation/task/search/" + this.props.item._id)
            .then((res) => {
                let cont = res.data.data.content;
                console.log("getComponentInfo", cont);
                cont.map((item) => (
                    comInfo.push({
                        componentType: item.componentType,
                        dataType: item.dataType,
                        style: item.style,
                        dataIds: item.dataIds,
                        data: item.data[0],
                        key: item._id,
                    })
                ));
                this.setState({
                    componentsInfo: comInfo,
                });
            });

    }

    //生成uid
    uidGenerate = () => {
        this.setState({
            componentKey: uuidv4(),
        });
        return this.state.componentKey;
    };

    selectRow(record) {
        console.log("当前选中行数据", record);
        this.selectRecord = record;
    }

    addComponentInfo(componentType) {
        let comInfo = this.state.componentsInfo;
        let comData = this.selectRecord;
        console.log("componentsInfo", comInfo);
        console.log("comData", comData);

        let uid = uuidv4();

        //只加载查询到的第一条数据
        comInfo.push({
            componentType: componentType,
            dataType: componentType,
            style: {
                top: 200,
                left: 400,
                width: Number(this.state.widthValue),
                height: Number(this.state.heightValue)
            },
            dataIds: comData._id ? [comData._id] : [],
            data: comData,
            key: uid
        });
        this.hideModal();
        this.setState({
            componentsInfo: comInfo,
        });
        console.log("添加的组件信息", comInfo);

        // if (JSON.stringify(comData) == "{}") {
        //     alert("请选择数据");
        // } else {
        //     //只加载查询到的第一条数据
        //     comInfo.push({
        //         componentType: componentType,
        //         dataType: componentType,
        //         style: {
        //             top: 200,
        //             left: 400,
        //             width: Number(this.state.widthValue),
        //             height: Number(this.state.heightValue)
        //         },
        //         dataIds: [comData._id],
        //         data: comData,
        //         key: uid
        //     });
        //     this.hideModal();
        //     this.setState({
        //         componentsInfo: comInfo,
        //     });
        //     console.log("添加的组件信息", comInfo);
        // }
    }


    search(componentType) {
        //查询条件
        let con = {};
        this.momentTime["text"] = {
            value: this.textMomentTime._i,
        };
        this.momentTime["video"] = {
            value: this.videoMomentTime._i,
        };
        this.momentTime["audio"] = {
            value: this.audioMomentTime._i,
        };
        let curSearchCon = "time";
        let keywords = this.dateTime;
        if (this.dateTime == "") {
            keywords = this.momentTime[componentType].value;
        }

        let url = "http://94.191.49.160:8081/api/" + ITEMS[componentType].url;
        let pageSize = 2;

        console.log("url", url);
        let columns = ITEMS[componentType].cols;
        if (curSearchCon && keywords) {
            con[curSearchCon] = keywords;
        }

        switch (componentType) {
            case "text":
                this.textTableRef.current.search(url, columns, con,pageSize);
                break;
            case "video":
                this.videoTableRef.current.search(url, columns, con,pageSize);
                break;
            case "audio":
                this.audioTableRef.current.search(url, columns, con,pageSize);
                break;
        }


    }

    deleteComponentInfo(index, e) {
        let comInfo = this.state.componentsInfo;
        comInfo.splice(index, 1);
        this.setState({
            componentsInfo: comInfo,
        });
    }


    //加载组件数据到页面，渲染组件
    loadComponentDom = () => {
        let comInfo = this.state.componentsInfo;
        return (
            comInfo.map((item, index) => {
                    let comDom;
                    this.refKey = item.key;
                    switch (item.componentType) {
                        case "text":
                            comDom = (
                                <DraggableCard
                                    title="路况微博"
                                    closable={true}
                                    resizable={true}
                                    movable={true}
                                    onClose={this.deleteComponentInfo.bind(this, index)}
                                    key={item.key}
                                    style={{
                                        width: item.style.width,
                                        height: item.style.height,
                                        top: item.style.top,
                                        left: item.style.left
                                    }}
                                    id={item.key}
                                >
                                    <p>
                                        {
                                            item.data.time + item.data.content
                                        }
                                    </p>
                                </DraggableCard>
                            )
                            break;
                        case "video":
                            comDom = (
                                <DraggableCard
                                    title="路况监控视频"
                                    closable={true}
                                    resizable={true}
                                    movable={true}
                                    onClose={this.deleteComponentInfo.bind(this, index)}
                                    key={item.key}
                                    style={{
                                        width: item.style.width,
                                        height: item.style.height,
                                        top: item.style.top,
                                        left: item.style.left
                                    }}
                                    id={item.key}
                                >
                                    <video
                                        src={
                                            "http://94.191.49.160/resource/" + item.data.video
                                        }
                                        controls
                                    >
                                    </video>
                                </DraggableCard>
                            )
                            break;
                        case "audio":
                            comDom = (
                                <DraggableCard
                                    title="路况广播"
                                    closable={true}
                                    resizable={true}
                                    movable={true}
                                    onClose={this.deleteComponentInfo.bind(this, index)}
                                    key={item.key}
                                    style={{
                                        width: item.style.width,
                                        height: item.style.height,
                                        top: item.style.top,
                                        left: item.style.left
                                    }}
                                    id={item.key}
                                >
                                    <audio
                                        src={"http://94.191.49.160/resource/" + item.data.audio
                                        }
                                        controls
                                    >
                                    </audio>
                                </DraggableCard>
                            )
                            break;
                        case "allmap":
                            comDom = (
                                <DraggableCard
                                    title="allmap"
                                    closable={true}
                                    resizable={true}
                                    movable={true}
                                    onClose={this.deleteComponentInfo.bind(this, index)}
                                    key={item.key}
                                    style={{
                                        width: item.style.width,
                                        height: item.style.height,
                                        top: item.style.top,
                                        left: item.style.left
                                    }}
                                    id={item.key}
                                >
                                    <ALLMAP></ALLMAP>
                                </DraggableCard>
                            )
                            break;
                        case "busLineCompare":
                            comDom = (
                                <DraggableCard
                                    title="busLineCompare"
                                    closable={true}
                                    resizable={true}
                                    movable={true}
                                    onClose={this.deleteComponentInfo.bind(this, index)}
                                    key={item.key}
                                    style={{
                                        width: item.style.width,
                                        height: item.style.height,
                                        top: item.style.top,
                                        left: item.style.left
                                    }}
                                    id={item.key}
                                >
                                    <BUSLINECOMPARE></BUSLINECOMPARE>
                                </DraggableCard>
                            )
                            break;
                        case "dynamicMap":
                            comDom = (
                                <DraggableCard
                                    title="dynamicMap"
                                    closable={true}
                                    resizable={true}
                                    movable={true}
                                    onClose={this.deleteComponentInfo.bind(this, index)}
                                    key={item.key}
                                    style={{
                                        width: item.style.width,
                                        height: item.style.height,
                                        top: item.style.top,
                                        left: item.style.left
                                    }}
                                    id={item.key}
                                >
                                    <DYNAMICMAP></DYNAMICMAP>
                                </DraggableCard>
                            )
                            break;
                    }
                    return comDom;
                }
            )
        );
    }


    //数据配置,key唯一标识，type类型
    dataConfig = (componentKey, type) => {
        if (this.dataConfigDom.length) {
            this.dataConfigDom = [];
            this.setState({
                dataConfigDom: this.dataConfigDom,
            });
        }
        this.showModal();//加载模态框
        this.dataConfigtype = type;

        switch (type) {
            case "text":
                this.dataUrl = this.textUrl;
                this.momentTime = this.textMomentTime;
                this.dataConfigDom.push(
                    <div key={componentKey + "_" + type}>
                        <h3>宽高配置</h3>
                        <Card
                            style={{width: '100%', textAlign: "center"}}
                        >
                            <Input
                                style={{width: "180px"}}
                                addonBefore="宽"
                                addonAfter="px"
                                defaultValue="400"
                                onChange={(e) => {
                                    this.setState({widthValue: e.target.value});
                                }}
                            />
                            &nbsp;&nbsp;
                            <Input
                                style={{width: "180px"}}
                                addonBefore="高"
                                addonAfter="px"
                                defaultValue="300"
                                onChange={(e) => {
                                    this.setState({heightValue: e.target.value});
                                }}
                            />
                        </Card>
                        <h3>数据配置</h3>
                        <Card
                            style={{width: '100%'}}
                        >
                            <DatePicker showTime format="YYYY/MM/DD HH:mm:ss" defaultValue={this.textMomentTime}
                                        onChange={this.onChange.bind(this)}/>
                            &nbsp;&nbsp;&nbsp;
                            <Button
                                onClick={() => this.search(this.dataConfigtype)}
                            >
                                查询
                            </Button>
                            <TableRendering ref={this.textTableRef} cols={this.textCols} url={this.textUrl} pageSize={this.pageSize}
                                            selectRow={this.selectRow}/>

                        </Card>
                    </div>
                );
                break;
            case "video":
                this.dataUrl = this.videoUrl;
                this.momentTime = this.videoMomentTime;
                this.dataConfigDom.push(
                    <div key={componentKey + "_" + type}>
                        <h3>宽高配置</h3>
                        <Card
                            style={{width: '100%', textAlign: "center"}}
                        >
                            <Input
                                style={{width: "180px"}}
                                addonBefore="宽"
                                addonAfter="px"
                                defaultValue="400"
                                onChange={(e) => {
                                    this.setState({widthValue: e.target.value});
                                }}
                            />
                            &nbsp;&nbsp;
                            <Input
                                style={{width: "180px"}}
                                addonBefore="高"
                                addonAfter="px"
                                defaultValue="300"
                                onChange={(e) => {
                                    this.setState({heightValue: e.target.value});
                                }}
                            />
                        </Card>
                        <h3>数据配置</h3>
                        <Card
                            style={{width: '100%'}}
                        >
                            <DatePicker showTime format="YYYY/MM/DD HH:mm:ss" defaultValue={this.videoMomentTime}
                                        onChange={this.onChange.bind(this)}/>
                            &nbsp;&nbsp;&nbsp;
                            <Button
                                onClick={() => this.search(this.dataConfigtype)}
                            >
                                查询
                            </Button>
                            <TableRendering ref={this.videoTableRef} cols={this.videoCols} url={this.videoUrl} pageSize={this.pageSize}
                                            selectRow={this.selectRow}/>

                        </Card>
                    </div>
                );
                break;
            case "audio":
                this.dataUrl = this.audioUrl;
                this.momentTime = this.audioMomentTime;
                this.dataConfigDom.push(
                    <div key={componentKey + "_" + type}>
                        <h3>宽高配置</h3>
                        <Card
                            style={{width: '100%', textAlign: "center"}}
                        >
                            <Input
                                style={{width: "180px"}}
                                addonBefore="宽"
                                addonAfter="px"
                                defaultValue="400"
                                onChange={(e) => {
                                    this.setState({widthValue: e.target.value});
                                }}
                            />
                            &nbsp;&nbsp;
                            <Input
                                style={{width: "180px"}}
                                addonBefore="高"
                                addonAfter="px"
                                defaultValue="300"
                                onChange={(e) => {
                                    this.setState({heightValue: e.target.value});
                                }}
                            />
                        </Card>
                        <h3>数据配置</h3>
                        <Card
                            style={{width: '100%'}}
                        >
                            <DatePicker showTime format="YYYY/MM/DD HH:mm:ss" defaultValue={this.textMomentTime}
                                        onChange={this.onChange.bind(this)}/>
                            &nbsp;&nbsp;&nbsp;
                            <Button
                                onClick={() => this.search(this.dataConfigtype)}
                            >
                                查询
                            </Button>
                            <TableRendering ref={this.audioTableRef} cols={this.audioCols} url={this.audioUrl} pageSize={this.pageSize}
                                            selectRow={this.selectRow}/>

                        </Card>

                    </div>
                );
                break;
            case "allmap":
                this.dataUrl = this.audioUrl;
                this.momentTime = this.audioMomentTime;
                this.dataConfigDom.push(
                    <div key={componentKey + "_" + type}>
                        <h3>宽高配置</h3>
                        <Card
                            style={{width: '100%', textAlign: "center"}}
                        >
                            <Input
                                style={{width: "180px"}}
                                addonBefore="宽"
                                addonAfter="px"
                                defaultValue="400"
                                onChange={(e) => {
                                    this.setState({widthValue: e.target.value});
                                }}
                            />
                            &nbsp;&nbsp;
                            <Input
                                style={{width: "180px"}}
                                addonBefore="高"
                                addonAfter="px"
                                defaultValue="300"
                                onChange={(e) => {
                                    this.setState({heightValue: e.target.value});
                                }}
                            />
                        </Card>
                    </div>
                );
                break;
            case "busLineCompare":
                this.dataUrl = this.audioUrl;
                this.momentTime = this.audioMomentTime;
                this.dataConfigDom.push(
                    <div key={componentKey + "_" + type}>
                        <h3>宽高配置</h3>
                        <Card
                            style={{width: '100%', textAlign: "center"}}
                        >
                            <Input
                                style={{width: "180px"}}
                                addonBefore="宽"
                                addonAfter="px"
                                defaultValue="400"
                                onChange={(e) => {
                                    this.setState({widthValue: e.target.value});
                                }}
                            />
                            &nbsp;&nbsp;
                            <Input
                                style={{width: "180px"}}
                                addonBefore="高"
                                addonAfter="px"
                                defaultValue="300"
                                onChange={(e) => {
                                    this.setState({heightValue: e.target.value});
                                }}
                            />
                        </Card>
                    </div>
                );
                break;
            case "dynamicMap":
                this.dataUrl = this.audioUrl;
                this.momentTime = this.audioMomentTime;
                this.dataConfigDom.push(
                    <div key={componentKey + "_" + type}>
                        <h3>宽高配置</h3>
                        <Card
                            style={{width: '100%', textAlign: "center"}}
                        >
                            <Input
                                style={{width: "180px"}}
                                addonBefore="宽"
                                addonAfter="px"
                                defaultValue="400"
                                onChange={(e) => {
                                    this.setState({widthValue: e.target.value});
                                }}
                            />
                            &nbsp;&nbsp;
                            <Input
                                style={{width: "180px"}}
                                addonBefore="高"
                                addonAfter="px"
                                defaultValue="300"
                                onChange={(e) => {
                                    this.setState({heightValue: e.target.value});
                                }}
                            />
                        </Card>
                    </div>
                );
                break;
        }
    };

    //发布更新前获取当前组件信息
    getComInfo = () => {
        let comInfo = this.state.componentsInfo;
        //当前dom节点内组件节点信息
        let comDomNode = this.componentParentRef.current.childNodes;

        for (let i = 0; i < comInfo.length; i++) {
            for (let j = 0; j < comDomNode.length; j++) {
                if (comInfo[i].key == comDomNode[j].id) {
                    comInfo[i].style = {
                        top: comDomNode[j].offsetTop,
                        left: comDomNode[j].offsetLeft,
                        width: comDomNode[j].offsetWidth,
                        height: comDomNode[j].offsetHeight
                    };
                }
            }
        }
        console.log("更新后comInfo", comInfo);
        return comInfo;
    }

    ////更新聚合任务
    publishTask() {
        axios
            .post("http://94.191.49.160:8081/api/aggregation/task/add", {
                name: "南京市城市交通态势聚合任务",
                content: this.getComInfo(),
            })
            .then((res) => {
                console.log("更新成果结果返回", res);
                message.success("发布成功");
            });
    }

    //更新聚合任务
    updateTask() {
        axios
            .post("http://94.191.49.160:8081/api/aggregation/task/update/" + this.props.item._id, {
                name: "南京市城市交通态势聚合任务",
                content: this.getComInfo(),
            })
            .then((res) => {
                console.log("更新成果结果返回", res);
                message.success("更新成功");
            });
    }

    render() {
        const {visible, visibleModal, disabled, bounds} = this.state;
        return (
            <div id="componentParent"
                 style={{width: "100%", height: "650px", position: "relative", backgroundColor: "lightgrey"}}>
                {/*组件容器*/}
                <div
                    ref={this.componentParentRef}
                    id="componentParent"
                    style={{
                        height: "90%",
                        width: "100%",
                        position: "relative",
                        padding: "0",
                    }}
                >
                    {
                        this.loadComponentDom()
                    }
                </div>
                {/*更新按钮*/}
                <div className={style["btnBox"]}>
                    <Button type="link" icon={<RollbackOutlined/>} onClick={this.props.changeMode}>返回</Button>
                    <Button type="text" icon={<SaveOutlined/>} onClick={this.publishTask.bind(this)}>发布</Button>
                    <Button type="text" icon={<SaveOutlined/>} onClick={this.updateTask.bind(this)}>更新</Button>
                </div>
                {/*打开组件按钮*/}
                <Button
                    style={{position: "fixed", left: "0", top: "75px"}}
                    onClick={this.showDrawer}
                    icon={<MenuUnfoldOutlined/>}
                >
                </Button>
                <Drawer
                    title="组件"
                    placement={"left"}
                    style={{position: 'absolute'}}
                    getContainer={false}
                    closable={true}
                    mask={false}
                    onClose={this.onClose}
                    visible={visible}
                >
                    <Card
                        title="文本"
                        style={{width: "90%", marginTop: "5px"}}
                        hoverable
                        onClick={this.dataConfig.bind(
                            this,
                            this.state.componentKey,
                            "text"
                        )}
                    >
                        <img src={_Text} style={{height: 120, width: 130}}/>
                    </Card>
                    <Card
                        title="视频"
                        style={{width: "90%", marginTop: "5px"}}
                        hoverable
                        onClick={this.dataConfig.bind(
                            this,
                            this.state.componentKey,
                            "video"
                        )}
                    >
                        <img src={_VIDEO} style={{height: 120, width: 130}}/>
                    </Card>
                    <Card
                        title="音频"
                        style={{width: "90%", marginTop: "5px"}}
                        hoverable
                        onClick={this.dataConfig.bind(
                            this,
                            this.state.componentKey,
                            "audio"
                        )}
                    >
                        <img src={_AUDIO} style={{height: 120, width: 130}}/>
                    </Card>
                    <Card
                        title="allmap"
                        style={{width: "90%", marginTop: "5px"}}
                        hoverable
                        onClick={this.dataConfig.bind(
                            this,
                            this.state.componentKey,
                            "allmap"
                        )}
                    >
                        <img src={_MAP} style={{height: 120, width: 130}}/>
                    </Card>
                    <Card
                        title="busLineCompare"
                        style={{width: "90%", marginTop: "5px"}}
                        hoverable
                        onClick={this.dataConfig.bind(
                            this,
                            this.state.componentKey,
                            "busLineCompare"
                        )}
                    >
                        <img src={_MAP} style={{height: 120, width: 130}}/>
                    </Card>
                    <Card
                        title="dynamicMap"
                        style={{width: "90%", marginTop: "5px"}}
                        hoverable
                        onClick={this.dataConfig.bind(
                            this,
                            this.state.componentKey,
                            "dynamicMap"
                        )}
                    >
                        <img src={_MAP} style={{height: 120, width: 130}}/>
                    </Card>
                </Drawer>

                {/*模态弹出框-设置面板*/}
                <Modal
                    title={
                        <div
                            style={{
                                width: '100%',
                                cursor: 'move',
                            }}
                            onMouseOver={() => {
                                if (disabled) {
                                    this.setState({
                                        disabled: false,
                                    });
                                }
                            }}
                            onMouseOut={() => {
                                this.setState({
                                    disabled: true,
                                });
                            }}
                            onFocus={() => {
                            }}
                            onBlur={() => {
                            }}
                            // end
                        >
                            配置
                        </div>
                    }
                    visible={visibleModal}
                    // 点击确定执行查询，并关闭模态框
                    onOk={this.addComponentInfo.bind(this, this.dataConfigtype)}
                    onCancel={this.hideModal}
                    closable={true}
                    modalRender={modal => (
                        <Draggable
                            disabled={disabled}
                            bounds={bounds}
                            onStart={(event, uiData) => this.onStart(event, uiData)}
                        >
                            <div ref={this.draggleRef}>{modal}</div>
                        </Draggable>
                    )}
                >
                    <div>{this.dataConfigDom}</div>
                </Modal>
            </div>
        );
    }
}

export default Operation;
