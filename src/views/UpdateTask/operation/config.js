import { stopPropagation } from "@/utility";
import audioSvg from "@/assets/image/data-query-audio.svg";
import videoSvg from "@/assets/image/data-query-video.svg";
const ITEMSCon = {
    "text": {
        "title": "文本",
        "url": "text/meta/query",
        "searchCons": [
            {
                "label": "内容",
                "value": "content"
            },
            {
                "label": "来源",
                "value": "author"
            },
            {
                "label": "发布日期",
                "value": "time"
            }
        ],
        "cols": [
            {
                title: '发布日期',
                dataIndex: 'time',
                width: "200px",
                onCell: stopPropagation
            },{
                title: "内容",
                dataIndex: "content",
                ellipsis: {
                    showTitle: false
                },
                className: "pointer"
            },{
                title: '来源',
                dataIndex: 'author',
                width: "200px",
                onCell: stopPropagation
            }
        ]
    },
    // "audio": {
    //     "title": "音频",
    //     "url": "audio/meta/query",
    //     "searchCons": [
    //         {
    //             "label": "节目名",
    //             "value": "programName"
    //         },
    //         {
    //             "label": "播出时间",
    //             "value": "date"
    //         }
    //     ],
    //     "cols": [
    //         {
    //             "title": "播出时间",
    //             "dataIndex": "date",
    //             "width": "150px",
    //             "ellipsis": true,
    //             "onCell": stopPropagation
    //         },
    //         {
    //             "title": "内容",
    //             "dataIndex": "url",
    //             "render": () => <img src={audioSvg} width="20" height="20" alt="音频" />,
    //             "className": "pointer"
    //         },
    //     ]
    // },
    // "video": {
    //     "title": "视频",
    //     "url": "video/meta/query",
    //     "searchCons": [
    //         {
    //             "label": "拍摄时间",
    //             "value": "date"
    //         },
    //         {
    //             "label": "摄像头位置",
    //             "value": "location"
    //         }
    //     ],
    //     "cols": [
    //         {
    //             "title": "摄像头位置",
    //             "dataIndex": "location",
    //             "ellipsis": true,
    //             "className": "pointer"
    //         },
    //         {
    //             "title": "内容",
    //             "dataIndex": "video",
    //             "ellipsis": true,
    //             "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
    //             "className": "pointer"
    //         },
    //         {
    //             "title": "拍摄时间",
    //             "dataIndex": "date",
    //             //"width": "150px",
    //             "ellipsis": true,
    //             "onCell": stopPropagation
    //         },
    //     ]
    // }
};

export default ITEMSCon;
