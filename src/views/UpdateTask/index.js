import React, { Component } from "react";
import { Breadcrumb, Card } from "antd";
import taiwan from "@/assets/image/Taiwan.jpg";
import hongkong from "@/assets/image/Hongkong.jpg";
import traffic from "@/assets/image/traffic.jpg";
import addnew from "@/assets/image/icon_add.png";
import { Link } from "react-router-dom";
import { NAV_ITEMS, ROOT_ROUTE } from "@/routes";
import axios from "axios";
import List from "./list";
import Task from "./Task";



class updateTask extends Component{
    constructor(props){
        super(props);
        this.state = {
            "mode": "list",        //模式，代表当前视图，可选值为"list"、"Task"、"subject"
            "item": null
        };
    }

    componentDidMount() {
    }

    changeMode(mode, item){
        this.setState({ mode, item });
    }

    render(){
        let com;
        let { mode, item } = this.state;
        switch(mode){
            case "list":
                com = <List changeMode={this.changeMode.bind(this)} />;
                break;
            case "task":
                com = <Task  item={item} changeMode={this.changeMode.bind(this, "list")} />;
                break;
            default:
                break;
        }
        return (
            // <div className="container">
            <div  style={{width:"100%",height:"718px",padding:"8px 35px 35px 35px"}}>
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>动态更新</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">更新任务</Breadcrumb.Item>
                    {
                        mode !== "list" &&
                        <Breadcrumb.Item className="breadcrumb-item">{item.title}</Breadcrumb.Item>
                    }
                </Breadcrumb>
                { com }
                {/*<div>*/}
                {/*    <Card>*/}
                {/*        <img/>*/}
                {/*        <p></p>*/}
                {/*    </Card>*/}
                {/*</div>*/}
                {/*<div className="cardBox" style={{display:"flex", justifyContent:"space-around", flexWrap:"wrap"}}>*/}
                {/*    <Card hoverable={true} style={{width:"300px",textAlign:"center", overflow:"hidden",margin:"5px"}}>*/}
                {/*        <img src={taiwan} style={{height:200}} />*/}
                {/*        <p>台风专题聚合任务</p>*/}
                {/*    </Card>*/}
                {/*    <Card hoverable={true} style={{width:"300px",textAlign:"center", overflow:"hidden",margin:"5px"}}>*/}
                {/*        <img src={hongkong} style={{height:200}} />*/}
                {/*        <p>香港暴力事件聚合任务</p>*/}
                {/*    </Card>*/}
                {/*    <Card*/}
                {/*    hoverable={true}*/}
                {/*    style={{width:"300px",textAlign:"center", overflow:"hidden",margin:"5px"}}*/}
                {/*    >*/}
                {/*        <Link to={`${ROOT_ROUTE}/update-Task-nanjing-city-traffic-aggregation`}>*/}
                {/*        <img src={traffic} style={{height:200}} />*/}
                {/*        <p>南京市城市交通态势聚合任务</p>*/}
                {/*        </Link>*/}
                {/*    </Card>*/}
                {/*    <Card hoverable={true} style={{width:"300px",textAlign:"center", overflow:"hidden",margin:"5px"}}>*/}
                {/*        <img src={addnew} style={{height:200}} />*/}
                {/*        <p>定制新任务</p>*/}
                {/*    </Card>*/}
                {/*</div>*/}
            </div>
        );
    }
}

export default updateTask;
