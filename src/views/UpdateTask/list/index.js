import React, { useState, useEffect } from "react";
import { Card, message } from "antd";
import axios from "axios";
import { NODE_PATH,FILE_SERVER} from "@/assets/global";
import style from "./index.module.css";
import traffic from "@/assets/image/traffic.jpg";

function List(props){
    let [subjects, setSubjects] = useState([]);

    useEffect(() => {
        axios.get(`${NODE_PATH}/aggregation/task_/search`).then(res => {
            console.log(res.data.data);
            setSubjects(res.data.data);
        }).catch(err => {
            console.log(err);
            message.error("查询聚合失败");
        });
    }, []);

    return (
        <div className={style["wrapper"]}>
            {
                subjects.map(item => (
                    <Card
                        key={item._id}
                        hoverable={true}
                        className={style["card"]}
                        cover={<img src={`${FILE_SERVER}/${item.cover}`} className={style["card-cover"]} />}
                        onClick={() => props.changeMode("task", item)}
                    >
                        <p className={style["card-title"]}>{item.title}</p>
                    </Card>
                ))
            }
        </div>
    );
}

export default List;
