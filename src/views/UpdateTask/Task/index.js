import React, {useState, useEffect, Fragment} from "react";
import {Button, Spin, message, Drawer, Modal, Layout, Menu} from "antd";
import {MenuUnfoldOutlined, NotificationOutlined, RollbackOutlined, SaveOutlined} from "@ant-design/icons";
import axios from "axios";
import {Card} from "@/components";
import {Components, Allias_Com_Map, DataType_Url_Map} from "@/components/aggregation";
import {NODE_PATH} from "@/assets/global";

import Draggable from "react-draggable";
// import DynamicMap from "@/components/aggregation/customizeMap_dynamicMap"
// import AllMap from "@/components/aggregation/customizeMap_allmap"
// import BusLineCompare from "@/components/aggregation/customizeMap_busLineCompare"
// import NetworkDashboard from "@/components/aggregation/customizeMap_NetworkDashboard"
// import ChinaNetworkDashboard from "@/components/aggregation/customizeMap_ChinaNetworkDashboard"
// import GlobalNetworkDashboard from "@/components/aggregation/customizeMap_GlobalNetworkDashboard"
// import TreeMap from "@/components/aggregation/customizeMap_TreeMap"
// import SelectMap from "@/components/aggregation/customizeMap_SelectMap"
// import EconDashboardComposite from '@/components/aggregation/customizeMap_EconDashboard_Composite'

const {Sider} = Layout;

function Task(props) {
    let [themes, setThemes] = useState([]);
    let [loading, setLoading] = useState(true);
    let [selectedThemeIndex, setSelectedThemeIndex] = useState(0);
    let {changeMode, item} = props;
    let components = {};
    let boxStyle = {
        position: "relative",
        height: "800px",
        marginTop: "15px",
        overflow: "auto"
    };

    item.componentIds.forEach(item => {
        components[item._id] = item;
    });

    useEffect(() => {
        axios.get(`${NODE_PATH}/aggregation/theme/search`, {
            params: {
                taskId: item._id
            }
        }).then(res => {
            if (res.data.code) {
                setThemes(res.data.data);
                setLoading(false);
            } else {
                throw new Error(res.data.msg);
            }
        }).catch(err => {
            setLoading(false);
            console.log(err);
            message.error("查询失败");
        });
    }, []);

    //当前选中选题
    function selectMenu(selectedKeys) {
        setSelectedThemeIndex(selectedKeys.key);
    }


    return (
        <Fragment>
            <Layout style={{width: "100%", height: "643px", position: "relative", backgroundColor: "#F1F6F9"}}>
                <Sider width={200}>
                    <Menu
                        mode="inline"
                        defaultSelectedKeys={[selectedThemeIndex.toString()]}
                        defaultOpenKeys={[selectedThemeIndex.toString()]}
                        style={{height: '100%', backgroundColor: "#F0F0F0"}}
                        onSelect={(selectedKeys) => selectMenu(selectedKeys)}
                    >
                        {
                            themes.map((item, index) => {
                                let themeDom;
                                themeDom = (
                                    <Menu.Item key={index.toString()} icon={<NotificationOutlined/>}>
                                        {item.title}
                                    </Menu.Item>
                                )
                                return themeDom;
                            })
                        }
                    </Menu>
                </Sider>
                {/*组件容器*/}
                <div
                    style={{
                        height: "90%",
                        width: "100%",
                        position: "relative",
                        padding: "0",
                    }}

                >
                    <Spin size="large" tip="加载中……" spinning={loading}>
                        <div style={boxStyle}>
                            {
                                themes.length > 0 && themes[selectedThemeIndex].content.map((item, i) => {
                                    let comInfo = components[item.componentId];
                                    let Com = Allias_Com_Map[comInfo.alias];
                                    let card, url, queryCon;
                                    if (comInfo.dataType) {
                                        url = DataType_Url_Map[comInfo.dataType];
                                        queryCon = {ids: item.dataIds};
                                        card = (
                                            <Card key={i} title={item.title} style={item.style}>
                                                <Com url={`${NODE_PATH}/${url}`} queryCon={queryCon}/>
                                            </Card>
                                        );
                                    } else {
                                        card = (
                                            <Card key={i} title={item.title} style={item.style}>
                                                <Com/>
                                            </Card>
                                        );
                                    }
                                    return card;
                                })
                            }

                            {/* <Card title="地图" style={{width:800, height:650}}>
                        <Map id="myMap" center={[118.8, 32.07]} zoom={12}  />
                    </Card> */}

                            {/* <Card title="2021年6月30日G312国道部分路段实时交通路口动态展示" style={{ width: 800, height: 650 }}>
                        <DynamicMap center={[118.8, 32.07]} zoom={10} />
                    </Card> */}

                            {/* <Card title="台风动态轨迹" style={{ width: 800, height: 650 }}>
                        <DynamicTyphoon center={[118.8, 32.07]} zoom={4}>
                    </Card> */}
                            {/* <Card title="全景地图" style={{ width: 1200, height: 600 }}>
                        <AllMap id="myMap_1" zoom={12} geoJson />
                    </Card> */}
                            {/* 单独使用Network系列组件示例 */}
                            {/* <Card title="长三角区域内部合作形态" style={{ width: 566, height: 486 }}>
                        <NetworkDashboard id="NetworkDashboard" onRef={function onRef() {
                        }} area="YRD" />
                    </Card> */}

                            {/* <Card title="合作组合组件" style={{ width: 1600, height: 700 }}>
                        <SelectMap id="SelectMap" />
                    </Card> */}
                            {/* <Card title="工业创新环境综合指标分布图" style={{ width: 485, height: 623 }}>
                        <EconDashboardComposite id="EconDashboardComposite" />
                    </Card> */}
                        </div>
                    </Spin>
                </div>
                {/*更新按钮*/}
                <div style={{display: "flex", position: "fixed", right: "35px", top: "72px"}}>
                    <Button type="link" icon={<RollbackOutlined/>} onClick={() => changeMode("list")}>返回</Button>
                </div>
            </Layout>


        </Fragment>
    );
}

export default Task;
