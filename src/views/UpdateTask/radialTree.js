import React, { Component } from "react";
import * as echarts from "echarts";
import axios from "axios";

class RadialTree extends Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        this.chart = echarts.init(document.getElementById(this.props.id));
        console.log("this.props.id",this.props.id);
        this.chart.showLoading();
        this.updateChart();
    }

    render(){
        return (
            <div id={this.props.id} className={this.props.className}>
                哈哈哈哈
            </div>
        );
    }

    updateChart(){
        //展示30条数据
        axios.get("https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/asset/data/flare.json").then((res) => {
            console.log("返回的flare.json",res.data);
            let data =res.data;
            console.log("径向树状图数据",data);
            let option = setOption(data);
            console.log(option);
            this.chart.hideLoading();
            this.chart.setOption(option);
        })


    }
}



function setOption(data){
    return{
        tooltip: {
            trigger: 'item',
            triggerOn: 'mousemove'
        },
        series: [
            {
                type: 'tree',

                data: [data],

                top: '18%',
                bottom: '14%',

                layout: 'radial',

                symbol: 'emptyCircle',

                symbolSize: 7,

                initialTreeDepth: 3,

                animationDurationUpdate: 750,

                emphasis: {
                    focus: 'descendant'
                }
            }
        ]
    }
}

export default RadialTree;
