import React, { Component, Fragment } from "react";
import { Row, Col, Breadcrumb, Select, Input, Button, Spin, message, Tag, Radio} from "antd";
import { Chart, TableRendering, CustomizeModal, Paragraph } from "@/components";
import { colorMap_text, colorMap_image, colorMap_map, colorMap_chart, colorMap_audio, colorMap_video } from "@/components/Title";
import axios from "axios";
import { ITEMS } from "./config.js";
import { NODE_PATH, FILE_SERVER } from "@/assets/global";
import sankeyConf from "./sankeyConfig";
import style from "./index.module.css";

class ProcessSrc extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "curItem": "text",
            "curSearchCon": "",
            "keywords": "",
            "spining": false,
            "sankeyConfig": sankeyConf.defaultConf(),
            
            "curType": "meta",              //桑基图当前选中的类型
            "paraModalData": null,          //“段落”型弹框数据
            "paraModalVisible": false,      //“段落”型弹框
            "tableModalVisible": false,     //“表格”型弹框
        };

        this.tableRef = React.createRef();
        this.modalTabRef = React.createRef();
        this.defaultUrl = `${NODE_PATH}/${ITEMS.text.element.url}`;    //默认查询文本类型数据的“内容列表”
        this.defaultCols = ITEMS.text.element.cols;    
        
        this.curRow = null;             //表格中当前选中行    
        this.curTypeTagValue = null;    //当前选中行对应的类型标签  

        this.search = this.search.bind(this);
        this.onItemChange = this.onItemChange.bind(this);
        this.onSearchConChange = this.onSearchConChange.bind(this);
        this.onKeywordsChange = this.onKeywordsChange.bind(this);

        this.selectRow = this.selectRow.bind(this);
        this.clickEdge = this.clickEdge.bind(this);
        this.showModal = this.showModal.bind(this);
    }

    render() {
        let { curSearchCon, spining, sankeyConfig, paraModalVisible, paraModalData, tableModalVisible, curType, curItem } = this.state;
        let item = ITEMS[curItem];

        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>信息溯源</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">过程溯源</Breadcrumb.Item>
                </Breadcrumb>
                <Spin spinning={spining}>
                    <Row>
                        <Col span={8}>
                            <div>
                                <Radio.Group value={curItem} onChange={this.onItemChange} style={{ margin: 10 }}>
                                    {
                                        Object.keys(ITEMS).map(item => <Radio key={item} value={item}>{ITEMS[item].title}</Radio>)
                                    }
                                </Radio.Group>
                            </div>
                            <div className={style["formBox"]}>
                                <label>查询条件：</label>
                                <Select
                                    value={curSearchCon}
                                    className={style["formBox-select"]}
                                    onChange={this.onSearchConChange}
                                    options={item.element.searchCons}
                                />
                                <label>关键词：</label>
                                <Input className={style["formBox-input"]} value={this.state.keywords} onChange={this.onKeywordsChange} />
                                <Button type="primary" onClick={this.search}>查询</Button>
                            </div>
                            <TableRendering ref={this.tableRef} cols={this.defaultCols} url={this.defaultUrl} selectRow={this.selectRow} />
                        </Col>
                        <Col span={16}>
                            <Chart data={sankeyConfig} style={{ minHeight: "800px" }} />
                        </Col>
                    </Row>
                </Spin>
                {/*段落型弹框*/}
                <CustomizeModal
                    visible = { paraModalVisible }
                    title = { ITEMS[curItem][curType].title }
                    close = { this.hideModal.bind(this, "paraModalVisible") }
                >
                    <Paragraphs type={`${curItem}_${curType}`} {...paraModalData} />
                </CustomizeModal>
                {/*表格型弹框*/}
                <CustomizeModal
                    visible = { tableModalVisible }
                    title = { ITEMS[curItem][curType].title }
                    close = { this.hideModal.bind(this, "tableModalVisible") }
                >
                    <TableRendering ref={this.modalTabRef} />
                </CustomizeModal>
            </div>
        )
    }

    onItemChange(e){
        this.setState({
            "curItem": e.target.value,
            "curSearchCon": "",
            "sankeyConfig": sankeyConf.defaultConf()
        });
        let item = ITEMS[e.target.value].element;
        this.tableRef.current.search(`${NODE_PATH}/${item.url}`, item.cols, {});
    }

    onSearchConChange(val){
        this.setState({
            "curSearchCon": val
        });
    }

    onKeywordsChange(e){
        this.setState({
            "keywords": e.target.value
        });
    }

    search(){
        let con = {};
        let { curItem, keywords, curSearchCon } = this.state;
        let item = ITEMS[curItem].element;
        if(curSearchCon && keywords){
            con[curSearchCon] = keywords;
        }
        
        this.tableRef.current.search(`${NODE_PATH}/${item.url}`, item.cols, con);
    }

    selectRow(record){
        this.setState({
            spining: true
        });
        this.curRow = record;

        let curItem = this.state.curItem;
        let getSankeyConf = sankeyConf[curItem];
        // let metaId = "";
        // let url = ITEMS[curItem].typeTag.url;
        //由于数据库问题，地图、图表类型的原数据需要根据id查询
        // if(curItem === "chart" || curItem === "map"){
        //     metaId = record.tagid.metaId.id;
        // }
        // else{
        //     metaId = record.tagid.metaId._id;
        // }

        // axios.get(`${NODE_PATH}/${url}`, {
        //     params: { metaId }
        // }).then(res => {
        //     if(!res.data.code){
        //         throw new Error(res.data.msg);
        //     }
        //     this.curTypeTagValue = res.data.data.textData[0];

        //     this.setState({
        //         spining: false,
        //         sankeyConfig: getSankeyConf(res.data.data.textData[0], this.clickEdge, record.tagName || record.tagType)
        //     });
        // }).catch(err => {
        //     console.log(err);
        //     message.error("加载数据失败");
        // });
        axios.get(`${NODE_PATH}/${curItem}/track`, {
            params: { elementId: record._id}
        }).then(res => {
            // console.log(res.data);
            if (!res.data.code) {
                throw new Error(res.data.msg);
            }
            this.curTypeTagValue = res.data.data.typeTags[0];
            //由于数据库问题，地图、图表类型的原数据查询结果为数组类型
            if (curItem === "chart" || curItem === "map") {
                this.curTypeTagValue.metaId = res.data.data.metaData[0];
            }
            else {
                this.curTypeTagValue.metaId = res.data.data.metaData;
            }
            this.setState({
                spining: false,
                sankeyConfig: getSankeyConf(res.data.data, this.clickEdge, record.tagName || record.tagType)
            });
        }).catch(err => {
            console.log(err);
            message.error("加载数据失败");
            this.setState({
                spining: false
            });
        });
    }
    
    clickEdge(params){
        if(params.dataType === "node" || typeof params.data.sourceType === "undefined"){
            return;
        }
        this.showModal(params.data);
    }

    showModal(data){
        let sourceType = data.sourceType;
        let curItem = this.state.curItem;
        let metaId = "";
        //由于数据库问题，地图、图表类型的原数据需要根据id查询
        if(curItem === "chart" || curItem === "map"){
            metaId = this.curRow.tagid.metaId.id;
        }
        else{
            metaId = this.curRow.tagid.metaId._id;
        }
        let queryCon = { metaId };
        let item = ITEMS[this.state.curItem];
        let cols, url;
        switch(sourceType){
            case "meta":
                this.setState({
                    "curType": "meta",
                    "paraModalVisible": true,
                    "paraModalData": this.curRow.tagid.metaId
                });
                break;
            case "typeTag":
                this.setState({
                    "curType": "typeTag",
                    "paraModalVisible": true,
                    "paraModalData": this.curTypeTagValue
                });
                break;
            default:
                this.setState({
                    "curType": sourceType,
                    "tableModalVisible": true,
                });
                queryCon[data.field] = data.source;
                url = `${NODE_PATH}/${item[sourceType].url}`;
                cols = item[sourceType].cols;
                this.modalTabRef.current.search(url, cols, queryCon);
                break;
        }
    }

    hideModal(key){
        var obj = {};
        obj[key] = false;
        this.setState(obj);
    }
}

function Paragraphs(props){
    let com = null;
    switch(props.type){
        case "text_meta": 
            com = (
                <Fragment>
                    <Paragraph title="来源：" content={props.source} />
                    <Paragraph title="时间：" content={props.recordDatetime} />
                    <Paragraph title="摘要：" content={props.description} />
                    <Paragraph title="内容：" content={props.content} />
                </Fragment>
            );
            break;
        case "text_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="来源：" content={props.metaId && props.metaId.source} />
                    <Paragraph title="时间：" content={props.metaId && props.metaId.recordDatetime} />
                    <Paragraph title="摘要：" content={props.metaId && props.metaId.description} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => <Tag color={colorMap_text.get(item)} key={ind}>{props.categoryList[item]}</Tag>)}
                    />
                    <Paragraph title="内容：" content={props.metaId && props.metaId.content} />
                </Fragment>
            );
            break;
        case "img_meta":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.recordDatetime} />
                    <Paragraph title="来源：" content={props.website} />
                    <Paragraph title="介绍：" content={props.description} />
                    <Paragraph title="图片：" image={props.imageName && `${FILE_SERVER}/images/metaData_Img/${props.imageName}`} />
                </Fragment>
            );
            break;
        case "img_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="日期：" content={ props.metaId && props.metaId.recordDatetime } />
                    <Paragraph title="来源：" content={ props.metaId && props.metaId.website } />
                    <Paragraph title="描述：" content={ props.metaId && props.metaId.description } />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if(props.categoryList[item] === "FALSE"){
                                return false;
                            }
                            return (<Tag color={colorMap_image.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph image={props.metaId && `${FILE_SERVER}/images/metaData_Img/${props.metaId.imageName}`} />
                </Fragment>
            );
            break;
        case "chart_meta":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.recordedTime} />
                    <Paragraph title="文件格式：" content={props.format} />
                    <Paragraph title="来源：" content={<a href={props.source} target="_blank">{props.source}</a>} />
                    <Paragraph title="图片：" image={props.imagePath && `${FILE_SERVER}/${props.imagePath}`} />
                </Fragment>
            );
            break;
        case "chart_typeTag":
            console.log(props);
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.metaId && props.metaId.recordedTime} />
                    <Paragraph title="文件格式：" content={props.metaId && props.metaId.format} />
                    <Paragraph title="来源：" content={props.metaId && <a href={props.metaId.source} target="_blank">{props.metaId.source}</a>} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if (props.categoryList[item] === "FALSE") {
                                return false;
                            }
                            return (<Tag color={colorMap_chart.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph title="图片：" image={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/${props.metaId.imagePath}`} />
                </Fragment>
            );
            break;
        case "map_meta": 
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.recordDatetime} />
                    <Paragraph title="来源：" content={props.website} />
                    <Paragraph title="网址：" content={<a href={props.source} target="_blank">{props.source}</a>} />
                    <Paragraph title="图片：" image={props.imagePath && `${FILE_SERVER}/map/${props.imagePath}`} />
                </Fragment>
            );
            break;
        case "map_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.metaId && props.metaId.recordDatetime} />
                    <Paragraph title="来源：" content={props.metaId && props.metaId.website} />
                    <Paragraph title="网址：" content={<a href={props.metaId && props.metaId.source} target="_blank">{props.metaId && props.metaId.source}</a>} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if (props.categoryList[item] === "FALSE") {
                                return false;
                            }
                            return (<Tag color={colorMap_map.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph title="图片：" image={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/map/${props.metaId.imagePath}`} />
                </Fragment>
            );
            break;
        case "audio_meta":
            com = (
                <Fragment>
                    <Paragraph title="节目名：" content={props.programName} />
                    <Paragraph title="频道名：" content={props.channelName} />
                    <Paragraph title="播出时间：" content={props.date} />
                    <Paragraph title="每日播出时间段：" content={props.period} />
                    <Paragraph title="频道（FM）：" content={props.FM} />
                    <Paragraph title="音频：" audio={props.url && `${FILE_SERVER}/audio/${props.url}`} />
                </Fragment>
            );
            break;
        case "audio_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="节目名：" content={props.metaId && props.metaId.programName} />
                    <Paragraph title="频道名：" content={props.metaId && props.metaId.channelName} />
                    <Paragraph title="播出时间：" content={props.metaId && props.metaId.date} />
                    <Paragraph title="每日播出时间段：" content={props.metaId && props.metaId.period} />
                    <Paragraph title="频道（FM）：" content={props.metaId && props.metaId.FM} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if (props.categoryList[item] === "FALSE") {
                                return false;
                            }
                            return (<Tag color={colorMap_audio.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph title="音频：" audio={props.metaId && props.metaId.url && `${FILE_SERVER}/audio/${props.metaId.url}`} />
                </Fragment>
            );
            break;
        case "video_meta":
            com = (
                <Fragment>
                    <Paragraph title="拍摄时间：" content={props.date} />
                    <Paragraph title="摄像头位置：" content={props.location} />
                    <Paragraph title="视频格式：" content={props.suffix} />
                    <Paragraph title="视频文件大小：" content={props.size} />
                    <Paragraph title="视频：" video={props.url && `${FILE_SERVER}/${props.url}`} />
                </Fragment>
            );
            break;
        case "video_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="拍摄时间：" content={props.metaId && props.metaId.date} />
                    <Paragraph title="摄像头位置：" content={props.metaId && props.metaId.location} />
                    <Paragraph title="视频格式：" content={props.metaId && props.metaId.suffix} />
                    <Paragraph title="视频文件大小：" content={props.metaId && props.metaId.size} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if (props.categoryList[item] === "FALSE") {
                                return false;
                            }
                            return (<Tag color={colorMap_video.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph title="视频：" video={props.metaId && props.metaId.url && `${FILE_SERVER}/${props.metaId.url}`} />
                </Fragment>
            );
            break;
        default:
            break;
    }
    return com;
}

export default ProcessSrc;