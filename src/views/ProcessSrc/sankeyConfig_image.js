//文本类型桑基图配置
//@param data {Object}    选中记录的类型标签数据
//@param clickEdge {Function} 点击桑基图中的边时的回调函数
const relationMap = {
    "time": "时间",
    "place": "地点",
    "person": "人物",
    "thing": "事物",
    "event": "事件"
};
function setImageConf(data, clickEdge, feature) {
    let conf = {
        title: {
            subtext: '',
            left: 'center'
        },
        backgroundColor: '#FFFFFF',
        series: [
            {
                type: 'sankey',
                x: 'center',
                y: 'top',
                left: 50.0,
                top: 20.0,
                right: 50.0,
                bottom: 25.0,
                nodeGap: 32,
                layoutIterations: 0,
                data: [
                    {
                        'name': '音频',
                        'localY': 0.1,
                        'depth': 0,
                        'value': 1,
                    },
                    {
                        'name': '视频',
                        'localY': 0.2,
                        'depth': 0,
                        'value': 1,
                    },
                    {
                        'name': '图像',
                        'localY': 0.3,
                        'depth': 0,
                        'itemStyle': {
                            'color': '#CCFF00',
                            'borderColor': '#CCFF00'
                        }
                    },
                    {
                        'name': '图表',
                        'localY': '0.5',
                        'depth': 0,
                        'value': 1
                    },
                    {
                        'name': '文本',
                        'localY': '0.6',
                        'depth': 0,
                        'value': 1
                    },
                    {
                        'name': '地图',
                        'localY': '0.7',
                        'depth': 0,
                        'value': 1
                    },
                    {
                        "name": "标题",
                        'depath': 1,
                        'itemStyle': {
                            'color': '#567C73',
                            'borderColor': '#567C73'
                        }
                    },
                    {
                        "name": "描述文本",
                        'depath': 1,
                        'itemStyle': {
                            'color': '#567C73',
                            'borderColor': '#567C73'
                        }
                    },
                    {
                        "name": "来源网站",
                        'depath': 1,
                        'itemStyle': {
                            'color': '#567C73',
                            'borderColor': '#567C73'
                        }
                    },
                    // {
                    //     "name": "place",
                    //     'depath': 1,
                    //     'itemStyle': {
                    //         'color': '#567C73',
                    //         'borderColor': '#567C73'
                    //     }
                    // },
                    // {
                    //     "name": "time",
                    //     'depath': 1,
                    //     'itemStyle': {
                    //         'color': '#567C73',
                    //         'borderColor': '#567C73'
                    //     }
                    // },
                    // {
                    //     "name": "person",
                    //     'depath': 1,
                    //     'itemStyle': {
                    //         'color': '#567C73',
                    //         'borderColor': '#567C73'
                    //     }
                    // },
                    // {
                    //     "name": "thing",
                    //     'depath': 1,
                    //     'itemStyle': {
                    //         'color': '#567C73',
                    //         'borderColor': '#567C73'
                    //     }
                    // },
                    // {
                    //     "name": "event",
                    //     'depath': 1,
                    //     'itemStyle': {
                    //         'color': '#567C73',
                    //         'borderColor': '#567C73'
                    //     }
                    // },
                    {
                        'name': '结构',
                        'depth': 2,
                        'localY': 0.2,
                        'itemStyle': {
                            'color': '#FFFF00',
                            'borderColor': '#FFFF00'
                        }
                    },
                    {
                        'name': '内容',
                        'depth': 2,
                        'localY': 0.4,
                        'itemStyle': {
                            'color': '#006699',
                            'borderColor': '#006699'
                        }
                    },
                    {
                        'name': '类型',
                        'depth': 2,
                        'localY': 0.6,
                        'itemStyle': {
                            'color': '#FF6666',
                            'borderColor': '#FF6666'
                        }
                    },
                    {
                        'name': '知识库',
                        'depth': 3,
                        'itemStyle': {
                            'color': '#CC6699',
                            'borderColor': '#CC6699'
                        }
                    },
                    {
                        'name': '算法库',
                        'depth': 3,
                        'itemStyle': {
                            'color': '#99CC66',
                            'borderColor': '#99CC66'
                        }
                    },
                    {
                        'name': '时间',
                        'depth': 4,
                        'itemStyle': {
                            'color': '#FFB6C1',
                            'borderColor': '#FFB6C1'
                        }
                    },
                    {
                        'name': '人物',
                        'depth': 4,
                        'itemStyle': {
                            'color': '#800080',
                            'borderColor': '#800080'
                        }
                    },
                    {
                        'name': '地点',
                        'depth': 4,
                        'itemStyle': {
                            'color': '#0000FF',
                            'borderColor': '#0000FF'
                        }
                    },
                    {
                        'name': '事物',
                        'depth': 4,
                        'itemStyle': {
                            'color': '#66CDAA',
                            'borderColor': '#66CDAA'
                        }
                    },
                    {
                        'name': '事件',
                        'depth': 4,
                        'itemStyle': {
                            'color': '#FFFF00',
                            'borderColor': '#FFFF00'
                        }
                    },
                    {
                        'name': '现象',
                        'depth': 4,
                        'itemStyle': {
                            'color': '#FF0000',
                            'borderColor': '#FF0000'
                        }
                    },
                    {
                        'name': '场景',
                        'depth': 4,
                        'itemStyle': {
                            'color': '#FF8C00',
                            'borderColor': '#FF8C00'
                        }
                    },
                    {
                        'name': '内容聚合',
                        'depth': 5,
                        'localY': 0.1,
                        'itemStyle': {
                            'color': '#CCCCCC',
                            'borderColor': '#CCCCCC'
                        }
                    }
                ],
                links: [
                    {source: "图像", target: "标题", value: 0.2, sourceType: "meta"},
                    {source: "图像", target: "描述文本", value: 0.2, sourceType: "meta"},
                    {source: "图像", target: "来源网站", value: 0.2, sourceType: "meta"},
                    {source: "标题", target: "结构", value: 0.2, sourceType: "structureTag", field: "tagType"},
                    {source: "描述文本", target: "结构", value: 0.2, sourceType: "structureTag", field: "tagType"},
                    {source: "来源网站", target: "结构", value: 0.2, sourceType: "structureTag", field: "tagType"},
                    // {source: "图像", target: "place", value: 0.2, sourceType: "meta"},
                    // {source: "图像", target: "time", value: 0.2, sourceType: "meta"},
                    // {source: "图像", target: "event", value: 0.2, sourceType: "meta"},
                    // {source: "图像", target: "thing", value: 0.2, sourceType: "meta"},
                    // {source: "图像", target: "person", value: 0.2, sourceType: "meta"},
                    // {source: "place", target: "内容", value: 0.2, sourceType: "contentTag", field: "tagName"},
                    // {source: "time", target: "内容", value: 0.2, sourceType: "contentTag", field: "tagName"},
                    // {source: "event", target: "内容", value: 0.2, sourceType: "contentTag", field: "tagName"},
                    // {source: "thing", target: "内容", value: 0.2, sourceType: "contentTag", field: "tagName"},
                    // {source: "person", target: "内容", value: 0.2, sourceType: "contentTag", field: "tagName"},
                    {
                        'source': '类型',
                        'target': '知识库',
                        'value': 2
                    },{
                        'source': '结构',
                        'target': '知识库',
                        'value': 2
                    },{
                        'source': '内容',
                        'target': '知识库',
                        'value': 2
                    },{
                        'source': '类型',
                        'target': '算法库',
                        'value': 2
                    },{
                        'source': '结构',
                        'target': '算法库',
                        'value': 2
                    },{
                        'source': '内容',
                        'target': '算法库',
                        'value': 2
                    },
                    // {
                    //     'source': '知识库',
                    //     'target': '时间',
                    //     'value': 2
                    // },{
                    //     'source': '知识库',
                    //     'target': '人物',
                    //     'value': 2
                    // },{
                    //     'source': '知识库',
                    //     'target': '地点',
                    //     'value': 2
                    // },{
                    //     'source': '知识库',
                    //     'target': '事物',
                    //     'value': 2
                    // },{
                    //     'source': '知识库',
                    //     'target': '事件',
                    //     'value': 2
                    // },{
                    //     'source': '知识库',
                    //     'target': '现象',
                    //     'value': 2
                    // },{
                    //     'source': '知识库',
                    //     'target': '场景',
                    //     'value': 2
                    // },{
                    //     'source': '算法库',
                    //     'target': '时间',
                    //     'value': 2
                    // },{
                    //     'source': '算法库',
                    //     'target': '人物',
                    //     'value': 2
                    // },{
                    //     'source': '算法库',
                    //     'target': '地点',
                    //     'value': 2
                    // },{
                    //     'source': '算法库',
                    //     'target': '事物',
                    //     'value': 2
                    // },{
                    //     'source': '算法库',
                    //     'target': '事件',
                    //     'value': 2
                    // },{
                    //     'source': '算法库',
                    //     'target': '现象',
                    //     'value': 2
                    // },{
                    //     'source': '算法库',
                    //     'target': '场景',
                    //     'value': 2
                    // },
                    {
                        'source': '时间',
                        'target': '内容聚合',
                        'value': 2
                    },{
                        'source': '地点',
                        'target': '内容聚合',
                        'value': 2
                    },{
                        'source': '人物',
                        'target': '内容聚合',
                        'value': 2
                    },{
                        'source': '事物',
                        'target': '内容聚合',
                        'value': 2
                    },{
                        'source': '事件',
                        'target': '内容聚合',
                        'value': 2
                    },{
                        'source': '现象',
                        'target': '内容聚合',
                        'value': 2
                    },{
                        'source': '场景',
                        'target': '内容聚合',
                        'value': 2
                    }
                ],
                lineStyle: {
                    color: 'source',
                    curveness: 0.5
                },
                itemStyle: {
                    color: 'gray',
                    borderColor: 'gray'
                },
                label: {
                    color: 'rgba(0,0,0,0.7)',
                    fontFamily: 'Arial',
                    fontSize: 11
                }
            }],
        tooltip: {
            trigger: 'item',
            triggerOn: 'click',
            formatter: clickEdge
        }
    };
    
    let nodeNames = conf.series[0].data.map(item => item.name);
    let nodes = conf.series[0].data;
    let links = conf.series[0].links;
    let categoryList = data.typeTags[0].categoryList;
    let contentTags = data.contentTags;
    let pro, i, len;
    //内容标签
    for(i = 0, len = contentTags.length; i < len; i++){
        generateNodes(nodeNames, nodes, links, "图像", "内容", "tagName", contentTags[i].tagName, 0.2, "contentTag");
    }
    //类型标签
    for(pro in categoryList){
        generateNodes(nodeNames, nodes, links, "图像", "类型", pro, categoryList[pro], 0.2, "typeTag");
    }

    links.push({
        'source': "知识库",
        'target': relationMap[feature],
        'value': 2
    });
    links.push({
        'source': "算法库",
        'target': relationMap[feature],
        'value': 2
    });
    return conf;
}

function generateNodes(nodeNames, nodes, links, itemName, tagType, tagName, tagValue, value, tagType_en){
    //防止桑基图中有重复节点
    if(!tagValue || nodeNames.includes(tagValue) || tagValue === "FALSE"){
        return;
    }

    nodeNames.push(tagValue);
    nodes.push({
        'name': tagValue,
        'depth': 1,
        'itemStyle': {
            'color': '#567C73',
            'borderColor': '#567C73'
        }
    });
    links.push({
        'source': itemName,
        'target': tagValue,
        'value': value,
        "sourceType": "meta"
    });
    links.push({
        'source': tagValue,
        'target': tagType,
        'value': value,
        "sourceType": tagType_en,      //source结点的类型
        "field": tagName              //value对应的字段
    });
};

export default setImageConf;