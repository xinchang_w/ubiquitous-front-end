const defaultConf = {
    title: {
        subtext: '',
        left: 'center'
    },
    backgroundColor: '#FFFFFF',
    series: [
        {
            type: 'sankey',
            x: 'center',
            y: 'top',
            left: 50.0,
            top: 20.0,
            right: 150.0,
            bottom: 25.0,
            data: [
                {
                    'name': '音频',
                    'localY': '0.2',
                    'itemStyle': {
                        'color': '#f18bbf',
                        'borderColor': '#f18bbf',


                    }
                }, {
                    'name': '音频体裁',
                    'itemStyle': {
                        'color': '#3891A7',
                        'borderColor': '#3891A7'

                    }
                },
                {
                    'name': '音频长度',
                    'itemStyle': {
                        'color': '#3891A7',
                        'borderColor': '#3891A7'
                    }
                }, {
                    'name': '音频主题',
                    'itemStyle': {
                        'color': '#3891A7',
                        'borderColor': '#3891A7'
                    }
                }, {
                    'name': '音频结构化程度',
                    'itemStyle': {
                        'color': '#3891A7',
                        'borderColor': '#3891A7'
                    }
                }, {
                    'name': '音频语种',
                    'itemStyle': {
                        'color': '#3891A7',
                        'borderColor': '#3891A7'
                    }
                }, {
                    'name': '音频传播渠道',
                    'itemStyle': {
                        'color': '#3891A7',
                        'borderColor': '#3891A7'
                    }
                }, {
                    'name': '音频媒介',
                    'itemStyle': {
                        'color': '#3891A7',
                        'borderColor': '#3891A7'
                    }
                }, {
                    'name': '文本',
                    'localY': '0.3',
                    'itemStyle': {
                        'color': '#CCFF00',
                        'borderColor': '#CCFF00'
                    }
                }, {
                    'name': '文本体裁',
                    'itemStyle': {
                        'color': '#567C73',
                        'borderColor': '#567C73'
                    }
                }, {
                    'name': '文本长度',
                    'itemStyle': {
                        'color': '#567C73',
                        'borderColor': '#567C73'
                    }
                }, {
                    'name': '文本主题',
                    'itemStyle': {
                        'color': '#567C73',
                        'borderColor': '#567C73'
                    }
                }, {
                    'name': '文本结构化程度',
                    'itemStyle': {
                        'color': '#567C73',
                        'borderColor': '#567C73'
                    }
                }, {
                    'name': '文本语种',
                    'itemStyle': {
                        'color': '#567C73',
                        'borderColor': '#567C73'
                    }
                }, {
                    'name': '文本传播渠道',
                    'itemStyle': {
                        'color': '#567C73',
                        'borderColor': '#567C73'
                    }
                }, {
                    'name': '文本媒介',
                    'itemStyle': {
                        'color': '#567C73',
                        'borderColor': '#567C73'
                    }
                }, {
                    'name': '视频',
                    'localY': '0.4',
                    'itemStyle': {
                        'color': '#006699',
                        'borderColor': '#006699'
                    }
                }, {
                    'name': '视频体裁',
                    'itemStyle': {
                        'color': '#0099CC',
                        'borderColor': '#0099CC'
                    }
                }, {
                    'name': '视频长度',
                    'itemStyle': {
                        'color': '#0099CC',
                        'borderColor': '#0099CC'
                    }
                }, {
                    'name': '视频主题',
                    'itemStyle': {
                        'color': '#0099CC',
                        'borderColor': '#0099CC'
                    }
                }, {
                    'name': '视频结构化程度',
                    'itemStyle': {
                        'color': '#0099CC',
                        'borderColor': '#0099CC'
                    }
                }, {
                    'name': '视频语种',
                    'itemStyle': {
                        'color': '#0099CC',
                        'borderColor': '#0099CC'
                    }
                }, {
                    'name': '视频传播渠道',
                    'itemStyle': {
                        'color': '#0099CC',
                        'borderColor': '#0099CC'
                    }
                }, {
                    'name': '视频媒介',
                    'itemStyle': {
                        'color': '#0099CC',
                        'borderColor': '#0099CC'
                    }
                }, {
                    'name': '图表',
                    'localY': '0.5',
                    'itemStyle': {
                        'color': '#FF9900',
                        'borderColor': '#FF9900'
                    }
                }, {
                    'name': '图表体裁',
                    'itemStyle': {
                        'color': '#CCCCFF',
                        'borderColor': '#CCCCFF'
                    }
                }, {
                    'name': '图表长度',
                    'itemStyle': {
                        'color': '#CCCCFF',
                        'borderColor': '#CCCCFF'
                    }
                }, {
                    'name': '图表主题',
                    'itemStyle': {
                        'color': '#CCCCFF',
                        'borderColor': '#CCCCFF'
                    }
                }, {
                    'name': '图表结构化程度',
                    'itemStyle': {
                        'color': '#CCCCFF',
                        'borderColor': '#CCCCFF'
                    }
                }, {
                    'name': '图表语种',
                    'itemStyle': {
                        'color': '#CCCCFF',
                        'borderColor': '#CCCCFF'
                    }
                }, {
                    'name': '图表传播渠道',
                    'itemStyle': {
                        'color': '#CCCCFF',
                        'borderColor': '#CCCCFF'
                    }
                }, {
                    'name': '图表媒介',
                    'itemStyle': {
                        'color': '#CCCCFF',
                        'borderColor': '#CCCCFF'
                    }
                }, {
                    'name': '图片',
                    'localY': '0.6',
                    'itemStyle': {
                        'color': '#FFFF66',
                        'borderColor': '#FFFF66'
                    }
                }, {
                    'name': '图片体裁',
                    'itemStyle': {
                        'color': '#FF0033',
                        'borderColor': '#FF0033'
                    }
                }, {
                    'name': '图片长度',
                    'itemStyle': {
                        'color': '#FF0033',
                        'borderColor': '#FF0033'
                    }
                }, {
                    'name': '图片主题',
                    'itemStyle': {
                        'color': '#FF0033',
                        'borderColor': '#FF0033'
                    }
                }, {
                    'name': '图片结构化程度',
                    'itemStyle': {
                        'color': '#FF0033',
                        'borderColor': '#FF0033'
                    }
                }, {
                    'name': '图片语种',
                    'itemStyle': {
                        'color': '#FF0033',
                        'borderColor': '#FF0033'
                    }
                }, {
                    'name': '图片传播渠道',
                    'itemStyle': {
                        'color': '#FF0033',
                        'borderColor': '#FF0033'
                    }
                }, {
                    'name': '图片媒介',
                    'itemStyle': {
                        'color': '#FF0033',
                        'borderColor': '#FF0033'
                    }
                }, {
                    'name': '地图',
                    'localY': '0.7',
                    'itemStyle': {
                        'color': '#993399',
                        'borderColor': '#993399'
                    }
                }, {
                    'name': '地图体裁',
                    'itemStyle': {
                        'color': '#FF9900',
                        'borderColor': '#FF9900'
                    }
                }, {
                    'name': '地图长度',
                    'itemStyle': {
                        'color': '#FF9900',
                        'borderColor': '#FF9900'
                    }
                }, {
                    'name': '地图主题',
                    'itemStyle': {
                        'color': '#FF9900',
                        'borderColor': '#FF9900'
                    }
                }, {
                    'name': '地图结构化程度',
                    'itemStyle': {
                        'color': '#FF9900',
                        'borderColor': '#FF9900'
                    }
                }, {
                    'name': '地图语种',
                    'itemStyle': {
                        'color': '#FF9900',
                        'borderColor': '#FF9900'
                    }
                }, {
                    'name': '地图传播渠道',
                    'itemStyle': {
                        'color': '#FF9900',
                        'borderColor': '#FF9900'
                    }
                }, {
                    'name': '地图媒介',
                    'itemStyle': {
                        'color': '#FF9900',
                        'borderColor': '#FF9900'
                    }
                }, {
                    'name': '类型',
                    'localY': '0.3',
                    'itemStyle': {
                        'color': 'FF6666',
                        'borderColor': 'FF6666'
                    }
                }, {
                    'name': '结构',
                    'localY': '0.4',
                    'itemStyle': {
                        'color': '#FFFF00',
                        'borderColor': '#FFFF00'
                    }
                }, {
                    'name': '内容',
                    'localY': '0.5',
                    'itemStyle': {
                        'color': '#006699',
                        'borderColor': '#006699'
                    }
                }, {
                    'name': '知识库',
                    'localY': '0.25',
                    'itemStyle': {
                        'color': '#CC6699',
                        'borderColor': '#CC6699'
                    }
                }, {
                    'name': '算法库',
                    'localY': '0.45',
                    'itemStyle': {
                        'color': '#99CC66',
                        'borderColor': '#99CC66'
                    }
                }, {
                    'name': '时间',
                    'localY': '0.1',
                    'itemStyle': {
                        'color': '#FFB6C1',
                        'borderColor': '#FFB6C1'
                    }
                }, {
                    'name': '人物',
                    'localY': '0.2',
                    'itemStyle': {
                        'color': '#800080',
                        'borderColor': '#800080'
                    }
                }, {
                    'name': '地点',
                    'localY': '0.3',
                    'itemStyle': {
                        'color': '#0000FF',
                        'borderColor': '#0000FF'
                    }
                }, {
                    'name': '事物',
                    'localY': '0.4',
                    'itemStyle': {
                        'color': '#66CDAA',
                        'borderColor': '#66CDAA'
                    }
                }, {
                    'name': '事件',
                    'localY': '0.5',
                    'itemStyle': {
                        'color': '#FFFF00',
                        'borderColor': '#FFFF00'
                    }
                }, {
                    'name': '现象',
                    'localY': '0.6',
                    'itemStyle': {
                        'color': '#FF0000',
                        'borderColor': '#FF0000'
                    }
                }, {
                    'name': '场景',
                    'localY': '0.7',
                    'itemStyle': {
                        'color': '#FF8C00',
                        'borderColor': '#FF8C00'
                    }
                }, {
                    'name': '内容聚合',
                    'localY': '0.35',
                    'itemStyle': {
                        'color': '#CCCCCC',
                        'borderColor': '#CCCCCC'
                    }
                },


            ],

            links: [
                {
                    'source': '音频',
                    'target': '音频体裁',
                    'value': 1.8399963378906
                }, {
                    'source': '音频',
                    'target': '音频长度',
                    'value': 1.8399963378906
                }, {
                    'source': '音频',
                    'target': '音频主题',
                    'value': 1.8399963378906
                }, {
                    'source': '音频',
                    'target': '音频结构化程度',
                    'value': 1.8399963378906
                }, {
                    'source': '音频',
                    'target': '音频语种',
                    'value': 1.8399963378906
                }, {
                    'source': '音频',
                    'target': '音频传播渠道',
                    'value': 1.8399963378906
                }, {
                    'source': '音频',
                    'target': '音频媒介',
                    'value': 1.8399963378906
                }, {
                    'source': '文本',
                    'target': '文本体裁',
                    'value': 1.8399963378906
                }, {
                    'source': '文本',
                    'target': '文本长度',
                    'value': 1.8399963378906
                }, {
                    'source': '文本',
                    'target': '文本主题',
                    'value': 1.8399963378906
                }, {
                    'source': '文本',
                    'target': '文本结构化程度',
                    'value': 1.8399963378906
                }, {
                    'source': '文本',
                    'target': '文本语种',
                    'value': 1.8399963378906
                }, {
                    'source': '文本',
                    'target': '文本传播渠道',
                    'value': 1.8399963378906
                }, {
                    'source': '文本',
                    'target': '文本媒介',
                    'value': 1.8399963378906
                }, {
                    'source': '视频',
                    'target': '视频体裁',
                    'value': 1.8399963378906
                }, {
                    'source': '视频',
                    'target': '视频长度',
                    'value': 1.8399963378906
                }, {
                    'source': '视频',
                    'target': '视频主题',
                    'value': 1.8399963378906
                }, {
                    'source': '视频',
                    'target': '视频结构化程度',
                    'value': 1.8399963378906
                }, {
                    'source': '视频',
                    'target': '视频语种',
                    'value': 1.8399963378906
                }, {
                    'source': '视频',
                    'target': '视频传播渠道',
                    'value': 1.8399963378906
                }, {
                    'source': '视频',
                    'target': '视频媒介',
                    'value': 1.8399963378906
                }, {
                    'source': '图表',
                    'target': '图表体裁',
                    'value': 1.8399963378906
                }, {
                    'source': '图表',
                    'target': '图表长度',
                    'value': 1.8399963378906
                }, {
                    'source': '图表',
                    'target': '图表主题',
                    'value': 1.8399963378906
                }, {
                    'source': '图表',
                    'target': '图表结构化程度',
                    'value': 1.8399963378906
                }, {
                    'source': '图表',
                    'target': '图表语种',
                    'value': 1.8399963378906
                }, {
                    'source': '图表',
                    'target': '图表传播渠道',
                    'value': 1.8399963378906
                }, {
                    'source': '图表',
                    'target': '图表媒介',
                    'value': 1.8399963378906
                }, {
                    'source': '图片',
                    'target': '图片体裁',
                    'value': 1.8399963378906
                }, {
                    'source': '图片',
                    'target': '图片长度',
                    'value': 1.8399963378906
                }, {
                    'source': '图片',
                    'target': '图片主题',
                    'value': 1.8399963378906
                }, {
                    'source': '图片',
                    'target': '图片结构化程度',
                    'value': 1.8399963378906
                }, {
                    'source': '图片',
                    'target': '图片语种',
                    'value': 1.8399963378906
                }, {
                    'source': '图片',
                    'target': '图片传播渠道',
                    'value': 1.8399963378906
                }, {
                    'source': '图片',
                    'target': '图片媒介',
                    'value': 1.8399963378906
                }, {
                    'source': '地图',
                    'target': '地图体裁',
                    'value': 1.8399963378906
                }, {
                    'source': '地图',
                    'target': '地图长度',
                    'value': 1.8399963378906
                }, {
                    'source': '地图',
                    'target': '地图主题',
                    'value': 1.8399963378906
                }, {
                    'source': '地图',
                    'target': '地图结构化程度',
                    'value': 1.8399963378906
                }, {
                    'source': '地图',
                    'target': '地图语种',
                    'value': 1.8399963378906
                }, {
                    'source': '地图',
                    'target': '地图传播渠道',
                    'value': 1.8399963378906
                }, {
                    'source': '地图',
                    'target': '地图媒介',
                    'value': 1.8399963378906
                },
                {
                    'source': '文本体裁',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '文本长度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '文本主题',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '文本结构化程度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '文本语种',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '文本传播渠道',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '文本媒介',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '音频体裁',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '音频长度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '音频主题',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '音频结构化程度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '音频语种',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '音频传播渠道',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '音频媒介',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '视频体裁',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '视频长度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '视频主题',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '视频结构化程度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '视频语种',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '视频传播渠道',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '视频媒介',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '图片体裁',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '图片长度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '图片主题',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '图片结构化程度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '图片语种',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '图片传播渠道',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '图片媒介',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '图表体裁',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '图表长度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '图表主题',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '图表结构化程度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '图表语种',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '图表传播渠道',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '图表媒介',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '地图体裁',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '地图长度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '地图主题',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '地图结构化程度',
                    'target': '结构',
                    'value': 0.5
                }, {
                    'source': '地图语种',
                    'target': '内容',
                    'value': 0.5
                }, {
                    'source': '地图传播渠道',
                    'target': '类型',
                    'value': 0.5
                }, {
                    'source': '地图媒介',
                    'target': '类型',
                    'value': 0.5
                },
                {
                    'source': '类型',
                    'target': '知识库',
                    'value': 2
                }, {
                    'source': '结构',
                    'target': '知识库',
                    'value': 2
                }, {
                    'source': '内容',
                    'target': '知识库',
                    'value': 2
                }, {
                    'source': '类型',
                    'target': '算法库',
                    'value': 2
                }, {
                    'source': '结构',
                    'target': '算法库',
                    'value': 2
                }, {
                    'source': '内容',
                    'target': '算法库',
                    'value': 2
                },
                {
                    'source': '知识库',
                    'target': '时间',
                    'value': 3
                }, {
                    'source': '知识库',
                    'target': '人物',
                    'value': 3
                }, {
                    'source': '知识库',
                    'target': '地点',
                    'value': 3
                }, {
                    'source': '知识库',
                    'target': '事物',
                    'value': 3
                }, {
                    'source': '知识库',
                    'target': '事件',
                    'value': 3
                }, {
                    'source': '知识库',
                    'target': '现象',
                    'value': 3
                }, {
                    'source': '知识库',
                    'target': '场景',
                    'value': 3
                }, {
                    'source': '算法库',
                    'target': '时间',
                    'value': 3
                }, {
                    'source': '算法库',
                    'target': '人物',
                    'value': 3
                }, {
                    'source': '算法库',
                    'target': '地点',
                    'value': 3
                }, {
                    'source': '算法库',
                    'target': '事物',
                    'value': 3
                }, {
                    'source': '算法库',
                    'target': '事件',
                    'value': 3
                }, {
                    'source': '算法库',
                    'target': '现象',
                    'value': 3
                }, {
                    'source': '算法库',
                    'target': '场景',
                    'value': 3
                },
                {
                    'source': '时间',
                    'target': '内容聚合',
                    'value': 3
                }, {
                    'source': '地点',
                    'target': '内容聚合',
                    'value': 3
                }, {
                    'source': '人物',
                    'target': '内容聚合',
                    'value': 3
                }, {
                    'source': '事物',
                    'target': '内容聚合',
                    'value': 3
                }, {
                    'source': '事件',
                    'target': '内容聚合',
                    'value': 3
                }, {
                    'source': '现象',
                    'target': '内容聚合',
                    'value': 3
                }, {
                    'source': '场景',
                    'target': '内容聚合',
                    'value': 3
                }
            ],
            lineStyle: {
                color: 'source',
                curveness: 0.5
            },
            itemStyle: {
                color: '#1f77b4',
                borderColor: '#1f77b4'
            },
            label: {
                color: 'rgba(0,0,0,0.7)',
                fontFamily: 'Arial',
                fontSize: 11
            }
        }],
    tooltip: {
        trigger: 'item'
    }
};

function setDefaultConf(){
    return defaultConf;
}

export default setDefaultConf;