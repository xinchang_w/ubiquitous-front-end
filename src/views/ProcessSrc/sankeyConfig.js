import setDefaultConf from "./sankeyConfig_default";
import setTextConf from "./sankeyConfig_text";
import setImageConf from "./sankeyConfig_image";
import setChartConf from "./sankeyConfig_chart";
import setMapConf from "./sankeyConfig_map";
import setAudioConf from "./sankeyConfig_audio";
import setVideoConf from "./sankeyConfig_video";

const sankeyConf = {
    "defaultConf": setDefaultConf,
    "text": setTextConf,
    "img": setImageConf,
    "chart": setChartConf,
    "map": setMapConf,
    "audio": setAudioConf,
    "video": setVideoConf
};

export default sankeyConf;