import videoSvg from "@/assets/image/data-query-video.svg";
import { FILE_SERVER } from "@/assets/global";
const ITEMS = {
    "text": {
        "title": "文本",
        "meta": {
            "title": "原数据"
        },
        "typeTag": {
            "title": "类型标签",
            "url": "text/typeTag/query/1/10"
        },
        "structureTag": {
            "title": "结构标签",
            "url": "text/structureTag/query",
            "cols": [
                {
                    "title": "类型",
                    "dataIndex": "tagType",
                    "align": "center"
                },{
                    "title": "值",
                    "dataIndex": "tagValue",
                    "align": "center",
                    "ellipsis": true
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "text/contentTag/query",
            "cols": [
                {
                    "title": "类型",
                    "dataIndex": "tagType",
                    "align": "center"
                },{
                    "title": "名称",
                    "dataIndex": "tagName",
                    "align": "center"
                },{
                    "title": "值",
                    "dataIndex": "tagValue",
                    "align": "center"
                }
            ]
        },
        "element": {
            "title": "文本",
            "url": "text/element/query", 
            "searchCons": [
                {
                    "label": "请选择",
                    "value": ""
                },
                {
                    "label": "类型",
                    "value": "type"
                },
                {
                    "label": "值",
                    "value": "featureValue"
                },
                {
                    "label": "语义",
                    "value": "feature"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "key": "type",
                    "dataIndex": ["value", "type"],
                    "align": "center"
                }, {
                    "title": "语义",
                    "key": "feature",
                    "dataIndex": ["value", "feature"],
                    "align": "center"
                }, {
                    "title": "值",
                    "key": "featureValue",
                    "dataIndex": ["value", "featureValue"],
                    "align": "center"
                }
            ]
        }
    },
    "img": {
        "title": "图像",
        "meta": {
            "title": "原数据"
        },
        "typeTag": {
            "title": "类型标签",
            "url": "img/typeTag/query/1/10"    //一条原数据只对应一条类型标签，因此查询第一页即可
        },
        "structureTag": {
            "title": "结构标签",
            "url": "img/structureTag/query",
            "cols": [
                {
                    "title": "类型",
                    "dataIndex": "tagType",
                    "align": "center",
                    "ellipsis": true
                },{
                    "title": "值",
                    "dataIndex": "tagValue",
                    "align": "center",
                    "ellipsis": true
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "img/contentTag/query",
            "cols": [
                {
                    title: "内容类型",
                    dataIndex: "tagName",
                    align: "center",
                    ellipsis: true
                },
                {
                    title: "值",
                    dataIndex: "tagValue",
                    align: "center",
                    ellipsis: true
                }
            ]
        },
        "element": {
            "title": "内容",
            "url": "img/element/query",
            "searchCons": [
                {
                    "label": "请选择",
                    "value": ""
                },
                {
                    "label": "内容类型",
                    "value": "tagName"
                },
                {
                    "label": "值",
                    "value": "tag"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "dataIndex": "tagName",
                    "ellipsis": true
                },
                {
                    "title": "值",
                    "dataIndex": "tag",
                    "ellipsis": true
                }
            ]
        }
    },
    "chart": {
        "title": "图表",
        "meta": {
            "title": "原数据"
        },
        "typeTag": {
            "title": "类型标签",
            "url": "chart/typeTag/query/1/10"    //一条原数据只对应一条类型标签，因此查询第一页即可
        },
        "structureTag": {
            "title": "结构标签",
            "url": "chart/structureTag/query",
            "cols": [
                {
                    "title": "类型",
                    "dataIndex": "tagType",
                    "align": "center",
                    "ellipsis": true
                },{
                    "title": "值",
                    children: [
                        {
                            title: "left",
                            dataIndex: "left",
                            render: (text) => Math.floor(Number(text))
                        },
                        {
                            title: "top",
                            dataIndex: "top",
                            render: (text) => Math.floor(Number(text))
                        },
                        {
                            title: "width",
                            dataIndex: "width",
                            render: (text) => Math.floor(Number(text))
                        },
                        {
                            title: "height",
                            dataIndex: "height",
                            render: (text) => Math.floor(Number(text))
                        }
                    ]
                    // "dataIndex": "tagValue",
                    // "align": "center",
                    // "ellipsis": true
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "chart/contentTag/query",
            "cols": [
                {
                    title: "内容类型",
                    dataIndex: "tagType",
                    align: "center",
                    ellipsis: true
                },
                {
                    title: "值",
                    dataIndex: "tagValue",
                    align: "center",
                    ellipsis: true
                }
            ]
        },
        "element": {
            "title": "内容",
            "url": "chart/element/query",
            "searchCons": [
                {
                    "label": "请选择",
                    "value": ""
                },
                {
                    "label": "内容类型",
                    "value": "tagType"
                },
                {
                    "label": "值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "内容类型",
                    "ellipsis": true,
                    "dataIndex": "tagType"
                },
                {
                    "title": "值",
                    "ellipsis": true,
                    "dataIndex": "tagValue"
                }
            ]
        }
    },
    "map": {
        "title": "地图",
        "meta": {
            "title": "原数据"
        },
        "typeTag": {
            "title": "类型标签",
            "url": "map/typeTag/query/1/10"    //一条原数据只对应一条类型标签，因此查询第一页即可
        },
        "structureTag": {
            "title": "结构标签",
            "url": "map/structureTag/query",
            "cols": [
                {
                    "title": "类型",
                    "dataIndex": "tagType",
                    "align": "center",
                    "ellipsis": true
                },{
                    "title": "值",
                    "dataIndex": "tagValue",
                    "align": "center",
                    "ellipsis": true
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "map/contentTag/query",
            "cols": [
                {
                    title: "内容类型",
                    dataIndex: "tagType",
                    align: "center",
                    ellipsis: true
                },
                {
                    title: "值",
                    dataIndex: "tagName",
                    align: "center",
                    ellipsis: true
                }
            ]
        },
        "element": {
            "title": "内容",
            "url": "map/element/query",
            "searchCons": [
                {
                    "label": "请选择",
                    "value": ""
                },
                {
                    "label": "内容类型",
                    "value": "tagType"
                },
                {
                    "label": "值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "内容类型",
                    "ellipsis": true,
                    "dataIndex": "tagType"
                },
                {
                    "title": "值",
                    "ellipsis": true,
                    "dataIndex": "tagValue"
                }
            ]
        }
    },
    "audio": {
        "title": "音频",
        "meta": {
            "title": "原数据"
        },
        "typeTag": {
            "title": "类型标签",
            "url": "audio/typeTag/query/1/10"    //一条原数据只对应一条类型标签，因此查询第一页即可
        },
        "structureTag": {
            "title": "结构标签",
            "url": "audio/structureTag/query",
            "cols": [
                {
                    "title": "时长",
                    "dataIndex": "duration",
                    "width": "150px",
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "文件大小",
                    "dataIndex": "fileSize",
                    "width": "150px",
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "比特率",
                    "dataIndex": "bitRate",
                    "width": "150px",
                    "ellipsis": true,
                    "className": "pointer"
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "audio/contentTag/query",
            "cols": [
                {
                    "title": "类型",
                    "dataIndex": "tagType",
                    "ellipsis": true
                },
                {
                    "title": "值",
                    "dataIndex": "tagValue",
                    "ellipsis": true
                }
            ]
        },
        "element": {
            "title": "内容",
            "url": "audio/element/query",
            "searchCons": [
                {
                    "label": "类型",
                    "value": "tagType"
                },
                {
                    "label": "值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "ellipsis": true,
                    "dataIndex": "tagType"
                },
                {
                    "title": "值",
                    "ellipsis": true,
                    "dataIndex": "tagValue"
                }
            ]
        }
    },
    "video": {
        "title": "视频",
        "meta": {
            "title": "原数据"
        },
        "typeTag": {
            "title": "类型标签",
            "url": "video/typeTag/query/1/10"    //一条原数据只对应一条类型标签，因此查询第一页即可
        },
        "structureTag": {
            "title": "结构标签",
            "url": "video/structureTag/query",
            "cols": [
                {
                    "title": "时长",
                    "dataIndex": "duration",
                    "ellipsis": true
                },
                {
                    "title": "分辨率",
                    "dataIndex": "resolutionWidth",
                    "ellipsis": true,
                    "render": (text, record) => `${record.resolutionWidth}*${record.resolutionHeight}`
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "video/contentTag/query",
            "cols": [
                {
                    "title": "类型",
                    "dataIndex": "tagType",
                    "ellipsis": true
                },
                {
                    "title": "值",
                    "dataIndex": "url",
                    "ellipsis": true,
                    "render": (text) => <a href={`${FILE_SERVER}/video/contentTag/${text}`} target="_blank">{text ? "查看" : undefined}</a>
                }
            ]
        },
        "element": {
            "title": "内容",
            "url": "video/element/query",
            "searchCons": [
                {
                    "label": "类型",
                    "value": "tagType"
                },
                {
                    "label": "值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "ellipsis": true,
                    "dataIndex": "tagType"
                },
                {
                    "title": "值",
                    "ellipsis": true,
                    "dataIndex": "tagValue"
                }
            ]
        }
    }
};

export { ITEMS };