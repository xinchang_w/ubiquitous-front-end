import React, { Component, Fragment } from 'react';
import { Button, Input, Select, Breadcrumb, Modal, Radio } from "antd";
import { TableRendering, Paragraph, CustomizeModal } from "@/components";
import { setHighlight } from "../../utility";
import { NODE_PATH, FILE_SERVER } from "@/assets/global/index";
import ITEMS from "./config";

class ContentList extends Component{
    constructor(props){
        super(props);
        this.state = {
            "curItem": "text",
            "curSearchCon": "type",
            "keywords": "",
            "modalData": null
        };

        this.tableRendering = React.createRef();
        this.defaultUrl = `${NODE_PATH}/text/element/query`;
        this.defaultCols = ITEMS["text"].cols;

        this.onItemChange = this.onItemChange.bind(this);
        this.onConChange = this.onConChange.bind(this);
        this.onKeywordChange = this.onKeywordChange.bind(this);
        this.search = this.search.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }

    render(){
        let { curItem, curSearchCon, keywords, modalData } = this.state;
        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>信息检索</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">内容列表</Breadcrumb.Item>
                </Breadcrumb>
                <Radio.Group onChange={this.onItemChange} style={{margin:10}} value={curItem}>
                    {
                        Object.keys(ITEMS).map(i => (<Radio value={i} key={i}>{ITEMS[i].title}</Radio>))
                    }
                </Radio.Group>
                <div>
                    <div className="searchBox">
                        <div>
                            <label className="searchBox-label">查询条件：</label>
                            <Select
                                placeholder="类型"
                                className="searchBox-input"
                                value={curSearchCon}
                                onChange={this.onConChange}
                                options={ITEMS[curItem].searchCons}
                            />
                            <label className="searchBox-label">关键词：</label>
                            <Input
                                className="searchBox-input"
                                placeholder="关键词"
                                value={keywords}
                                onChange={this.onKeywordChange}
                            />
                            <Button
                                type="primary"
                                className="searchBox-btn"
                                onClick={this.search}
                            >
                                查询
                            </Button>
                        </div>
                    </div>
                    <TableRendering ref={this.tableRendering} cols={this.defaultCols} url={this.defaultUrl} clickRow={this.showModal} />
                    <CustomizeModal
                        visible={ modalData !== null }
                        title={ `内容列表-${ITEMS[curItem].title}` }
                        close={this.hideModal}
                    >
                        <Paragraphs type={curItem} {...modalData} />
                    </CustomizeModal>
                </div>
            </div>
        );
    }

    onItemChange(e){
        let val = e.target.value;
        this.setState({
            "curItem": val,
            "curSearchCon": ITEMS[val].searchCons[0].value,
            "keywords": ""
        }, this.search);
        
    }

    onConChange(value){
        this.setState({
            "curSearchCon": value
        });
    }

    onKeywordChange(e){
        this.setState({
            "keywords": e.target.value
        })
    }

    search(){
        let con = {};
        let { keywords, curSearchCon, curItem } = this.state;
        let url = `${NODE_PATH}/${ITEMS[curItem].url}`;
        let columns = ITEMS[curItem].cols;
        if(curSearchCon && keywords){
            con[curSearchCon] = keywords;
        }
        
        this.tableRendering.current.search(url, columns, con);
    }

    showModal(record){
        this.setState({
            "modalData": record
        });
    }
    
    hideModal(){
        this.setState({
            "modalData": null
        });
    }
};

function Paragraphs(props){
    let com = null;
    switch(props.type){
        case "text": 
            com = (
                <Fragment>
                    <Paragraph title="来源：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.source} />
                    <Paragraph title="时间：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.recordDatetime} />
                    <Paragraph title="摘要：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.description} />
                    <Paragraph title="标签名：" content={props.tagid && props.tagid.tagName} />
                    <Paragraph title="标签值：" content={props.tagid && props.tagid.tagValue} />
                    <Paragraph title="标签类型：" content={props.value && props.value.type} />
                    <Paragraph title="标签语义：" content={props.value && props.value.feature} />
                    <Paragraph title="内容：" content={props.tagid && setHighlight(props.tagid.metaId.content, props.tagid.startPosition, props.tagid.endPosition)} />
                </Fragment>
            );
            break;
        case "image":
            com = (
                <Fragment>
                    <Paragraph title="日期：" content={ props.tagid && props.tagid.metaId && props.tagid.metaId.recordDatetime } />
                    <Paragraph title="来源：" content={ props.tagid && props.tagid.metaId && props.tagid.metaId.website } />
                    <Paragraph title="描述：" content={ props.tagid && props.tagid.metaId &&  props.tagid.metaId.description } />
                    <Paragraph title="内容类型：" content={ props.tagName } />
                    <Paragraph title="内容：" content={ props.tag } />
                    <Paragraph image={ props.tagid && props.tagid.metaId && `${FILE_SERVER}/images/metaData_Img/${props.tagid.metaId.imageName}`} />
                </Fragment>
            ); 
            break;
        case "chart": 
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.recordedTime} />
                    <Paragraph title="文件格式：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.format} />
                    <Paragraph title="来源：" content={props.tagid && props.tagid.metaId && <a href={props.tagid.metaId.source} target="_blank">{props.tagid.metaId.source}</a>} />
                    <Paragraph title="内容类型：" content={props.tagType} />
                    <Paragraph title="内容：" content={props.tagValue} />
                    <Paragraph title="图片：" image={props.tagid && props.tagid.metaId && props.tagid.metaId.imagePath && `${FILE_SERVER}/${props.tagid.metaId.imagePath}`} />
                </Fragment>
            );
            break;
        case "map": 
            com = (
                <Fragment>
                    <Paragraph title="标题：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.mapName} />
                    <Paragraph title="发布时间：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.recordDatetime} />
                    <Paragraph title="来源：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.website} />
                    <Paragraph title="内容类型：" content={props.tagType}  />
                    <Paragraph title="内容值：" content={props.tagValue} />
                    <Paragraph title="图片：" image={props.tagid && props.tagid.metaId && props.tagid.metaId.imagePath && `${FILE_SERVER}/map/${props.tagid.metaId.imagePath}`} />
                </Fragment>
            ); 
            break;
        case "audio": 
            com = (
                <Fragment>
                    <Paragraph title="节目名：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.programName} />
                    <Paragraph title="频道名：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.channelName} />
                    <Paragraph title="播出时间：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.date} />
                    <Paragraph title="内容类型：" content={props.tagType}  />
                    <Paragraph title="内容值：" content={props.tagValue} />
                    <Paragraph title="音频：" audio={props.tagid && props.tagid.metaId && props.tagid.metaId.url && `${FILE_SERVER}/audio/${props.tagid.metaId.url}`} />
                </Fragment>
            );
            break;
        case "video": 
            com = (
                <Fragment>
                    <Paragraph title="拍摄时间：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.date} />
                    <Paragraph title="摄像头位置：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.location} />
                    <Paragraph title="内容类型：" content={props.tagType}  />
                    <Paragraph title="内容值：" content={props.tagValue} />
                    <Paragraph title="视频：" video={props.tagid && props.tagid.url && `${FILE_SERVER}/video/contentTag/${props.tagid.url}`} />
                </Fragment>
            );
            break;
        default:
            break;
    }
    return com;
}


export default ContentList;
