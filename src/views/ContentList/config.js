import { stopPropagation } from "@/utility";
import imgSvg from "@/assets/image/data-query-image.svg";
import mapSvg from "@/assets/image/data-query-map.svg";
import chartSvg from "@/assets/image/data-query-chart.svg";
import audioSvg from "@/assets/image/data-query-audio.svg";
import videoSvg from "@/assets/image/data-query-video.svg";
const ITEMS = {
    "text": {
        "title": "文本",
        "url": "text/element/query",
        "searchCons": [
            {
                value: "type",
                label: "类型"
            }, {
                value: "featureValue",
                label: "值",
            }, {
                value: "feature",
                label: "语义"
            }
        ],
        "cols": [
            {
                "title": "时间",
                "key": "recordDatetime",
                "dataIndex": ["tagid", "metaId", "recordDatetime"],
                "width": 150,
                "align": "center",
                "ellipsis": true,
                "onCell": stopPropagation
            },{
                "title": "来源",
                "key": "source",
                "dataIndex": ["tagid", "metaId", "source"],
                "width": 150,
                "align": "center",
                "ellipsis": true,
                "onCell": stopPropagation
            },{
                "title": '摘要',
                "dataIndex": ["tagid", "metaId", "description"],
                "key": 'description',
                "align": "center",
                "ellipsis": true,
                "className": "pointer"
            },{
                "title": "标签",
                "children": [
                    {
                        "title": "类型",
                        "key": "tagType",
                        "dataIndex": ["tagid", "tagType"],
                        "align": "center",
                        "onCell": stopPropagation
                    },{
                        "title": "名称",
                        "key": "tagName",
                        "dataIndex": ["tagid", "tagName"],
                        "align": "center",
                        "onCell": stopPropagation
                    },{
                        "title": "值",
                        "key": "tagValue",
                        "dataIndex": ["tagid", "tagValue"],
                        "align": "center",
                        "className": "pointer"
                    }
                ]
            },{
                "title": "内容",
                "children": [
                    {
                        "title": "类型",
                        "key": "type",
                        "dataIndex": ["value", "type"],
                        "align": "center",
                        "onCell": stopPropagation
                    },{
                        "title": "语义",
                        "key": "feature",
                        "dataIndex": ["value", "feature"],
                        "align": "center",
                        "onCell": stopPropagation
                    }
                ]
            }
        ]
    },
    "image": {
        "title": "图像",
        "url": "img/element/query",
        "searchCons": [
            {
                "label": "内容类型",
                "value": "tagName"
            },
            {
                "label": "值",
                "value": "tag"
            }
        ],
        "cols": [
            {
                title: "时间",
                dataIndex: ["tagid", "metaId", "recordDatetime"],
                "width": 150,
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                title: "来源",
                dataIndex: ["tagid", "metaId", "website"],
                "width": 150,
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                title: "标题",
                dataIndex: ["tagid", "metaId", "caption"],
                "ellipsis": true,
                "className": "pointer"
            },
            {
                title: "内容",
                children: [
                    {
                        title: "类型",
                        dataIndex: "tagName",
                        "width": 200,
                        "ellipsis": true,
                        "onCell": stopPropagation
                    },
                    {
                        title: "值",
                        dataIndex: "tag",
                        "width": 200,
                        "ellipsis": true,
                        "className": "pointer"
                    }
                ]
            }
        ]
    },
    "chart": {
        "title": "图表",
        "url": "chart/element/query",
        "searchCons": [
            {
                "label": "类型",
                "value": "tagType"
            },
            {
                "label": "值",
                "value": "tagValue"
            }
        ],
        "cols": [
            {
                "title": "标题",
                "dataIndex": ["tagid", "metaId", "description"],
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "dataIndex": ["tagid", "metaId", "imagePath"],
                "render": () => <img src={chartSvg} width="20" heigh="20" alt="chart" />,
                "width": "200px",
                "ellipsis": true,
                "className": "pointer"
            },
            {
                "title": "来源",
                "dataIndex": ["tagid", "metaId", "website"],
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "记录时间",
                "dataIndex": ["tagid", "metaId", "recordedTime"],
                "width": "200px",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "children": [
                    {
                        "title": "类型",
                        "width": "200px",
                        "ellipsis": true,
                        "dataIndex": "tagType",
                        "className": "pointer"
                    },
                    {
                        "title": "值",
                        "width": "200px",
                        "ellipsis": true,
                        "dataIndex": "tagValue",
                        "className": "pointer"
                    }
                ]
            }
        ]
    },
    "map": {
        "title": "地图",
        "url": "map/element/query",
        "searchCons": [
            {
                "label": "类型",
                "value": "tagType"
            },
            {
                "label": "值",
                "value": "tagValue"
            }
        ],
        "cols": [
            {
                "title": "标题",
                "dataIndex": ["tagid", "metaId", "mapName"],
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "dataIndex": ["tagid", "metaId", "imagePath"],
                "ellipsis": true,
                "render": () => <img src={mapSvg} alt="地图" width="20" height="20" />,
                "className": "pointer"
            },
            {
                "title": "来源",
                "ellipsis": true,
                "dataIndex": ["tagid", "metaId", "website"],
                "onCell": stopPropagation
            },
            {
                "title": "时间",
                "dataIndex": ["tagid", "metaId", "recordDatetime"],
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "children": [
                    {
                        "title": "类型",
                        "width": "200px",
                        "ellipsis": true,
                        "dataIndex": "tagType",
                        "className": "pointer"
                    },
                    {
                        "title": "值",
                        "width": "200px",
                        "ellipsis": true,
                        "dataIndex": "tagValue",
                        "className": "pointer"
                    }
                ]
            }
        ]
    },
    "audio": {
        "title": "音频",
        "url": "audio/element/query",
        "searchCons": [
            {
                "label": "类型",
                "value": "tagType"
            },
            {
                "label": "值",
                "value": "tagValue"
            }
        ],
        "cols": [
            {
                "title": "标题",
                "dataIndex": ["tagid", "metaId", "channelName"],
                "render": (text, record) => `${text}-${record.tagid.metaId.programName}`, 
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "dataIndex": ["tagid", "metaId", "url"],
                "render": () => <img src={audioSvg} width="20" height="20" alt="音频" />,
                "className": "pointer"
            },
            {
                "title": "播出时间",
                "dataIndex": ["tagid", "metaId", "date"],
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "children": [
                    {
                        "title": "类型",
                        "ellipsis": true,
                        "width": 200,
                        "dataIndex": "tagType",
                        "className": "pointer"
                    },
                    {
                        "title": "值",
                        "ellipsis": true,
                        "width": 200,
                        "dataIndex": "tagValue",
                        "className": "pointer"
                    }
                ]
            }
        ]
    },
    "video": {
        "title": "视频",
        "url": "video/element/query",
        "searchCons": [
            {
                "label": "类型",
                "value": "tagType"
            },
            {
                "label": "值",
                "value": "tagValue"
            }
        ],
        "cols": [
            {
                "title": "标题",
                "dataIndex": ["tagid", "metaId", "location"],
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "dataIndex": ["tagid", "metaId", "url"],
                "ellipsis": true,
                "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
                "className": "pointer"
            },
            {
                "title": "拍摄时间",
                "dataIndex": ["tagid", "metaId", "date"],
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "children": [
                    {
                        "title": "类型",
                        "width": "200px",
                        "ellipsis": true,
                        "dataIndex": "tagType",
                        "className": "pointer"
                    },
                    {
                        "title": "值",
                        "width": "200px",
                        "ellipsis": true,
                        "dataIndex": "tagValue",
                        "className": "pointer"
                    }
                ]
            }
        ]
    }
}

export default ITEMS;