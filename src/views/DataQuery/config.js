import { stopPropagation } from "@/utility";
import imgSvg from "@/assets/image/data-query-image.svg";
import mapSvg from "@/assets/image/data-query-map.svg";
import chartSvg from "@/assets/image/data-query-chart.svg";
import audioSvg from "@/assets/image/data-query-audio.svg";
import videoSvg from "@/assets/image/data-query-video.svg";
const ITEMS = {
    "text": {
        "title": "文本",
        "url": "text/meta/query",
        "searchCons": [
            {
                "label": "标题",
                "value": "description"
            },
            {
                "label": "内容",
                "value": "content"
            },
            {
                "label": "来源",
                "value": "source"
            },
            {
                "label": "发布日期",
                "value": "recordDatetime"
            }
        ],
        "cols": [
            {
                title: "标题",
                dataIndex: 'description',
                width: "200px",
                ellipsis: true,
                className: "pointer",
            },{
                title: "内容",
                dataIndex: "content",
                ellipsis: {
                    showTitle: false
                },
                className: "pointer"
                //render: (text, record, index) => (<span style={{cursor:"pointer"}} onClick={() => this.showContent(text, record, index) }>{text}</span>)
            },{
                title: '来源',
                dataIndex: 'source',
                width: "200px",
                onCell: stopPropagation
            },{
                title: '发布日期',
                dataIndex: 'recordDatetime',
                width: "200px",
                onCell: stopPropagation
            }
        ]
    },
    "img": {
        "title": "图像",
        "url": "img/meta/query",
        "searchCons": [
            {
                "label": "标题",
                "value": "caption"
            },
            {
                "label": "描述",
                "value": "description"
            },
            {
                "label": "发布时间",
                "value": "recordDateTime"
            },
            {
                "label": "网址",
                "value": "website"
            }
        ],
        "cols": [
            {
                "title": "标题",
                "dataIndex": "caption",
                "ellipsis": true,
                "className": "pointer"
            },
            {
                "title": "内容",
                "dataIndex": "description",
                "render": () => <img src={imgSvg} width="20" heigh="20" alt="image" />,
                "ellipsis": true,
                "className": "pointer"
            },
            // {
            //     "title": "图片",
            //     "dataIndex": "imageName",
            //     "width": "200px",
            //     "ellipsis": true,
            //     "className": "pointer"
            // },
            {
                "title": "来源",
                "dataIndex": "website",
                "width": "150px",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "发布日期",
                "dataIndex": "recordDatetime",
                "width": "150px",
                "ellipsis": true,
                "onCell": stopPropagation
            }
        ]
    },
    "chart": {
        "title": "图表",
        "url": "chart/meta/query",
        "searchCons": [
            {
                "label": "标题",
                "value": "title"
            },
            {
                "label": "发布时间",
                "value": "time"
            }
        ],
        "cols": [
            {
                "title": "标题",
                "dataIndex": "description",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "dataIndex": "imagePath",
                "render": () => <img src={chartSvg} width="20" heigh="20" alt="chart" />,
                "width": "200px",
                "ellipsis": true,
                "className": "pointer"
            },
            {
                "title": "来源",
                "dataIndex": "website",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "记录时间",
                "dataIndex": "recordedTime",
                "width": "200px",
                "ellipsis": true,
                "onCell": stopPropagation
            }
            // {
            //     "title": "名称",
            //     "dataIndex": "name",
            //     "className": "pointer",
            //     "width": "250px",
            //     "ellipsis": true,
            // },
            // {
            //     "title": "来源",
            //     "dataIndex": "source",
            //     "render": text => <a href={text} target="_blank">{text}</a>,
            //     "onCell": stopPropagation
            // },
        ]
    },
    "map": {
        "title": "地图",
        "url": "map/meta/query",
        "searchCons": [
            {
                "label": "发布时间",
                "value": "publicTime"
            },
            {
                "label": "创建时间",
                "value": "crateTime"
            }
        ],
        "cols": [
            {
                "title": "标题",
                "dataIndex": "mapName",
                "ellipsis": true,
                "className": "pointer"
            },
            {
                "title": "内容",
                "dataIndex": "imagePath",
                "ellipsis": true,
                "render": () => <img src={mapSvg} alt="地图" width="20" height="20" />,
                "className": "pointer"
            },
            {
                "title": "来源",
                "dataIndex": "website",
                "onCell": stopPropagation
            },
            {
                "title": "时间",
                "dataIndex": "recordDatetime",
                "onCell": stopPropagation
            }
        ]
    },
    "audio": {
        "title": "音频",
        "url": "audio/meta/query",
        "searchCons": [
            {
                "label": "频道名",
                "value": "channelName"
            },
            {
                "label": "节目名",
                "value": "programName"
            },
            {
                "label": "播出时间",
                "value": "date"
            }
        ],
        "cols": [
            // {
            //     "title": "节目名",
            //     "dataIndex": "programName",
            //     "className": "pointer"
            // },
            // {
            //     "title": "频道名",
            //     "dataIndex": "channelName",
            //     "onCell": stopPropagation
            // },
            {
                "title": "标题",
                "dataIndex": "channelName",
                "render": (text, record) => `${record.channelName}-${record.programName}`,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "dataIndex": "url",
                "render": () => <img src={audioSvg} width="20" height="20" alt="音频" />,
                "className": "pointer"
            },
            {
                "title": "播出时间",
                "dataIndex": "date",
                "width": "150px",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "每日播出时间段",
                "dataIndex": "period",
                "width": "150px",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "频道（FM）",
                "dataIndex": "FM",
                "width": "150px",
                "ellipsis": true,
                "onCell": stopPropagation
            }
        ]
    },
    "video": {
        "title": "视频",
        "url": "video/meta/query",
        "searchCons": [
            {
                "label": "拍摄时间",
                "value": "date"
            },
            {
                "label": "摄像头位置",
                "value": "location"
            }
        ],
        "cols": [
            // {
            //     "title": "摄像头位置",
            //     "dataIndex": "location",
            //     "ellipsis": true,
            //     "className": "pointer"
            // },
            {
                "title": "标题",
                "dataIndex": "location",
                "ellipsis": true,
                "className": "pointer"
            },
            {
                "title": "内容",
                "dataIndex": "url",
                "ellipsis": true,
                "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
                "className": "pointer"
            },
            {
                "title": "拍摄时间",
                "dataIndex": "date",
                //"width": "150px",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            // {
            //     "title": "来源网址",
            //     "dataIndex": "source",
            //     "render": text => <a href={text} target="_blank">{text}</a>,
            //     "ellipsis": true,
            //     "onCell": stopPropagation
            // }
        ]
    }
};

export default ITEMS;
