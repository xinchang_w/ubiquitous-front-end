import React, { Component, Fragment } from 'react';
import { Button, Input, Select, Modal, Breadcrumb, Radio } from "antd";
import { NODE_PATH, FILE_SERVER } from "@/assets/global/index";
import { Paragraph, CustomizeModal } from "@/components";
import { TableRendering } from "@/components";
import ITEMS from "./config";

class DataQuery extends Component{
    constructor(props){
        super(props);
        this.state = {
            "curItem": "text",
            "curSearchCon": "description",
            "keywords": "",
            "modalData": null
        };

        this.defaultUrl = `${NODE_PATH}/${ITEMS["text"].url}`;
        this.defaultCols = ITEMS["text"].cols;

        this.tableRendering = React.createRef();
        this.onItemChange = this.onItemChange.bind(this);
        this.onConChange = this.onConChange.bind(this);
        this.onKeywordsChange = this.onKeywordsChange.bind(this);
        this.showContent = this.showContent.bind(this);
        this.search = this.search.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }

    render(){
        let { curItem, curSearchCon, modalData, keywords } = this.state; 
        let width = 800;
        switch(curItem){
            case "text":
            case "image":
                width = 800;
                break;
            case "audio":
                width = 550;
                break;
            case "video":
                width = 550;
                break;
            default:
                break;
        }

        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>数据资源</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">数据查询</Breadcrumb.Item>
                </Breadcrumb>
                <Radio.Group onChange={this.onItemChange} style={{margin:10}} value={curItem}>
                    {
                        Object.keys(ITEMS).map(i => (<Radio value={i} key={i}>{ITEMS[i].title}</Radio>))
                    }
                </Radio.Group>
                <div>
                    <div className="searchBox">
                        <label className="searchBox-label">查询条件：</label>
                        <Select
                            placeholder="类型"
                            className="searchBox-input"
                            value={curSearchCon}
                            onChange={this.onConChange}
                            options={ITEMS[curItem].searchCons}
                        />
                        <label className="searchBox-label">关键词：</label>
                        <Input
                            className="searchBox-input"
                            placeholder="关键词"
                            value={keywords}
                            onChange={this.onKeywordsChange}
                        />
                        <Button
                            type="primary"
                            className="searchBox-btn"
                            onClick={() => this.search()}
                        >
                            查询
                        </Button>
                    </div>
                    <TableRendering cols={this.defaultCols} url={this.defaultUrl} ref={this.tableRendering} clickRow={this.showContent} />
                    <CustomizeModal
                        visible={modalData !== null}
                        title={modalData && (modalData.caption || modalData.description || modalData.programName || modalData.location || modalData.title || "地图")}
                        close={this.hideModal}
                        width={width}
                    >
                        <Paragraphs curItem={curItem} {...modalData} />
                    </CustomizeModal>
                </div>
            </div>
        );
    }

    onItemChange(e){
        let val = e.target.value;
        this.setState({
            "curItem": val,
            "curSearchCon": ITEMS[val].searchCons[0].value,
            "keywords": ""
        }, this.search);
        
    }

    onConChange(value){
        this.setState({
            "curSearchCon": value
        });
    }

    onKeywordsChange(e){
        this.setState({
            "keywords": e.target.value
        })
    }

    search(){
        let con = {};
        let { keywords, curSearchCon, curItem } = this.state;
        let url = `${NODE_PATH}/${ITEMS[curItem].url}`;
        let columns = ITEMS[curItem].cols;
        if(curSearchCon && keywords){
            con[curSearchCon] = keywords;
        }
        
        this.tableRendering.current.search(url, columns, con);
    }

    showContent(record){
        this.setState({
            "modalData": record
        });
    }

    hideModal(){
        this.setState({
            "modalData": null
        });
    }
};

function Paragraphs(props){
    if(props.curItem === "text"){
        return (
            <Fragment>
                <Paragraph title="来源：" content={props.source} />
                <Paragraph title="时间：" content={props.recordDatetime} />
                <Paragraph title="摘要：" content={props.description} />
                <Paragraph title="内容：" content={props.content} />
            </Fragment>
        );
    }

    if(props.curItem === "img"){
        return (
            <Fragment>
                <Paragraph title="发布时间：" content={props.recordDatetime} />
                <Paragraph title="来源：" content={props.website} />
                <Paragraph title="介绍：" content={props.description} />
                <Paragraph title="图片：" image={props.imageName && `${FILE_SERVER}/images/metaData_Img/${props.imageName}`} />
            </Fragment>
        );
    }

    if(props.curItem === "audio"){
        return (
            <Fragment>
                <Paragraph title="节目名：" content={props.programName} />
                <Paragraph title="频道名：" content={props.channelName} />
                <Paragraph title="播出时间：" content={props.date} />
                <Paragraph title="每日播出时间段：" content={props.period} />
                <Paragraph title="频道（FM）：" content={props.FM} />
                <Paragraph title="音频：" audio={props.url && `${FILE_SERVER}/audio/${props.url}`} />
            </Fragment>
        );
    }

    if(props.curItem === "video"){
        return (
            <Fragment>
                <Paragraph title="拍摄时间：" content={props.date} />
                <Paragraph title="摄像头位置：" content={props.location} />
                <Paragraph title="视频格式：" content={props.suffix} />
                <Paragraph title="视频文件大小：" content={props.size} />
                <Paragraph title="视频：" video={props.url && `${FILE_SERVER}/${props.url}`} />
            </Fragment>
        );
    }

    if(props.curItem === "chart"){
        return (
            <Fragment>
                <Paragraph title="发布时间：" content={props.recordedTime} />
                <Paragraph title="文件格式：" content={props.format} />
                <Paragraph title="来源：" content={<a href={props.source} target="_blank">{props.source}</a>} />
                <Paragraph title="图片：" image={props.imagePath && `${FILE_SERVER}/${props.imagePath}`} />
            </Fragment>
        );
    }

    if(props.curItem === "map"){
        return (
            <Fragment>
                {/* <Paragraph title="标题：" content={props.mapName} /> */}
                <Paragraph title="发布时间：" content={props.recordDatetime} />
                <Paragraph title="来源：" content={props.website} />
                <Paragraph title="网址：" content={<a href={props.source} target="_blank">{props.source}</a>} />
                <Paragraph title="图片：" image={props.imagePath && `${FILE_SERVER}/map/${props.imagePath}`} />
            </Fragment>
        );
    }

    return null;
}

export default DataQuery;

