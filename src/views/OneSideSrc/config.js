import { Title, colorMap_text, colorMap_image, colorMap_map, colorMap_chart, colorMap_audio, colorMap_video } from "@/components/Title";
import { Tag } from "antd";
import { stopPropagation } from "@/utility";
import mapSvg from "@/assets/image/data-query-map.svg";
import chartSvg from "@/assets/image/data-query-chart.svg";
import audioSvg from "@/assets/image/data-query-audio.svg";
import videoSvg from "@/assets/image/data-query-video.svg";
const ITEMS = {
    "text": {
        "title": "文本",
        "tag": {
            "title": "标签",
            "typeTag": {
                "title": "类型标签",
                "url": "text/typeTag/query",
                "searchCons": [
                    {
                        "label": "体裁",
                        "value": "体裁"
                    },
                    {
                        "label": "功能",
                        "value": "功能"
                    },
                    {
                        "label": "规范性",
                        "value": "规范性"
                    },
                    {
                        "label": "语种",
                        "value": "语种"
                    },
                    {
                        "label": "邻域",
                        "value": "邻域"
                    }
                ],
                "cols": [
                    {
                        "title": <Title type="text" />,
                        "dataIndex": "categoryList",
                        "render": list => { return (
                            <span>
                                {
                                    Object.keys(list).map((item, ind) => {
                                        return <Tag color={colorMap_text.get(item)} key={ind}>{list[item]}</Tag>
                                    })
                                }
                            </span>
                        )}
                    }
                ]
            },
            "structureTag": {
                "title": "结构标签",
                "url": "text/structureTag/query",
                "searchCons": [
                    {
                        "label": "标签类型",
                        "value": "tagType"
                    },
                    {
                        "label": "标签值",
                        "value": "tagValue"
                    }
                ],
                "cols": [
                    {
                        "title": "类型",
                        "dataIndex": "tagType",
                        "align": "center"
                    },{
                        "title": "值",
                        "dataIndex": "tagValue",
                        "align": "center",
                        "ellipsis": true
                    }
                ]
            },
            "contentTag": {
                "title": "内容标签",
                "url": "text/contentTag/query",
                "searchCons": [
                    {
                        "label": "标签类型",
                        "value": "tagType"
                    },
                    {
                        "label": "标签值",
                        "value": "tagValue"
                    },
                    {
                        "label": "标签名称",
                        "value": "tagName"
                    }
                ],
                "cols": [
                    {
                        "title": "类型",
                        "dataIndex": "tagType",
                        "align": "center"
                    },{
                        "title": "名称",
                        "dataIndex": "tagName",
                        "align": "center"
                    },{
                        "title": "值",
                        "dataIndex": "tagValue",
                        "align": "center"
                    }
                ]
            }
        },
        "element": {
            "title": "内容",
            "url": "text/element/query",
            "searchCons": [
                {
                    "label": "类型",
                    "value": "type"
                },
                {
                    "label": "语义",
                    "value": "feature"
                },
                {
                    "label": "值",
                    "value": "featureValue"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "key": "type",
                    "dataIndex": ["value", "type"],
                    "align": "center"
                },{
                    "title": "语义",
                    "key": "feature",
                    "dataIndex": ["value", "feature"],
                    "align": "center"
                },{
                    "title": "值",
                    "key": "featureValue",
                    "dataIndex": ["value", "featureValue"],
                    "align": "center"
                }
            ]
        }
    },
    "image": {
        "title": "图像",
        "tag": {
            "title": "标签",
            "typeTag": {
                "title": "类型标签",
                "url": "img/typeTag/query",
                "searchCons": [
                    {
                        label: "应用",
                        value: "应用"
                    },
                    {
                        label: "图片格式",
                        value: "储存格式"
                    },
                    {
                        label: "波段数",
                        value: "波段数"
                    }
                ],
                "cols": [
                    {
                        "title": <Title type="image" />,
                        "dataIndex": "categoryList",
                        "render": list => { return (
                            <span>
                                {
                                    Object.keys(list).map((item, ind) => {
                                        if(list[item] === "FALSE"){
                                            return null;
                                        }
                                        return <Tag color={colorMap_image.get(item)} key={ind}>{list[item]}</Tag>
                                    })
                                }
                            </span>
                        )}
                    }
                ]
            },
            "structureTag": {
                "title": "结构标签",
                "url": "img/structureTag/query",
                "searchCons": [
                    {
                        "label": "标签类型",
                        "value": "tagType"
                    },
                    {
                        "label": "标签值",
                        "value": "tagValue"
                    }
                ],
                "cols": [
                    {
                        title: "类型",
                        dataIndex: "tagType",
                        align: "center"
                    },
                    {
                        title: "值",
                        dataIndex: "tagValue",
                        align: "center",
                        ellipsis: true
                    }
                ]
            },
            "contentTag": {
                "title": "内容标签",
                "url": "img/contentTag/query",
                "searchCons": [
                    {
                        "label": "标签名",
                        "value": "tagName"
                    },
                    {
                        "label": "标签值",
                        "value": "tagValue"
                    }
                ],
                "cols": [
                    {
                        title: "名称",
                        dataIndex: "tagName",
                        align: "center",
                        onCell: stopPropagation
                    },
                    {
                        title: "值",
                        dataIndex: "tagValue",
                        align: "center",
                        ellipsis: true
                    }
                ]
            }
        },
        "element": {
            "title": "内容",
            "url": "img/element/query",
            "searchCons": [
                {
                    "label": "内容类型",
                    "value": "tagName"
                },
                {
                    "label": "值",
                    "value": "tag"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "dataIndex": "tagName",
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "值",
                    "dataIndex": "tag",
                    "ellipsis": true,
                    "className": "pointer"
                }
            ]
        }
    },
    "chart": {
        "title": "图表",
        "tag": {
            "title": "标签",
            "typeTag": {
                "title": "类型标签",
                "url": "chart/typeTag/query",
                "searchCons": [
                    {
                        "label": "专题",
                        "value": "专题"
                    }
                ],
                "cols": [
                    {
                        "title": <Title type="chart" />,
                        "dataIndex": "categoryList",
                        "render": list => (
                            <span>
                                {
                                    Object.keys(list).map((item, ind) => {
                                        if (list[item] === "FALSE") {
                                            return null;
                                        }
                                        return <Tag color={colorMap_chart.get(item)} key={ind}>{list[item]}</Tag>
                                    })
                                }
                            </span>
                        )
                    }
                ]
            },
            "structureTag": {
                "title": "结构标签",
                "url": "chart/structureTag/query",
                "searchCons": [
                    {
                        "label": "标签类型",
                        "value": "tagType"
                    },
                    {
                        "label": "标签值",
                        "value": "tagValue"
                    }
                ],
                "cols": [
                    {
                        "title": "类型",
                        "dataIndex": "tagType",
                        "ellipsis": true
                    },
                    {
                        "title": "值",
                        "dataIndex": "tagValue",
                        "ellipsis": true
                    }
                ]
            },
            "contentTag": {
                "title": "内容标签",
                "url": "chart/contentTag/query",
                "searchCons": [
                    {
                        "label": "标签类型",
                        "value": "tagType"
                    },
                    {
                        "label": "标签值",
                        "value": "tagValue"
                    }
                ],
                "cols": [
                    {
                        "title": "类型",
                        "dataIndex": "tagType",
                        "ellipsis": true
                    },
                    {
                        "title": "值",
                        "dataIndex": "tagValue",
                        "ellipsis": true
                    }
                ]
            }
        },
        "element": {
            "title": "内容",
            "url": "chart/element/query",
            "searchCons": [
                {
                    "label": "类型",
                    "value": "tagType"
                },
                {
                    "label": "值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "width": "200px",
                    "ellipsis": true,
                    "dataIndex": "tagType",
                    "className": "pointer"
                },
                {
                    "title": "值",
                    "width": "200px",
                    "ellipsis": true,
                    "dataIndex": "tagValue",
                    "className": "pointer"
                }
            ]
        }
    },
    "map": {
        "title": "地图",
        "tag": {
            "title": "标签",
            "typeTag": {
                "title": "类型标签",
                "url": "map/typeTag/query",
                "searchCons": [
                    {
                        "label": "专题",
                        "value": "专题"
                    }
                ],
                "cols": [
                    {
                        "title": <Title type="map" />,
                        "dataIndex": "categoryList",
                        "render": list => (
                            <span>
                                {
                                    Object.keys(list).map((item, ind) => {
                                        if(list[item] === "FALSE"){
                                            return null;
                                        }
                                        return <Tag color={colorMap_map.get(item)} key={ind}>{list[item]}</Tag>
                                    })
                                }
                            </span>
                        ),
                        "className": "pointer"
                    }
                ]
            },
            "structureTag": {
                "title": "结构标签",
                "url": "map/structureTag/query",
                "searchCons": [
                    {
                        "label": "标签类型",
                        "value": "tagType"
                    },
                    {
                        "label": "标签值",
                        "value": "tagValue"
                    }
                ],
                "cols": [
                    {
                        "title": "标签类型",
                        "dataIndex": "tagType",
                        "ellipsis": true,
                        "className": "pointer"
                    },
                    {
                        "title": "标签值",
                        "dataIndex": "tagValue",
                        "ellipsis": true,
                        "className": "pointer"
                    }
                ]
            },
            "contentTag": {
                "title": "内容标签",
                "url": "map/contentTag/query",
                "searchCons": [
                    {
                        "label": "标签类型",
                        "value": "tagType"
                    },
                    {
                        "label": "标签值",
                        "value": "tagName"
                    }
                ],
                "cols": [
                    {
                        "title": "标签类型",
                        "dataIndex": "tagType",
                        "ellipsis": true,
                        "className": "pointer"
                    },
                    {
                        "title": "标签值",
                        "dataIndex": "tagName",
                        "ellipsis": true,
                        "className": "pointer"
                    }
                ]
            }
        },
        "element": {
            "title": "内容",
            "url": "map/element/query",
            "searchCons": [
                {
                    "label": "类型",
                    "value": "tagType"
                },
                {
                    "label": "值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "ellipsis": true,
                    "dataIndex": "tagType"
                },
                {
                    "title": "值",
                    "ellipsis": true,
                    "dataIndex": "tagValue"
                }
            ]
        }
    },
    "audio": {
        "title": "音频",
        "tag": {
            "title": "标签",
            "typeTag": {
                "title": "类型标签",
                "url": "audio/typeTag/query",
                "searchCons": [
                    {
                        "label": "专题",
                        "value": "专题"
                    },
                    {
                        "label": "数据格式",
                        "value": "数据格式"
                    }
                ],
                "cols": [
                    {
                        "title": <Title type="audio" />,
                        "dataIndex": "categoryList",
                        "render": list => (
                            <span>
                                {
                                    Object.keys(list).map((item, ind) => {
                                        if(list[item] === "FALSE"){
                                            return null;
                                        }
                                        return <Tag color={colorMap_audio.get(item)} key={ind}>{list[item]}</Tag>
                                    })
                                }
                            </span>
                        )
                    }
                ]
            },
            "structureTag": {
                "title": "结构标签",
                "url": "audio/structureTag/query",
                "searchCons": [
                    {
                        "label": "时长",
                        "value": "duration"
                    }
                ],
                "cols": [
                    {
                        "title": "时长",
                        "dataIndex": "duration",
                        "width": "150px",
                        "ellipsis": true,
                        "className": "pointer"
                    },
                    {
                        "title": "文件大小",
                        "dataIndex": "fileSize",
                        "width": "150px",
                        "ellipsis": true,
                        "className": "pointer"
                    },
                    {
                        "title": "比特率",
                        "dataIndex": "bitRate",
                        "width": "150px",
                        "ellipsis": true,
                        "className": "pointer"
                    }
                ]
            },
            "contentTag": {
                "title": "内容标签",
                "url": "audio/contentTag/query",
                "searchCons": [
                    {
                        "label": "标签类型",
                        "value": "tagType"
                    },
                    {
                        "label": "标签值",
                        "value": "tagValue"
                    }
                ],
                "cols": [
                    {
                        "title": "类型",
                        "dataIndex": "tagType",
                        "ellipsis": true,
                        "className": "pointer"
                    },
                    {
                        "title": "值",
                        "dataIndex": "tagValue",
                        "ellipsis": true,
                        "className": "pointer"
                    }
                ]
            }
        },
        "element": {
            "title": "内容",
            "url": "audio/element/query",
            "searchCons": [
                {
                    "label": "类型",
                    "value": "tagType"
                },
                {
                    "label": "值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "ellipsis": true,
                    "dataIndex": "tagType",
                    "className": "pointer"
                },
                {
                    "title": "值",
                    "ellipsis": true,
                    "dataIndex": "tagValue",
                    "className": "pointer"
                }
            ]
        }
    },
    "video": {
        "title": "视频",
        "tag": {
            "title": "标签",
            "typeTag": {
                "title": "类型标签",
                "url": "video/typeTag/query",
                "searchCons": [
                    {
                        "label": "视频编码",
                        "value": "视频编码"
                    },
                    {
                        "label": "专题",
                        "value": "专题"
                    },
                    {
                        "label": "数据格式",
                        "value": "数据格式"
                    }
                ],
                "cols": [
                    {
                        "title": <Title type="video" />,
                        "dataIndex": "categoryList",
                        "render": list => (
                            <span>
                                {
                                    Object.keys(list).map((item, ind) => {
                                        if(list[item] === "FALSE"){
                                            return null;
                                        }
                                        return <Tag color={colorMap_video.get(item)} key={ind}>{list[item]}</Tag>
                                    })
                                }
                            </span>
                        )
                    }
                ]
            },
            "structureTag": {
                "title": "结构标签",
                "url": "video/structureTag/query",
                "searchCons": [
                    {
                        "label": "分辨率",
                        "value": "resolution"
                    }
                ],
                "cols": [
                    {
                        "title": "时长",
                        "dataIndex": "duration",
                        "ellipsis": true,
                        "className": "pointer"
                    },
                    {
                        "title": "分辨率",
                        "dataIndex": "resolutionWidth",
                        "ellipsis": true,
                        "render": (text, record) => `${record.resolutionWidth}*${record.resolutionHeight}`,
                        "className": "pointer"
                    }
                ]
            },
            "contentTag": {
                "title": "内容标签",
                "url": "video/contentTag/query",
                "searchCons": [
                    {
                        "label": "标签类型",
                        "value": "tagType"
                    }
                ],
                "cols": [
                    {
                        "title": "类型",
                        "dataIndex": "tagType",
                        "ellipsis": true,
                        "className": "pointer"
                    },
                    {
                        "title": "值",
                        "dataIndex": "url",
                        "ellipsis": true,
                        "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
                        "className": "pointer"
                    }
                ]
            }
        },
        "element": {
            "title": "内容",
            "url": "video/element/query",
            "searchCons": [
                {
                    "label": "类型",
                    "value": "tagType"
                },
                {
                    "label": "值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "类型",
                    "ellipsis": true,
                    "dataIndex": "tagType",
                    "className": "pointer"
                },
                {
                    "title": "值",
                    "ellipsis": true,
                    "dataIndex": "tagValue",
                    "className": "pointer"
                }
            ]
        }
    }
};

const METAS = {
    "text": {
        "meta": {
            "title": "原数据",
            "url": "text/meta/query",
            "searchCons": [
                {
                    "label": "摘要",
                    "value": "description"
                },
                {
                    "label": "内容",
                    "value": "content"
                },
                {
                    "label": "来源",
                    "value": "source"
                },
                {
                    "label": "发布日期",
                    "value": "recordDatetime"
                }
            ],
            "cols": [
                {
                    "title": '来源',
                    "dataIndex": 'source',
                    "width": "200px"
                },
                {
                    "title": "摘要",
                    "dataIndex": 'description',
                    "width": "200px",
                    "ellipsis": true
                }
            ]
        }
    },
    "image": {
        "meta": {
            "title": "原数据",
            "url": "img/meta/query",
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": "caption",
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "内容",
                    "dataIndex": "description",
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "来源",
                    "dataIndex": "website",
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "发布日期",
                    "dataIndex": "recordDatetime",
                    "ellipsis": true,
                    "className": "pointer"
                }
            ]
        }
    },
    "chart": {
        "meta": {
            "title": "图表",
            "url": "chart/meta/query",
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": "description",
                    "ellipsis": true
                },
                {
                    "title": "内容",
                    "dataIndex": "imagePath",
                    "render": () => <img src={chartSvg} width="20" heigh="20" alt="chart" />,
                    "width": "200px",
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "来源",
                    "dataIndex": "website",
                    "ellipsis": true
                }
            ]
        }
    },
    "map": {
        "meta": {
            "title": "原数据",
            "url": "map/meta/query",
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": "mapName",
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "内容",
                    "dataIndex": "imagePath",
                    "ellipsis": true,
                    "render": () => <img src={mapSvg} alt="地图" width="20" height="20" />,
                    "className": "pointer"
                },
                {
                    "title": "来源",
                    "dataIndex": "website",
                    "onCell": stopPropagation
                }
            ]
        }
    },
    "audio": {
        "meta": {
            "title": "原数据",
            "url": "audio/meta/query",
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": "channelName",
                    "render": (text, record) => `${record.channelName}-${record.programName}`, 
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": "url",
                    "render": () => <img src={audioSvg} width="20" height="20" alt="音频" />,
                    "className": "pointer"
                },
                {
                    "title": "播出时间",
                    "dataIndex": "date",
                    "width": "150px",
                    "ellipsis": true,
                    "onCell": stopPropagation
                }
            ]
        }
    },
    "video": {
        "meta": {
            "title": "原数据",
            "url": "video/meta/query",
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": "location",
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "内容",
                    "dataIndex": "url",
                    "ellipsis": true,
                    "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
                    "className": "pointer"
                },
                {
                    "title": "拍摄时间",
                    "dataIndex": "date",
                    "ellipsis": true,
                    "onCell": stopPropagation
                }
            ]
        }

    }
}

export { ITEMS, METAS };