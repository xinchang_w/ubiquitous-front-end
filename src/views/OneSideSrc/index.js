import React, { Component } from "react";
import { Breadcrumb, Row, Col } from "antd";
import Left from "./left";
import Right from "./right";

class OneSideSrc extends Component{
    constructor(props){
        super(props);
    
        this.leftCom = React.createRef();
        this.rightCom = React.createRef();

        this.trace = this.trace.bind(this);
        this.clearTable = this.clearTable.bind(this);
        this.continueTrace = this.continueTrace.bind(this);
    }

    render(){
        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>信息溯源</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">单向溯源</Breadcrumb.Item>
                </Breadcrumb>
                <Row>
                    <Col span={11}>
                        <Left ref={this.leftCom} trace={this.trace} clearTable={this.clearTable} />
                    </Col>
                    <Col span={2}>
                    </Col>
                    <Col span={11}>
                        <Right ref={this.rightCom} continueTrace={ this.continueTrace } />
                    </Col>
                </Row>
            </div>
        );
    }

    //溯源
    trace(item, category, tag, data){
        if(category === "tag"){
            this.rightCom.current.traceToMeta(item, category, tag, data);
        }
        else{
            this.rightCom.current.traceToTag(item, category, data);
        }
    }

    //清空表格
    clearTable(item, category, tag){
        this.rightCom.current.clear(item, category, tag);
    }

    //继续溯源
    continueTrace(record){
        this.leftCom.current.continueTrace(record);
    }
}

export default OneSideSrc;