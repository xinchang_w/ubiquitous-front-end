import React, { Component, Fragment } from "react";
import { Button } from "antd";
import { TableRendering, CustomizeModal, Paragraph } from "@/components";
import { ITEMS, METAS } from "./config";
import { NODE_PATH, FILE_SERVER } from "@/assets/global";
import { stopPropagation } from "../../utility";
import style from "./right.module.css";

/*
* @param continueTrace {Function} 继续溯源函数
*/
class Right extends Component{
    constructor(props){
        super(props);
        this.state = {
            "item": "",
            "title": "原数据",
            "source": "meta",
            "modalData": null
        };

        this.defaultCols = METAS["text"].meta.cols;

        this.tableRef = React.createRef();
        this.clickRow = this.clickRow.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    render(){
        const { title, source, modalData, item } = this.state;
        
        return (
            <div className={style["wrapper"]}>
                <h5 className={style["wrapper-h5"]}>{ title }</h5>
                <TableRendering ref={this.tableRef} cols={this.defaultCols} clickRow={this.clickRow}  />
                <CustomizeModal
                    visible={ Boolean(modalData) } 
                    title={ title } 
                    close={ this.closeModal }
                    width={ source === "meta" ? 800 : item === "video" ? 550 : 450 }
                >
                    <Paragraphs type={`${item}_${source}`} {...modalData} />
                </CustomizeModal>
            </div>
        ); 
    }

    //标签溯源到原数据
    traceToMeta(item, category, tag, data){
        let url = `${NODE_PATH}/${METAS[item].meta.url}`;
        let cols = METAS[item].meta.cols;
        this.tableRef.current.search(url, cols, {
            "id": data.metaId._id
        });
        this.setState({
            "item": item,
            "title": METAS[item].meta.title,
            "source": "meta"
        });
    }

    //“内容”溯源到“内容标签”
    traceToTag(item, category, data){
        //目前所有的“内容”都溯源到“内容标签”
        let url = `${NODE_PATH}/${ITEMS[item].tag.contentTag.url}`;
        let cols = ITEMS[item].tag.contentTag.cols;
        cols = cols.concat([{
            width: 100,
            title: "操作",
            render: (text, record) => (
                <Button
                    size="small"
                    type="primary"
                    onClick={this.continueTrace.bind(this, record, item)}
                >
                    继续溯源
                </Button>
            ),
            "onCell": stopPropagation
        }]);
        
        this.tableRef.current.search(url, cols, {
            "id": data.tagid._id
        });
        this.setState({
            "item": item,
            "title": ITEMS[item].tag.contentTag.title,
            "source": "contentTag"
        });
    }

    //继续溯源
    continueTrace(record, item){
        this.setState({
            "item": item,
            "title": METAS[item].meta.title
        });
        this.tableRef.current.clearTable(METAS[item].meta.cols);
        this.props.continueTrace(record);
    }

    //清空
    clear(item, category, tag){
        let cols, title;
        if(category === "tag"){
            cols = METAS[item].meta.cols;
            title = METAS[item].meta.title;
        }
        else{
            cols = ITEMS[item].tag.contentTag.cols;
            title = ITEMS[item].tag.contentTag.title;
        }
        this.setState({ title });
        this.tableRef.current.clearTable(cols);
    }

    clickRow(record){
        this.setState({
            "modalData": record
        });
    }

    closeModal(){
        this.setState({
            "modalData": null
        });
    }
}

function Paragraphs(props){
    let com = null;
    switch(props.type){
        case "text_meta":
            com = (
                <Fragment>
                    <Paragraph title={"来源："} content={props.source} />
                    <Paragraph title={"时间："} content={props.recordDatetime} />
                    <Paragraph title={"摘要："} content={props.description} />
                    <Paragraph title={"内容："} content={props.content} />
                </Fragment>
            );
            break;
        case "text_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="标签名：" content={props.tagName} />
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签值：" content={props.tagValue} />
                </Fragment>
            );
            break;
        case "image_meta":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.recordDatetime} />
                    <Paragraph title="来源：" content={props.website} />
                    <Paragraph title="介绍：" content={props.description} />
                    <Paragraph title="图片：" image={props.imageName && `${FILE_SERVER}/images/metaData_Img/${props.imageName}`} />
                </Fragment>
            );
            break;
        case "image_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="标签名：" content={props.tagName} />
                    <Paragraph title="标签值：" content={props.tagValue} />
                </Fragment>
            );
            break;
        case "chart_meta":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.recordedTime} />
                    <Paragraph title="文件格式：" content={props.format} />
                    <Paragraph title="来源：" content={<a href={props.source} target="_blank">{props.source}</a>} />
                    <Paragraph title="图片：" image={props.imagePath && `${FILE_SERVER}/${props.imagePath}`} />
                </Fragment>
            );
            break;
        case "chart_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签值：" content={props.tagValue} />
                </Fragment>
            );
            break;
        case "map_meta":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.recordDatetime} />
                    <Paragraph title="来源：" content={props.website} />
                    <Paragraph title="网址：" content={<a href={props.source} target="_blank">{props.source}</a>} />
                    <Paragraph title="图片：" image={props.imagePath && `${FILE_SERVER}/map/${props.imagePath}`} />
                </Fragment>
            );
            break;
        case "map_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="标签类型：" content={props.tagName} />
                    <Paragraph title="标签值：" content={props.tagType} />
                </Fragment>
            );
            break;
        case "audio_meta":
            com = (
                <Fragment>
                    <Paragraph title="节目名：" content={props.programName} />
                    <Paragraph title="频道名：" content={props.channelName} />
                    <Paragraph title="播出时间：" content={props.date} />
                    <Paragraph title="每日播出时间段：" content={props.period} />
                    <Paragraph title="频道（FM）：" content={props.FM} />
                    <Paragraph title="音频：" audio={props.url && `${FILE_SERVER}/audio/${props.url}`} />
                </Fragment>
            );
            break;
        case "audio_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签值：" content={props.tagValue} />
                </Fragment>
            );
            break;
        case "video_meta":
            com = (
                <Fragment>
                    <Paragraph title="拍摄时间：" content={props.date} />
                    <Paragraph title="摄像头位置：" content={props.location} />
                    <Paragraph title="视频格式：" content={props.suffix} />
                    <Paragraph title="视频文件大小：" content={props.size} />
                    <Paragraph title="视频：" video={props.url && `${FILE_SERVER}/${props.url}`} />
                </Fragment>
            );
            break;
        case "video_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签内容：" video={props.url && `${FILE_SERVER}/video/contentTag/${props.url}`} />
                </Fragment>
            );
            break;
        default:
            break;
    }

    return com;
}

export default Right;