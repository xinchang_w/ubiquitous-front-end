import React, { Component } from "react";
import { Radio, Select, Input, Button } from "antd";
import { TableRendering } from "@/components";
import { ITEMS } from "./config";
import { NODE_PATH } from "@/assets/global";
import style from "./left.module.css";

/*
@param trace {Function} 溯源函数
@param clearTable {Function} 当item, category, tag变化时，清空右侧表格
*/

const { Option } = Select;
class Left extends Component{
    constructor(props){
        super(props);
        this.state = {
            "curItem": "text",
            "curCategory": "tag",
            "curTag": "typeTag",
            "curSearchCon": "",
            "keywords": ""
        };

        this.tableRef = React.createRef();
        this.defaultCols = ITEMS.text.tag.typeTag.cols;
        this.defaultUrl = `${NODE_PATH}/${ITEMS.text.tag.typeTag.url}`;

        this.onItemChange = this.onItemChange.bind(this);
        this.onCategoryChange = this.onCategoryChange.bind(this);
        this.onTagChange = this.onTagChange.bind(this);
        this.onSearchConChange = this.onSearchConChange.bind(this);
        this.onKeywordsChange = this.onKeywordsChange.bind(this);
        this.search = this.search.bind(this);
        this.selectRow = this.selectRow.bind(this);
    }

    render(){
        let { curItem, curCategory, curSearchCon, curTag, keywords } = this.state;
        let item = ITEMS[curItem];
        let searchCons = curCategory === "tag" ? item[curCategory][curTag].searchCons : item[curCategory].searchCons;
        
        return (
            <div className={style["left"]}>
                <div>
                    <Radio.Group value={curItem} onChange={this.onItemChange} style={{margin:10}}>
                        {
                            Object.keys(ITEMS).map(item => <Radio key={item} value={item}>{ITEMS[item].title}</Radio>)
                        }
                    </Radio.Group>
                    <br />
                    <Radio.Group value={curCategory} onChange={this.onCategoryChange} style={{margin:10}}>
                        {
                            Object.keys(item).map(i => i !== "title" && <Radio key={i} value={i}>{item[i].title}</Radio>)
                        }
                    </Radio.Group>
                </div>
                <div className={style["formBox"]}>
                    <label>查询条件：</label>
                    <Select value={curSearchCon} className={style["formBox-select"]} onChange={this.onSearchConChange} options={[{"label": "请选择", "value": ""}].concat(searchCons)}>
                        {/* {
                            Object.keys(searchCons).map(item => <Option key={item} value={item}>{searchCons[item]}</Option>)
                        } */}
                    </Select>
                    <label>关键词：</label>
                    <Input className={style["formBox-input"]} value={keywords} onChange={this.onKeywordsChange} />
                    <Button type="primary" onClick={this.search}>查询</Button>
                </div>
                <div style={{"float":"left"}}>
                    {
                        curCategory === "tag" &&
                        <Select value={curTag} className={style["formBox-select"]} onChange={this.onTagChange}>
                            {
                                Object.keys(item.tag).map(t => t !== "title" && <Option key={t} value={t}>{item.tag[t].title}</Option>)
                            }
                        </Select>
                    }
                </div>
                <TableRendering ref={this.tableRef} cols={this.defaultCols} url={this.defaultUrl} selectRow={this.selectRow} />
            </div>
        );
    }

    onItemChange(e){
        this.setState({
            "curItem": e.target.value
        }, this.search);
        this.props.clearTable(e.target.value, this.state.curCategory, this.state.curTag);
    }

    onCategoryChange(e){
        this.setState({
            "curCategory": e.target.value,
            "curSearchCon": ""
        }, this.search);
        this.props.clearTable(this.state.curItem, e.target.value, this.state.curTag);
    }

    onTagChange(val){
        this.setState({
            "curTag": val,
            "curSearchCon": ""
        }, this.search);
        this.props.clearTable(this.state.curItem, this.state.curCategory, val);
    }

    onSearchConChange(val){
        this.setState({
            "curSearchCon": val
        });
    }

    onKeywordsChange(e){
        this.setState({
            "keywords": e.target.value
        });
    }

    search(){
        let { curItem, curCategory, curTag, curSearchCon, keywords } = this.state;
        let item = ITEMS[curItem];
        let cols, url;
        let queryCon = {};
        if(curCategory === "tag"){
            cols = item.tag[curTag].cols;
            url = `${NODE_PATH}/${item.tag[curTag].url}`;
        }
        else{
            cols = item[curCategory].cols;
            url = `${NODE_PATH}/${item[curCategory].url}`;
        }
        if(curSearchCon && keywords){
            queryCon[curSearchCon] = keywords;
        }
        
        this.tableRef.current.search(url, cols, queryCon);
    }

    selectRow(record){
        let { curItem, curCategory, curTag } = this.state;
        this.props.trace(curItem, curCategory, curTag, record);
    }

    continueTrace(record){
        this.setState({
            "curCategory": "tag",
            "curTag": "contentTag"
        });
        let contentTag = ITEMS[this.state.curItem].tag.contentTag;

        this.tableRef.current.search(`${NODE_PATH}/${contentTag.url}`, contentTag.cols, {
            id: record._id
        });
    }
}

export default Left;