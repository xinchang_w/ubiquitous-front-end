import React, { Component } from "react";
import { Col, Row, Card, Empty } from 'antd'
import { IFRAME } from "../../assets/global";

import '@/assets/style/home.css'
import textIcon from '@/assets/image/文本.png'
import videoIcon from '@/assets/image/视频.png'
import soundIcon from '@/assets/image/音频.png'
import chartIcon from '@/assets/image/图表.png'
import imageIcon from '@/assets/image/图片.png'
import mapIcon from '@/assets/image/地图.png'

const { Meta } = Card;

const CardTitle = (props) => (
  <h3 className='cardTitle'>
    <img src={props.icon} width={30} height={30} />
    {props.titleContent}
  </h3>
)

const cardList = [{
  key: 'text',
  cardTitle: <CardTitle icon={textIcon} titleContent='文本' />,
  description: '文本，是指书面语言的表现形式，从文学角度说，通常是具有完整、系统含义（Message）的一个句子或多个句子的组合。一个文本可以是一个句子（Sentence）、一个段落（Paragraph）或者一个篇章（Discourse）。',
  onClick: toTextTool
},{
  key: 'video',
  cardTitle: <CardTitle icon={videoIcon} titleContent='视频' />,
  description: '视频（Video）泛指将一系列静态影像以电信号的方式加以捕捉、记录、处理、储存、传送与重现的各种技术。',
  onClick: toVideoTool
},{
  key: 'sound',
  cardTitle: <CardTitle icon={soundIcon} titleContent='音频' />,
  description: '人类能够听到的所有声音都称之为音频，它可能包括噪音等。',
  onClick: toVoiceTool
},{
  key: 'chart',
  cardTitle: <CardTitle icon={chartIcon} titleContent='图表' />,
  description: '表示各种情况和注明各种数字的图和表的总称，如示意图、统计表等。',
  onClick: toChartTool
},{
  key: 'image',
  cardTitle: <CardTitle icon={imageIcon} titleContent='图片' />,
  description: '图片是指由图形、图像等构成的平面媒体。图片的格式很多，但总体上可以分为点阵图和矢量图两大类。',
  onClick: toImageTool
},{
  key: 'map',
  cardTitle: <CardTitle icon={mapIcon} titleContent='地图' />,
  description: '把地球表面（包括地 壳和大气层）的自然和人文现象，用地图投影等方法，按照一定的比例尺描绘在平面上的图形。',
  onClick: toMapTool
},]

const genCard = cardDataList => {
  if (cardDataList.length) {

    const rawDomList = [];
    const rowNum = Math.ceil(cardDataList.length / 3);


    for (let i = 0; i < rowNum; i++) {
      const cardDomList = [];
      for (let j = 0; j < 3; j++) {
        const tempCard = cardDataList[i * 3 + j]

        tempCard && cardDomList.push(
          <Col span={8} style={{ display: 'flex', justifyContent: 'center' }}>
            <Card
              className='tag-col-card'
              key={tempCard.cardId}
              hoverable
              style={{ width: 360 }}
              onClick={ tempCard.onClick || "" }
            >
              <Meta title={tempCard.cardTitle} description={tempCard.description} />
            </Card>
          </Col>
        )

        !tempCard && cardDomList.push(<Col span={8}></Col>)
      }
      rawDomList.push(
        <Row justify='space-around' gutter={16} className='tag-method-card-raw'>
          {cardDomList}
        </Row>
      )
    }
    return rawDomList;
  } else {
    return <div style={{ height: '100%', width: '100%' }}>
      <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
    </div>;
  }
}

//点击地图后跳转到地图工具页面
function toMapTool(){
  var url = `./map-tool`;
  window.open(url, "mapTool");
}

function toVideoTool(){
  var url = `./video-tool`;
  window.open(url, "videoTool");
}

function toVoiceTool(){
  var url = `./voice-tool`;
  window.open(url, "voiceTool");
}

function toTextTool(){
  var url = `./text-tool`;
  window.open(url, "textTool");
}

function toImageTool(){
  var url = `./image-tool`;
  window.open(url, "imageTool");
}

function toChartTool(){
  var url = `./chart-tool`;
  window.open(url, "chartTool");
}

class TagTool extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let iframeSty = {
      width: "100%",
      border: "none",
      height: "100%"
    };
    // return (
    //     <iframe style={iframeSty} src={`${IFRAME}/tagTool`} seamless></iframe>
    // )
    return (
      <div style={{ display: 'flex', justifyContent: 'center', height: '100%', width: '100%' }}>
        <div style={{width:'70%'}}>
          <div className='jumbotron'>
            <h2 style={{ fontFamily: 'Microsoft Himalaya', textAlign: 'center' }}>
              标签工具
          </h2>
            <p>
              标签方法、标签结果、标签工具这三个页面中要体现六大来源（文本、音频、视频、图表、图片、地图）的内容，发挥创造力，
              找到一个好的表现方式，可能主要内容依然是通过表格呈现但其他部分需要通过不同的方式呈现，但其他部分需要通过不同的方式呈现。
          </p>
          </div>

          {genCard(cardList)}
        </div>

      </div>
    )
  }
}

export default TagTool;