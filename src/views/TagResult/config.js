import { Title, colorMap_text, colorMap_image, colorMap_map, colorMap_chart, colorMap_audio, colorMap_video } from "@/components/Title";
import { Tag } from "antd";
import { stopPropagation } from "@/utility";
import imgSvg from "@/assets/image/data-query-image.svg";
import mapSvg from "@/assets/image/data-query-map.svg";
import chartSvg from "@/assets/image/data-query-chart.svg";
import audioSvg from "@/assets/image/data-query-audio.svg";
import videoSvg from "@/assets/image/data-query-video.svg";
const ITEMS = {
    "text": {
        "title": "文本",
        "typeTag": {
            "title": "类型标签",
            "url": "text/typeTag/query",
            "searchCons": [
                {
                    value: "体裁",
                    label: "体裁"
                }, 
                {
                    value: "功能",
                    label: "功能",
                }, 
                {
                    value: "规范性",
                    label: "规范性",
                }, 
                {
                    value: "语种",
                    label: "语种",
                }, 
                {
                    value: "邻域",
                    label: "邻域",
                } 
            ],
            "cols": [
                {
                    title: "时间",
                    key: "recordDatetime",
                    dataIndex: ["metaId", "recordDatetime"],
                    width: 150,
                    ellipsis: true,
                    onCell: stopPropagation
                },{
                    title: "来源",
                    key: "source",
                    dataIndex: ["metaId", "source"],
                    width: 150,
                    ellipsis: true,
                    onCell: stopPropagation
                },{
                    title: '摘要',
                    dataIndex: ["metaId", "description"],
                    key: 'description',
                    ellipsis: true,
                    className: "pointer"
                },{
                    title: <Title type="text" />,
                    dataIndex: "categoryList",
                    render: list => list && (
                        <span>
                            { 
                                Object.keys(list).map((item, ind) => <Tag color={colorMap_text.get(item)} key={ind}>{list[item]}</Tag>)
                            }
                        </span>
                    ),
                    onCell: stopPropagation
                }
            ]
        },
        "structureTag": {
            "title": "结构标签",
            "url": "text/structureTag/query",
            "searchCons": [
                {
                    "label": "标签类型",
                    "value": "tagType"
                },
                {
                    "label": "标签值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    title: "时间",
                    key: "recordDatetime",
                    dataIndex: ["metaId", "recordDatetime"],
                    align: "center",
                    ellipsis: true,
                    onCell: stopPropagation
                },{
                    title: "来源",
                    key: "source",
                    dataIndex: ["metaId", "source"],
                    align: "center",
                    ellipsis: true,
                    onCell: stopPropagation
                },{
                    title: "摘要",
                    dataIndex: ["metaId", "description"],
                    key: 'description',
                    align: "center",
                    ellipsis: true,
                    className: "pointer"
                },{
                    title: "标签",
                    children: [
                        {
                            title: "类型",
                            dataIndex: "tagType",
                            align: "center",
                            onCell: stopPropagation
                        },{
                            title: "值",
                            dataIndex: "tagValue",
                            align: "center",
                            ellipsis: true,
                            className: "pointer"
                        },{
                            title: "起始位置",
                            dataIndex: "startPosition",
                            align: "center",
                            onCell: stopPropagation
                        },{
                            title: "结束位置",
                            dataIndex: "endPosition",
                            align: "center",
                            onCell: stopPropagation
                        }
                    ]
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "text/contentTag/query",
            "searchCons": [
                {
                    "label": "标签类型",
                    "value": "tagType"
                },
                {
                    "label": "标签名称",
                    "value": "tagName"
                },
                {
                    "label": "标签值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    title: "时间",
                    key: "recordDatetime",
                    dataIndex: ["metaId", "recordDatetime"],
                    width: 150,
                    align: "center",
                    ellipsis: true,
                    onCell: stopPropagation
                },{
                    title: "来源",
                    key: "source",
                    dataIndex: ["metaId", "source"],
                    width: 150,
                    align: "center",
                    ellipsis: true,
                    onCell: stopPropagation
                },{
                    title: '摘要',
                    dataIndex: ["metaId", "description"],
                    key: 'description',
                    align: "center",
                    ellipsis: true,
                    className: "pointer"
                },{
                    title: "标签",
                    children: [
                        {
                            title: "类型",
                            dataIndex: "tagType",
                            align: "center",
                            onCell: stopPropagation
                        },{
                            title: "名称",
                            dataIndex: "tagName",
                            align: "center",
                            onCell: stopPropagation
                        },{
                            title: "值",
                            dataIndex: "tagValue",
                            align: "center",
                            className: "pointer"
                        },{
                            title: "起始位置",
                            dataIndex: "startPosition",
                            align: "center",
                            onCell: stopPropagation
                        },{
                            title: "结束位置",
                            dataIndex: "endPosition",
                            align: "center",
                            onCell: stopPropagation
                        }
                    ]
                }
            ]
        }
    },
    "image": {
        "title": "图像",
        "typeTag": {
            "title": "类型标签",
            "url": "img/typeTag/query",
            "searchCons": [
                {
                    label: "应用",
                    value: "应用"
                },
                {
                    label: "图片格式",
                    value: "储存格式"
                },
                {
                    label: "波段数",
                    value: "波段数"
                }
            ],
            "cols": [
                {
                    "title": "时间",
                    "dataIndex": ["metaId", "recordDatetime"],
                    width: 150,
                    ellipsis: true,
                    onCell: stopPropagation
                },
                {
                    "title": "来源",
                    "dataIndex": ["metaId", "website"],
                    width: 150,
                    ellipsis: true,
                    onCell: stopPropagation
                },
                {
                    "title": "标题",
                    "dataIndex": ["metaId", "caption"],
                    ellipsis: true,
                    className: "pointer"
                },
                {
                    "title": <Title type="image" />,
                    "dataIndex": "categoryList",
                    "render": list => { return (
                        <span>
                            {
                                Object.keys(list).map((item, ind) => {
                                    if(list[item] === "FALSE"){
                                        return null;
                                    }
                                    return <Tag color={colorMap_image.get(item)} key={ind}>{list[item]}</Tag>
                                })
                            }
                        </span>
                    )},
                    "className": "pointer"
                }
            ]
        },
        "structureTag": {
            "title": "结构标签",
            "url": "img/structureTag/query",
            "searchCons": [
                {
                    "label": "标签类型",
                    "value": "tagType"
                },
                {
                    "label": "标签值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "时间",
                    "dataIndex": ["metaId", "recordDatetime"],
                    width: 150,
                    ellipsis: true,
                    onCell: stopPropagation
                },
                {
                    "title": "来源",
                    "dataIndex": ["metaId", "website"],
                    width: 150,
                    ellipsis: true,
                    onCell: stopPropagation
                },
                {
                    "title": "标题",
                    "dataIndex": ["metaId", "caption"],
                    ellipsis: true,
                    className: "pointer"
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            title: "类型",
                            dataIndex: "tagType",
                            align: "center",
                            onCell: stopPropagation
                        },
                        {
                            title: "值",
                            dataIndex: "tagValue",
                            align: "center",
                            ellipsis: true,
                            className: "pointer"
                        }
                    ]
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "img/contentTag/query",
            "searchCons": [
                {
                    "label": "标签名",
                    "value": "tagName"
                },
                {
                    "label": "标签值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "时间",
                    "dataIndex": ["metaId", "recordDatetime"],
                    width: 150,
                    ellipsis: true,
                    onCell: stopPropagation
                },
                {
                    "title": "来源",
                    "dataIndex": ["metaId", "website"],
                    width: 150,
                    ellipsis: true,
                    onCell: stopPropagation
                },
                {
                    "title": "标题",
                    "dataIndex": ["metaId", "caption"],
                    ellipsis: true,
                    className: "pointer"
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            title: "名称",
                            dataIndex: "tagName",
                            align: "center",
                            onCell: stopPropagation
                        },
                        {
                            title: "值",
                            dataIndex: "tagValue",
                            align: "center",
                            ellipsis: true,
                            className: "pointer"
                        }
                    ]
                }
            ]
        }
    },
    "chart": {
        "title": "图表",
        "typeTag": {
            "title": "类型标签",
            "url": "chart/typeTag/query",
            "searchCons": [
                {
                    "label": "专题",
                    "value": "专题"
                }
            ],
            "cols": [
                {
                    "title": "记录时间",
                    "dataIndex": ["metaId", "recordedTime"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "来源",
                    "dataIndex": ["metaId", "website"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "imagePath"],
                    "render":  () => (<img src={chartSvg} alt="图表" width="20" height="20" />),
                    "className": "pointer"
                },
                {
                    "title": <Title type="chart" />,
                    "dataIndex": "categoryList",
                    "render": list => (
                        <span>
                            {
                                Object.keys(list).map((item, ind) => {
                                    if(list[item] === "FALSE"){
                                        return null;
                                    }
                                    return <Tag color={colorMap_chart.get(item)} key={ind}>{list[item]}</Tag>
                                })
                            }
                        </span>
                    ),
                    "className": "pointer"
                }
            ]
        },
        "structureTag": {
            "title": "结构标签",
            "url": "chart/structureTag/query",
            "searchCons": [
                {
                    "label": "标签类型",
                    "value": "tagType"
                },
                {
                    "label": "标签值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "时间",
                    "dataIndex": ["metaId", "recordedTime"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "来源",
                    "dataIndex": ["metaId", "website"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "imagePath"],
                    "render":  () => (<img src={chartSvg} alt="图表" width="20" height="20" />),
                    "className": "pointer"
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            "title": "类型",
                            "dataIndex": "tagType",
                            "ellipsis": true,
                            "className": "pointer"
                        },
                        // {
                        //     "title": "值",
                        //     "dataIndex": "tagValue",
                        //     "ellipsis": true,
                        //     "className": "pointer"
                        // }
                    ]
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "chart/contentTag/query",
            "searchCons": [
                {
                    "label": "标签类型",
                    "value": "tagType"
                },
                {
                    "label": "标签值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "时间",
                    "dataIndex": ["metaId", "recordedTime"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "来源",
                    "dataIndex": ["metaId", "website"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "imagePath"],
                    "render":  () => (<img src={chartSvg} alt="图表" width="20" height="20" />),
                    "className": "pointer"
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            "title": "类型",
                            "dataIndex": "tagType",
                            "ellipsis": true,
                            "className": "pointer"
                        },
                        {
                            "title": "值",
                            "dataIndex": "tagValue",
                            "ellipsis": true,
                            "className": "pointer"
                        }
                    ]
                }
            ]
        }
    },
    "map": {
        "title": "地图",
        "typeTag": {
            "title": "类型标签",
            "url": "map/typeTag/query",
            "searchCons": [
                {
                    "label": "专题",
                    "value": "专题"
                }
            ],
            "cols": [
                {
                    "title": "时间",
                    "dataIndex": ["metaId", "recordDatetime"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "来源",
                    "dataIndex": ["metaId", "website"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "imagePath"],
                    "render":  () => (<img src={mapSvg} alt="地图" width="20" height="20" />),
                    "className": "pointer"
                },
                {
                    "title": <Title type="map" />,
                    "dataIndex": "categoryList",
                    "render": list => (
                        <span>
                            {
                                Object.keys(list).map((item, ind) => {
                                    if(list[item] === "FALSE"){
                                        return null;
                                    }
                                    return <Tag color={colorMap_map.get(item)} key={ind}>{list[item]}</Tag>
                                })
                            }
                        </span>
                    ),
                    "className": "pointer"
                }
            ]
        },
        "structureTag": {
            "title": "结构标签",
            "url": "map/structureTag/query",
            "searchCons": [
                {
                    "label": "标签类型",
                    "value": "tagType"
                },
                {
                    "label": "标签值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "时间",
                    "dataIndex": ["metaId", "recordDatetime"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "来源",
                    "dataIndex": ["metaId", "website"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "imagePath"],
                    "render":  () => (<img src={mapSvg} alt="地图" width="20" height="20" />),
                    "className": "pointer"
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            "title": "类型",
                            "dataIndex": "tagType",
                            "ellipsis": true,
                            "className": "pointer"
                        },
                        {
                            "title": "值",
                            "dataIndex": "tagValue",
                            "ellipsis": true,
                            "className": "pointer"
                        }
                    ]
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "map/contentTag/query",
            "searchCons": [
                {
                    "label": "标签类型",
                    "value": "tagType"
                },
                {
                    "label": "标签值",
                    "value": "tagName"
                }
            ],
            "cols": [
                {
                    "title": "时间",
                    "dataIndex": ["metaId", "recordDatetime"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "来源",
                    "dataIndex": ["metaId", "website"],
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "imagePath"],
                    "render":  () => (<img src={mapSvg} alt="地图" width="20" height="20" />),
                    "className": "pointer"
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            "title": "类型",
                            "dataIndex": "tagType",
                            "ellipsis": true,
                            "className": "pointer"
                        },
                        {
                            "title": "值",
                            "dataIndex": "tagName",
                            "ellipsis": true,
                            "className": "pointer"
                        }
                    ]
                }
            ]
        }
    },
    "audio": {
        "title": "音频",
        "typeTag": {
            "title": "类型标签",
            "url": "audio/typeTag/query",
            "searchCons": [
                {
                    "label": "专题",
                    "value": "专题"
                },
                {
                    "label": "数据格式",
                    "value": "数据格式"
                }
            ],
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": ["metaId", "channelName"],
                    "render": (text, record) => `${record.metaId.channelName}-${record.metaId.programName}`, 
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "url"],
                    "render": () => <img src={audioSvg} width="20" height="20" alt="音频" />,
                    "className": "pointer"
                },
                {
                    "title": "播出时间",
                    "dataIndex": ["metaId", "date"],
                    "width": "150px",
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": <Title type="audio" />,
                    "dataIndex": "categoryList",
                    "render": list => (
                        <span>
                            {
                                Object.keys(list).map((item, ind) => {
                                    if(list[item] === "FALSE"){
                                        return null;
                                    }
                                    return <Tag color={colorMap_audio.get(item)} key={ind}>{list[item]}</Tag>
                                })
                            }
                        </span>
                    ),
                    "className": "pointer"
                }
            ]
        },
        "structureTag": {
            "title": "结构标签",
            "url": "audio/structureTag/query",
            "searchCons": [
                {
                    "label": "时长",
                    "value": "duration"
                }
            ],
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": ["metaId", "channelName"],
                    "render": (text, record) => `${record.metaId.channelName}-${record.metaId.programName}`, 
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "url"],
                    "render": () => <img src={audioSvg} width="20" height="20" alt="音频" />,
                    "className": "pointer"
                },
                {
                    "title": "播出时间",
                    "dataIndex": ["metaId", "date"],
                    "width": "150px",
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            "title": "时长",
                            "dataIndex": "duration",
                            "width": "150px",
                            "ellipsis": true,
                            "className": "pointer"
                        },
                        {
                            "title": "文件大小",
                            "dataIndex": "fileSize",
                            "width": "150px",
                            "ellipsis": true,
                            "className": "pointer"
                        },
                        {
                            "title": "比特率",
                            "dataIndex": "bitRate",
                            "width": "150px",
                            "ellipsis": true,
                            "className": "pointer"
                        }
                    ]
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "audio/contentTag/query",
            "searchCons": [
                {
                    "label": "标签类型",
                    "value": "tagType"
                },
                {
                    "label": "标签值",
                    "value": "tagValue"
                }
            ],
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": ["metaId", "channelName"],
                    "render": (text, record) => `${record.metaId.channelName}-${record.metaId.programName}`, 
                    "onCell": stopPropagation
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "url"],
                    "render": () => <img src={audioSvg} width="20" height="20" alt="音频" />,
                    "className": "pointer"
                },
                {
                    "title": "播出时间",
                    "dataIndex": ["metaId", "date"],
                    "width": "150px",
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            "title": "类型",
                            "dataIndex": "tagType",
                            "width": "150px",
                            "ellipsis": true,
                            "className": "pointer"
                        },
                        {
                            "title": "值",
                            "dataIndex": "tagValue",
                            "width": "150px",
                            "ellipsis": true,
                            "className": "pointer"
                        }
                    ]
                }
            ]
        }
    },
    "video": {
        "title": "视频",
        "typeTag": {
            "title": "类型标签",
            "url": "video/typeTag/query",
            "searchCons": [
                {
                    "label": "视频编码",
                    "value": "视频编码"
                },
                {
                    "label": "专题",
                    "value": "专题"
                },
                {
                    "label": "数据格式",
                    "value": "数据格式"
                }
            ],
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": ["metaId", "location"],
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "url"],
                    "ellipsis": true,
                    "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
                    "className": "pointer"
                },
                {
                    "title": "拍摄时间",
                    "dataIndex": ["metaId", "date"],
                    //"width": "150px",
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": <Title type="video" />,
                    "dataIndex": "categoryList",
                    "render": list => (
                        <span>
                            {
                                Object.keys(list).map((item, ind) => {
                                    if(list[item] === "FALSE"){
                                        return null;
                                    }
                                    return <Tag color={colorMap_video.get(item)} key={ind}>{list[item]}</Tag>
                                })
                            }
                        </span>
                    ),
                    "className": "pointer"
                }
            ]
        },
        "structureTag": {
            "title": "结构标签",
            "url": "video/structureTag/query",
            "searchCons": [
                {
                    "label": "分辨率",
                    "value": "resolution"
                }
            ],
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": ["metaId", "location"],
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "url"],
                    "ellipsis": true,
                    "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
                    "className": "pointer"
                },
                {
                    "title": "拍摄时间",
                    "dataIndex": ["metaId", "date"],
                    //"width": "150px",
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            "title": "时长",
                            "dataIndex": "duration",
                            "width": "150px",
                            "ellipsis": true,
                            "className": "pointer"
                        },
                        {
                            "title": "分辨率",
                            "dataIndex": "resolutionWidth",
                            "width": "150px",
                            "ellipsis": true,
                            "render": (text, record) => `${record.resolutionWidth}*${record.resolutionHeight}`,
                            "className": "pointer"
                        }
                    ]
                }
            ]
        },
        "contentTag": {
            "title": "内容标签",
            "url": "video/contentTag/query",
            "searchCons": [
                {
                    "label": "标签类型",
                    "value": "tagType"
                }
            ],
            "cols": [
                {
                    "title": "标题",
                    "dataIndex": ["metaId", "location"],
                    "ellipsis": true,
                    "className": "pointer"
                },
                {
                    "title": "内容",
                    "dataIndex": ["metaId", "url"],
                    "ellipsis": true,
                    "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
                    "onCell": stopPropagation
                },
                {
                    "title": "拍摄时间",
                    "dataIndex": ["metaId", "date"],
                    //"width": "150px",
                    "ellipsis": true,
                    "onCell": stopPropagation
                },
                {
                    "title": "标签",
                    "children": [
                        {
                            "title": "类型",
                            "dataIndex": "tagType",
                            "width": "150px",
                            "ellipsis": true,
                            "className": "pointer"
                        },
                        {
                            "title": "值",
                            "dataIndex": "url",
                            "width": "150px",
                            "ellipsis": true,
                            "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
                            "className": "pointer"
                        }
                    ]
                }
            ]
        }
    }
};

export default ITEMS;