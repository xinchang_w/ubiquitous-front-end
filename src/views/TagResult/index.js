import React, { Component, Fragment } from "react";
import { Button, Input, Select, Modal, Breadcrumb, Radio, Tag } from "antd";
import { NODE_PATH, FILE_SERVER } from "@/assets/global/index";
import { colorMap_text, colorMap_image, colorMap_map, colorMap_chart, colorMap_audio, colorMap_video } from "@/components/Title";
import { TableRendering, Paragraph, CustomizeModal, CusImage } from "@/components";
import { setHighlight } from "@/utility";
import ITEMS from "./config";

class TagResult extends Component{
    constructor(props){
        super(props);
        this.state = {
            "curItem": "text",
            "curTag": "typeTag",
            "curSearchCon": "体裁",
            "keywords": "",
            "modalData": null
        };

        this.defaultUrl = `${NODE_PATH}/${ITEMS["text"]["typeTag"].url}`;
        this.defaultCols = ITEMS["text"]["typeTag"].cols;

        this.tableRendering = React.createRef();
        this.onItemChange = this.onItemChange.bind(this);
        this.onTagChange = this.onTagChange.bind(this);
        this.onConChange = this.onConChange.bind(this);
        this.onKeywordsChange = this.onKeywordsChange.bind(this);
        this.showContent = this.showContent.bind(this);
        this.search = this.search.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }

    render(){
        let { curItem, curTag, curSearchCon, modalData, keywords } = this.state; 
        let item = ITEMS[curItem];
        
        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>数据资源</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">标签结果</Breadcrumb.Item>
                </Breadcrumb>
                <Radio.Group onChange={this.onItemChange} style={{margin:10}} value={curItem}>
                    {
                        Object.keys(ITEMS).map(i => (<Radio value={i} key={i}>{ITEMS[i].title}</Radio>))
                    }
                </Radio.Group>
                <br />
                <Radio.Group onChange={this.onTagChange} style={{margin:10}} value={curTag}>
                    {
                        Object.keys(item).map(i => i !== "title" && (<Radio value={i} key={i}>{item[i].title}</Radio>))
                    }
                </Radio.Group>
                <div>
                    <div className="searchBox">
                        <label className="searchBox-label">查询条件：</label>
                        <Select
                            placeholder="类型"
                            className="searchBox-input"
                            value={curSearchCon}
                            onChange={this.onConChange}
                            options={item[curTag].searchCons}
                        />
                        <label className="searchBox-label">关键词：</label>
                        <Input
                            className="searchBox-input"
                            placeholder="关键词"
                            value={keywords}
                            onChange={this.onKeywordsChange}
                        />
                        <Button
                            type="primary"
                            className="searchBox-btn"
                            onClick={() => this.search()}
                        >
                            查询
                        </Button>
                    </div>
                    <TableRendering cols={this.defaultCols} url={this.defaultUrl} ref={this.tableRendering} clickRow={this.showContent} />
                    <CustomizeModal
                        visible={modalData !== null}
                        title={`${ITEMS[curItem].title}-${ITEMS[curItem][curTag].title}`}
                        close={this.hideModal}
                    >
                        <Paragraphs type={`${curItem}_${curTag}`} {...modalData} />
                    </CustomizeModal>
                </div>
            </div>
        );
    }

    onItemChange(e){
        let val = e.target.value;
        let item = ITEMS[val];
        this.setState(prevState => {
            return {
                "curItem": val,
                "curSearchCon": item[prevState.curTag].searchCons[0].value,
                "keywords": ""
            }
        }, this.search);
    }

    onTagChange(e){
        let val = e.target.value;
        let item = ITEMS[this.state.curItem];
        this.setState({
            "curTag": val,
            "curSearchCon": item[val].searchCons[0].value,
            "keywords": ""
        }, this.search);
    }

    onConChange(value){
        this.setState({
            "curSearchCon": value
        });
    }

    onKeywordsChange(e){
        this.setState({
            "keywords": e.target.value
        })
    }

    search(){
        let con = {};
        let { keywords, curSearchCon, curItem, curTag } = this.state;
        let item = ITEMS[curItem];
        let url = `${NODE_PATH}/${item[curTag].url}`;
        let columns = item[curTag].cols;
        if(curSearchCon && keywords){
            con[curSearchCon] = keywords;
        }
        
        this.tableRendering.current.search(url, columns, con);
    }

    showContent(record){
        this.setState({
            "modalData": record
        });
    }

    hideModal(){
        this.setState({
            "modalData": null
        });
    }
}

function Paragraphs(props){
    let com = null;
    switch(props.type){
        case "text_typeTag":
            com = (
                <Fragment>
                    {/* <Paragraph title="数据类型：" content="文本" /> */}
                    <Paragraph title="来源：" content={props.metaId && props.metaId.source} />
                    <Paragraph title="时间：" content={props.metaId && props.metaId.recordDatetime} />
                    <Paragraph title="摘要：" content={props.metaId && props.metaId.description} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => <Tag color={colorMap_text.get(item)} key={ind}>{props.categoryList[item]}</Tag>)}
                    />
                    <Paragraph title="内容：" content={props.metaId && props.metaId.content} />
                </Fragment>
            );
            break;
        case "text_structureTag":
            com = (
                <Fragment>
                    {/* <Paragraph title="数据类型：" content="文本" /> */}
                    <Paragraph title="来源：" content={ props.metaId && props.metaId.source } />
                    <Paragraph title="时间：" content={ props.metaId && props.metaId.recordDatetime } />
                    <Paragraph title="摘要：" content={ props.metaId && props.metaId.description } />
                    <Paragraph title="标签类型：" content={ props.tagType }  />
                    <Paragraph title="标签值：" content={ props.tagValue } />
                    <Paragraph title="内容：" content={ props.metaId && setHighlight(props.metaId.content, props.startPosition, props.endPosition) } />
                </Fragment>
            );
            break;
        case "text_contentTag": 
            com = (
                <Fragment>
                    {/* <Paragraph title="数据类型：" content="文本" /> */}
                    <Paragraph title="来源：" content={ props.metaId && props.metaId.source } />
                    <Paragraph title="时间：" content={ props.metaId && props.metaId.recordDatetime } />
                    <Paragraph title="摘要：" content={ props.metaId && props.metaId.description } />
                    <Paragraph title="标签名：" content={ props.tagName }  />
                    <Paragraph title="标签值：" content={ props.tagValue } />
                    <Paragraph title="内容：" content={ props.metaId && setHighlight(props.metaId.content, props.startPosition, props.endPosition) } />
                </Fragment>
            );
            break;
        case "image_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="日期：" content={ props.metaId && props.metaId.recordDatetime } />
                    <Paragraph title="来源：" content={ props.metaId && props.metaId.website } />
                    <Paragraph title="描述：" content={ props.metaId && props.metaId.description } />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if(props.categoryList[item] === "FALSE"){
                                return false;
                            }
                            return (<Tag color={colorMap_image.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph image={props.metaId && `${FILE_SERVER}/images/metaData_Img/${props.metaId.imageName}`} />
                </Fragment>
            );
            break;
        case "image_structureTag":
            com = (
                <Fragment>
                    <Paragraph title="日期：" content={ props.metaId && props.metaId.recordDatetime } />
                    <Paragraph title="来源：" content={ props.metaId && props.metaId.website } />
                    <Paragraph title="描述：" content={ props.metaId && props.metaId.description } />
                    <Paragraph title="标签类型：" content={props.tagType}/>
                    <Paragraph title="标签值：" content={props.tagValue} />
                    <Paragraph image={props.metaId && `${FILE_SERVER}/images/metaData_Img/${props.metaId.imageName}`} />
                </Fragment>
            );
            break;
        case "image_contentTag":
            com = (
                <Fragment>
                    {/* <Paragraph title="数据类型：" content="图像" /> */}
                    <Paragraph title="日期：" content={ props.metaId && props.metaId.recordDatetime } />
                    <Paragraph title="来源：" content={ props.metaId && props.metaId.website } />
                    <Paragraph title="描述：" content={ props.metaId && props.metaId.description } />
                    <Paragraph title="标签名：" content={props.tagName} />
                    <Paragraph title="标签值：" content={props.tagValue} />
                    <Paragraph image={props.metaId && `${FILE_SERVER}/images/metaData_Img/${props.metaId.imageName}`} />
                </Fragment>
            );
            break;
        case "map_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.metaId && props.metaId.recordDatetime} />
                    <Paragraph title="来源：" content={props.metaId && props.metaId.website} />
                    <Paragraph title="网址：" content={<a href={props.metaId && props.metaId.source} target="_blank">{props.metaId && props.metaId.source}</a>} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if(props.categoryList[item] === "FALSE"){
                                return false;
                            }
                            return (<Tag color={colorMap_map.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph title="图片：" image={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/map/${props.metaId.imagePath}`} />
                </Fragment>
            );
            break;
        case "map_structureTag":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.metaId && props.metaId.recordDatetime} />
                    <Paragraph title="来源：" content={props.metaId && props.metaId.website} />
                    <Paragraph title="网址：" content={<a href={props.metaId && props.metaId.source} target="_blank">{props.metaId && props.metaId.source}</a>} />
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签值：" content={props.tagValue} />
                    <CusImage 
                        width="720px" 
                        url={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/map/${props.metaId.imagePath}`} 
                        rect={{
                            color: "rgba(250,231,51, 0.5)",
                            x: props.left,
                            y: props.top,
                            w: props.width,
                            h: props.height
                        }}
                    />
                    {/* <Paragraph title="图片：" image={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/map/${props.metaId.imagePath}`} /> */}
                </Fragment>
            );
            break;
        case "map_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.metaId && props.metaId.recordDatetime} />
                    <Paragraph title="来源：" content={props.metaId && props.metaId.website} />
                    <Paragraph title="网址：" content={<a href={props.metaId && props.metaId.source} target="_blank">{props.metaId && props.metaId.source}</a>} />
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签值：" content={props.tagName} />
                    <Paragraph title="图片：" image={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/map/${props.metaId.imagePath}`} />
                </Fragment>
            );
            break;
        case "chart_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.metaId && props.metaId.recordedTime} />
                    <Paragraph title="文件格式：" content={props.metaId && props.metaId.format} />
                    <Paragraph title="来源：" content={props.metaId && <a href={props.metaId.source} target="_blank">{props.metaId.source}</a>} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if(props.categoryList[item] === "FALSE"){
                                return false;
                            }
                            return (<Tag color={colorMap_chart.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph title="图片：" image={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/${props.metaId.imagePath}`} />
                </Fragment>
            );
            break;
        case "chart_structureTag":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.metaId && props.metaId.recordedTime} />
                    <Paragraph title="文件格式：" content={props.metaId && props.metaId.format} />
                    <Paragraph title="来源：" content={props.metaId && <a href={props.metaId.source} target="_blank">{props.metaId.source}</a>} />
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签值：" content={props.tagValue} />
                    {/* <Paragraph title="图片：" image={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/${props.metaId.imagePath}`} /> */}
                    <CusImage 
                        width="720px" 
                        url={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/${props.metaId.imagePath}`} 
                        rect={{
                            color: "rgba(250,231,51, 0.5)",
                            x: props.left,
                            y: props.top,
                            w: props.width,
                            h: props.height
                        }}
                    />
                </Fragment>
            );
            break;
        case "chart_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="发布时间：" content={props.metaId && props.metaId.recordedTime} />
                    <Paragraph title="文件格式：" content={props.metaId && props.metaId.format} />
                    <Paragraph title="来源：" content={props.metaId && <a href={props.metaId.source} target="_blank">{props.metaId.source}</a>} />
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签值：" content={props.tagValue} />
                    <Paragraph title="图片：" image={props.metaId && props.metaId.imagePath && `${FILE_SERVER}/${props.metaId.imagePath}`} />
                </Fragment>
            );
            break;
        case "audio_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="节目名：" content={props.metaId && props.metaId.programName} />
                    <Paragraph title="频道名：" content={props.metaId && props.metaId.channelName} />
                    <Paragraph title="播出时间：" content={props.metaId && props.metaId.date} />
                    <Paragraph title="每日播出时间段：" content={props.metaId && props.metaId.period} />
                    <Paragraph title="频道（FM）：" content={props.metaId && props.metaId.FM} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if(props.categoryList[item] === "FALSE"){
                                return false;
                            }
                            return (<Tag color={colorMap_audio.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph title="音频：" audio={props.metaId && props.metaId.url && `${FILE_SERVER}/audio/${props.metaId.url}`} />
                </Fragment>
            );
            break;
        case "audio_structureTag":
            com = (
                <Fragment>
                    <Paragraph title="节目名：" content={props.metaId && props.metaId.programName} />
                    <Paragraph title="频道名：" content={props.metaId && props.metaId.channelName} />
                    <Paragraph title="播出时间：" content={props.metaId && props.metaId.date} />
                    <Paragraph title="每日播出时间段：" content={props.metaId && props.metaId.period} />
                    <Paragraph title="频道（FM）：" content={props.metaId && props.metaId.FM} />
                    <Paragraph title="时长：" content={props.duration} />
                    <Paragraph title="文件大小：" content={props.fileSize} />
                    <Paragraph title="比特率：" content={props.bitRate} />
                    <Paragraph title="音频：" audio={props.metaId && props.metaId.url && `${FILE_SERVER}/audio/${props.metaId.url}`} />
                </Fragment>
            );
            break;
        case "audio_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="节目名：" content={props.metaId && props.metaId.programName} />
                    <Paragraph title="频道名：" content={props.metaId && props.metaId.channelName} />
                    <Paragraph title="播出时间：" content={props.metaId && props.metaId.date} />
                    <Paragraph title="每日播出时间段：" content={props.metaId && props.metaId.period} />
                    <Paragraph title="频道（FM）：" content={props.metaId && props.metaId.FM} />
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签值：" content={props.tagValue} />
                    <Paragraph title="音频：" audio={props.metaId && props.metaId.url && `${FILE_SERVER}/audio/${props.metaId.url}`} />
                </Fragment>
            );
            break;
        case "video_typeTag":
            com = (
                <Fragment>
                    <Paragraph title="拍摄时间：" content={props.metaId && props.metaId.date} />
                    <Paragraph title="摄像头位置：" content={props.metaId && props.metaId.location} />
                    <Paragraph title="视频格式：" content={props.metaId && props.metaId.suffix} />
                    <Paragraph title="视频文件大小：" content={props.metaId && props.metaId.size} />
                    <Paragraph
                        title="标签："
                        content={props.categoryList && Object.keys(props.categoryList).map((item, ind) => {
                            if(props.categoryList[item] === "FALSE"){
                                return false;
                            }
                            return (<Tag color={colorMap_video.get(item)} key={ind}>{props.categoryList[item]}</Tag>)
                        })}
                    />
                    <Paragraph title="视频：" video={props.metaId && props.metaId.url && `${FILE_SERVER}/${props.metaId.url}`} />
                </Fragment>
            );
            break;
        case "video_structureTag": 
            com = (
                <Fragment>
                    <Paragraph title="拍摄时间：" content={props.metaId && props.metaId.date} />
                    <Paragraph title="摄像头位置：" content={props.metaId && props.metaId.location} />
                    <Paragraph title="视频格式：" content={props.metaId && props.metaId.suffix} />
                    <Paragraph title="视频文件大小：" content={props.metaId && props.metaId.size} />
                    <Paragraph title="视频分辨率：" content={`${props.resolutionWidth}*${props.resolutionHeight}`} />
                    <Paragraph title="视频：" video={props.metaId && props.metaId.url && `${FILE_SERVER}/${props.metaId.url}`} />
                </Fragment>
            );
            break;
        case "video_contentTag":
            com = (
                <Fragment>
                    <Paragraph title="拍摄时间：" content={props.metaId && props.metaId.date} />
                    <Paragraph title="摄像头位置：" content={props.metaId && props.metaId.location} />
                    <Paragraph title="视频格式：" content={props.metaId && props.metaId.suffix} />
                    <Paragraph title="视频文件大小：" content={props.metaId && props.metaId.size} />
                    <Paragraph title="标签类型：" content={props.tagType} />
                    <Paragraph title="标签内容：" video={props.url && `${FILE_SERVER}/video/contentTag/${props.url}`} />
                </Fragment>
            );
            break;
        default:
            break;
    }

    return com;
}

export default TagResult;