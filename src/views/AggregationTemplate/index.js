import React, { Component } from "react";
import { Breadcrumb } from "antd";
import List from "./list";
import Operation from "./operation";

const colorSheet = ["", "#f50", "#2db7f5", "#87d068", "#108ee9", "#ccc", "#2db7f5", "#f55",];
class AggrTem extends Component{
    constructor(props){
        super(props);
        this.state = {
            mode: "list",
            item: [-1, -1]
        };
    }
    render(){
        let mode = this.state.mode;
        let com = null, caption = "";
        switch(mode){
            case "list":
                com = <List changeMode={this.changeMode.bind(this, "operation")} history={this.props.history} item={this.state.item} />;
                caption = "";
                break;
            case "operation":
                com = <Operation changeMode={this.changeMode.bind(this, "list")} />;
                caption = "新建聚合模板";
                break;
            default:
                com = null;
        }
        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>位置聚合</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">聚合模板</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">{caption}</Breadcrumb.Item>
                </Breadcrumb>
                { com }
            </div>
        );
    }

    changeMode(mode, item){
        let obj;
        if(item){
            obj = { mode, item };
        }
        else{
            obj = { mode };
        }
        this.setState(obj);
    }
}

export default AggrTem;