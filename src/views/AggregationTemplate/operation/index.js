import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { Row, Col, Card, Button, Modal, Form, Input, Upload, Checkbox, message } from "antd";
import { UploadOutlined, RollbackOutlined, SaveOutlined } from "@ant-design/icons";
import { Card as DraggableCard } from "@/components";
import { Allias_Com_Map, DataType_Url_Map } from "@/components/aggregation";
import { NODE_PATH, FILE_SERVER } from "@/assets/global";
import style from "./index.module.css";

function Opr(props){
    let [components, setComponents] = useState([]);
    let [instances, setInstances] = useState([]);
    let [modalVisible, setModalVisible] = useState(false);
    let instancesDom = useRef();
    let formRef = useRef();
    
    let formItemLayout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 15 }
    };
    let categories = [
        { label: "时间", value: "时间" },
        { label: "地点", value: "地点" },
        { label: "人物", value: "人物" },
        { label: "事物", value: "事物" },
        { label: "事件", value: "事件" },
        { label: "现象", value: "现象" },
        { label: "场景", value: "场景" }
    ];
    let componentObj = {};
    components.forEach(c => {
        componentObj[c._id] = c;
    });
    
    useEffect(() => {
        axios.get(`${NODE_PATH}/aggregation/component/search`).then(res => {
            setComponents(res.data.data);
        }).catch(err => {
            console.log(err);
        });
    }, []);


    function addComInstance(id){
        let com = componentObj[id];
        let instance = {
            title: com.title,
            style: {
                top: 50,
                left: 50,
                width: 300,
                height: 200
            },
            componentId: id
        };
   
        instances.push(instance);
        setInstances([...instances]);
    }

    function removeInstance(index){
        instances.splice(index, 1);
        setInstances([...instances]);
    }

    //保存
    function save(values){
        let nodes = instancesDom.current.childNodes;
        let id, ids = [];
        let form = new FormData();
        nodes.forEach(n => {
            id = n.id;
            ids.push(instances[id].componentId);
            instances[id].style = {
                "left": n.offsetLeft,
                "top": n.offsetTop,
                "width": n.offsetWidth,
                "height": n.offsetHeight
            };
        });
        form.append("title", values.title);
        form.append("description", values.description);
        form.append("categories", JSON.stringify(values.categories));
        form.append("cover", values.cover.file);
        form.append("componentIds", JSON.stringify([...new Set(ids)]));
        form.append("content", JSON.stringify(instances));
        axios.post(`${NODE_PATH}/aggregation/template/add`, form).then(result => {
            message.success("新增聚合模板成功！", 0.5, () => props.changeMode());
        }).catch(err => {
            message.error("新增聚合模板失败！");
        });
    }

    function hideModal(){
        setModalVisible(false);
    }

    function showModal(){
        setModalVisible(true);
    }

    return (
        <div className={style.wrapper}>
            <div className={style.btnBox}>
                <Button type="link" icon={<RollbackOutlined />} onClick={() => props.changeMode()}>返回</Button>
                <Button type="text" icon={<SaveOutlined />} onClick={showModal}>保存</Button>
            </div>
            <Row className={style.row}>
                <Col span={4} className={style.leftBox}>
                    {
                        components.map(com => (
                            <Card
                                onClick={() => addComInstance(com._id)}
                                key={com._id}
                                className={style.card}
                                hoverable
                                title={com.title}
                                cover={<img src={`${FILE_SERVER}/${com.cover}`} alt="组件封面" />}
                            />
                        ))
                    }
                </Col>
                <Col span={20} className={style.rightBox} ref={instancesDom}>
                    {
                        instances.map((ins, index) => {
                            let comInfo = componentObj[ins.componentId];
                            let Com = Allias_Com_Map[comInfo.alias];
                            return (
                                <DraggableCard
                                    id={index}
                                    style={ins.style}
                                    key={index}
                                    title={ins.title}
                                    movable={true}
                                    resizable={true}
                                    closable={true}
                                    onClose={() => removeInstance(index)}
                                >
                                    <Com />
                                </DraggableCard>
                            );
                        })
                    }
                </Col>
            </Row>
            <Modal
                visible={modalVisible}
                title="聚合模板"
                onCancel={hideModal}
                onOk={() => formRef.current.submit()}
            >
                <Form ref={formRef} onFinish={save}>
                    <Form.Item {...formItemLayout} name="title" label="名称" rules={[{required:true}]}>
                        <Input placeholder="请输入模板名称" />
                    </Form.Item>
                    <Form.Item {...formItemLayout} name="categories" label="类别">
                        <Checkbox.Group options={categories} />
                    </Form.Item>
                    <Form.Item {...formItemLayout} name="cover" label="图标" >
                        <Upload beforeUpload={() => false}>
                            <Button icon={<UploadOutlined />}>选择文件</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item {...formItemLayout} name="description" label="简介">
                        <Input.TextArea placeholder="请输入模板简介" />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}

export default Opr;