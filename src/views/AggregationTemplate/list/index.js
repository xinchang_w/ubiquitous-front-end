import React, { Component, Fragment } from "react";
import { Card, Row, Col, Modal, Form, Input, Upload, Button, Checkbox, message } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import axios from "axios";
import { NODE_PATH, FILE_SERVER } from "@/assets/global";
import style from "./index.module.css";
import addIcon from "@/assets/image/icon_add.png";

const colorSheet = ["", "#f50", "#2db7f5", "#87d068", "#108ee9", "#ccc", "#2db7f5", "#f55",];
const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 15 }
};
class List extends Component{
    constructor(props){
        super(props);
        this.state = {
            selectedCol: props.item[1],
            selectedRow: props.item[0],
            modalVisible: false,
            templates: []
        };

        this.selectedComplateId = "";
        this.formRef = React.createRef();
        this.categories = ["", "时间", "地点", "人物", "事物", "事件", "现象", "场景"];
        this.generateGrides = this.generateGrides.bind(this);
        this.switchCategory = this.switchCategory.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.save = this.save.bind(this);
    }

    componentDidMount(){
        let { selectedRow:r, selectedCol:c } = this.state;
        let categories = removeDuplicate([this.categories[r], this.categories[c]]);
        axios.get(`${NODE_PATH}/aggregation/template/search`, {
            params: { categories }
        }).then(res => {
            this.setState({
                templates: res.data.data
            });
        }).catch(err => {
            console.log(err);
        });
    }
    
    render(){
        let { templates, modalVisible, selectedRow, selectedCol } = this.state;
        return (
            <Fragment>
                <Row>
                    <Col span={12} className={style.leftBox}>
                        <Card title="组合类型：" bordered={false}>
                            {this.generateGrides()}
                        </Card>
                    </Col>
                    <Col span={12} className={style.rightBox}>
                        <h5 className={style["rightBox-h5"]}>模板列表（点击其中一个模板可以直接据此创建任务）：</h5>
                        <Card
                            hoverable
                            className={style.card}
                            cover={<Cover cover={addIcon} />}
                            onClick={() => this.props.changeMode([selectedRow, selectedCol])}
                        >
                            <p className={style["card-title"]}>新增模板</p>
                        </Card>
                        {
                            templates.map(item => {
                                return (
                                    <Card
                                        key={item._id}
                                        hoverable
                                        className={style.card}
                                        cover={<Cover cover={`${FILE_SERVER}/${item.cover}`} />}
                                        onClick={() => this.showModal(item)}
                                    >
                                        <p className={style["card-title"]}>{item.title}</p>
                                    </Card>
                                );
                            })
                        }
                    </Col>
                </Row>
                <Modal
                    title="创建聚合任务"
                    visible={modalVisible}
                    onCancel={this.hideModal}
                    onOk={() => this.formRef.current.submit()}
                >
                    <Form ref={this.formRef} onFinish={this.save}>
                        <Form.Item {...formItemLayout} name="title" label="名称" rules={[{ required: true }]}>
                            <Input placeholder="请输入任务名称" />
                        </Form.Item>
                        <Form.Item {...formItemLayout} name="cover" label="图标" >
                            <Upload beforeUpload={() => false}>
                                <Button icon={<UploadOutlined />}>选择文件</Button>
                            </Upload>
                        </Form.Item>
                        <Form.Item {...formItemLayout} name="description" label="简介">
                            <Input.TextArea placeholder="请输入任务简介" />
                        </Form.Item>
                    </Form>
                </Modal>
            </Fragment>
        );
    }

    //生成格子
    generateGrides(){
        let i, j, len = this.categories.length;
        let com = [];
        for(i = 0; i < len; i++){
            for(j = 0; j < len; j++){
                if(i === 0 && j === 0){
                    com.push(<Card.Grid key={`${i},${j}`} hoverable={false} className={style.grid}>&nbsp;</Card.Grid>);
                    continue;
                }
                if(i === this.state.selectedRow && j === this.state.selectedCol){
                    com.push(
                        <Card.Grid
                            key={`${i},${j}`}
                            className={`${style.grid} ${style["selected"]}`}
                            data-category={`${i},${j}`}
                            onClick={this.switchCategory}
                        >
                            &nbsp;
                        </Card.Grid>
                    );
                    continue;
                }
                if(i === 0){
                    com.push(
                        <Card.Grid
                            key={`${i},${j}`}
                            hoverable={false}
                            className={style.grid}
                            style={{backgroundColor:colorSheet[j]}}
                        >
                            {this.categories[j]}
                        </Card.Grid>
                    );
                    continue;
                }
                if(j === 0){
                    com.push(
                        <Card.Grid
                            key={`${i},${j}`}
                            hoverable={false}
                            className={style.grid}
                            style={{backgroundColor:colorSheet[i]}}
                        >
                                {this.categories[i]}
                        </Card.Grid>
                    );
                    continue;
                }
                com.push(
                    <Card.Grid
                        key={`${i},${j}`}
                        className={`${style.grid} ${style.gridBtn}`}
                        data-category={`${i},${j}`}
                        onClick={this.switchCategory }
                    >
                        &nbsp;
                    </Card.Grid>
                );
            }
        }
        return com;
    }

    //点击格子
    switchCategory(e){
        let [ r, c ] = e.target.dataset.category.split(",");
        r = Number(r);
        c = Number(c);
        let categories = removeDuplicate([this.categories[r], this.categories[c]]);
        this.setState({
            selectedRow: r,
            selectedCol: c
        });
        axios.get(`${NODE_PATH}/aggregation/template/search`, {
            params: { categories }
        }).then(res => {
            this.setState({
                templates: res.data.data
            });
        }).catch(err => {
            console.log(err);
        });
    }

    showModal(item){
        this.selectedComplateId = item._id;
        this.setState({
            modalVisible: true
        });
    }

    hideModal(){
        this.setState({
            modalVisible: false
        });
    }

    save(values){
        let form  = new FormData();
        form.append("title", values.title);
        form.append("cover", values.cover.file);
        form.append("description", values.description);
        form.append("templateId", this.selectedComplateId);

        axios.post(`${NODE_PATH}/aggregation/template/createTask`, form).then(res => {
            message.success(`聚合任务“${values.title}”已成功创建，即将跳转到“聚合任务”页面……`, 0.5, () => {
                this.props.history.push("./aggregation-Task");
            });
        }).catch(err => {
            console.log(err);
        });
    }
}

function Cover(props){
    let { cover } = props;
    return (
        <div className={style["card-cover"]}>
            <img alt="封面" className={style["cover-img"]} src={cover} />
        </div>
    );
}

function removeDuplicate(arr){
    if(Array.isArray(arr)){
        return [...new Set(arr)];
    }
    return [];
}

export default List;