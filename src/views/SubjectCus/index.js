import React, { Component } from "react";
import { Breadcrumb } from "antd";
import List from "./list";
import Operation from "./operation";
import Subject from "./subject";

class SubjectCus extends Component{
    constructor(props){
        super(props);
        this.state = {
            "mode": "list",        //模式，代表当前视图，可选值为"list"、"Task"、"subject"
            "item": null
        };
    }

    render(){
        let com;
        let { mode, item } = this.state;
        switch(mode){
            case "list":
                com = <List changeMode={this.changeMode.bind(this)} />;
                break;
            case "operation":
                com = <Operation changeMode={this.changeMode.bind(this, "list")} />;
                break;
            case "subject":
                com = <Subject changeMode={this.changeMode.bind(this, "list")} item={this.state.item} />;
                break;
            default:
                break;
        }

        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>数据资源</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">内容服务</Breadcrumb.Item>
                    {
                        mode !== "list" &&
                            <Breadcrumb.Item className="breadcrumb-item">{item ? item.title : "定制"}</Breadcrumb.Item>
                    }
                </Breadcrumb>
                { com }
            </div>
        );
    }

    changeMode(mode, item){
        this.setState({ mode, item });
    }
}

export default SubjectCus;
