const ITEMS = {
    "element": {
        "title": "内容列表",
        "searchCons": {
            "type": "类型",
            "featureValue": "值",
            "feature": "语义"
        },
        "cols": [
            {
                "title": "类型",
                "key": "type",
                "dataIndex": ["value", "type"],
                "align": "center"
            },{
                "title": "语义",
                "key": "feature",
                "dataIndex": ["value", "feature"],
                "align": "center"
            },{
                "title": "值",
                "key": "featureValue",
                "dataIndex": ["value", "featureValue"],
                "align": "center"
            }
        ]
    }
};

export { ITEMS };