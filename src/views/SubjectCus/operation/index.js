import React, { Component, Fragment } from "react";
import { Button, Form, Input, Select, Space, message, Modal } from "antd";
import { PlusOutlined, SearchOutlined, MinusCircleOutlined, RollbackOutlined, SaveOutlined } from "@ant-design/icons";
import axios from "axios";
import { TableRendering, Paragraph, CustomizeModal } from "@/components";
import { setHighlight } from "@/utility";
import { NODE_PATH } from "@/assets/global";
import { ITEMS } from "../config";
import style from "./index.module.css";
import { Model } from "echarts/src/export";


class Operation extends Component{
    constructor(props){
        super(props);

        this.state = {
            isVisible: false,
            loading: false,
            modalData: null
        };

        this.defaultUrl = `${NODE_PATH}/text/element/query/multiCons`;
        this.modalFormRef = React.createRef();
        this.tableRendering = React.createRef();
        this.searchFormRef = React.createRef();
        this.search = this.search.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.clickRow = this.clickRow.bind(this);
        this.hideCusModal = this.hideCusModal.bind(this);
        this.save = this.save.bind(this);
    }

    render(){
        const { cols, searchCons } = ITEMS["element"];
        let { isVisible, loading, modalData } = this.state;
        let layout = {
            labelCol: { offset:2, span:6 },
            wrapperCol: { span:12 }
        };
        return (
            <div className={style["container"]}>
                <div className={style["containerLeft"]}>
                    <div className={style["btnBox"]}>
                        <Button type="link" icon={<RollbackOutlined />} onClick={this.props.changeMode}>返回</Button>
                        <Button type="text" icon={<SaveOutlined />} onClick={this.showModal}>保存</Button>
                    </div>
                    <div className={style["formBox"]}>
                        <h5 className={style["formBox-h5"]}>
                            查询条件设置
                        </h5>
                        <Form className={style["formBox-form"]} onFinish={this.search} ref={this.searchFormRef}>
                            <Form.List name="names">
                                {
                                    (fields, { add, remove }, { errors }) => (
                                        <>
                                            {
                                                fields.map((field, index) => (
                                                    <Space key={index} direction="horizontal" size="middle" align="baseline">
                                                        <Form.Item
                                                            label="条件"
                                                            name={[field.name, "condition"]}
                                                            fieldKey={[field.key, "condition"]}
                                                        >
                                                            {/* <Select options={searchCons} style={{width:"100px"}} value="type" /> */}
                                                            <Select style={{width:"100px"}}>
                                                                {
                                                                    Object.keys(searchCons).map(i => (<Select.Option value={i}>{searchCons[i]}</Select.Option>))
                                                                }
                                                            </Select>
                                                        </Form.Item>
                                                        <Form.Item
                                                            label="关键词"
                                                            name={[field.name, "keywords"]}
                                                            fieldKey={[field.key, "keywords"]}
                                                        >
                                                            <Input style={{width:"100px"}} />
                                                        </Form.Item>
                                                        <MinusCircleOutlined
                                                            className="dynamic-delete-button"
                                                            onClick={() => remove(field.name)}
                                                        />
                                                    </Space>
                                                ))
                                            }
                                            <Form.Item>
                                                <Button type="dashed" onClick={() => { if(fields.length > 9){ message.warn("最多可添加10个条件"); return; } add(); }} block icon={<PlusOutlined />}>
                                                    添加条件
                                                </Button>
                                            </Form.Item>
                                            <Form.Item>
                                                <Button type="primary" block icon={<SearchOutlined />} htmlType="submit">
                                                    查询
                                                </Button>
                                            </Form.Item>
                                        </>
                                    )
                                }
                            </Form.List>
                        </Form>
                    </div>
                </div>
                <div className={style["containerRight"]}>
                    <h5 className={style["containerRight-h5"]}>内容列表</h5>
                    <TableRendering ref={this.tableRendering} cols={cols} url={this.defaultUrl} clickRow={this.clickRow} />
                </div>
                {/*弹窗-保存*/}
                <Modal
                    visible={isVisible}
                    title="保存"
                    onCancel={this.hideModal}
                    onOk={this.hideModal}
                    footer={[
                        <Button type="default" onClick={this.hideModal}>
                            取消
                        </Button>,
                        <Button type="primary" loading={loading} onClick={() => this.modalFormRef.current.submit()}>
                            保存
                        </Button>
                    ]}
                >
                    <Form  {...layout} onFinish={this.save} ref={this.modalFormRef}>
                        <Form.Item 
                            label="专题名称"
                            name="title"
                            rules={[{required:true}]}
                        >
                            <Input />
                        </Form.Item>
                    </Form>
                </Modal>
                {/*弹窗-详细信息*/}
                <CustomizeModal
                    visible={ Boolean(modalData) }
                    close={ this.hideCusModal }
                    title={ modalData && `${modalData.value.type}-${modalData.value.feature}` }
                >
                    <Paragraphs {...modalData} />
                </CustomizeModal>
            </div>
        );
    }

    search(values){
        if(!values.names){
            this.tableRendering.current.search(this.defaultUrl, ITEMS.element.cols);
            return;
        }
        
        let conditions = getConditions(values);
        if(conditions.length === 0){
            this.tableRendering.current.search(this.defaultUrl, ITEMS.element.cols);
        }
        else{
            this.tableRendering.current.search(this.defaultUrl, ITEMS.element.cols, { "conditions": JSON.stringify(conditions) });
        }
    }

    showModal(){
        this.setState({
            isVisible: true
        });
    }

    hideModal(){
        this.setState({
            isVisible: false
        });
    }

    clickRow(record){
        this.setState({
            modalData: record
        });
    }

    hideCusModal(){
        this.setState({
            modalData: null
        });
    }

    save(){
        this.setState({
            loading: true
        });

        let cons = [], item;
        let fields = this.searchFormRef.current.getFieldsValue(true).names;
        if(fields){
            for(item of fields){
                if(item && item.condition && item.keywords){
                    cons.push(item);
                }
            }
        }
        
        axios.post(`${NODE_PATH}/custom/element/add`, {
            title: this.modalFormRef.current.getFieldValue("title"),
            conditions: cons
        }).then(res => {
            message.success("保存成功", 0.5, () => this.props.changeMode());
        }).catch(err => {
            console.log(err);
            this.setState({loading: false});
            message.error("保存失败");
        });
    }
}

function getConditions(formFields){
    let conditions = [], temp, item;
    for (item of formFields.names) {
        if (item.condition && item.keywords) {
            temp = {};
            temp[item.condition] = item.keywords;
            conditions.push(temp);
        }
    }
    return conditions;
}

function Paragraphs(props){
    return (
        <Fragment>
            <Paragraph title="来源：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.source} />
            <Paragraph title="时间：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.recordDatetime} />
            <Paragraph title="摘要：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.description} />
            <Paragraph title="标签名：" content={props.tagid && props.tagid.tagName} />
            <Paragraph title="标签值：" content={props.tagid && props.tagid.tagValue} />
            <Paragraph title="标签类型：" content={props.value && props.value.type} />
            <Paragraph title="标签语义：" content={props.value && props.value.feature} />
            <Paragraph title="内容：" content={props.tagid && setHighlight(props.tagid.metaId.content, props.tagid.startPosition, props.tagid.endPosition)} />
        </Fragment>
    );
}

export default Operation;