import React, { useState, useEffect } from "react";
import { Card, message } from "antd";
import axios from "axios";
import { NODE_PATH } from "@/assets/global";
import style from "./index.module.css";
import addIcon from "@/assets/image/icon_add.png";
import peopleIcon from "@/assets/image/E-People.png";

function List(props){
    let [subjects, setSubjects] = useState([]);

    useEffect(() => {
        axios.get(`${NODE_PATH}/custom/element/search`).then(res => {
            setSubjects(res.data.data);
        }).catch(err => {
            console.log(err);
            message.error("查询专题信息失败");
        });
    }, []);

    return (
        <div className={style["wrapper"]}>
            <Card
                hoverable={true}
                className={style["card"]}
                cover={<img src={addIcon} className={style["card-cover"]} />}
                onClick={() => props.changeMode("operation")}
            >
                <p className={style["card-title"]}>定制</p>
            </Card>
            {
                subjects.map(item => (
                    <Card
                        key={item._id}
                        hoverable={true}
                        className={style["card"]}
                        cover={<img src={peopleIcon} className={style["card-cover"]} />}
                        onClick={() => props.changeMode("subject", item)}
                    >
                        <p className={style["card-title"]}>{item.title}</p>
                    </Card>
                ))
            }
        </div>
    );
}

export default List;