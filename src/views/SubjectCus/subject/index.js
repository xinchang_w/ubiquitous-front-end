import { useState, Fragment } from "react";
import { Button, Form, Input, Space } from "antd";
import { RollbackOutlined } from "@ant-design/icons";
import { TableRendering, Paragraph, CustomizeModal } from "@/components";
import { setHighlight } from "@/utility";
import { NODE_PATH } from "@/assets/global";
import { ITEMS } from "../config";
import style from "./index.module.css";

function Subject(props){
    let [modalData, setModalData] = useState(null);
    let queryCon = [], con, temp;
    for(con of props.item.conditions){
        temp = {};
        temp[con.condition] = con.keywords;
        queryCon.push(temp);
    }

    function clickRow(record){
        setModalData(record);
    }

    function hideModal(){
        setModalData(null);
    }

    return (
        <div className={style["container"]}>
            <div className={style["containerLeft"]}>
                <div className={style["btnBox"]}>
                    <Button type="link" icon={<RollbackOutlined />} onClick={props.changeMode}>返回</Button>
                </div>
                <div className={style["formBox"]}>
                    <h5 className={style["formBox-h5"]}>
                        查询条件
                    </h5>
                    <CusForm conditions={props.item.conditions} />
                </div>
            </div>
            <div className={style["containerRight"]}>
                <h5 className={style["containerRight-h5"]}>内容列表</h5>
                <TableRendering 
                    cols={ITEMS.element.cols} 
                    url={`${NODE_PATH}/text/element/query/multiCons`} 
                    queryCon={{conditions:JSON.stringify(queryCon)}}
                    clickRow={clickRow}
                />
            </div>
            <CustomizeModal
                visible={ Boolean(modalData) }
                close={ hideModal }
                title={ modalData && `${modalData.value.type}-${modalData.value.feature}` }
            >
                <Paragraphs {...modalData} />
            </CustomizeModal>
        </div>
    );
}

function CusForm(props){
    let { conditions } = props;
    if(!Array.isArray(conditions) || conditions.length === 0){
        return (
            <p style={{textAlign:"center"}}>空</p>
        );
    }

    return (
        <Form className={style["formBox-form"]}>
            {
                conditions.map(item => (
                    <Space key={item._id}>
                        <Form.Item label="条件">
                            <Input defaultValue={ITEMS.element.searchCons[item.condition]} disabled />
                        </Form.Item>
                        <Form.Item label="关键词">
                            <Input defaultValue={item.keywords} disabled />
                        </Form.Item>
                    </Space>
                ))
            }
        </Form>
    );
}

function Paragraphs(props){
    return (
        <Fragment>
            <Paragraph title="来源：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.source} />
            <Paragraph title="时间：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.recordDatetime} />
            <Paragraph title="摘要：" content={props.tagid && props.tagid.metaId && props.tagid.metaId.description} />
            <Paragraph title="标签名：" content={props.tagid && props.tagid.tagName} />
            <Paragraph title="标签值：" content={props.tagid && props.tagid.tagValue} />
            <Paragraph title="标签类型：" content={props.value && props.value.type} />
            <Paragraph title="标签语义：" content={props.value && props.value.feature} />
            <Paragraph title="内容：" content={props.tagid && setHighlight(props.tagid.metaId.content, props.tagid.startPosition, props.tagid.endPosition)} />
        </Fragment>
    );
}

export default Subject;