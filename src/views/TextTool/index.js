import './index.css'
import React, { useContext, useState, useEffect, useRef } from 'react'
import { Layout, List, Divider, Row, Col, Input, Button, Typography, Tabs, Table, Popconfirm, Form, Tooltip, Modal, Space, message, Upload, Card, Tag, Select, Carousel} from 'antd';
import ColorPicker from "./colorPicker"
import axios from 'axios';
import TableRendering from './originDataTable'
import { BASE_PATH } from "@/assets/global/index";
import Paragraph from 'antd/lib/skeleton/Paragraph';
import DataServerTool from '../../components/DataServerTool/Tool'

const {Option} = Select
const { TextArea } = Input;
const { Title } = Typography;
const { Header, Content, Sider } = Layout;
const { TabPane } = Tabs;
const {Search} = Input;
const contentStyle = {
  height: '300px',
  color: '#fff',
  lineHeight: '300px',
  textAlign: 'center',
  background: '#364d79',
};
const buttonLeft = {
  marginLeft: '10px'
}

const borderStyle = {
  border: '1px solid #d9d9d9',
  borderRadius: '2px'
}

// 类型标签中添加新行
const EditableContext = React.createContext(null);

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  text,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef(null);
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        <Tooltip placement="bottomLeft" title={text}>
        {children}
        </Tooltip> 
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

// 添加新的文本的表单
function UploadTextModal(props) {
  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };

  const [form] = Form.useForm();

  const nowTime = () => {//获取当前时间
    let now= new Date();
    let _month = ( 10 > (now.getMonth()+1) ) ? '0' + (now.getMonth()+1) : now.getMonth()+1;
    let _day = ( 10 > now.getDate() ) ? '0' + now.getDate() : now.getDate();
    let _hour = ( 10 > now.getHours() ) ? '0' + now.getHours() : now.getHours();
    let _minute = ( 10 > now.getMinutes() ) ? '0' + now.getMinutes() : now.getMinutes();
    let _second = ( 10 > now.getSeconds() ) ? '0' + now.getSeconds() : now.getSeconds();
    return now.getFullYear() + '-' + _month + '-' + _day + ' ' + _hour + ':' + _minute + ':' + _second;
  }

  const onFinish = (values) => {
    console.log('upload text')
    values.content = props.content
    values.recordDatetime = nowTime()
    let metaData;
    axios({   // 原数据入库
      url: `${BASE_PATH}/GenericMap/meta`,
      method: 'POST',
      data: values,
    }).then(res => {
      console.log('res: ', res)
      metaData = res.data.data
      if(res.data.code === 0) {
        message.success('提交成功')
        axios({   // 创建一个类型标签
          url:  `${BASE_PATH}/GenericMap/typeTag`,
          method: 'POST',
          data: {
            metaId: metaData.id,
            metaSource: 'ULA_MetaData_Text'
          }
        }).then(console.log('元数据和类型标签创建成功'))
        props.setNowTextMetaId(metaData.id)
      }
    })
  }

  const onReset = () => {
    form.resetFields()
  }

  return(
  <Modal
  title='添加新图片'
  visible={props.visible}
  onOk={props.handleOk}
  onCancel={props.handleCancel}>
    <Form {...layout} form={form} onFinish={onFinish} encType="multipart/form-data">
    <Form.Item 
        name='description'
        label='数据描述'
        rules={[{required: true}]}
        ><Input placeholder='eg: 文章标题' />
    </Form.Item>
    <Form.Item 
        name='source'
        label='数据源'
        rules={[{required: true}]}
        ><Input placeholder='eg: 中国新闻网' />
    </Form.Item>
    <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
        提交
        </Button>
        <Button htmlType="button" onClick={onReset}>
        重置
        </Button>
    </Form.Item>
    </Form>
  
  </Modal>
  )
}

// 添加已有的文本
function AddTextModal (props) {
  const handleDelete = (id) => {
    console.log('delete text : ', id)
    axios({
      url: `${BASE_PATH}/GenericMap/meta/${id}`,
      method: 'DELETE'
    }).then(res => {
      console.log('res: ', res)
      if(res.status === 200) {
        message.info('已删除这条文本')
      } else {
        message.info('删除失败')
      }
    }).catch(err => message.info('删除失败: ', err))
  }
  const columns = [
    {
      title: '文本标题',
      dataIndex: 'description',
      key: 'description',
      align:'center',
      ellipsis: true
    },
    {
      title: '操作',
      dataIndex: 'Action',
      key: 'Action',
      align:'center',
      render: (_, record) => {
        return(
          <div>
            <Button type='primary' onClick={() => props.setNowFile(record)}>加载文本</Button>
            <Popconfirm title="确认删除吗?" onConfirm={() => handleDelete(record.id)}>
              <Button>删除</Button>
                </Popconfirm>
          </div>
        )
      }
    }
  ];

  return(
    <Modal
    className='textModal'
    title='添加已有文本'
    visible={props.visible}
    onOk={props.handleOk}
    onCancel={props.handleCancel}>
        <Space direction='horizontal'>
        <Input 
            id='searchText'
            className="searchBox-input" 
            placeholder="关键词" 
          />
          <Button 
              type="primary" 
              className="searchBox-btn" 
              onClick={props.search}
          >
              查询
          </Button>
        </Space>
        <Divider />
          <TableRendering cols={columns} url={props.url} />
    </Modal>
  )
}


class TextTool extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      nowTag: 'type',
      urlFileList: `${BASE_PATH}/GenericMap/meta/all`,
      nowTextMetaId: '',
      nowTextId: '',
      nowText: '',

      uploadTextVisible: false,
      uplodFile: '',
      addTextVisible: false,

      onMouseDownTimeoutFlag: null,     // 文本框的一些属性
      textAreaSelectValue: '',

      selectData: '',   // 三种标签的值
      categoryList: [],
      structrueTagList: [],
      contentTagList: [],    

      typeColumns: [
        {
          title: '可视化',
          align: 'center',
          width: '25%',
          dataIndex: 'color',
          render:(_, record) =>
          this.state.categoryList.length >= 1 ? (
            <div style={{display: 'inline-block'}}>
            <ColorPicker color={this.hexToRgba(record.color)} categoryListKey={record.key} type={this.nowTag} handleColorChange={this.handleTypeColorChange} />
            </div>

          ) : null,
        },
        {
          title: '分类标志',
          dataIndex: 'type',
          width: '25%',
          align: 'center',
          editable: true,
        },
        {
          title: '分类结果',
          dataIndex: 'value',
          width: '25%',
          align: 'center',
          editable: true,
        },
        {
          title: '操作',
          width: '25%',
          align: 'center',
          dataIndex: 'operation',
          render: (_, record) =>
            this.state.categoryList.length >= 1 ? (
              <Popconfirm title="确认删除吗?" onConfirm={() => this.typeHandleDelete(record.key)}>
                <Button>删除</Button>
              </Popconfirm>
            ) : null,
        }
      ],

      structureColumns: [
        {
          title: '可视化',
          align: 'center',
          width: '30%',
          dataIndex: 'color',
          render:(_, record) =>
          this.state.categoryList.length >= 1 ? (
            <div style={{display: 'inline-block'}}>
              <div style={{float: 'left'}}>
              <ColorPicker style={{float: 'left'}} color={this.hexToRgba(record.color)} categoryListKey={record.key} type={this.nowTag} handleColorChange={this.handleStructureColorChange} />
              </div>
            <Button type='primary' size='small' onClick={() => this.setHighlight(record.isShow, record.startPosition, record.endPosition, record.color, this.state.allowInsert1)}>可视化</Button>
            </div>

          ) : null,
        },
        {
          title: '结构类型',
          align: 'center',
          dataIndex: 'tagType',
          key: 'tagType',
          width: '20%',
          ellipsis: true,
          render: (_, record) => {
            return(
              <select defaultValue={record.tagType} id={'selectValue'+record.key} onChange={() => this.changeSelect(record)}>
                <option value='word'>word</option>
                <option value='sentence'>sentence</option>
                <option value='paragraph'>paragraph</option>
              </select>
            )
          }
        },
        {
          title: '值',
          dataIndex: 'tagValue',
          align: 'center',
          width: '20%',
          ellipsis: true,
        },
        {
          title: 'operation',
          width: '30%',
          alight: 'center',
          dataIndex: 'operation',
          render: (_, record) =>
            this.state.structrueTagList.length >= 1 ? (
              <div>
                <Button type='primary' size='small' style={{marginLeft: '4px'}} onClick={() => {
                  this.setState({
                    row: record
                  })
                  this.state.startSelectValue = !this.state.startSelectValue
                  message.info('请从文本框中选择对应值')
                }}>选择值</Button>
                <Button type='primary' size='small' style={{marginLeft: '4px'}} onClick={() => this.structureHandleSave(record)}>保存</Button>
                <Button size='small' style={{marginLeft: '4px'}} onClick={() => this.structureHandleDelete(record)}>删除</Button>
              </div>
            ) : null,
        },
      ],

      contentColumns: [
        {
          title: '可视化',
          align: 'center',
          width: '30%',
          dataIndex: 'color',
          render:(_, record) =>
          this.state.categoryList.length >= 1 ? (
            <div style={{display: 'inline-block'}}>
              <div style={{float: 'left'}}>
              <ColorPicker color={this.hexToRgba(record.color)} categoryListKey={record.key} type={this.nowTag} handleColorChange={this.handleContentColorChange} />
              </div>
            <Button type='primary' size='small' onClick={() => this.setHighlight(record.isShow, record.startPosition, record.endPosition, record.color, this.state.allowInsert2)}>可视化</Button>
            </div>

          ) : null,
        },
        {
          title: '内容类型',
          dataIndex: 'tagName',
          width: '20%',
          align: 'center',
          editable: true,
          ellipsis: true
        },
        {
          title: '值',
          dataIndex: 'tagValue',
          width: '20%',
          align: 'center',
          ellipsis: true
        },
        {
          title: 'operation',
          dataIndex: 'operation',
          width: '30%',
          align: 'center',
          render: (_, record) =>
            this.state.contentTagList.length >= 1 ? (
              <div>
                <Button type='primary' size='small' style={{marginLeft: '4px'}} onClick={() => {
                  this.setState({
                    row: record
                  })
                  this.state.startSelectValue = !this.state.startSelectValue
                  message.info('请从文本框中选择对应值')
                }}>选择值</Button>
                <Button type='primary' size='small' style={{marginLeft: '4px'}} onClick={() => this.contentHandleSave(record)}>保存</Button>
                <Button size='small' style={{marginLeft: '4px'}} onClick={() => this.contentHandleDelete(record)}>删除</Button>
              </div>
            ) : null,
        },
      ],

      // 为了实现多个可视化效果
      allowInsert1: [],
      allowInsert2: [],
    }

    this.typeHandleSave = this.typeHandleSave.bind(this)
    this.uploadHandleSet = this.uploadHandleSet.bind(this)
    this.addHandleSet = this.addHandleSet.bind(this)
    this.beforeUpload = this.beforeUpload.bind(this)
    this.setNowFile = this.setNowFile.bind(this)
    this.searchText = this.searchText.bind(this)
    this.handleTypeColorChange = this.handleTypeColorChange.bind(this)
    this.handleStructureColorChange = this.handleStructureColorChange.bind(this)
    this.handleContentColorChange = this.handleContentColorChange.bind(this)
    this.structureHandleChange = this.structureHandleChange.bind(this)
    this.structureHandleSave = this.structureHandleSave.bind(this)
    this.structureHandleDelete = this.structureHandleDelete.bind(this)
    this.changeSelect = this.changeSelect.bind(this)
    this.contentHandleChange2 = this.contentHandleChange2.bind(this)
    this.setNowTextMetaId = this.setNowTextMetaId.bind(this)
  }

  componentDidMount() {

  }

  init() {
    this.setState({
      nowType: 'type',
      nowText: '',
      nowTextId: '',
      categoryList: [],
      structrueTagList: [],
      contentTagList: [],  
      allowInsert1: [],
      allowInsert2: [],
    })
  }

  setNowTextMetaId(id) {
    this.setState({
      nowTextMetaId: id
    })
  }

  onMouseDown() {
    if(this.state.nowTag === 'type' || !this.state.startSelectValue) return
    if(this.state.onMouseDownTimeoutFlag != null){
      clearTimeout(this.state.onMouseDownTimeoutFlag)
    }
    this.state.onMouseDownTimeoutFlag = setTimeout(message.info('开始选择值'), 1000)
  }

  selectValue(e) {
    if(this.state.nowTag === 'type' || !this.state.startSelectValue) return
    console.log('select value')
    let res = {}
    if(window.getSelection) {
      let data = window.getSelection()
      console.log('data: ', data)
      let position1 = data.anchorOffset
      let position2 = data.focusOffset
      res['startPosition'] = position1 < position2 ? position1 : position2
      res['endPosition'] = position1 > position2 ? position1 : position2
      res['value'] = data.toString()
      console.log('res: ', res)
      if(res['value'] != '') {
        message.info('已勾选完值：', res['value'])
        if(this.state.nowTag === 'structure')
          this.structureHandleChange(res)
        else
          this.contentHandleChange(res)
      }
    } else {
      window.event? window.event.cancelBubble = true : e.stopPropagation(); // 当选中内容为空时，阻止事件发生
    }
  }

  searchText() {
    console.log('search text')
    let url = "";
    let value = document.getElementById('searchText').value
    if(value){
        url = `${BASE_PATH}/GenericMap/meta/description/${value}`;
        console.log('url: ', url)
    }
    else{
        url = `${BASE_PATH}/GenericMap/meta/all`
    }
    this.setState({
      urlFileList: url
    });
  }

  setNowFile(record) {
    console.log('load text: ', record)
    this.init()
    this.setState({
      nowTextMetaId: record.id,
      nowText: record.content,
      record: record
    })
    this.getTypeTag(record.id)
    this.getStructureTag(record.id)
    this.getcontentTag(record.id)
  }

  getTypeTag(id) {
    console.log('get type tag: ', `${BASE_PATH}//GenericMap/typeTag/metaId/${id}/1/1`)
    let result = []
    let num = 0
    let that = this
    axios({
      url: `${BASE_PATH}//GenericMap/typeTag/metaId/${id}/1/1`,
      method: 'GET'
    }).then(res => {
      if(res.data.code === 0) {
        console.log('res: ', res)
        if(res.data.data.length === 0){
          message.info('该文本没有类型标签')  // 没有类型标签就重新入库
          this.setState({
            categoryList: [],
          })
        } else{
          let data = res.data.data[0].categoryList
          if(data instanceof Object) {
            Object.keys(data).forEach(key => result.push({
              key: num++ + '',    // num 需要转为字符串，转完之后还得自增
              type: key,
              color: that.getRandomColor(),
              value: data[key],
              isShow: false   // 判断该 item 是否已经可视化
            }))
            this.setState({
              categoryList: result,
              nowTextId: res.data.data[0].id
            })
          } else {
            console.log('读取类型标签出错')
          }
        }
      }
    })
  }

  getStructureTag(id) {
    console.log('get structure tag:', `${BASE_PATH}//GenericMap/structureTag/metaId/${id}`)
    let that = this
    axios({
      url: `${BASE_PATH}//GenericMap/structureTag/metaId/${id}`,
      method: 'GET'
    }).then(res => {
      if(res.data.code === 0) {
        if(res.data.data.length ===0) {
          message.info('该文本没有结构标签')
        }
        let key = 0;
        for(let i of res.data.data) {   // table 必须要有一个 key 属性
          i['key'] = key++ + ''
          i['color'] = that.getRandomColor()
          i['isShow'] = false
        }
        that.setState({
          structrueTagList: res.data.data
        })
      }
    }).catch(err => {
      message.info('该文本没有结构标签')
      console.log('acquire structure tag err: ', err)
    })
  }

  getcontentTag(id) {
    console.log('get content tag: ', `${BASE_PATH}//GenericMap/contentTag/metaId/${id}`)
    let that = this
    axios({
      url: `${BASE_PATH}//GenericMap/contentTag/metaId/${id}`,
      method: 'GET'
    }).then(res => {
      if(res.data.code === 0) {
        if(res.data.data.length ===0) {
          message.info('该文本没有内容标签')
        }
        let key = 0;
        for(let i of res.data.data) {   // table 必须要有一个 key 属性
          i['key'] = key++ + ''
          i['color'] = that.getRandomColor()
          i['isShow'] = false
        }
        that.setState({
          contentTagList: res.data.data
        })
      }
    }).catch(err => {
      message.info('该文本没有内容标签')
      console.log('acquire content tag err: ', err)
    })
  }

  tagToText() {
    if(this.state.nowTag == 'type') return '类型标签'
    else if(this.state.nowTag == 'structure') return '结构标签'
    else return '内容标签'
  }

  typeHandleDelete(key) {
    const categoryList = this.state.categoryList
    this.setState({
      categoryList: categoryList.filter((item) => item.key !== key)
    })
  }

  typeHandleAdd() {
    if(this.state.nowTextMetaId === '') {
      message.info('文本请先入库')
      return
    }
    console.log('add type tag')
    const categoryList = this.state.categoryList
    const newData = {
      key: categoryList.length + '',
      type: 'eg: 体裁',
      color: this.getRandomColor(),
      value: 'eg: 日常记叙'
    }
    this.setState({
      categoryList: [...categoryList, newData]
    })
  }

  typeHandleSave(row) {
    const categoryList = this.state.categoryList
    const index = categoryList.findIndex((item) => row.key === item.key)
    const item = categoryList[index]
    categoryList.splice(index, 1, {...item, ...row})
    this.setState({
      categoryList: [...categoryList]
    })
  }

  typeSubmit() {
    console.log('save type tag')
    const that = this;
    let categoryList = {}
    for (let temp of this.state.categoryList) {
      categoryList[temp.type] = temp.value
    }
    axios({   // 查询库里面有没有标签，没有先入库才能修改
      url: `${BASE_PATH}//GenericMap/typeTag/metaId/${that.state.nowTextMetaId}/1/1`,
      method: 'GET',
    }).then(res => {
      if(!res.data.data) {
        console.log('post')
        axios({
          url: `${BASE_PATH}/GenericMap/typeTag`,
          method: 'POSt',
          data: {
            metaId: that.state.nowTextMetaId,
            metaSource: "ULA_MetaData_Text",
            categoryList: categoryList,
            metaInfo: that.state.record
          }
        }).then(res => {
          if(res.data.code === 0) {
            message.success('保存类型标签成功')
          }
        }).catch(err => {
          message.error('保存类型标签失败： ', err)
        })
      } else {
        console.log('put')
        axios({
          url: `${BASE_PATH}/GenericMap/typeTag`,
          method: 'PUT',
          data: {
            id: this.state.nowTextId,
            categoryList: categoryList
          }
        }).then(res => {
          console.log('put res: ', res)
          if(res.data.code === 0) {
            message.success('保存类型标签成功')
          }
        }).catch(err => {
          message.error('保存类型标签失败： ', err)
        })
      }
    })
  }

  changeSelect(record) {
    console.log('change select value')
    let select = document.getElementById('selectValue' + record.key)
    record.tagType = select[select.selectedIndex].value
  }

  structureHandleDelete(record) {
    console.log('delete structure tag: ', `${BASE_PATH}/GenericMap/structureTag/${record.id}`)
    const structrueTagList = this.state.structrueTagList
    axios({
      url: `${BASE_PATH}/GenericMap/structureTag/${record.id}`,
      method:'DELETE',
    }).then(res => {
      if(res.data.code === 0){
        message.success('已删除该标签')
        this.setState({
          structrueTagList: structrueTagList.filter((item) => item.key !== record.key)
        })
      }
    }).catch(err => message.error('删除标签失败：', err))
  }

  structureHandleAdd() {
    if(this.state.nowTextMetaId === '') {
      message.info('文本请先入库')
      return
    }
    console.log('add structure tag')
    const structrueTagList = this.state.structrueTagList
    const newData = {
      key: structrueTagList.length + '',
      metaId: this.state.nowTextMetaId,
      metaSource: 'ULA_MetaData_Text',
      tagType: 'word',
      tagValue: 'eg: 包子',
      startPosition: '-1',
      endPosition: '-1',
      color: this.getRandomColor(),
    }
    this.setState({
      structrueTagList: [...structrueTagList, newData]
    })
  }

  structureHandleChange(res) {
    console.log('change structure value')
    let row = this.state.row
    row['metaId'] = this.state.nowTextMetaId
    row['metaSource'] = 'ULA_MetaData_Text'
    row['tagValue'] = res.value
    row['startPosition'] = res.startPosition
    row['endPosition'] = res.endPosition
    const structrueTagList = this.state.structrueTagList
    const index = structrueTagList.findIndex((item) => row.key === item.key)
    console.log('data: ', row)
    structrueTagList.splice(index, 1, row)
    this.setState({
      structrueTagList: [...structrueTagList]
    })
    this.state.startSelectValue = !this.state.startSelectValue
  }

  structureHandleSave(row) {
    console.log('save structure tag')
    if(row.id) {
      axios({
        url: `${BASE_PATH}/GenericMap/structureTag`,
        method: 'PUT',
        data: row
      }).then(res => {
        if(res.data.code === 0) {
          message.info('保存结构标签成功')
          this.getStructureTag(this.state.nowTextMetaId)
        }
      }).catch(err => message.info('保存结构标签失败：', err))
    } else {
      axios({
        url: `${BASE_PATH}/GenericMap/structureTag`,
        method: 'POST',
        data: row
      }).then(res => {
        if(res.data.code === 0) {
          message.info('保存结构标签成功')
          this.getStructureTag(this.state.nowTextMetaId)
        }
      }).catch(err => message.info('保存结构标签失败：', err))
    
    }
  }

  structureSubmit() {
    console.log('save structure tag')
  }

  structureCancel() {
    this.getStructureTag(this.state.nowTextMetaId)
  }

  contentHandleDelete(record) {
    const contentTagList = this.state.contentTagList
    axios({
      url: `${BASE_PATH}/GenericMap/contentTag/${record.id}`,
      method:'DELETE'
    }).then(res => {
      if(res.data.code === 0){
        message.success('已删除该标签')
        this.setState({
          contentTagList: contentTagList.filter((item) => item.key !== record.key)
        })
      }
    }).catch(err => message.error('删除标签失败：', err))
  }

  contentHandleAdd() {
    if(this.state.nowTextMetaId === '') {
      message.info('文本请先入库')
      return
    }
    console.log('add content tag')
    const contentTagList = this.state.contentTagList
    const newData = {
      key: contentTagList.length + '',
      color: this.getRandomColor(),
      metaId: this.state.nowTextMetaId,
      metaSource: 'ULA_MetaData_Text',
      tagType: 'Text',
      tagName: 'eg: location',
      tagValue: 'eg: 大凉山',
      startPosition: '-1',
      endPosition: '-1',
    }
    this.setState({
      contentTagList: [...contentTagList, newData]
    })
  }

  contentHandleChange(res) {
    console.log('change content value')
    let row = this.state.row
    console.log('row: ', row)
    row['tagValue'] = res.value
    row['startPosition'] = res.startPosition
    row['endPosition'] = res.endPosition
    const contentTagList = this.state.contentTagList
    const index = contentTagList.findIndex((item) => row.key === item.key)
    const item = contentTagList[index]
    contentTagList.splice(index, 1, row)
    this.setState({
      contentTagList: [...contentTagList]
    })
    this.state.startSelectValue = !this.state.startSelectValue
  }

  contentHandleChange2(row) {
    const contentTagList = this.state.contentTagList
    const index = contentTagList.findIndex((item) => row.key === item.key)
    const item = contentTagList[index]
    contentTagList.splice(index, 1, {...item, ...row})
    this.setState({
      contentTagList: [...contentTagList]
    })
  }

  contentHandleSave(row) {
    console.log('save content tag')
    if(row.id) {
      axios({
        url: `${BASE_PATH}/GenericMap/contentTag`,
        method: 'PUT',
        data: row
      }).then(res => {
        if(res.data.code === 0) {
          message.info('保存内容标签成功')
          this.getcontentTag(this.state.nowTextMetaId)
        }
      }).catch(err => message.info('保存内容标签失败：', err))
    }
    else {
      axios({
        url: `${BASE_PATH}/GenericMap/contentTag`,
        method: 'POST',
        data: row
      }).then(res => {
        if(res.data.code === 0) {
          message.info('保存内容标签成功')
          this.getcontentTag(this.state.nowTextMetaId)
        }
      }).catch(err => message.info('保存内容标签失败：', err))
    }
  }

  contentCancel() {
    this.getcontentTag(this.state.nowTextMetaId)
  }

  uploadHandleSet() {
    if(this.state.uploadTextVisible === false && this.state.nowText === '') {
      message.info('文本框内没有文字，无法入库')
      return
    }
    this.setState({
      uploadTextVisible: !this.state.uploadTextVisible
    })
  }

  addHandleSet() {
    this.setState({
      addTextVisible: !this.state.addTextVisible
    })
  }

  beforeUpload(file) {
    console.log(file.type)
    let that = this
    console.log("nowText: ", that.state.nowText)
    const isTextFile = file.type == 'application/json' || file.type == 'text/plain' ||file.type == 'text/xml'
    if (!isTextFile) {
      message.error(`${file.name} 不是文本文件, 请选择 txt、json 或 xml 格式的文件`);
    }
    const isLt5M = file.size / 1024 / 1024 < 5;
    if (!isLt5M) {
      message.error('文件大小不能超过 5MB!');
    }
    if(isTextFile && isLt5M) {
      let reader = new FileReader()
      reader.onload = () => {
        if(reader.result)
          that.setState({
            nowText: reader.result
          })
        console.log("nowText: ", that.state.nowText)
      }
      reader.readAsText(file)
      message.success('读取文件成功！')
      this.init()
      return true
    } else {
      return false
    }
  }


  // 开始写可视化的部分了
  getRandomColor(type){   // 随机生成一个颜色
    var color = '#';  
    for(var i=0;i<6;i++){
      color += (Math.random()*16 | 0).toString(16);
    }
    return color
  }

  hexToRgba(hex) {
    let res = {}
    res.r = parseInt('0x' + hex.slice(1, 3))   // 将 hex 转换为 rgba
    res.g = parseInt('0x' + hex.slice(3, 5))
    res.b = parseInt('0x' + hex.slice(5, 7))
    res.a = 0.9
    return res;
  }

  handleTypeColorChange(key, color) {
    let list = this.state.categoryList
    let index = parseInt(key)
    let item = this.state.categoryList[index] 
    item.color = color
    list.splice(index, 1, item)
    this.setState({
      categoryList: list
    })
  }

  handleStructureColorChange(key, color) {
    let list = this.state.structrueTagList
    let index = parseInt(key)
    let item = this.state.structrueTagList[index] 
    item.color = color
    list.splice(index, 1, item)
    this.setState({
      structrueTagList: list
    })
  }

  handleContentColorChange(key, color) {
    let list = this.state.contentTagList
    let index = parseInt(key)
    let item = this.state.contentTagList[index] 
    item.color = color
    list.splice(index, 1, item)
    this.setState({
      contentTagList: list
    })
  }

  setBorderBottom(startPos, endPos, color) {   // 传入参数，设置下划线
    console.log('set underline')
    let s = Number(startPos);
    let e = Number(endPos);
    const textArea = document.getElementById('textArea')
    let str = textArea.innerHTML;
    if(isNaN(s) || isNaN(e) || s >= e || s<0 || str.length < e){
      message.info('参数有误')
      return
    }
    let subStr = str.slice(s - 1, e);
    let resStr = str.replace(subStr, `<span style="border-bottom: 1px solid ${color}; padding-bottom: 2px">${subStr}</span>`);
    textArea.innerHTML = resStr
  }



  setHighlight(isShow, startPos, endPos, color, array){    // 传入参数，对对应的文字染色
    if(isShow) {
      message.info('已经可视化过了')
      return
    }
    console.log('set heighLight')
    let str = this.state.nowText;
    let startPosition = Number(startPos);
    let endPosition = Number(endPos);
    let i = 0
    if(isNaN(startPosition) || isNaN(endPosition) || startPosition >= endPosition || startPosition<0){
      message.info('参数有误')
      return
    }

    if(array.length === 0) {
      array.push(0, str.length)
    } else {
      for(i=0; i < array.length; i += 2) {
        if(startPosition >= array[i] && endPosition <= array[i+1]) break
      }
      if(i >= array.length){
        message.info('可视化部分重叠，请先清楚可视化效果')
        return
      }
    }

    let textArea = document.getElementById('textArea')
    let resStr = textArea.innerHTML
    let subStr = str.slice(startPosition - 1, endPosition);
    if(this.state.nowTag === 'content') {
      textArea.innerHTML = resStr.replace(subStr, `<span style="background-color: ${color}">${subStr}</span>`);
    } else {
      textArea.innerHTML = resStr.replace(subStr, `<span style="border-bottom: 1px solid ${color}; padding-bottom: 2px">${subStr}</span>`);
    }
    
    let temp = array[i]
    let temp2 = array[i+1]
    console.log('i start end: ',i, startPosition, endPosition)
    array.splice(i, 2, temp, startPosition, endPosition, temp2)
    console.log('array: ', array)
  }

  clearVisualEffect() {
    document.getElementById('textArea').innerHTML = this.state.nowText
    if(this.state.nowTag === 'structure') this.setState({allowInsert1: []})
    else this.setState({allowInsert2: []})
  }



  render(){
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    }
    const typeColumns = this.state.typeColumns.map((col) => {
      if (!col.editable) return col;
      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.typeHandleSave,
          text: '单击更改文本'
        })
      }
    })
    const structureColumns = this.state.structureColumns.map((col) => {
      if (!col.editable) return col;
      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          // handleSave: this.structureHandleSave,
        })
      }
    })
    const contentColumns = this.state.contentColumns.map((col) => {
      if (!col.editable) return col;
      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.contentHandleChange2,
        })
      }
    })

    return (
    <Layout id='layout' style={{height: 'calc(100vh - 65px)', minWidth: '1000px', minHeight: '600px'}}>
      {/* <Header className="header" style={{height: '60px'}}>
        <h1 id='header_h1'>泛在数据信息建模平台工具</h1>
      </Header> */}
      <Row style={{height: '100%', marginTop: '10px'}}>

        <Col span={4}>
        <Title level={3} style={{textAlign: 'center'}}>文本标签方法库</Title>
        <DataServerTool type='Text' isWindow = 'true' />
        {/* <div style={{height: '30%'}}>
        <Title level={3} style={{textAlign: 'center'}}>七要素</Title>
        <Carousel autoplay>
          <div>
            <h3 style={contentStyle}>1</h3>
          </div>
          <div>
            <h3 style={contentStyle}>2</h3>
          </div>
          <div>
            <h3 style={contentStyle}>3</h3>
          </div>
          <div>
            <h3 style={contentStyle}>4</h3>
          </div>
        </Carousel>
        </div> */}
      </Col>

        <Col span={12}>
        <Row justify="center" align="middle">
          <Upload
            maxCount={1}
            beforeUpload={this.beforeUpload}>
              <Button type='primary' style={buttonLeft}>读取文本数据</Button>
          </Upload>
          <Button type='primary' style={buttonLeft} onClick={() => this.uploadHandleSet()}>当前文本入库</Button>
          <Button type='primary' style={buttonLeft} onClick={() => this.addHandleSet()}>读取已有文本</Button>

          <Button type='primary' style={buttonLeft} onClick={() => this.clearVisualEffect()}>清除可视化效果</Button>
          <Button type='primary' style={buttonLeft} onClick={() => {
            this.setState({nowText: ''})
            this.init()
            }}>重置</Button>
          </Row>
          
          <div
          contentEditable='true'
          style={{height: '80%', marginTop: '5px', marginLeft: '5px'}}
          id='textArea'
          onMouseDown={() => this.onMouseDown()}
          onMouseUp={() => this.selectValue()}
          value = {this.state.nowText}
          onChange={(e) => this.setState({nowText: e.target.value})}
          rows={24} 
          placeholder='请读取文本文件或者粘贴文本'
          showCount 
          maxLength={100000}>{this.state.nowText}</div>
          {/* <Paragraph /> */}
          <div id='typeValueCard'>
          <Row justify="center" align="middle">
            <Col span={24}>
            {this.state.nowTag === 'type' && this.state.categoryList.length > 0 && 
                <List 
                split={false}
                size='small'
                itemLayout="horizontal"
                dataSource={this.state.categoryList}
                renderItem={item => (
                  <List.Item style={{padding:'2px'}}>
                    <List.Item.Meta
                      description={                  
                          <Row>
                            <Tag key={item.type} color={item.color}>{item.value}</Tag>
                          </Row>
                      }
                    />
                  </List.Item>
                )}
              />
            }
            </Col>
          </Row>
          </div>
          <UploadTextModal visible={this.state.uploadTextVisible} handleOk={this.uploadHandleSet} handleCancel={this.uploadHandleSet} content={this.state.nowText} setNowTextMetaId={this.setNowTextMetaId} />
          <AddTextModal visible={this.state.addTextVisible} handleOk={this.addHandleSet} handleCancel={this.addHandleSet} search={this.searchText} url={this.state.urlFileList} setNowFile={this.setNowFile}/>
        </Col>

        <Col span={8}>
        <Tabs type="card" defaultActiveKey="type"  onChange={(key) => this.setState({nowTag: key})} style={{height: '50%'}}>
            <TabPane tab="类型标签" key="type" style={{marginTop: '5px'}}>
              <Table
                components={components}
                rowClassName={() => 'editable-row'}
                bordered
                dataSource={this.state.categoryList}
                columns={typeColumns}
                pagination= {{pageSize: 10, simple: true}}
                scroll={{y: 200}}
              />
              <Row type="flex" justify="center" align="small">
                <Col>
                  <Button
                  style={buttonLeft}
                  type='primary'
                  onClick={() => {this.typeHandleAdd()}}
                  >添加分类</Button>
                  <Button type="primary" style={buttonLeft} onClick={()=> this.typeSubmit()}>
                  保存
                  </Button>
                  <Button type="primary" style={buttonLeft} onClick={()=> this.typeCancel()}>
                  重置
                  </Button>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="结构标签" key="structure">
            <Table
                components={components}
                rowClassName={() => 'editable-row'}
                bordered
                dataSource={this.state.structrueTagList}
                columns={structureColumns}
                pagination= {{pageSize: 10, simple: true}}
                scroll={{y: 200}}
              />
              <Row type="flex" justify="center" align="small">
                <Col>
                  <Button
                  type='primary'
                  style={buttonLeft}
                  onClick={() => {this.structureHandleAdd()}}
                  >添加分类</Button>
                  <Button type="primary" style={buttonLeft} onClick={()=> this.structureCancel()}>
                  重置
                  </Button>
                </Col>
              </Row>
            </TabPane>
            <TabPane tab="内容标签" key="content">    
             {/* 代码写的真丑陋，为了赶时间只能复制写过的代码了 */}
            <Table
                components={components}
                rowClassName={() => 'editable-row'}
                bordered
                dataSource={this.state.contentTagList}
                columns={contentColumns}
                pagination= {{pageSize: 10, simple: true}}
                scroll={{y: 200}}
              />
              <Row type="flex" justify="center" align="small">
                <Col>
                  <Button
                  type='primary'
                  style={buttonLeft}
                  onClick={() => {this.contentHandleAdd()}}
                  >添加分类</Button>
                  <Button type="primary" style={buttonLeft} onClick={()=> this.contentCancel()}>
                  重置
                  </Button>
                </Col>
              </Row>
            </TabPane>
        </Tabs>
        <Tabs type="card"  style={{height: '50%'}}>
            <TabPane tab='信息要素'>

            </TabPane>
        </Tabs>
        

        {/* <Title level={5}  justify="center" align="middle">{this.tagToText()}</Title> */}
          {/* <Row justify="center" align="middle">
            <Col span={24}>
            {
            this.state.nowTag === 'type' && this.state.categoryList.length > 0 && 
            <List 
              itemLayout="horizontal"
              pagination={{
                pageSize: 5,
                simple:true
              }}
              dataSource={this.state.categoryList}
              renderItem={item => (
                <List.Item>
                <List.Item.Meta
                    description={    
                  <div>
                    <Row style={{padding: '0px'}}>
                    <Col justify="center" align="middle" span={6}>
                    <ColorPicker color={this.hexToRgba(item.color)} categoryListKey={item.key} type={this.nowTag} handleColorChange={this.handleTypeColorChange}/>
                    </Col>
                    <Col justify="center" align="middle" span={8}>
                      {item.type}
                    </Col>
                    <Col justify="center" align="middle" span={10}>
                      <Button type='primary'>可视化</Button>
                    </Col>
                  </Row>
                  <div class='itemTagValue'>
                      {item.value }
                  </div>
                  </div>
                }
              />

                </List.Item>
              )}
            />
            }
            {
            this.state.nowTag === 'structure' && this.state.structrueTagList.length > 0 && 
            <List 
              itemLayout="horizontal"
              pagination={{
                pageSize: 5,
                simple:true
              }}
              dataSource={this.state.structrueTagList}
              renderItem={item => (
                <List.Item>
                <List.Item.Meta
                    description={    
                  <div>
                    <Row style={{padding: '0px'}}>
                    <Col justify="center" align="middle" span={6}>
                    <ColorPicker color={this.hexToRgba(item.color)} categoryListKey={item.key} type={this.nowTag} handleColorChange={this.handleStructureColorChange}/>
                    </Col>
                    <Col justify="center" align="middle" span={8}>
                      {item.tagType}
                    </Col>
                    <Col justify="center" align="middle" span={10}>
                      <Button type='primary' onClick={() => this.setHighlight(item.isShow, item.startPosition, item.endPosition, item.color, this.state.allowInsert1)}>可视化</Button>
                    </Col>
                  </Row>
                  <div class='itemTagValue'>
                      {item.tagValue }
                  </div>
                  </div>
                }
              />

                </List.Item>
              )}
            />
            }
            {
            this.state.nowTag === 'content' && this.state.contentTagList.length > 0 && 
            <List 
              itemLayout="horizontal"
              pagination={{
                pageSize: 5,
                simple:true
              }}
              dataSource={this.state.contentTagList}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta
                    description={    
                  <div>
                    <Row style={{padding: '0px'}}>
                    <Col justify="center" align="middle" span={6}>
                    <ColorPicker color={this.hexToRgba(item.color)} categoryListKey={item.key} type={this.nowTag} handleColorChange={this.handleContentColorChange} />
                    </Col>
                    <Col justify="center" align="middle" span={8}>
                      {item.tagName}
                    </Col>
                    <Col justify="center" align="middle" span={10}>
                      <Button type='primary' onClick={() => this.setHighlight(item.isShow, item.startPosition, item.endPosition, item.color, this.state.allowInsert2)}>可视化</Button>
                    </Col>
                  </Row>
                  <div class='itemTagValue'>
                      {item.tagValue }
                  </div>
                  </div>
                }
                />

                </List.Item>
              )}
            />
            }
            </Col>
          </Row>
         */}
        </Col>
      </Row>
    </Layout>
    );
  }
}


export default TextTool;