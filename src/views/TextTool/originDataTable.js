import React, { Component } from "react";
import axios from "axios";
import { Table, message } from "antd";
message.config({ "top": 300 });
class TableRendering extends Component{
    constructor(props){
        super(props);

        this.state = {
            tableLoading: true,                                          //表格是否处于加载中
            tableData: null,                                             //表格数据
            total: 0,                                                    //表格数据总条目数
            curPage: 1,                                                  //当前页数
        };

        this.onPageChange = this.onPageChange.bind(this);
        this.renderTable = this.renderTable.bind(this);
    }

    componentDidMount(){
        let url = `${this.props.url}/1/5`
        getTableData(url, this.renderTable);
    }

    componentDidUpdate(prevProps){
        if(prevProps.url == this.props.url){
            return;
        }
        let url = `${this.props.url}/1/5`
        getTableData(url, this.renderTable);
    }

    render(){
        let { cols } = this.props;
        return (
            <Table
                rowKey="id"
                dataSource={this.state.tableData}
                columns={cols}
                bordered={true}
                loading={this.state.tableLoading}
                pagination={{
                    simple: true,
                    current: this.state.curPage,
                    total: this.state.total,
                    showQuickJumper: true,
                    showSizeChanger: false,
                    onChange: this.onPageChange
                }}
            />
        );
    }

    onPageChange(page, pageSize){
        pageSize = 5
        this.setState({
            "curPage": page,
            "tableLoading": true
        });
        let url = `${this.props.url}/${page}/${pageSize}`;
        getTableData(url, this.renderTable);
    }

    renderTable(err, response){
        if(err){
            this.setState({ "tableLoading": false });
            return;
        }

        if(response.data.code != 0){
            getTbDataError();
            this.setState({ "tableLoading": false });
            console.log(response.data.msg);
            return;
        }

        let data = response.data;
        this.setState({
            "tableData": data.data.textData,
            "tableLoading": false,
            "total": data.data.count
        });
    }
}



function getTableData(url, callback){
    axios.get(url).then(res => {
        callback(null, res);
    }).catch(err => {
        getTbDataError();
        callback(err);
    })
}

//加载表格数据失败
function getTbDataError(){
    message.error({
        "content": "加载数据时出错",
    });
}

export default TableRendering;