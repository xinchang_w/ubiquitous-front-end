import React, { Component } from "react";
import { Breadcrumb, Card, Drawer, Button, Input, Table, Space,DatePicker,Modal ,message} from "antd";
import moment from 'moment';
import {
  MenuUnfoldOutlined,
  CloseOutlined,
  MinusOutlined,
  CheckOutlined,
  SettingOutlined
} from "@ant-design/icons";
//本页面样式
import style from "./CityTrafficAggregation.module.css";
//import "@/assets/style/common.css";
//组件图标
import _MAP from "@/assets/image/地图.png";
import _Text from "@/assets/image/文本.png";
import _CHART from "@/assets/image/图表.png";
import _TABLE from "@/assets/image/表格.png";
import _BAIDUMAP from "@/assets/image/百度地图.png";
import _VIDEO from "@/assets/image/视频.png"
import _AUDIO from "@/assets/image/音频.png"

//随机码
import { v4 as uuidv4 } from "uuid";
//ajax请求
import axios from "axios";

//可拖拽组件
import Draggable from "react-draggable";
//图表组件
import ChinaMap from "@/views/Home/chinaMap";
import {WORLD_COUNTRIES, CHINA_PROVINCES, NODE_PATH} from "@/assets/global/index";
import "@/assets/style/home.css";

import {Chart, LineChart, TableRendering} from "@/components";
import EchartsMap from "@/components/EchartsMap";

// import { options } from '@/views/ProcessSrc/echartsOptions'
import { options } from "@/views/NanJingCityTrafficAggregation/lineChartData";

//react dom序列化
import { serialize, deserialize } from "react-serialize";
import ITEMS from "@/views/DataQuery/config";
class NanJingCityTrafficAggregation extends Component {
  constructor(props) {
    super(props);

    //拖拽面板append jsx dom
    this.mainComponentDom = [];
    //数据配置面板append jsx dom
    this.dataConfigDom = [];
    //记录哪个类型组件被点击
    this.dataConfigtype = "";
    //此可视化案例唯一标识
    this.uid = window.location.search.slice(4);
    //组件信息数据，用于复现
    this.componentsInfo = [];
    this.positionInfoObj = {};
    this.dataIds = [];

    this.dataUrl = "";
    this.textUrl = "http://47.110.86.123:8080/api/aggregation/text/query/1/10";
    this.videoUrl = "http://47.110.86.123:8080/api/aggregation/video/query/1/10";
    this.audioUrl = "http://47.110.86.123:8080/api/aggregation/audio/query/1/10";
    this.dateTime = "";
    this.textMomentTime =  moment('2021/06/26 17:00:00', 'YYYY/MM/DD HH:mm:ss');
    this.videoMomentTime = moment('2021/06/29 18:37:21', 'YYYY/MM/DD HH:mm:ss');
    this.audioMomentTime = moment('2021/06/29 17:04:00', 'YYYY/MM/DD HH:mm:ss') ;
    this.momentTime = {};
    this.weiboData = [];
    this.videoData = "";
    this.audioData = "";


    this.draggleRef = React.createRef();
    this.clickComponent = this.clickComponent.bind(this);
    this.getData = this.getData.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }


  state = {
    //组件唯一标识
    componentKey: uuidv4(),
    //左侧抽屉参数
    visible: false,
    placement: "left",
    //模态框参数
    disabled:true,
    visibleModal: false,
    bounds: { left: 0, top: 0, bottom: 0, right: 0 },
    //拖拽组件参数
    activeDrags: 0,
    //拖拽面板赋值dom元素
    mainHtml: this.mainComponentDom,
    //数据配置
    dataConfigDom: this.dataConfigDom,
    //组件宽高配置
    widthValue: 400,
    heightValue: 300,
    //组件信息数据，用于复现
    componentsInfoData: this.componentsInfo,
    positionInfoObj: this.positionInfoObj,
  };
  //生命周期函数
  componentWillMount() {
    console.log("uid", this.uid);
  }
  componentDidMount() {
    console.log("uid", this.uid);
    //this.getContent();
    // this.state.mainComponentDom
  }
  //左侧抽屉关闭事件
  onClose = () => {
    this.setState({
      visible: false,
    });
  };
  //左侧抽屉打开事件
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  //组件拖拽事件
  onStart = (e, position) => {
    //console.log('position',position.x,position.y)
    //this.setState({ activeDrags: ++this.state.activeDrags });
  };
  //组件拖拽事件
  onStop = (e, position) => {
    // console.log('position',position.x,position.y)
    //this.setState({ activeDrags: --this.state.activeDrags });
    let key = e.path[2].id;
    this.positionInfoObj[key] = {
      top:position.y,
      left:position.x
    };
    console.log(key,e, position);
    this.setState({
      positionInfoObj: this.positionInfoObj[key],
    });
  };
  //组件拖拽事件

  onDragEvent=(e, position)=>{
    // let key = e.path[1].id;
    // this.positionInfoObj[key] = {
    //   top:position.y,
    //   left:position.x
    // };
    // //console.log(key,e.path,position.x, position.y);
    // this.setState({
    //   positionInfoObj: this.positionInfoObj[key],
    // });
  }

  //linchart Drag
  onDragLineChart = (e, position) => {
    //LineChart Key e.path[3].id
    let key = e.path[2].id;
    this.positionInfoObj[key] = {
      x: position.x,
      y: position.y,
    };
    console.log(position.x, position.y);
    this.setState({
      positionInfoObj: this.positionInfoObj,
    });
  };

  //生成uid
  uidGenerate = () => {
    this.setState({
      componentKey: uuidv4(),
    });
    return this.state.componentKey;
  };

  //查询微博数据
  getData(url,componentType, queryCon){

    let con = {};
    this.momentTime["text"]={
      value : this.textMomentTime._i,
    };
    this.momentTime["video"]={
      value : this.videoMomentTime._i,
    };
    this.momentTime["audio"]={
      value : this.audioMomentTime._i,
    };
    let curSearchCon = "time";
    let keywords =this.dateTime;
    if(this.dateTime == ""){
      keywords = this.momentTime[componentType].value;
    }
    if(curSearchCon && keywords){
      con[curSearchCon] = keywords;
    }
    queryCon = con;
    axios.get(url, { params: queryCon ? queryCon : {} }).then(res => {
      console.log(res.data.data.textData);
      this.dataIds =[res.data.data.textData[0]._id];
      //判断组件类型，将查询到的结果赋值给相应数据接收参数，并加载对应组件
      switch (componentType) {
          //Map
        case "map":
          this.clickComponent("map");
          break;
        case "text":
          this.weiboData = res.data.data.textData[0].time + res.data.data.textData[0].content;
          this.clickComponent("text");
          break;
        case "video":
          this.videoData = "http://94.191.49.160/resource/"+res.data.data.textData[0].video;
          this.clickComponent("video");
          break;
        case "audio":
          this.audioData = "http://94.191.49.160/resource/"+res.data.data.textData[0].audio;
          this.clickComponent("audio");
          break;
      };
      this.hideModal();
    }).catch(err => {
      console.log(err);
    })
  }

  //获取时间设置面板所选的时间
  onChange(value, dateString){
    this.dateTime = dateString;
    console.log(value,dateString,this.dateTime);
  }

  //模态框的显示与隐藏
  showModal(){
    this.setState({
      visibleModal: true,
      dataConfigDom: [],
    });
  }

  hideModal(){
    this.setState({
      visibleModal: false,
    });
  }

  onStart = (event, uiData) => {
    const { clientWidth, clientHeight } = window?.document?.documentElement;
    const targetRect = this.draggleRef?.current?.getBoundingClientRect();
    this.setState({
      bounds: {
        left: -targetRect?.left + uiData?.x,
        right: clientWidth - (targetRect?.right - uiData?.x),
        top: -targetRect?.top + uiData?.y,
        bottom: clientHeight - (targetRect?.bottom - uiData?.y),
      },
    });
  };

  //点击组件，添加可拖拽组件事件
  //componentType 组件类型，包括map,text,chart
  clickComponent = (componentType) => {
    console.log(componentType);
    let uid = this.uidGenerate();
    //判断组件类型,并添加
    switch (componentType) {
        //Map
      case "map":
        this.mainComponentDom.unshift(
            <Draggable
                defaultPosition={{ x: 400, y: 50 }}
                onStart={() => this.onStart()}
                onStop={this.onStop}
                key={uid}
                _type="Map"
                id={uid}
                onDrag={this.onDragEvent}
            >
              <div
                  id={uid}
                  className={style["component"]}
                  style={{
                    width: this.state.widthValue + "px",
                    height: this.state.heightValue + "px",
                  }}
                  onDoubleClick={this.dataConfig.bind(
                      this,
                      uid,
                      "Map"
                  )}
              >
                <CloseOutlined
                    style={{fontSize:"20px",color:"darkgray",float:"right"}}
                    onClick={this.closeComponent.bind(
                        this,
                        uid
                    )}
                />
                <EchartsMap
                    id={uid}
                    style={{ height: "100%", width: "100%" }}
                    option={createBaiMapData()}
                />
              </div>
            </Draggable>
        );
        this.componentsInfo.unshift({
          componentType:"Map",
          key: uid,
          type: "Map",
          position: this.positionInfoObj[uid],
          dataIds: createBaiMapData(CHINA_PROVINCES),
        });
        break;
      case "text":
        this.mainComponentDom.unshift(
            <Draggable
                defaultPosition={{ x: 400, y: 50 }}
                onStart={(e,position) => this.onStart(e,position)}
                onStop={(e,position) => this.onStop(e,position)}
                key={uid}
                _type="text"
                id={uid}
                onDrag={this.onDragEvent}
            >
              <div
                  id={uid}
                  className={style["component"]}
                  style={{
                    width: this.state.widthValue + "px",
                    height: this.state.heightValue + "px",
                  }}
                  onDoubleClick={this.dataConfig.bind(
                      this,
                      uid,
                      "text"
                  )}
              >
                <div>
                  <CloseOutlined
                      className={style["closeIcon"]}
                      onClick={this.closeComponent.bind(
                          this,
                          uid
                      )}
                  />
                </div>
                <div className={style["handle"]}>
                  <h1>路况直播微博</h1>
                  <p style={{textAlign:"left"}}>{this.weiboData}</p>
                </div>
              </div>
            </Draggable>
        );
        // TODO 文字的存储
        this.componentsInfo.unshift({
          key: uid,
          componentType:"text",
          dataType: "text",
          style:{
            top:50,
            left:400,
            width:400,
            height:300
          },
          dataIds: [this.dataIds],
        });
        break;

      case "video":
        this.mainComponentDom.unshift(
            <Draggable
                defaultPosition={{ x: 400, y: 50 }}
                onStart={(e,position) => this.onStart(e,position)}
                onStop={(e,position) => this.onStop(e,position)}
                key={uid}
                _type="video"
                onDrag={this.onDragEvent}
                id = {uid}
                //id={uid}
            >
              <div
                  className={style["component"]}
                  style={{
                    width: this.state.widthValue + "px",
                    height: this.state.heightValue + "px",
                  }}
                  onDoubleClick={this.dataConfig.bind(
                      this,
                      uid,
                      "video"
                  )}
                  id={uid}
              >
                <CloseOutlined
                    className={style["closeIcon"]}
                    onClick={this.closeComponent.bind(
                        this,
                        uid
                    )}
                />
                <div className={style["handle"]}>
                  <h1>实时监控</h1>
                </div>
                <video
                    src={this.videoData}
                    controls
                    style={{
                      width: this.state.widthValue + "px",
                      height: this.state.heightValue/1.2 + "px",
                    }}
                >
                </video>
              </div>
            </Draggable>
        );
        this.componentsInfo.unshift({
          key: uid,
          componentType:"video",
          dataType: "video",
          style:{
            top:50,
            left:400,
            width:400,
            height:300
          },
          dataIds: [this.dataIds],
        });
        break;
      case "audio":
        this.mainComponentDom.unshift(
            <Draggable
                defaultPosition={{ x: 400, y: 50 }}
                onStart={(e,position) => this.onStart(e,position)}
                onStop={(e,position) => this.onStop(e,position)}
                key={uid}
                _type="audio"
                onDrag={this.onDragAudio}
                id={uid}
            >
              <div
                  className={style["component"]}
                  onDoubleClick={this.dataConfig.bind(
                      this,
                      uid,
                      "audio"
                  )}
                  style={{
                    width: this.state.widthValue + "px",
                    height: this.state.heightValue/2 + "px",
                  }}
                  id={uid}
              >
                <CloseOutlined
                    className={style["closeIcon"]}
                    onClick={this.closeComponent.bind(
                        this,
                        uid
                    )}
                />
                <div className={style["handle"]}>
                  <h1>Audio</h1>
                </div>
                <audio
                    src={this.audioData}
                    controls
                    style={{
                      width: this.state.widthValue + "px",
                      height: this.state.heightValue/4 + "px",
                    }}
                >
                </audio>
              </div>
            </Draggable>
        );
        this.componentsInfo.unshift({
          key: uid,
          componentType:"audio",
          dataType: "audio",
          style:{
            top:50,
            left:400,
            width:400,
            height:300
          },
          dataIds: [this.dataIds],
        });
        break;
      case "LineChart":
        this.mainComponentDom.unshift(
            <Draggable
                defaultPosition={{ x: 400, y: 50 }}
                onStart={this.onStart}
                onStop={this.onStop}
                key={this.uidGenerate()}
                onDrag={this.onDragLineChart}
                _type="LineChart"
                id={this.state.componentKey}
                className="LineChart"
            >
              <div
                  className={style["component"]}
                  onDoubleClick={this.dataConfig.bind(
                      this,
                      this.state.componentKey,
                      "LineChart"
                  )}
                  id={this.state.componentKey}
              >
                <MinusOutlined
                    onClick={this.closeComponent.bind(
                        this,
                        this.state.componentKey
                    )}
                />

                <Chart
                    option={options}
                    id={this.state.componentKey}
                    style={{ height: "100%", width: "100%" }}
                />
              </div>
            </Draggable>
        );
        this.componentsInfo.unshift({
          key: this.state.componentKey,
          type: "LineChart",
          position: this.positionInfoObj[this.state.componentKey],
          data: options,
        });
        break;
      case "Table":
        this.mainComponentDom.unshift(
            <Draggable
                defaultPosition={{ x: 400, y: 50 }}
                onStart={this.onStart}
                onStop={this.onStop}
                onDrag={this.onDrag}
                key={this.uidGenerate()}
                _type="Table"
            >
              <div
                  className={style["component"]}
                  onDoubleClick={this.dataConfig.bind(
                      this,
                      this.state.componentKey,
                      "Table"
                  )}
              >
                <MinusOutlined
                    onClick={this.closeComponent.bind(
                        this,
                        this.state.componentKey
                    )}
                />
                TABLE
              </div>
            </Draggable>
        );
        this.componentsInfo.unshift({
          key: this.state.componentKey,
          type: "Table",
          position: this.positionInfoObj[this.state.componentKey],
          data: options,
        });
        break;
    }

    //重置state数据
    this.setState({
      mainComponentDom: this.mainComponentDom,
    });
    //打印组件数组，按添加先后顺序
    console.log("add component", this.mainComponentDom, this.componentsInfo);
  };
  //依据唯一标识删除组件
  closeComponent = (key) => {
    //删除组件
    console.log("del", key, this.mainComponentDom);

    let delKey;
    for (let i in this.mainComponentDom) {
      if (this.mainComponentDom[i].key == key) {
        delKey = i;
        break;
      }
    }
    this.mainComponentDom.splice(delKey, 1);

    //删除组件信息数据
    for (let i in this.componentsInfo) {
      if (this.componentsInfo[i].key == key) {
        delKey = i;
        break;
      }
    }
    this.componentsInfo.splice(delKey, 1);
    //重置state数据
    this.setState({
      mainComponentDom: this.mainComponentDom,
    });
  };
  //数据配置,key唯一标识，type类型
  dataConfig = (key, type) => {
    this.hideModal();
    if (this.dataConfigDom.length) {
      this.dataConfigDom = [];
      this.setState({
        dataConfigDom: this.dataConfigDom,
      });
    }

    this.showModal();//加载模态框
    this.dataConfigtype = type;
    switch (type) {
      case "map":
        this.dataConfigDom.push(
            <div key={key + "_" + type}>
              <h2>Map Config</h2>
              <br />

              <Card title="宽高设置：" style={{textAlign:"center"}}>
                <Input
                    style={{width: "180px"}}
                    addonBefore="宽"
                    addonAfter="px"
                    defaultValue="400"
                    onChange={(e) => {
                      this.setState({ widthValue: e.target.value });
                    }}
                />
                <br/>
                <Input
                    style={{width: "180px"}}
                    addonBefore="高"
                    addonAfter="px"
                    defaultValue="300"
                    onChange={(e) => {
                      this.setState({ heightValue: e.target.value });
                    }}
                />
                <br/>
                {/*<Button*/}
                {/*    type="dashed"*/}
                {/*    onClick={this.widthHeightConfig.bind(this, key, type)}*/}
                {/*>*/}
                {/*  配置*/}
                {/*</Button>*/}
              </Card>
              <Card title="数据挑选：" style={{textAlign:"center"}}>
                <DatePicker showTime format="YYYY/MM/DD HH:mm:ss"  onChange={this.onChange.bind(this)}/>
                <Button
                    type="dashed"
                    onClick={this.clickComponent.bind(this, "Map")}
                >
                  确定
                </Button>
              </Card>
            </div>
        );
        break;
      case "text":
        this.dataUrl = this.textUrl;
        this.dataConfigDom.push(
            <div key={key + "_" + type}>
              <h2>Text Config</h2>
              <br />
              <Card title="宽高设置：" style={{textAlign:"center"}}>
                <Input
                    style={{width: "180px"}}
                    addonBefore="宽"
                    addonAfter="px"
                    defaultValue="400"
                    onChange={(e) => {
                      this.setState({ widthValue: e.target.value });
                    }}
                />
                <br/>
                <Input
                    style={{width: "180px"}}
                    addonBefore="高"
                    addonAfter="px"
                    defaultValue="300"
                    onChange={(e) => {
                      this.setState({ heightValue: e.target.value });
                    }}
                />
                <br/>
                {/*<Button*/}
                {/*    type="dashed"*/}
                {/*    onClick={this.widthHeightConfig.bind(this, key, type)}*/}
                {/*>*/}
                {/*  配置*/}
                {/*</Button>*/}
              </Card>

              <Card title="数据挑选：" style={{textAlign:"center"}}>
                <DatePicker showTime format="YYYY/MM/DD HH:mm:ss" defaultValue={this.textMomentTime}  onChange={this.onChange.bind(this)}/>
                <Button
                    type="dashed"
                    onClick = {this.getData.bind(this,this.textUrl,"text")}
                >
                  确定
                </Button>
              </Card>
            </div>
        );
        break;
      case "video":
        this.dataUrl = this.videoUrl;
        this.dataConfigDom.push(
            <div key={key + "_" + type}>
              <h2>Video Config</h2>
              <br />

              <Card title="宽高设置：" style={{textAlign:"center"}}>
                <Input
                    style={{width: "180px"}}
                    addonBefore="宽"
                    addonAfter="px"
                    defaultValue="400"
                    onChange={(e) => {
                      this.setState({ widthValue: e.target.value });
                    }}
                />
                <br/>
                <Input
                    style={{width: "180px"}}
                    addonBefore="高"
                    addonAfter="px"
                    defaultValue="300"
                    onChange={(e) => {
                      this.setState({ heightValue: e.target.value });
                    }}
                />
                <br/>
                {/*<Button*/}
                {/*    type="dashed"*/}
                {/*    onClick={this.widthHeightConfig.bind(this, key, type)}*/}
                {/*>*/}
                {/*  配置*/}
                {/*</Button>*/}
              </Card>
              <Card title="数据挑选：" style={{textAlign:"center"}}>
                <DatePicker showTime defaultValue={this.videoMomentTime} format="YYYY/MM/DD HH:mm:ss"   onChange={this.onChange.bind(this)}/>
                <Button
                    type="dashed"
                    onClick={this.getData.bind(this,this.videoUrl,"video")}
                >
                  确定
                </Button>
              </Card>
            </div>
        );
        break;
      case "audio":
        this.dataUrl = this.audioUrl;
        this.dataConfigDom.push(
            <div key={key + "_" + type}>
              <h2>Audio Config</h2>
              <br />
              <Card title="宽高设置：" style={{textAlign:"center"}}>
                <Input
                    style={{width: "180px"}}
                    addonBefore="宽"
                    addonAfter="px"
                    defaultValue="400"
                    onChange={(e) => {
                      this.setState({ widthValue: e.target.value });
                    }}
                />
                <br/>
                <Input
                    style={{width: "180px"}}
                    addonBefore="高"
                    addonAfter="px"
                    defaultValue="300"
                    onChange={(e) => {
                      this.setState({ heightValue: e.target.value });
                    }}
                />
                <br/>
                {/*<Button*/}
                {/*    type="dashed"*/}
                {/*    onClick={this.widthHeightConfig.bind(this, key, type)}*/}
                {/*>*/}
                {/*  配置*/}
                {/*</Button>*/}
              </Card>
              <Card title="数据挑选：" style={{textAlign:"center"}}>
                <DatePicker showTime format="YYYY/MM/DD HH:mm:ss" defaultValue={this.audioMomentTime}  onChange={this.onChange.bind(this)}/>
                <Button
                    type="dashed"
                    onClick={this.getData.bind(this, this.audioUrl,"audio")}
                >
                  确定
                </Button>
              </Card>
            </div>
        );
        break;
      case "LineChart":
        this.dataConfigDom.push(
            <div key={key + "_" + type}>
              <h2>LineChart Config</h2>
              <br />

              <Card title="宽高设置：">
                <Input
                    addonBefore="宽"
                    addonAfter="px"
                    defaultValue="0"
                    onChange={(e) => {
                      this.setState({ widthValue: e.target.value });
                    }}
                />
                <Input
                    addonBefore="高"
                    addonAfter="px"
                    defaultValue="0"
                    onChange={(e) => {
                      this.setState({ heightValue: e.target.value });
                    }}
                />
                <Button
                    type="dashed"
                    onClick={this.widthHeightConfig.bind(this, key, type)}
                >
                  配置
                </Button>
              </Card>

              <Card title="数据挑选：">
                <Table
                    dataSource={[
                      {
                        key: "1",
                        name: "南京市",
                        age: 32,
                        address: "西湖区湖底公园1号",
                      },
                    ]}
                    columns={[
                      {
                        title: "数据",
                        dataIndex: "name",
                        key: "name",
                      },

                      {
                        title: "挑选",
                        key: "action",
                        render: (text, record) => (
                            <Space size="middle">
                              <Button
                                  type="dashed"
                                  shape="circle"
                                  icon={<CheckOutlined />}
                              />
                            </Space>
                        ),
                      },
                    ]}
                />
                ;
              </Card>
            </div>
        );
        break;
      case "Table":
        this.dataConfigDom.push(
            <div key={key + "_" + type}>
              <h2>Table Config</h2>
              <br />
              <h3>宽高设置：</h3>
              <Input
                  addonBefore="宽"
                  addonAfter="px"
                  defaultValue="0"
                  onChange={(e) => {
                    this.setState({ widthValue: e.target.value });
                  }}
              />
              <Input
                  addonBefore="高"
                  addonAfter="px"
                  defaultValue="0"
                  onChange={(e) => {
                    this.setState({ heightValue: e.target.value });
                  }}
              />
              <Button
                  type="dashed"
                  onClick={this.widthHeightConfig.bind(this, key, type)}
              >
                配置
              </Button>
              <br />
              <h3>数据挑选：</h3>
              <Table
                  dataSource={[
                    {
                      key: "1",
                      name: "南京市",
                      age: 32,
                      address: "西湖区湖底公园1号",
                    },
                  ]}
                  columns={[
                    {
                      title: "数据",
                      dataIndex: "name",
                      key: "name",
                    },

                    {
                      title: "挑选",
                      key: "action",
                      render: (text, record) => (
                          <Space size="middle">
                            <Button
                                type="dashed"
                                shape="circle"
                                icon={<CheckOutlined />}
                            />
                          </Space>
                      ),
                    },
                  ]}
              />
              ;
            </div>
        );
        break;
    }
  };
  //TODO 与插入组件保持一致,样式信息保存，记得修改
  //宽高配置
  widthHeightConfig = (key, type) => {
    let editKey;
    for (let i in this.mainComponentDom) {
      if (this.mainComponentDom[i].key == key) {
        editKey = i;
        break;
      }
    }
    this.mainComponentDom.splice(editKey, 1);
    console.log(
        this.state.widthValue,
        this.state.heightValue,
        this.mainComponentDom[editKey],
        type
    );

    switch (type) {
      case "text":
        this.mainComponentDom.unshift(
            <Draggable
                defaultPosition={{ x: 400, y: 50 }}
                onStart={this.onStart}
                onStop={this.onStop}
                onDrag={this.onDrag}
                key={this.uidGenerate()}
                _type="text"
                id={this.state.componentKey}
            >
              <div
                  id={this.state.componentKey}
                  className={style["component"]}
                  style={{
                    width: this.state.widthValue + "px",
                    height: this.state.heightValue + "px",
                  }}
                  onDoubleClick={this.dataConfig.bind(
                      this,
                      this.state.componentKey,
                      "text"
                  )}
              >
                <div>
                  <CloseOutlined
                      className={style["closeIcon"]}
                      onClick={this.closeComponent.bind(
                          this,
                          this.state.componentKey
                      )}
                  />
                </div>
                <div className={style["handle"]}>
                  <h1>路况直播微博</h1>
                </div>
              </div>
            </Draggable>
        );
        // TODO 文字的存储
        this.componentsInfo.unshift({
          key: this.state.componentKey,
          type: "text",
          position: this.positionInfoObj[this.state.componentKey],
          data: "text",
        });
        break;
      case "LineChart":
        this.mainComponentDom.unshift(
            <Draggable
                defaultPosition={{ x: 400, y: 50 }}
                onStart={this.onStart}
                onStop={this.onStop}
                key={this.uidGenerate()}
            >
              <div
                  className={style["component_reset"]}
                  style={{
                    width: this.state.widthValue + "px",
                    height: this.state.heightValue + "px",
                  }}
                  onDoubleClick={this.dataConfig.bind(
                      this,
                      this.state.componentKey,
                      "LineChart"
                  )}
              >
                <MinusOutlined
                    onClick={this.closeComponent.bind(
                        this,
                        this.state.componentKey
                    )}
                />

                <Chart
                    option={options}
                    style={{ height: "100%", width: "100%" }}
                />
              </div>
            </Draggable>
        );
        break;
      case "Table":
        this.mainComponentDom.unshift(
            <Draggable
                defaultPosition={{ x: 400, y: 50 }}
                onStart={this.onStart}
                onStop={this.onStop}
                key={this.uidGenerate()}
            >
              <div
                  className={style["component_reset"]}
                  style={{
                    width: this.state.widthValue + "px",
                    height: this.state.heightValue + "px",
                  }}
                  onDoubleClick={this.dataConfig.bind(
                      this,
                      this.state.componentKey,
                      "Table"
                  )}
              >
                <MinusOutlined
                    onClick={this.closeComponent.bind(
                        this,
                        this.state.componentKey
                    )}
                />
                TABLE
              </div>
            </Draggable>
        );
        break;
    }

    //重置state数据
    this.setState({
      mainComponentDom: this.mainComponentDom,
    });
  };
  //获取组件案例内容,初始加载时从接口中查询聚合任务并加载显示
  getContent = () => {
    let that = this;
    axios
        .get("http://47.110.86.123:8080/api/aggregation/task/search/60f12829918b8522085682aa")
        .then((res) => {
          let cont = res.data.data.content;
          console.log("getcontent", cont);
          for (let i in cont) {
            let componentPosition = {
              x:cont[i].style.left,
              y:cont[i].style.top,
            };
            //判断组件类型,并添加
            switch (cont[i].componentType.toLowerCase()) {
                //Map
              case "map":
                that.mainComponentDom.unshift(
                    <Draggable
                        defaultPosition={componentPosition}
                        onStart={() => that.onStart()}
                        onStop={that.handleStop}
                        key={cont[i]._id}
                        _type="Map"
                        onDrag={that.onDragEvent}
                    >
                      <div
                          id={cont[i]._id}
                          className={style["component_reset"]}
                          style={{
                            width: this.state.widthValue + "px",
                            height: this.state.heightValue + "px",
                          }}
                          onDoubleClick={this.dataConfig.bind(
                              that,
                              cont[i]._id,
                              "map"
                          )}
                          _type="Map"
                      >
                        <MinusOutlined
                            onClick={that.closeComponent.bind(that, cont[i].key)}
                        />
                        <EchartsMap
                            id={that.state.componentKey}
                            style={{ height: "100%", width: "100%" }}
                            option={createBaiMapData()}
                        />
                      </div>
                    </Draggable>
                );
                that.positionInfoObj[cont[i].key] = cont[i].position;
                that.componentsInfo.unshift({
                  key: cont[i].key,
                  type: cont[i].type,
                  position: that.positionInfoObj[cont[i].key],
                  data: cont[i].data,
                });
                break;
              case "text":
                that.mainComponentDom.unshift(
                    <Draggable
                        defaultPosition={componentPosition}
                        onStart={(e,position) => this.onStart(e,position)}
                        onStop={(e,position) => this.onStop(e,position)}
                        key={cont[i]._id}
                        _type="text"
                        id={cont[i]._id}
                        onDrag={this.onDragEvent}
                    >
                      <div
                          id={cont[i]._id}
                          className={style["component"]}
                          style={{
                            width: this.state.widthValue + "px",
                            height: this.state.heightValue + "px",
                          }}
                          onDoubleClick={this.dataConfig.bind(
                              that,
                              cont[i]._id,
                              "text"
                          )}
                      >
                        <div>
                          <CloseOutlined
                              className={style["closeIcon"]}
                              onClick={this.closeComponent.bind(
                                  this,
                                  cont[i]._id
                              )}
                          />
                        </div>
                        <div className={style["handle"]}>
                          <h1>路况直播微博</h1>
                          <p style={{textAlign:"left"}}>{cont[i].data[0].time+cont[i].data[0].content}</p>
                        </div>
                      </div>
                    </Draggable>
                );
                // TODO 文字的存储
                this.componentsInfo.unshift({
                  key: cont[i]._id,
                  componentType:"text",
                  dataType: "text",
                  style: cont[i].style,
                  dataIds: [cont[i].dataIds],
                });
                break;
              case "video":
                this.mainComponentDom.unshift(
                    <Draggable
                        defaultPosition={componentPosition}
                        onStart={(e,position) => this.onStart(e,position)}
                        onStop={(e,position) => this.onStop(e,position)}
                        key={cont[i]._id}
                        _type="video"
                        onDrag={this.onDragEvent}
                        id={cont[i]._id}
                    >
                      <div
                          className={style["component"]}
                          style={{
                            width: this.state.widthValue + "px",
                            height: this.state.heightValue + "px",
                          }}
                          onDoubleClick={this.dataConfig.bind(
                              this,
                              cont[i]._id,
                              "video"
                          )}
                          id={cont[i]._id}
                      >
                        <CloseOutlined
                            className={style["closeIcon"]}
                            onClick={this.closeComponent.bind(
                                this,
                                cont[i]._id
                            )}
                        />
                        <div className={style["handle"]}>
                          <h1>实时监控</h1>
                          <video
                              src={"http://94.191.49.160/resource/"+cont[i].data[0].video}
                              controls
                              style={{
                                width: this.state.widthValue + "px",
                                height: this.state.heightValue/1.2 + "px",
                              }}
                          >
                          </video>
                        </div>
                      </div>
                    </Draggable>
                );
                this.componentsInfo.unshift({
                  key: cont[i]._id,
                  componentType:"video",
                  dataType: "video",
                  style: cont[i].style,
                  dataIds: [cont[i].dataIds],
                });
                break;
              case "audio":
                this.mainComponentDom.unshift(
                    <Draggable
                        defaultPosition={componentPosition}
                        onStart={(e,position) => this.onStart(e,position)}
                        onStop={(e,position) => this.onStop(e,position)}
                        key={cont[i]._id}
                        _type="audio"
                        onDrag={this.onDragEvent}
                        id={cont[i]._id}
                    >
                      <div
                          className={style["component"]}
                          style={{
                            width: this.state.widthValue + "px",
                            height: this.state.heightValue/2 + "px",
                          }}
                          onDoubleClick={this.dataConfig.bind(
                              this,
                              cont[i]._id,
                              "audio"
                          )}
                          id={cont[i]._id}
                      >
                        <CloseOutlined
                            className={style["closeIcon"]}
                            onClick={this.closeComponent.bind(
                                this,
                                cont[i]._id
                            )}
                        />
                        <div className={style["handle"]}>
                          <h1>路况广播</h1>
                          <audio
                              src={"http://94.191.49.160/resource/"+cont[i].data[0].audio}
                              controls
                              style={{
                                width: this.state.widthValue + "px",
                                height: this.state.heightValue/4 + "px",
                              }}
                          >
                          </audio>
                        </div>
                      </div>
                    </Draggable>
                );
                this.componentsInfo.unshift({
                  key: cont[i]._id,
                  componentType:"audio",
                  dataType: "audio",
                  style: cont[i].style,
                  dataIds: [cont[i].dataIds],
                });
                break;
            }
          }
          //重置state数据
          that.setState({
            mainComponentDom: that.mainComponentDom,
          });
          //打印组件数组，按添加先后顺序
          console.log("add component", this.mainComponentDom, this.componentsInfo);
        });
  };
  //发布组件专题入库
  publishComponentsVisualization = () => {
    let that = this;
    for(var i=0;i<that.componentsInfo.length;i++){
      if(that.componentsInfo[i] == undefined)continue;
      that.componentsInfo[i].style.top = that.positionInfoObj[that.componentsInfo[i].key].top;
      that.componentsInfo[i].style.left = that.positionInfoObj[that.componentsInfo[i].key].left;
      console.log("style",that.componentsInfo[i].style);
    }
    console.log("publish", that.componentsInfo, that.positionInfoObj);

    axios
        .post("http://47.110.86.123:8080/api/aggregation/task/add", {
          name:"南京市城市交通态势聚合任务",
          content:that.componentsInfo,
        })
        .then((res) => {
          console.log(res);
          message.success("发布成功");
        });
  };
  updateComponent = () => {
    let that = this;
    console.log("that.positionInfoObj",that.componentsInfo,that.positionInfoObj);
    for(var i=0;i<that.componentsInfo.length;i++){
      if(that.componentsInfo[i] == undefined)continue;
      that.componentsInfo[i].style.top = that.positionInfoObj[that.componentsInfo[i].key].top;
      that.componentsInfo[i].style.left = that.positionInfoObj[that.componentsInfo[i].key].left;
      console.log("style",that.componentsInfo[i].style);
    }
    console.log("update", that.componentsInfo, that.positionInfoObj);

    axios
        .post("http://47.110.86.123:8080/api/aggregation/task/update/60f12829918b8522085682aa", {
          name:"南京市城市交通态势聚合任务",
          content:that.componentsInfo,
        })
        .then((res) => {
          console.log(res);
          message.success("更新成功");
        });
  }

  render() {
    const { placement, visible,  visibleModal,disabled,bounds} = this.state;

    return (
        <div className={style["container"]}>
          <Breadcrumb className="breadcrumb">
            <Breadcrumb.Item>动态更新</Breadcrumb.Item>
            <Breadcrumb.Item className="breadcrumb-item">
              更新任务
            </Breadcrumb.Item>
            <Breadcrumb.Item className="breadcrumb-item">
              南京市城市交通态势聚合任务
            </Breadcrumb.Item>
          </Breadcrumb>
          {/* 左侧抽屉 */}
          <div style={{width: "100%", height: "650px", position: "relative"}}>
            <Drawer
                title="Components"
                placement={placement}
                closable={false}
                onClose={this.onClose}
                visible={visible}
                key={placement}
                mask={false}
                getContainer={false}
                style={{ position: 'absolute' }}
                closable={true}
            >
              <Card
                  title="Map"
                  style={{ width: "90%", marginTop: "5px" }}
                  hoverable
                  //onClick={this.clickComponent.bind(this, "Map")}
                  onClick={this.dataConfig.bind(
                      this,
                      this.state.componentKey,
                      "Map"
                  )}
              >
                <img src={_MAP} style={{ height: 120, width: 130 }} />
              </Card>
              <Card
                  title="text"
                  style={{ width: "90%", marginTop: "5px" }}
                  hoverable
                  onClick={this.dataConfig.bind(
                      this,
                      this.state.componentKey,
                      "text"
                  )}
              >
                <img src={_Text} style={{ height: 120, width: 130 }} />
              </Card>
              <Card
                  title="video"
                  style={{ width: "90%", marginTop: "5px" }}
                  hoverable
                  //onClick={this.clickComponent.bind(this, "video")}
                  onClick={this.dataConfig.bind(
                      this,
                      this.state.componentKey,
                      "video"
                  )}
              >
                <img src={_VIDEO} style={{ height: 120, width: 130 }} />
              </Card>
              <Card
                  title="audio"
                  style={{ width: "90%", marginTop: "5px" }}
                  hoverable
                  //onClick={this.clickComponent.bind(this, "audio")}
                  onClick={this.dataConfig.bind(
                      this,
                      this.state.componentKey,
                      "audio"
                  )}
              >
                <img src={_AUDIO} style={{ height: 120, width: 130 }} />
              </Card>
              <Card
                  title="Line Chart"
                  style={{ width: "90%", marginTop: "5px" }}
                  hoverable
                  onClick={this.clickComponent.bind(this, "LineChart")}
              >
                <img src={_CHART} style={{ height: 120, width: 130 }} />
              </Card>
              <Card
                  title="Table"
                  style={{ width: "90%", marginTop: "5px" }}
                  hoverable
                  onClick={this.clickComponent.bind(this, "Table")}
              >
                <img src={_TABLE} style={{ height: 120, width: 130 }} />
              </Card>
            </Drawer>
            {/*模态弹出框-设置面板*/}
            <Modal
                title={
                  <div
                      style={{
                        width: '100%',
                        cursor: 'move',
                      }}
                      onMouseOver={() => {
                        if (disabled) {
                          this.setState({
                            disabled: false,
                          });
                        }
                      }}
                      onMouseOut={() => {
                        this.setState({
                          disabled: true,
                        });
                      }}
                      onFocus={() => {}}
                      onBlur={() => {}}
                      // end
                  >
                    Config
                  </div>
                }
                visible = {visibleModal}
                // 点击确定执行查询，并关闭模态框
                onOk={this.getData.bind(this,this.dataUrl,this.dataConfigtype)}
                onCancel={this.hideModal}
                closable={true}
                modalRender={modal => (
                    <Draggable
                        disabled={disabled}
                        bounds={bounds}
                        onStart={(event, uiData) => this.onStart(event, uiData)}
                    >
                      <div ref={this.draggleRef}>{modal}</div>
                    </Draggable>
                )}
            >
              <div>{this.dataConfigDom}</div>
            </Modal>
            {/* 左侧抽屉控制按钮 */}
            <Button
                type="primary"
                className={style["drawerBtn"]}
                onClick={this.showDrawer}
                icon={<MenuUnfoldOutlined />}
            >
            </Button>
            {/*发布组件专题按钮*/}
            {/*<Button*/}
            {/*  type="primary"*/}
            {/*  className={style["publishBtn"]}*/}
            {/*  onClick={this.publishComponentsVisualization}*/}
            {/*>*/}
            {/*  Publish*/}
            {/*</Button>*/}
            {/* 更新组件专题按钮 */}
            <Button
                type="primary"
                className={style["updateBtn"]}
                onClick={this.updateComponent}
            >
              Update
            </Button>
            {/* 组件容器 */}
            <div
                id="componentParent"
                style={{
                  height: "90%",
                  width: "100%",
                  position: "relative",
                  padding: "0",
                }}
            >
              {this.state.mainComponentDom}
            </div>
          </div>

        </div>
    );
  }
}

function createBaiMapData() {
  let data = [
    [
      1164383,
      401471,
      -11,
      -13,
      1,
      -49,
      -26,
      -14,
      99,
      -170,
      4,
      -36,
      87,
      -2,
      16,
      -141,
      -2,
      -15,
      -47,
      -6,
      -168,
      -9,
      -2,
      22,
      -74,
      -4,
      -138,
      10,
      12,
      -152,
      9,
      -55,
      -17,
      -111,
      13,
      -176,
      -20,
      -38,
      1,
      -57,
      31,
      -54,
      28,
      -85,
      -5,
      -126,
      -13,
      -62,
      1,
      -34,
      -84,
      1,
      -3,
      -218,
      15,
      6,
      78,
      2,
      4,
      -52,
      70,
      1,
      7,
      -142,
      99,
      2,
      21,
      -5,
      229,
      -150,
      16,
      -23,
      0,
      -180,
      168,
      1,
      -4,
      -257,
      82,
      0,
      33,
      -22,
      78,
      20,
    ],
    [
      1164311,
      399594,
      8,
      1,
      33,
      0,
      56,
      1,
      29,
      3,
      30,
      27,
      8,
      -5,
      21,
      -14,
      3,
      -2,
      1,
      -1,
      -4,
      -3,
      -13,
      -13,
      -21,
      -19,
      -9,
      -9,
      -16,
      -9,
      -93,
      -6,
      8,
      50,
      -100,
      -1,
      -1,
      0,
      -171,
      -3,
      -1,
      0,
      0,
      -1,
      0,
      -8,
      0,
      -15,
      1,
      -8,
      0,
      -28,
      0,
      -4,
      1,
      -11,
      0,
      -19,
      0,
      -12,
      2,
      -22,
      1,
      -26,
      1,
      -25,
      0,
      -25,
      1,
      -24,
      0,
      -4,
      2,
      -25,
      0,
      -1,
      0,
      -7,
      1,
      -28,
      2,
      -25,
      17,
      -30,
      0,
      -1,
      0,
      -2,
      2,
      -55,
      -44,
      -1,
      0,
      -32,
      1,
      0,
      48,
      3,
      11,
      0,
      12,
      -34,
      0,
      -7,
      -24,
      -9,
      0,
      -12,
      1,
      -12,
      0,
      -1,
      7,
      0,
      8,
      0,
      8,
      0,
      41,
      1,
      1,
      -10,
      0,
      -4,
      1,
      -5,
      0,
      -6,
      0,
      -3,
      2,
      -21,
      1,
      -12,
      0,
      -8,
      0,
      -2,
      2,
      0,
      31,
      -6,
      6,
      0,
      51,
      2,
      0,
      1,
      -70,
      0,
      7,
      13,
      7,
      5,
      30,
      22,
      1,
      1,
    ],
    [
      1163521,
      398776,
      16,
      1,
      55,
      2,
      47,
      1,
      0,
      1,
      0,
      0,
      0,
      4,
      0,
      8,
      0,
      1,
      -1,
      7,
      0,
      11,
      -1,
      16,
      0,
      4,
      0,
      15,
      -1,
      45,
      0,
      1,
      -1,
      16,
      -1,
      54,
      0,
      29,
      0,
      1,
      0,
      2,
      0,
      1,
      0,
      2,
      0,
      4,
      0,
      37,
      -1,
      7,
      0,
      7,
      0,
      18,
      107,
      0,
      2,
      0,
      30,
      0,
      129,
      2,
      56,
      2,
      36,
      1,
      22,
      1,
      53,
      1,
      47,
      2,
      7,
      0,
      8,
      0,
      8,
      0,
      41,
      1,
      2,
      1,
      17,
      0,
      4,
      0,
      10,
      0,
      33,
      1,
      7,
      0,
      18,
      0,
      1,
      -32,
      0,
      -2,
      37,
      0,
      1,
      0,
    ],
  ];
  var busLines = [].concat.apply([], data.map(function (busLine, idx) {
    var prevPt;
    var points = [];
    for (var i = 0; i < busLine.length; i += 2) {
      var pt = [busLine[i], busLine[i + 1]];
      if (i > 0) {
        pt = [
          prevPt[0] + pt[0],
          prevPt[1] + pt[1]
        ];
      }
      prevPt = pt;

      points.push([pt[0] / 1e4, pt[1] / 1e4]);
    }
    return {
      coords: points
    };
  }));

  let  bmap= {
    center: [116.46, 39.92],
    zoom: 10,
    roam: true,
    mapStyle: {
      styleJson: [{
        'featureType': 'water',
        'elementType': 'all',
        'stylers': {
          'color': '#d1d1d1'
        }
      }, {
        'featureType': 'land',
        'elementType': 'all',
        'stylers': {
          'color': '#f3f3f3'
        }
      }, {
        'featureType': 'railway',
        'elementType': 'all',
        'stylers': {
          'visibility': 'off'
        }
      }, {
        'featureType': 'highway',
        'elementType': 'all',
        'stylers': {
          'color': '#fdfdfd'
        }
      }, {
        'featureType': 'highway',
        'elementType': 'labels',
        'stylers': {
          'visibility': 'off'
        }
      }, {
        'featureType': 'arterial',
        'elementType': 'geometry',
        'stylers': {
          'color': '#fefefe'
        }
      }, {
        'featureType': 'arterial',
        'elementType': 'geometry.fill',
        'stylers': {
          'color': '#fefefe'
        }
      }, {
        'featureType': 'poi',
        'elementType': 'all',
        'stylers': {
          'visibility': 'off'
        }
      }, {
        'featureType': 'green',
        'elementType': 'all',
        'stylers': {
          'visibility': 'off'
        }
      }, {
        'featureType': 'subway',
        'elementType': 'all',
        'stylers': {
          'visibility': 'off'
        }
      }, {
        'featureType': 'manmade',
        'elementType': 'all',
        'stylers': {
          'color': '#d1d1d1'
        }
      }, {
        'featureType': 'local',
        'elementType': 'all',
        'stylers': {
          'color': '#d1d1d1'
        }
      }, {
        'featureType': 'arterial',
        'elementType': 'labels',
        'stylers': {
          'visibility': 'off'
        }
      }, {
        'featureType': 'boundary',
        'elementType': 'all',
        'stylers': {
          'color': '#fefefe'
        }
      }, {
        'featureType': 'building',
        'elementType': 'all',
        'stylers': {
          'color': '#d1d1d1'
        }
      }, {
        'featureType': 'label',
        'elementType': 'labels.text.fill',
        'stylers': {
          'color': '#999999'
        }
      }]
    }
  }
  let series= [{
    type: 'lines',
    coordinateSystem: 'bmap',
    polyline: true,
    data: busLines,
    silent: true,
    lineStyle: {
      color: 'rgb(200, 35, 45)',
      opacity: 1,
      width: 5
    },
    progressiveThreshold: 500,
    progressive: 200
  }]
  return {bmap,series};

}

export default NanJingCityTrafficAggregation;
