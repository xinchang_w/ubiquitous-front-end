import React, { Component } from 'react';
import axios from 'axios'
import { Layout, Menu, Divider , Row, Col, Pagination, List, Card, Image, message, Typography } from 'antd'

import '@/assets/style/home.css'
import './index.css'

const { Header} = Layout;
const { Meta } = Card;
const { Title } = Typography;

// 加拦截器，get里面设置content-type
const options = {
  headers: {
      'Content-Type': 'application/json'
  }
}
axios.interceptors.request.use(
  config => {
      config.headers['Content-Type'] = 'application/json'
      return config
  }
)
const request = axios.create(options)
request.interceptors.request.use(config => {
  if(config.method === 'get'){
      config.data = true
  }
  config.headers['Content-Type'] = 'application/json'
  return config
}, err => Promise.reject(err))

// 主类
class FileList extends Component {
  state = {
    nowType: 'image',

    folderDataUrl: 'http://172.21.213.50:5050/view/soundFolder',
    folderData: [],

    filesDataUrl: 'http://172.21.213.50:5050/view/files',
    folderOid: '',
    filesData: '',

    imageDataUrl: 'http://172.21.213.50:5050/view/file',
    imageDataRoute: '',
    imageData: '',
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.getFolderData()
  }

  getFolderData() {
    console.log('get folder data')
    let that = this
    axios({
      method: 'GET',
      url: this.state.folderDataUrl
    }).then((res) => {
      console.log('res: ', res)
      if(res.status == 200) {
        that.setState({
          fileData: res.data,
        })
      }
      }).catch((err) => console.log('get folder data wrong: ', err))
  }

  getFile() {

  }

  render() {
    return (
      <Layout id='layout'>
        <Header className="header" style={{height: '60px'}}>
          <h1 id='header_h1'>泛在数据信息建模平台音频展示</h1>
        </Header>

              <List
              grid={{
                  gutter: 16,
                  xs: 1,
                  sm: 2,
                  md: 3,
                  lg: 4,
                  xl: 5,
                  xxl: 6,
                }}
                size="small"
                pagination={{
                  onChange: page => {
                    console.log(page);
                  },
                  showQuickJumper: true,
                  showSizeChanger: true,
                  pageSize: 85,
                }}
                dataSource={this.state.fileData}
                renderItem={item => (
                    <List.Item onClick={() => window.open("http://172.21.213.50:5050/view/soundFile?name=" + item)}>
                      <Card
                        hoverable
                        size='small'
                        style={{ width: 200 }}
                      >
                        <Meta
                          title={item}
                        />
                      </Card>
                  </List.Item>
              )}
            />
      </Layout>
    )
  }
}

export default FileList;