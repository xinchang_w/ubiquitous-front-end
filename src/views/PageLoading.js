import React from 'react'
import {Loading} from '@/components'

export default function PageLoading() {
  return (
    <Loading size='large' masker={false} loading={true}/>
  )
}
