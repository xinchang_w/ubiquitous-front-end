import React, { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, Row, Col, message} from 'antd';
import axios from 'axios'

const EditableContext = React.createContext(null);


const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef(null);
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

class TypeTag extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: '分类标志',
        dataIndex: 'type',
        width: '30%',
        editable: true,
      },
      {
        title: '分类结果',
        dataIndex: 'name',
        width: '50%',
        editable: true,
      },
      {
        title: 'operation',
        dataIndex: 'operation',
        render: (_, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="确认删除吗?" onConfirm={() => this.handleDelete(record.key)}>
              <Button>删除</Button>
            </Popconfirm>
          ) : null,
      },
    ];
    this.state = {
      dataSource: [
        {
          key: '0',
          type: 'eg: 专题',
          name: 'eg：降雨(双击即可编辑)'
        }
      ],
      count: 1,

      metaId: props.metaId,
      getTagWithImageId: props.getTagWithImageId,
    };

    this.cancel = this.cancel.bind(this)
  }

  handleDelete = (key) => {
    const dataSource = [...this.state.dataSource];
    this.setState({
      dataSource: dataSource.filter((item) => item.key !== key),
    });
  };
  handleAdd = () => {
    const { count, dataSource } = this.state;
    const newData = {
      key: count,
      type:'eg: 专题',
      name: 'eg：降雨'
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  };
  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      dataSource: newData,
    });
  };

  submit() {
    let dataSource = this.state.dataSource
    if(dataSource.length <1){
      message.warning('没有数据')
      return
    }
    let metaId = this.state.metaId
    let categoryList = []
    let metaSource = "ULA_Meta_RasterMap"
    for(let i = 0; i < dataSource.length; ++i){
      let temp = {}
      temp[dataSource[i].type]= (dataSource[i].name)
      categoryList.push(temp)
    }

    let res = {
      metaId: metaId,
      metaSource: metaSource,
      categoryList: categoryList
    }
    let formData =  new FormData();
    formData.append('metaId', metaId);
    formData.append('metaSource', metaSource);
    formData.append('categoryList', categoryList);
    try {
      axios({
          url: 'http://106.14.78.235:8086/GenericMap/rasterMapTypeTag',
          method: 'POST',
          data: res,
          headers: { 'Content-Type': 'application/json;charset=UTF-8'}
      }).then(res =>{
          if(res.data.code === 0) {
              message.success('提交成功')
          }
      })
    } catch (error) {
        message.error('提交失败')
    }
    this.state.getTagWithImageId();
  }

  cancel() {
    this.setState({
      dataSource: [{
        key: '0',
        type: 'eg: 专题',
        name: 'eg：降雨(双击即可编辑)'
      }]
    })
  }

  render() {
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });

    return (
      <div>
        <Button
          onClick={this.handleAdd}
          type="primary"
          style={{
            marginBottom: 16,
          }}
        >
          Add a row
        </Button>
        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
          pagination= {{pageSize: 5}}
        />
        <Row type="flex" justify="center" align="middle">
          <Col>
            <Button type="primary" onClick={()=> this.submit()}>
            Submit
            </Button>
            <Button htmlType="button" onClick={() => this.cancel()}>
            Reset
            </Button>
          </Col>
        </Row>

      </div>
    );
  }
}

export default TypeTag;