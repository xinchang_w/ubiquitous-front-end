import DataServerTool from '../../components/DataServerTool/Tool'
import MapDiv from './Map'
import './index.css'
import React from 'react'
import { Layout, Menu, Divider, Row, Col } from 'antd';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

class VideoTool extends React.Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }

  render(){
    return (
    <Layout style={{height: '100vh', minWidth: '1000px', minHeight: '600px',}}>
      <Row style={{height: '100%', marginTop: '10px'}}>
        <Col span={4}>
          <DataServerTool type='Video' />
        </Col>
        <Col span={20}>
        <MapDiv />
        </Col>
        <Col span={4}>

        </Col>
      </Row>
    </Layout>
    );
  }
}


export default VideoTool;