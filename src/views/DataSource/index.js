import React, { Component } from "react";
import { Breadcrumb, Card, Button, Tooltip, Modal, Form, Input, Upload, message } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { Loading } from "@/components";
import style from "./index.module.css";
import defaultPic from "@/assets/image/default.png";
import axios from "axios";
import { NODE_PATH, FILE_SERVER } from "@/assets/global";

let { Grid } = Card;
class AggrTem extends Component{
    constructor(props){
        super(props);
        this.state = {
            visible: false,
            globalLoading: true,
            loadding: false,
            items: []
        };

        this.formRef = React.createRef();
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.save = this.save.bind(this);
    }

    componentDidMount(){
        this.search();
    }

    render(){
        let { visible, loading, globalLoading } = this.state;
        let layout = {
            labelCol: { offset:2, span:6 },
            wrapperCol: { span:12 }
        };
        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>数据资源</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">数据来源</Breadcrumb.Item>
                </Breadcrumb>
                <div className="searchBox">
                    <label className="searchBox-label">来源：</label>
                    <Tooltip title="新增" placement="bottom">
                        <Button shape="circle" size="small" onClick={this.showModal}>+</Button>
                    </Tooltip>
                </div>
                <div className={style["cardBox"]}>
                    <Loading loading={globalLoading}>
                        <Card bordered={false}>
                            {
                                this.state.items.map((item, ind) => (
                                    <Grid key={ind} className={style["grid"]}>
                                        <div className={style["grid-div"]}>
                                            <a className={style["anchor"]} href={item.site} target="_blank">
                                                <img
                                                    className={style["image"]}
                                                    alt={item.title}
                                                    src={item.picture ? `${FILE_SERVER}/${item.picture}` : defaultPic}
                                                    height="50"
                                                />
                                                <br />
                                                <span>{item.title}</span>
                                            </a>
                                        </div>
                                    </Grid>
                                ))
                            }
                        </Card>
                    </Loading>
                </div>
                {/*新增弹框*/}
                <Modal
                    visible={visible}
                    title="添加来源"
                    onOk={this.hideModal}
                    onCancel={this.hideModal}
                    footer={[
                        <Button key="back" onClick={this.hideModal}>
                            取消
                        </Button>,
                        <Button key="submit" loading={loading} type="primary" onClick={() => this.formRef.current.submit()}>
                            保存
                        </Button>
                    ]}
                >
                    <Form
                        {...layout}
                        onFinish={this.save}
                        ref={this.formRef}
                    >
                        <Form.Item
                            label="名称"
                            name="title"
                            rules={[{ required:true }]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="缩略图"
                            name="picture"
                        >
                            <Upload 
                                beforeUpload={() => false}
                                maxCount={1}
                                multiple={false}
                            >
                                <Button icon={<UploadOutlined />}>选择文件</Button>
                            </Upload>
                        </Form.Item>
                        <Form.Item
                            label="网址"
                            name="site"
                            rules={[{ required:true }, { pattern: /^http:\/\/|^https:\/\//, message:"请输入正确的网址" }]}
                        >
                            <Input />
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        );
    }

    showModal(){
        this.setState({
            visible: true
        });
    }

    hideModal(){
        this.formRef.current.resetFields();
        this.setState({
            visible: false,
            loading: false
        });
    }

    save(values){
        this.setState({
            "loading": true
        });

        let form = new FormData();
        form.append("title", values.title);
        form.append("site", values.site);
        form.append("picture", values.picture ? values.picture.file : "");
        axios.post(`${NODE_PATH}/dataSource/add`, form).then(res => {
            message.success("保存成功", 0.5, () => {
                this.setState({
                    "loading": false,
                    "visible": false,
                    "globalLoading": true
                }, () => {
                    this.formRef.current.resetFields();
                    this.search();
                });
            });
        }).catch(err => {
            message.error("保存失败");
        });
    }

    search(){
        axios.get(`${NODE_PATH}/dataSource/search`).then(res => {
            if(res.data.code){
                this.setState({
                    "items": res.data.data,
                    "globalLoading": false
                });
            }
        }).catch(err=>{
            console.log(err);
        });
    }
}

export default AggrTem;