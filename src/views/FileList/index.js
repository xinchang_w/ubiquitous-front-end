import React, { Component } from 'react';
import axios from 'axios'
import { Layout, Menu, Divider , Row, Col, Pagination, List, Card, Image, message, Typography } from 'antd'

import '@/assets/style/home.css'
import './index.css'

const { Header} = Layout;
const { Meta } = Card;
const { Title } = Typography;

// 加拦截器，get里面设置content-type
const options = {
  headers: {
      'Content-Type': 'application/json'
  }
}
axios.interceptors.request.use(
  config => {
      config.headers['Content-Type'] = 'application/json'
      return config
  }
)
const request = axios.create(options)
request.interceptors.request.use(config => {
  if(config.method === 'get'){
      config.data = true
  }
  config.headers['Content-Type'] = 'application/json'
  return config
}, err => Promise.reject(err))

// 主类
class FileList extends Component {
  state = {
    nowType: 'image',

    folderDataUrl: 'http://172.21.213.50:5050/view/folders',
    folderData: [],

    filesDataUrl: 'http://172.21.213.50:5050/view/files',
    folderOid: '',
    filesData: '',

    imageDataUrl: 'http://172.21.213.50:5050/view/file',
    imageDataRoute: '',
    imageData: '',
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.getFolderData()
  }

  getFolderData() {
    console.log('get folder data')
    let that = this
    axios({
      method: 'GET',
      url: this.state.folderDataUrl
    }).then((res) => {
      if(res.status == 200) {
        that.setState({
          folderData: res.data,
          folderOid: res.data[0].oid
        })
        that.getFilesData()
      }
      }).catch((err) => console.log('get folder data wrong: ', err))
  }

  getFilesData() {
    console.log('get files data')
    let formData = new FormData()
    console.log('folderOid: ', this.state.folderOid)
    formData.append('oid', this.state.folderOid)
    axios.post(this.state.filesDataUrl, formData).then((res) => {
      if(res.status == 200) {
        this.setState({
          filesData: res.data
        })
      }
    })
  }

  changeFiles(key) {
    console.log('change files')
    let oid =  this.state.folderData[key.key].oid
    this.setState({
      folderOid: oid
    })
    console.log('oid: ', this.state.folderOid)
    this.getFilesData()
  }

  getImageData(name) {
    console.log('name: ', name)
    axios({
      method: 'GET',
      url: this.state.imageDataUrl,
      params: {
        folderOid: this.state.folderOid,
        name: name
      }
    }).then( (res) => {
      if(res.status == 200) {
        return res.data
      }
    })
  }

  render() {
    return (
      <Layout id='layout'>
        <Header className="header" style={{height: '60px'}}>
          <h1 id='header_h1'>泛在数据信息建模平台图片展示</h1>
        </Header>
          <Row style={{marginTop: '5px'}}>
            <Col span={21} push={3}>
              {this.state.nowType === 'image' &&
                <List
                grid={{
                  gutter: 16,
                  xs: 1,
                  sm: 2,
                  md: 3,
                  lg: 4,
                  xl: 5,
                  xxl: 6,
                }}
                size="small"
                pagination={{
                  onChange: page => {
                    console.log(page);
                  },
                  showQuickJumper: true,
                  showSizeChanger: true,
                  pageSize: 24,
                }}
                dataSource={this.state.filesData}
                renderItem={item => (
                    <List.Item>
                      <Card
                        hoverable
                        size='small'
                        style={{ width: 200 }}
                        cover={
                          <Image
                          height={140}
                          src= {`http://127.0.0.1:5050/view/file?folderOid=${this.state.folderOid}&name=${item.name}`}
                          />
                        }
                      >
                        <Meta
                          title={item.name}
                        />
                      </Card>
                  </List.Item>
                )}
              />}
              {this.state.nowType === 'audio' &&
                <List
                grid={{
                  gutter: 16,
                  xs: 1,
                  sm: 2,
                  md: 3,
                  lg: 4,
                  xl: 5,
                  xxl: 6,
                }}
                size="small"
                pagination={{
                  onChange: page => {
                    console.log(page);
                  },
                  showQuickJumper: true,
                  showSizeChanger: true,
                  pageSize: 15,
                }}
                dataSource={this.state.filesData}
                renderItem={item => (
                    <List.Item>
                      <Card
                        hoverable
                        size='small'
                        style={{ width: 200 }}
                        cover={
                          <Image
                          height={140}
                          src= {`http://127.0.0.1:5050/view/file?folderOid=${this.state.folderOid}&name=${item.name}`}
                          />
                        }
                      >
                        <Meta
                          title={item.name}
                        />
                      </Card>
                  </List.Item>
                )}
              />}
              {this.state.nowType === 'text' &&
                <List
                grid={{
                  gutter: 16,
                  xs: 1,
                  sm: 2,
                  md: 3,
                  lg: 4,
                  xl: 5,
                  xxl: 6,
                }}
                size="small"
                pagination={{
                  onChange: page => {
                    console.log(page);
                  },
                  showQuickJumper: true,
                  showSizeChanger: true,
                  pageSize: 15,
                }}
                dataSource={this.state.filesData}
                renderItem={item => (
                    <List.Item>
                      <Card
                        hoverable
                        size='small'
                        style={{ width: 200 }}
                        cover={
                          <Image
                          height={140}
                          src= {`http://127.0.0.1:5050/view/file?folderOid=${this.state.folderOid}&name=${item.name}`}
                          />
                        }
                      >
                        <Meta
                          title={item.name}
                        />
                      </Card>
                  </List.Item>
                )}
              />}
          </Col>
          <Col span={3} pull={21}>
          <Menu
          id = 'menu'
          onClick={(key) => this.changeFiles(key)}
          defaultSelectedKeys={['0']}
          mode="inline"
          >
            <Title style={{fontSize: 15}}>文件夹（双击切换）</Title>
            {
              this.state.folderData.map( (item, key) => {
                return (<Menu.Item key = {key}>
                  {item['name']}
                </Menu.Item> )
              })
            }
          </Menu>
          </Col>
        </Row>
      </Layout>
    )
  }
}

export default FileList;