import React, { Component } from "react";
import { Card, Drawer, Button, Input, DatePicker, Modal, message, Steps, Divider } from "antd";
import { MenuUnfoldOutlined, RollbackOutlined, SaveOutlined } from "@ant-design/icons";
import Draggable from "react-draggable";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import DraggableCard from "@/components/customizeCard/index";
import axios from "axios";
import { TableRendering } from "@/components";
import { stopPropagation } from "@/utility";
import audioSvg from "@/assets/image/data-query-audio.svg";
import videoSvg from "@/assets/image/data-query-video.svg";
import { FILE_SERVER, NODE_PATH } from "@/assets/global";

import _TimeLine from "@/assets/image/时间轴.jpg"
import _G312First from "@/assets/image/G312国道宣布动工.jpg"
import _G312First_ from "@/assets/image/G312国道宣布动工简介.jpg"
import _G312Second from "@/assets/image/G312国道遮挡封禁介绍.jpg"
import _G312Second_ from "@/assets/image/G312国道遮挡封禁地图.jpg"
import _G312Third from "@/assets/image/G312国道主线打通.jpg"
import _G312Third_ from "@/assets/image/G312国道主线打通地图.jpg"
import _G312Forth from "@/assets/image/G312国道全线贯通.jpg"
import _G312Forth_ from "@/assets/image/G312国道全线贯通地图.jpg"

import { Layout, Menu } from 'antd';
import { NotificationOutlined } from '@ant-design/icons';
import { Allias_Com_Map, DataType_Url_Map } from "@/components/aggregation";

const { Sider } = Layout;
const { Step } = Steps;


const ITEMS = {
    "text": {
        "title": "文本",
        "url": "aggregation/text/query",
        "momentTime": "2021/06/26 17:00:00",
        "searchCons": [
            {
                "label": "内容",
                "value": "content"
            },
            {
                "label": "来源",
                "value": "author"
            },
            {
                "label": "发布日期",
                "value": "time"
            }
        ],
        "cols": [
            {
                title: '发布日期',
                dataIndex: 'time',
                width: "180px",
                onCell: stopPropagation
            }, {
                title: "内容",
                dataIndex: "content",

                ellipsis: {
                    showTitle: false
                },
                className: "pointer"
            }, {
                title: '来源',
                dataIndex: 'author',
                width: "140px",
                onCell: stopPropagation
            }
        ]
    },
    "video": {
        "title": "视频",
        "url": "aggregation/video/query",
        "momentTime": "2021/06/29 18:37:21",

        "searchCons": [
            {
                "label": "拍摄时间",
                "value": "date"
            },
            {
                "label": "摄像头位置",
                "value": "location"
            }
        ],
        "cols": [
            {
                "title": "拍摄时间",
                "dataIndex": "time",
                "width": "160px",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "dataIndex": "video",
                "ellipsis": true,
                "render": () => <img src={videoSvg} width="20" height="20" alt="视频" />,
                "className": "pointer"
            },
            {
                "title": "摄像头位置",
                "dataIndex": "location",
                "width": "150px",
                "ellipsis": true,
                "className": "pointer"
            },

        ]
    },
    "audio": {
        "title": "音频",
        "url": "aggregation/audio/query",
        "momentTime": "2021/06/29 17:04:00",
        "searchCons": [
            {
                "label": "节目名",
                "value": "programName"
            },
            {
                "label": "播出时间",
                "value": "date"
            }
        ],
        "cols": [
            {
                "title": "播出时间",
                "dataIndex": "time",
                "width": "200px",
                "ellipsis": true,
                "onCell": stopPropagation
            },
            {
                "title": "内容",
                "dataIndex": "url",
                "render": () => <img src={audioSvg} width="20" height="20" alt="音频" />,
                "className": "pointer"
            },
            {
                "title": '来源',
                "dataIndex": 'source',
                "width": "160px",
            }
        ]
    },
};

class Task extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //专题信息存储
            "themeInfo": [],
            //组件信息存储
            "componentsInfo": [],
            //组件宽高配置
            "widthValue": 400,
            "heightValue": 220,
            "selectedMenuKey": "",
            "cardTitle": "",
            "selectedThemeIndex": 0
        }

        this.dataDomG312 = [];
        //数据配置面板append jsx dom
        this.dataConfigDom = [];
        //记录哪个类型组件被点击
        this.dataConfigtype = "";
        //此可视化案例唯一标识
        this.uid = window.location.search.slice(4);

        this.dataIds = [];

        //左侧组件的id
        this.componentId = "";

        this.dataUrl = "";
        this.textUrl = `${NODE_PATH}/${ITEMS["text"].url}`;
        this.videoUrl = `${NODE_PATH}/${ITEMS["video"].url}`;
        this.audioUrl = `${NODE_PATH}/${ITEMS["audio"].url}`;
        this.textCols = ITEMS["text"].cols;
        this.videoCols = ITEMS["video"].cols;
        this.audioCols = ITEMS["audio"].cols;
        this.pageSize = 2;

        this.dateTime = "";
        this.textMomentTime = moment(ITEMS["text"].momentTime, 'YYYY/MM/DD HH:mm:ss');
        this.videoMomentTime = moment(ITEMS["video"].momentTime, 'YYYY/MM/DD HH:mm:ss');
        this.audioMomentTime = moment(ITEMS["audio"].momentTime, 'YYYY/MM/DD HH:mm:ss');
        this.momentTime = null;
        this.weiboData = [];
        this.videoData = "";
        this.audioData = "";

        //组件容器的ref
        this.componentParentRef = React.createRef();

        //可拖动模态框的ref
        this.draggleRef = React.createRef();

        this.uidGenerate = this.uidGenerate.bind(this);
        this.addComponentInfo = this.addComponentInfo.bind(this);
        this.deleteComponentInfo = this.deleteComponentInfo.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);

        this.textTableRef = React.createRef();
        this.videoTableRef = React.createRef();
        this.audioTableRef = React.createRef();
        this.tableRef = React.createRef();


        this.selectRow = this.selectRow.bind(this);
        this.selectRecord = {};


    }


    state = {
        //设置面板默认key

        //组件唯一标识
        componentKey: uuidv4(),
        //左侧抽屉参数
        visible: false,
        //模态框参数
        disabled: true,
        visibleModal: false,
        bounds: { left: 0, top: 0, bottom: 0, right: 0 },

        //数据配置
        dataConfigDom: this.dataConfigDom,

        current: 0,
        dataDomG312: this.dataDomG312,
    };

    componentDidMount() {
        //this.getComponentInfo();
        this.getThemeInfo();
    }

    componentDidUpdate() {

    }


    //组件面板（抽屉）的关闭与显示
    showDrawer = () => {
        this.setState({
            visible: !this.state.visible,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    //获取时间设置面板所选的时间
    onChange(value, dateString) {
        this.dateTime = dateString;
    }

    //模态框的显示与隐藏
    showModal() {
        this.setState({
            visibleModal: true,
            dataConfigDom: [],
        });
    }

    hideModal() {
        this.setState({
            visibleModal: false,
        });
    }

    onStart = (event, uiData) => {
        const { clientWidth, clientHeight } = window?.document?.documentElement;
        const targetRect = this.draggleRef?.current?.getBoundingClientRect();
        this.setState({
            bounds: {
                left: -targetRect?.left + uiData?.x,
                right: clientWidth - (targetRect?.right - uiData?.x),
                top: -targetRect?.top + uiData?.y,
                bottom: clientHeight - (targetRect?.bottom - uiData?.y),
            },
        });
    };

    //当前选中选题
    selectMenu = (selectedKeys) => {
        this.setState({
            selectedThemeIndex: selectedKeys.key
        });
    }

    //查询专题数据
    getThemeInfo = () => {
        let taskID = this.props.item._id;
        axios
            .get(`${NODE_PATH}/aggregation/theme/search?taskId=${taskID}`)
            .then((res) => {
                let themeInfo = res.data.data;
                this.setState({
                    themeInfo: themeInfo,
                });
            });
        //将查询到的第一个专题的id赋值给当前
    }
    //加载专题数据到页面，渲染专题的menu
    loadThemeDom = () => {
        return (
            <Menu
                mode="inline"
                defaultSelectedKeys={[this.state.selectedThemeIndex.toString()]}
                defaultOpenKeys={[this.state.selectedThemeIndex.toString()]}
                style={{ height: '100%', backgroundColor: "#F0F0F0" }}
                onSelect={(selectedKeys) => this.selectMenu(selectedKeys)}
            >
                {
                    this.state.themeInfo.map((item, index) => {
                        let themeDom;
                        themeDom = (
                            <Menu.Item key={index.toString()} icon={<NotificationOutlined />}>
                                {item.title}
                            </Menu.Item>
                        )
                        return themeDom;
                    })
                }
            </Menu>

        );
    }
    loadDrawCardDom = () => {
        let drawCard = this.props.item.componentIds;

        return (
            drawCard.map((item) => {
                let drawCardDom;
                drawCardDom = (
                    <Card
                        key={item._id}
                        title={item.title}
                        style={{ width: "90%", marginTop: "5px" }}
                        hoverable
                        onClick={this.dataConfig.bind(
                            this,
                            item._id,
                            item.alias
                        )}
                    >
                        <img src={`${FILE_SERVER}/${item.cover}`} style={{ height: 120, width: 130 }} />
                    </Card>
                )
                return drawCardDom;
            })
        );
    }

    //生成uid
    uidGenerate = () => {
        this.setState({
            componentKey: uuidv4(),
        });
        return this.state.componentKey;
    };

    selectRow(record) {
        this.selectRecord = record;
    }

    addComponentInfo(componentId) {
        let themeInfo = this.state.themeInfo;

        let components = {};
        this.props.item.componentIds.forEach(item => {
            components[item._id] = item;
        });
        let comInfo = components[componentId];

        if (comInfo.dataType) {
            let comData = this.selectRecord;
            themeInfo[this.state.selectedThemeIndex].content.push({
                componentId: componentId,
                title: this.state.cardTitle,
                style: {
                    top: 200,
                    left: 400,
                    width: Number(this.state.widthValue),
                    height: Number(this.state.heightValue)
                },
                dataIds: [comData._id],
            });
        } else {
            themeInfo[this.state.selectedThemeIndex].content.push({
                componentId: componentId,
                title: this.state.cardTitle,
                style: {
                    top: 200,
                    left: 400,
                    width: Number(this.state.widthValue),
                    height: Number(this.state.heightValue)
                },
            });
        }

        this.hideModal();
        this.setState({
            themeInfo: themeInfo,
        });
    }

    //简化设置面板调出方式时再用到下面两个函数
    search(componentType) {
        //查询条件
        let con = {};
        let curSearchCon = "time";
        let keywords = this.dateTime;
        if (this.dateTime == "") {
            keywords = this.momentTime[componentType].value;
        }

        let url = `${NODE_PATH}/${ITEMS[componentType].url}`;
        let pageSize = 2;
        let columns = ITEMS[componentType].cols;
        if (curSearchCon && keywords) {
            con[curSearchCon] = keywords;
        }
        this.tableRef.current.search(url, columns, con, pageSize);
    }

    dataConfig = (componentId, componentType) => {
        if (this.dataConfigDom.length) {
            this.dataConfigDom = [];
            this.setState({
                dataConfigDom: this.dataConfigDom,
            });
        }
        this.showModal();//加载模态框
        this.componentId = componentId;

        let components = {};
        this.props.item.componentIds.forEach(item => {
            components[item._id] = item;
        });
        let comInfo = components[componentId];
        //需要加载数据的控件设置信息面板
        if (comInfo.dataType) {
            this.momentTime = moment(ITEMS[componentType].momentTime, 'YYYY/MM/DD HH:mm:ss');
            this.momentTime[componentType] = {
                value: this.momentTime._i
            };
            this.dataConfigDom.push(
                <div key={componentId + "_" + componentType}>
                    <h3>标题设置</h3>
                    <Card
                        style={{ width: '100%', textAlign: "center" }}
                    >
                        <span>标题：</span>
                        <Input
                            style={{ width: "180px" }}
                            onChange={(e) => {
                                this.setState({ cardTitle: e.target.value });
                            }}>
                        </Input>
                    </Card>
                    <h3>宽高配置</h3>
                    <Card
                        style={{ width: '100%', textAlign: "center" }}
                    >
                        <Input
                            style={{ width: "180px" }}
                            addonBefore="宽"
                            addonAfter="px"
                            defaultValue="400"
                            onChange={(e) => {
                                this.setState({ widthValue: e.target.value });
                            }}
                        />
                        &nbsp;&nbsp;
                        <Input
                            style={{ width: "180px" }}
                            addonBefore="高"
                            addonAfter="px"
                            defaultValue="300"
                            onChange={(e) => {
                                this.setState({ heightValue: e.target.value });
                            }}
                        />
                    </Card>
                    <h3>数据配置</h3>
                    <Card
                        style={{ width: '100%' }}
                    >
                        <DatePicker showTime format="YYYY/MM/DD HH:mm:ss" defaultValue={this.momentTime}
                            onChange={this.onChange.bind(this)} />
                        &nbsp;&nbsp;&nbsp;
                        <Button
                            onClick={() => this.search(componentType)}
                        >
                            查询
                        </Button>
                        <TableRendering ref={this.tableRef} cols={ITEMS[componentType].cols} url={`${NODE_PATH}/${ITEMS[componentType].url}`}
                            pageSize={this.pageSize}
                            selectRow={this.selectRow} />

                    </Card>
                </div>
            );
        } else {
            //不需要加载数据的控件设置信息面板
            this.dataConfigDom.push(
                <div key={componentId + "_" + componentType}>
                    <h3>标题设置</h3>
                    <Card
                        style={{ width: '100%', textAlign: "center" }}
                    >
                        <span>标题：</span>
                        <Input
                            style={{ width: "180px" }}
                            onChange={(e) => {
                                this.setState({ cardTitle: e.target.value });
                            }}>
                        </Input>
                    </Card>
                    <h3>宽高配置</h3>
                    <Card
                        style={{ width: '100%', textAlign: "center" }}
                    >
                        <Input
                            style={{ width: "180px" }}
                            addonBefore="宽"
                            addonAfter="px"
                            defaultValue="400"
                            onChange={(e) => {
                                this.setState({ widthValue: e.target.value });
                            }}
                        />
                        &nbsp;&nbsp;
                        <Input
                            style={{ width: "180px" }}
                            addonBefore="高"
                            addonAfter="px"
                            defaultValue="300"
                            onChange={(e) => {
                                this.setState({ heightValue: e.target.value });
                            }}
                        />
                    </Card>
                </div>
            );
        }

    }

    deleteComponentInfo(themeId, index, e) {
        let themeInfo = this.state.themeInfo;
        themeInfo[themeId].content.splice(index, 1);
        this.setState({
            themeInfo: themeInfo,
        });
    }


    loadComponentDom = () => {
        let themeInfo = this.state.themeInfo;
        let components = {};
        this.props.item.componentIds.forEach(item => {
            components[item._id] = item;
        });

        for (let i = 0; i < themeInfo.length; i++) {
            if (themeInfo[i]._id) {
                return (
                    this.state.themeInfo[this.state.selectedThemeIndex].content.map((item, index) => {

                        let comInfo = components[item.componentId];
                        let Com = Allias_Com_Map[comInfo.alias];
                        let card, url, queryCon;
                        if (comInfo.dataType) {
                            url = DataType_Url_Map[comInfo.dataType];
                            queryCon = { ids: item.dataIds };
                            card = (
                                <DraggableCard key={index} id={index} title={item.title} style={item.style}
                                    movable={true} resizable={true} closable={true}
                                    onClose={this.deleteComponentInfo.bind(this, this.state.selectedThemeIndex, index)}>
                                    <Com url={`${NODE_PATH}/${url}`} queryCon={queryCon} />
                                </DraggableCard>
                            );
                        } else {
                            card = (
                                <DraggableCard key={index} id={index} title={item.title} style={item.style} movable={true}
                                    resizable={true} closable={true}
                                    onClose={this.deleteComponentInfo.bind(this, this.state.selectedThemeIndex, index)}>
                                    <Com />
                                </DraggableCard>
                            );
                        }
                        return card;
                    })
                );
            }
        }
    }

    //发布更新前获取当前组件信息
    getLastThemeInfo = () => {
        let themeInfo = this.state.themeInfo;
        //当前dom节点内组件节点信息
        let comDomNode = this.componentParentRef.current.childNodes;

        let themeIndex = this.state.selectedThemeIndex;
        for (let i = 0; i < themeInfo[this.state.selectedThemeIndex].content.length; i++) {
            for (let j = 0; j < comDomNode.length; j++) {
                if (comDomNode[j].id == i) {
                    themeInfo[themeIndex].content[i].style = {
                        top: comDomNode[j].offsetTop,
                        left: comDomNode[j].offsetLeft,
                        width: comDomNode[j].offsetWidth,
                        height: comDomNode[j].offsetHeight
                    };
                }
            }
        }
        this.setState({
            themeInfo: themeInfo,
        });
        return themeInfo[themeIndex].content;
    }


    //更新聚合任务
    updateTask() {
        let themeId = this.state.themeInfo[this.state.selectedThemeIndex]._id;
        axios
            .post(`${NODE_PATH}/aggregation/theme/update/${themeId}`, {
                content: this.getLastThemeInfo(),
            })
            .then((res) => {
                message.success("更新成功");
            });
    }


    stepsOnChange = current => {
        if (this.dataDomG312.length) {
            this.dataDomG312 = [];
            this.setState({
                dataDomG312: this.dataDomG312,
            });
        }

        switch (current) {
            case 0:
                this.dataDomG312.push(
                    <div>
                        <DraggableCard title="宣布施工" movable={true} resizable={true} closable={true}>
                            <img src={_G312First} style={{ height: 280, width: 240 }} />
                        </DraggableCard>
                        <DraggableCard title="宣布施工简介" movable={true} resizable={true} closable={true}>
                            <img src={_G312First_} style={{ height: 240, width: 560 }} />
                        </DraggableCard>
                    </div>
                )
                break;
            case 1:
                this.dataDomG312.push(
                    <div>
                        <DraggableCard title="312国道改扩建施工提醒" movable={true} resizable={true} closable={true}>
                            <img src={_G312Second} style={{ height: 280, width: 240 }} />
                        </DraggableCard>
                        <DraggableCard title="遮挡封禁地图预览" movable={true} resizable={true} closable={true}>
                            <img src={_G312Second_} style={{ height: 240, width: 560 }} />
                        </DraggableCard>
                    </div>
                )
                break;
            case 2:
                this.dataDomG312.push(
                    <div>
                        <DraggableCard title="主线打通" movable={true} resizable={true} closable={true}>
                            <img src={_G312Third} style={{ height: 280, width: 240 }} />
                        </DraggableCard>
                        <DraggableCard title="主线打通地图预览" movable={true} resizable={true} closable={true}>
                            <img src={_G312Third_} style={{ height: 240, width: 560 }} />
                        </DraggableCard>
                    </div>
                )
                break;
            case 3:
                this.dataDomG312.push(
                    <div>
                        <DraggableCard title="全线贯通" movable={true} resizable={true} closable={true}>
                            <img src={_G312Forth} style={{ height: 280, width: 240 }} />
                        </DraggableCard>
                        <DraggableCard title="全线贯通地图预览" movable={true} resizable={true} closable={true}>
                            <img src={_G312Forth_} style={{ height: 240, width: 560 }} />
                        </DraggableCard>
                    </div>
                )
                break;
            default:
                break;
        }
        this.setState({ current });
    };

    render() {
        const { visible, visibleModal, disabled, bounds, current } = this.state;

        return (
            <Layout style={{ width: "100%", height: "643px", position: "relative", backgroundColor: "#F1F6F9" }}>
                <Sider width={200}>
                    {this.loadThemeDom()}
                </Sider>
                {/*组件容器*/}
                <div
                    ref={this.componentParentRef}
                    id="componentParent"
                    style={{
                        height: "90%",
                        width: "100%",
                        position: "relative",
                        padding: "0",
                    }}
                >
                    {/*国道改建聚合任务的控件，暂时保留在这里*/}
                    {/*{this.dataDomG312}*/}
                    {/*<DraggableCard title="施工进度" movable={true} resizable={true} closable={true}>*/}
                    {/*    <Steps*/}
                    {/*        current={current}*/}
                    {/*        type="navigation"*/}
                    {/*        onChange={this.stepsOnChange}*/}
                    {/*    >*/}
                    {/*      <Step  title="2018-10-01" description="宣布动工"/>*/}
                    {/*      <Step  title="2020-03-28" description="遮挡封禁"/>*/}
                    {/*      <Step  title="2021-06-30" description="主线打通"/>*/}
                    {/*      <Step  title="2021-12-31" description="全线贯通"/>*/}
                    {/*    </Steps>*/}
                    {/*</DraggableCard>*/}
                    {this.loadComponentDom()}
                </div>
                {/*更新按钮*/}
                <div style={{ display: "flex", position: "fixed", right: "35px", top: "72px" }}>
                    <Button type="link" icon={<RollbackOutlined />} onClick={() => this.props.changeMode("list")}>返回</Button>
                    <Button type="text" icon={<SaveOutlined />} onClick={this.updateTask.bind(this)}>更新</Button>
                </div>
                {/*打开组件按钮*/}
                <Button
                    style={{ position: "fixed", left: "0px", top: "72px" }}
                    onClick={this.showDrawer}
                    icon={<MenuUnfoldOutlined />}
                >
                </Button>
                <Drawer
                    title="组件"
                    placement={"left"}
                    style={{ position: 'absolute' }}
                    getContainer={false}
                    closable={true}
                    mask={false}
                    onClose={this.onClose}
                    visible={visible}
                >
                    {/*时间轴控件，暂时保留在这里*/}
                    {/*<Card*/}
                    {/*    title="时间轴"*/}
                    {/*    style={{width: "90%", marginTop: "5px"}}*/}
                    {/*    hoverable*/}
                    {/*>*/}
                    {/*    <img src={_TimeLine} style={{height: 120, width: 130}}/>*/}
                    {/*</Card>*/}
                    {this.loadDrawCardDom()}

                </Drawer>

                {/*模态弹出框-设置面板*/}
                <Modal
                    title={
                        <div
                            style={{
                                width: '100%',
                                cursor: 'move',
                            }}
                            onMouseOver={() => {
                                if (disabled) {
                                    this.setState({
                                        disabled: false,
                                    });
                                }
                            }}
                            onMouseOut={() => {
                                this.setState({
                                    disabled: true,
                                });
                            }}
                            onFocus={() => {
                            }}
                            onBlur={() => {
                            }}
                        // end
                        >
                            配置
                        </div>
                    }
                    width={700}
                    visible={visibleModal}
                    // 点击确定执行查询，并关闭模态框
                    onOk={this.addComponentInfo.bind(this, this.componentId)}
                    onCancel={this.hideModal}
                    closable={true}
                    modalRender={modal => (
                        <Draggable
                            disabled={disabled}
                            bounds={bounds}
                            onStart={(event, uiData) => this.onStart(event, uiData)}
                        >
                            <div ref={this.draggleRef}>{modal}</div>
                        </Draggable>
                    )}
                >
                    <div>{this.dataConfigDom}</div>
                </Modal>
            </Layout>
        );
    }
}

export default Task;
