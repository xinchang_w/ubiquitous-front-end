import React, { useEffect } from "react";
import { Card } from "@/components";
import style from "./index.module.css";
function Text(props){
    let item = props.item;
    return (
        <Card title="文本" style={item.style}>
            <div className={style.textBox}>
                {
                    item.data.map(item => (
                        <div className={style.item} key={item._id}>
                            <h5 className={style["item-h5"]}>
                                <span>{item.author}</span>
                                <span>{item.time}</span>
                            </h5>
                            <p className={style["item-p"]}>{item.content}</p>
                        </div>
                    ))
                }
            </div>
        </Card>
    );
}

export default Text;