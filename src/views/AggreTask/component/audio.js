import React, { useEffect } from "react";
import { Card } from "@/components";
import { FILE_SERVER } from "@/assets/global";
import style from "./index.module.css";
function Audio(props){
    let { item } = props;
    return (
        <Card title="音频" style={item.style}>
            <audio controls src={`${FILE_SERVER}/${item.data[0].audio}`} className={style.audio}>
                你的浏览器不支持video标签
            </audio>
        </Card>
    );
}

export default Audio;