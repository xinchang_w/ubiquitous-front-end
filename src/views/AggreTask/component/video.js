import React from "react";
import { Card } from "@/components";
import style from "./index.module.css";
import { FILE_SERVER } from "@/assets/global";
function Video(props) {
    let { item } = props;
    return (
        <Card 
            title="视频" 
            style={item.style} 
            resizable={true}
        >
            <video controls src={`${FILE_SERVER}/${item.data[0].video}`} width="350" height="200" className={style.video}>
                你的浏览器不支持video标签
            </video>
        </Card>
    );
}

export default Video;