import React, { useState } from "react";
import { Breadcrumb } from "antd";
import List from "./list";
import Operation from "./operation";
import Task from "./task";

function AggreTask(props){
    let [ mode, setMode ] = useState("list");
    let [ item, setItem ] = useState(null);

    let com, caption;
    switch(mode){
        case "list":
            com = <List changeMode={changeMode} />;
            caption = "";
            break;
        case "operation":
            com = <Operation changeMode={changeMode} />;
            caption = "新增";
            break;
        case "task":
            com = <Task changeMode={changeMode} item={item} />;
            caption = item.title;
            break;
        default:
            com = null;
    }

    return (
        <div className="container">
            <Breadcrumb className="breadcrumb">
                <Breadcrumb.Item>位置聚合</Breadcrumb.Item>
                <Breadcrumb.Item className="breadcrumb-item">聚合任务</Breadcrumb.Item>
                <Breadcrumb.Item className="breadcrumb-item">{caption}</Breadcrumb.Item>
            </Breadcrumb>
            { com }
        </div>
    );

    function changeMode(mode, item){
        setMode(mode);
        setItem(item);
    }
}

export default AggreTask;