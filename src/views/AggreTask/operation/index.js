import React, { Fragment, useRef, useState, useEffect } from "react";
import { Button, Form, Input, message, Upload, Card, Checkbox, Spin } from "antd";
import { RollbackOutlined, SaveOutlined, UploadOutlined, PlusOutlined, MinusCircleOutlined } from "@ant-design/icons";
import axios from "axios";
import { NODE_PATH, FILE_SERVER } from "@/assets/global";
import style from "./index.module.css";

const imgReg = /jpg$|png$|gif$/i;
function Operation(props){
    let formItemLayout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 20 }
    };
    let formItemLayoutWithoutLabel = {
        wrapperCol: { offset: 4, span: 20}
    }
    
    let selectedComIds = new Set();    //选中的组件的_id
    let formRef = useRef();
    let [spinning, setSpinState] = useState(true);
    let [fileList, setFileList] = useState([]);
    let [componentArr, setComponentArr] = useState([]);
    let [loading, setLoading] = useState(false);
    
    useEffect(() => {
        axios.get(`${NODE_PATH}/aggregation/component/search`).then(res => {
            setSpinState(false);
            setComponentArr(res.data.data);
        }).catch(err => {
            console.log(err);
            message.error("加载组件信息失败！");
        });
    }, []);
    

    function onFinish(values){
        setLoading(true);

        let task = new FormData();
        task.append("title", values.title);
        task.append("description", values.description);
        task.append("cover", values.cover.file);
        task.append("themeNames", JSON.stringify(values.themeNames));
        task.append("componentIds", JSON.stringify([...selectedComIds]));
        
        axios.post(`${NODE_PATH}/aggregation/task_/add`, task).then(res => {
            if(res.data.code){
                message.success("新增任务成功！", 0.5, () => { props.changeMode("list") });
            }
            else{
                throw new Error(res.data.msg);
            }
        }).catch(err => {
            console.log(err);
            setLoading(false);
            message.error("新增任务失败！");
        });
    }

    //选中组件
    function addCom(id){
        selectedComIds.add(id);
    }

    //取消选中组件
    function deleteCom(id){
        selectedComIds.delete(id);
    }

    return (
        <div className={style.container}>
            <div className={style["container-left"]}>
                <div className={style["btnBox"]}>
                    <Button type="link" icon={<RollbackOutlined />} onClick={() => props.changeMode("list")}>返回</Button>
                    {/* <Button type="text" icon={<SaveOutlined />} >保存</Button> */}
                </div>
                <div className={style["formBox"]}>
                    <h5 className={style["formBox-h5"]}>
                        任务描述
                    </h5>
                    <Form 
                        className={style["formBox-form"]} 
                        ref={formRef}
                        onFinish={onFinish}
                    >
                        <Form.Item {...formItemLayout} name="title" label="名称" rules={[{required:true}]}>
                            <Input placeholder="请输入任务名称" className={style["form-input"]} />
                        </Form.Item>
                        <Form.Item 
                            {...formItemLayout} 
                            name="cover" 
                            label="图标" 
                            rules={[{ 
                                required: true, 
                                validator: () => { 
                                    if(fileList.length === 0 ){ 
                                        return Promise.reject(new Error("请选择图标")) 
                                    } 
                                    else if(!imgReg.test(fileList[0].name)){
                                        return Promise.reject(new Error("图标文件格式错误，仅支持图片格式"));

                                    } 
                                    else{ 
                                        return Promise.resolve() 
                                    } 
                                }}]}>
                            <Upload
                                beforeUpload={() => false}
                                onChange={info => {
                                    if(info.fileList.length === 0){
                                        setFileList([]);
                                    }
                                    else{
                                        setFileList([info.fileList.pop()]);
                                    }
                                }}
                                fileList={fileList}
                            >
                                <Button icon={<UploadOutlined />}>选择文件</Button>
                            </Upload>
                        </Form.Item>
                        <Form.Item {...formItemLayout} name="description" label="简介">
                            <Input.TextArea placeholder="请输入任务简介" className={style["form-input"]}  />
                        </Form.Item>
                        <Form.List 
                            name="themeNames"
                            rules={[
                                {
                                    validator: (_, names) => {
                                        if (!names || names.length === 0) {
                                            return Promise.reject(new Error("至少需要一个专题"));
                                        }
                                        else{
                                            return Promise.resolve();
                                        }
                                    }
                                }
                            ]}
                        >
                            {
                                (fields, { add, remove }, { errors }) => (
                                    <Fragment>
                                        {
                                            fields.map((field, index) => (
                                                <Form.Item
                                                    {...(index === 0 ? formItemLayout : formItemLayoutWithoutLabel)}
                                                    label={index === 0 ? '专题' : ''}
                                                    required={false}
                                                    key={field.key}
                                                >
                                                    <Form.Item
                                                        {...field}
                                                        rules={[
                                                            {
                                                                required: true,
                                                                whitespace: true,
                                                                message: "请输入专题名称，或者删除此控件",
                                                            },
                                                        ]}
                                                        noStyle
                                                    >
                                                        <Input placeholder="请输入专题名称" className={style["form-input"]} />
                                                    </Form.Item>
                                                    <MinusCircleOutlined
                                                        className={style["form-removeBtn"]}
                                                        onClick={() => remove(field.name)}
                                                    />
                                                </Form.Item>
                                            ))
                                        }
                                        <Form.Item {...formItemLayoutWithoutLabel}>
                                            <Button
                                                type="dashed"
                                                style={{width:"80%"}}
                                                onClick={() => { 
                                                    if(fields.length >= 5) { 
                                                        message.warning("最多可添加五个专题！"); 
                                                        return;
                                                    } 
                                                    add(); 
                                                }}
                                                icon={<PlusOutlined />}
                                            >
                                                添加专题
                                            </Button>
                                            <Form.ErrorList errors={errors} />
                                        </Form.Item>
                                    </Fragment>
                                )
                            }
                        </Form.List>
                        <Form.Item {...formItemLayoutWithoutLabel}>
                            <Button type="primary" htmlType="submit" loading={loading}>
                                保存
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
            <div className={style["container-right"]}>
                <div className={style["comBox"]}>
                    <h5 className={style["comBox-h5"]}>
                        控件列表
                    </h5>
                    <Spin spinning={spinning}>
                        <div className={style["comBox-div"]}>
                            {
                                componentArr.map(i => (
                                    <Card
                                        key={i._id}
                                        className={style.card}
                                        hoverable
                                        cover={<Cover id={i._id} cover={`${FILE_SERVER}/${i.cover}`} add={addCom} del={deleteCom} />}
                                    >
                                        <h5 className={style["card-h5"]}>{i.title}</h5>
                                        <p className={style["card-p"]}>{i.description}</p>
                                    </Card>
                                ))
                            }
                        </div>
                    </Spin>
                </div>
            </div>
        </div>
    );
}

function Cover(props){
    let { id, cover, add, del } = props;
    return (
        <div className={style.cover}>
            <Checkbox className={style["cover-checkbox"]} onChange={e => e.target.checked ? add(id) : del(id)} />
            <img alt="封面" className={style["cover-img"]} src={cover} />
        </div>
    );
}

export default Operation;