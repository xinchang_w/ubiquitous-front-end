import React, { useState, useEffect } from "react";
import axios from "axios";
import { NODE_PATH, FILE_SERVER } from "@/assets/global/index";
import { Card, message } from "antd";
import style from "./index.module.css";
import addIcon from "@/assets/image/icon_add.png";
import trafficCover from "@/assets/image/traffic.jpg";

function List(props){
    let [tasks, setTasks] = useState([]);

    useEffect(() => {
        axios.get(`${NODE_PATH}/aggregation/task_/search`).then(res => {
            setTasks(res.data.data);
        }).catch(err => {
            console.log(err);
            message.error("查询任务信息失败");
        });
    }, []);

    return (
        <div className={style["wrapper"]}>
            <Card
                hoverable={true}
                className={style["card"]}
                cover={<img src={addIcon} className={style["card-cover"]} />}
                onClick={() => props.changeMode("operation")}
            >
                <p className={style["card-title"]}>新增</p>
            </Card>
            {
                tasks.map(item => (
                    <Card
                        key={item._id}
                        hoverable={true}
                        className={style["card"]}
                        cover={<img src={item.cover ? `${FILE_SERVER}/${item.cover}` : trafficCover} className={style["card-cover"]} />}
                        onClick={() => props.changeMode("task", item)}
                    >
                        <p className={`${style["card-title"]} ellipsis`}>{item.title}</p>
                    </Card>
                ))
            }
        </div>
    );
}

export default List;