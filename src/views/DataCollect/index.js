import React, { Component, Fragment } from 'react';
import { Button, Input, Select, Breadcrumb, Modal, Radio } from "antd";
import { NODE_PATH, FILE_SERVER } from "@/assets/global/index";
import { Paragraph, CustomizeModal } from '@/components';
import { TableRendering } from "@/components";
import ITEMS from "./config";

class DataCollect extends Component{
    constructor(props){
        super(props);
        this.state = {
            "curItem": "text",
            "curSearchCon": "caption",
            "keywords": "",
            "modalData": null
        };

        this.defaultUrl = `${NODE_PATH}/${ITEMS["text"].url}`;
        this.defaultCols = ITEMS["text"].cols;

        this.tableRendering = React.createRef();
        this.onItemChange = this.onItemChange.bind(this);
        this.onConChange = this.onConChange.bind(this);
        this.onKeywordChange = this.onKeywordChange.bind(this);
        this.search = this.search.bind(this);
        this.showContent = this.showContent.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }

    render(){
        let { curItem, curSearchCon, keywords, modalData } = this.state; 
        
        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>数据资源</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">数据采集</Breadcrumb.Item>
                </Breadcrumb>
                <Radio.Group onChange={this.onItemChange} style={{margin:10}} value={curItem}>
                    {
                        Object.keys(ITEMS).map(i => (<Radio value={i} key={i}>{ITEMS[i].title}</Radio>))
                    }
                </Radio.Group>
                <div>
                    <div className="searchBox">
                        <div>
                            <label className="searchBox-label">查询条件：</label>
                            <Select
                                placeholder="类型"
                                className="searchBox-input"
                                value={curSearchCon}
                                onChange={this.onConChange}
                                options={ITEMS[curItem].searchCons}
                            />
                            <label className="searchBox-label">关键词：</label>
                            <Input
                                className="searchBox-input"
                                placeholder="关键词"
                                value={keywords}
                                onChange={this.onKeywordChange}
                            />
                            <Button
                                type="primary"
                                className="searchBox-btn"
                                onClick={this.search}
                            >
                                查询
                            </Button>
                        </div>
                    </div>
                    <TableRendering cols={this.defaultCols} url={this.defaultUrl} clickRow={this.showContent} ref={this.tableRendering} />
                </div>
                <CustomizeModal
                    visible={modalData !== null}
                    title={modalData && (modalData.caption || "暂无标题")}
                    close={this.hideModal}
                >
                    <Paragraphs curItem={curItem} {...modalData} />
                </CustomizeModal>
            </div>
        );
    }

    onItemChange(e){
        let val = e.target.value;
        this.setState({
            "curItem": val,
            "curSearchCon": ITEMS[val].searchCons[0].value,
            "keywords": ""
        }, this.search);
        
    }

    onConChange(value){
        this.setState({
            "curSearchCon": value
        });
    }

    onKeywordChange(e){
        this.setState({
            "keywords": e.target.value
        })
    }

    search(){
        let con = {};
        let { keywords, curSearchCon, curItem } = this.state;
        let url = `${NODE_PATH}/${ITEMS[curItem].url}`;
        let columns = ITEMS[curItem].cols;
        if(curSearchCon && keywords){
            con[curSearchCon] = keywords;
        }
        
        this.tableRendering.current.search(url, columns, con);
    }

    showContent(record){
        this.setState({
            "modalData": record
        });
    }

    hideModal(){
        this.setState({
            "modalData": null
        });
    }
};

function Paragraphs(props){
    if(props.curItem === "text"){
        return (
            <Fragment>
                <Paragraph title="作者：" content={props.author} />
                <Paragraph title="来源：" content={props.source} />
                <Paragraph title="时间：" content={props.accessDatetime} />
                <Paragraph title="内容：" content={props.content} />
            </Fragment>
        );
    }

    if(props.curItem === "img"){
        return (
            <Fragment>
                <Paragraph title="时间：" content={props.accessDateTime} />
                <Paragraph title="来源：" content={props.source} />
                <Paragraph title="介绍：" content={props.description} />
                <Paragraph title="图片：" image={props.imageName && `${FILE_SERVER}/images/metaData_Img/${props.imageName}`} />
            </Fragment>
        );
    }

    if(props.curItem === "map"){
        return (
            <Fragment>
                <Paragraph title="来源：" content={props.source} />
                <Paragraph title="时间：" content={props.recordedTime} />
                <Paragraph title="网址：" content={<a href={props.url} target="_blank">{props.url}</a>} />
            </Fragment>
        );
    }

    if(props.curItem === "chart"){
        return (
            <Fragment>
                <Paragraph title="来源：" content={props.source} />
                <Paragraph title="标题：" content={props.title} />
                <Paragraph title="网址：" content={<a href={props.sourceUrl} target="_blank">{props.sourceUrl}</a>} />
            </Fragment>
        );
    }

    if(props.curItem === "audio"){
        return (
            <Fragment>
                <Paragraph title="节目名：" content={props.name} />
                <Paragraph title="频道名：" content={props.channelName} />
                <Paragraph title="播出时间段：" content={props.period} />
                <Paragraph title="频率：" content={props.fm} />
                <Paragraph title="主持人：" content={props.anchor} />
                <Paragraph title="网址：" content={<a href={props.url} target="_blank">{props.url}</a>} />
            </Fragment>
        );
    }

    if(props.curItem === "video"){
        return (
            <Fragment>
                <Paragraph title="来源：" content={props.source} />
                <Paragraph title="摄像头位置：" content={props.location} />
                <Paragraph title="时间段：" content={props.period} />
                <Paragraph title="网址：" content={<a href={props.url} target="_blank">{props.url}</a>} />
            </Fragment>
        );
    }

    return null;
}


export default DataCollect;
