import React, { Component } from "react";
import { Breadcrumb, Card } from "antd";
import taiwan from "@/assets/image/Taiwan.jpg";
import hongkong from "@/assets/image/Hongkong.jpg";
import traffic from "@/assets/image/traffic.jpg";
import addnew from "@/assets/image/icon_add.png";
import { NAV_ITEMS, ROOT_ROUTE } from "@/routes";
import { Link } from "react-router-dom";
import axios from "axios";
class AggrTask extends Component {
    constructor(props) {
        super(props);
        this.componentsInstances = []

    };
    state = {
        initation: this.componentsInstances
    };
    //生命周期函数 组件渲染后，修改dom
    componentDidMount() {
        //请求库里组件案例列表
        this.initationInstances();
    };


    render() {


        return (
            <div className="container">
                <Breadcrumb className="breadcrumb">
                    <Breadcrumb.Item>位置聚合</Breadcrumb.Item>
                    <Breadcrumb.Item className="breadcrumb-item">聚合任务</Breadcrumb.Item>
                </Breadcrumb>
                <div className="cardBox" style={{ display: "flex", justifyContent: "space-around", flexWrap: "wrap" }}>

                    {this.state.initation}

                    <Card hoverable={true} style={{ width: "300px", textAlign: "center", overflow: "hidden", margin: "5px" }}>
                        <img src={addnew} style={{ height: 200 }} />
                        <p>定制新任务</p>
                    </Card>


                </div>
            </div>
        );
    };



    // 初始化组件案例列表 ad003d22-9b1b-45bc-a318-25e850aa2fe2
    initationInstances = () => {
        let that = this

        axios.get('http://106.14.78.235:8086/GenericMap/aTask').then(res => {

            console.log(res.data.data);

            let instanceArr = res.data.data;
            let pics = [traffic, hongkong, taiwan];
            let dom = null;

            for (var i = 0; i < instanceArr.length; i++) {
                dom = instanceArr[i];
                that.componentsInstances.push(
                    <Card
                        hoverable={true}
                        style={{ width: "300px", textAlign: "center", overflow: "hidden", margin: "5px" }}
                        key={dom.aid}
                    >
                        <Link to={`${ROOT_ROUTE}/update-task-nanjing-city-traffic-aggregation?id=${dom.aid}`}>
                            <img src={pics[i]} style={{ height: 200 }} />
                            <p>{dom.name}</p>
                        </Link>
                    </Card>
                )
            }
            //重置dom
            that.setState({
                initation: that.componentsInstances
            })

        })
    }
}

export default AggrTask;