import React, { useState } from 'react';
import { Modal, Button, Card, message, Row, Col } from 'antd';
import axios from 'axios'


function DownloadModal(props) {
    const baseUrl = 'http://172.21.213.50:5050'

    const download = () => {
        console.log('download zip')
        const baseUrl = 'http://172.21.213.50:5050'
        // axios.get(baseUrl + '/view/zip', {
        //     //参数列表
        //     params: {
        //         type: props.type,
        //         name: props.name
        //     }
        // }).then(res => {
        //       if(res.status === 200) {
        //           message.success('开始下载')
        //       } else {
        //           message.info('下载失败')
        //       }
        //   }).catch((err) => message.error('系统出错：', err))
        
        // axios 的方式不行，直接 window.open() 试试
        window.location.href = (baseUrl + '/view/zip?type=' + props.type + '&name=' + props.name)
    }

    return(
        <Modal
        title={props.name}
        centered
        visible={props.visible}
        onCancel={() => props.changeModalVisible()}
        footer={null}
        >
            <Card>
            <p style={{whiteSpace: 'pre-wrap'}}>{props.content}</p>
            </Card>
            {/* {props.type === 'tool' && <Button type='primary' block onClick={() => download()}>下载</Button>}
            {props.type != 'tool' &&  */}
                <Row>
                    <Col type="flex" justify="center" align="middle" span={12}>
                        <Button type='primary' onClick={() => download()}>下载</Button>
                    </Col>
                    <Col type="flex" justify="center" align="middle" span={12}>
                        <Button type='primary' onClick={() => window.open("./test/test.htm")}>可视化</Button>   
                    </Col>
                </Row>
            {/* } */}
        </Modal>   
    )
}

export default DownloadModal