import React, { Component } from 'react';
import { Tree, Row, Col, Tabs } from 'antd'
import MethodContent from './methodContent'
import { TAG_MENU, TAG_NAME, TAG_CONTENT } from './tagData.js'
import '@/assets/style/home.css'
import { Kinetic } from 'ol';

const { TabPane } = Tabs;



const TagContentadd=(cardlist)=>{
  var keyList=new Array();
  var oldKeyList=new Array();
  //首先获取属性键
  for(var k in cardlist){
      oldKeyList.push(k);
      
      //如果k的属性值大于1，就讲将n个字符
      if(k.length>0){
         for(var kIndex=0;kIndex<k.length;kIndex++){
            var newKey=k.substring(0,kIndex);
            var newKeyExist=0;
            //判断是否存在于keylist之中
            for(var keyIndex in keyList){
              if(newKey===keyList[keyIndex]){
                newKeyExist=1;
                break;
              }
            }
            if(newKeyExist===0){
              keyList.push(newKey);
            }
         }
      }
  }
  for(var keyIndex in keyList){
    console.log(keyList[keyIndex]);
    //先初始化json key值数组
    cardlist[keyList[keyIndex]]=new Array();
    //加入样本集
    cardlist[keyList[keyIndex]][0]=new Array();
    //先找到包含这个key的所有底层key
    cardlist[keyList[keyIndex]][1]=new Array();
    for(var oldkeyIndex in oldKeyList){
      //就说明这个是其中一个底层key
      if(oldKeyList[oldkeyIndex].indexOf(keyList[keyIndex])===0){
         for(var tempdata in cardlist[oldKeyList[oldkeyIndex]][0])
         { 
          cardlist[keyList[keyIndex]][0].push(cardlist[oldKeyList[oldkeyIndex]][0][tempdata]);
            // cardlist.keyList[keyIndex][0].push(tempdata);
         }
         for(var tempdata in cardlist[oldKeyList[oldkeyIndex]][1])
         { 
          cardlist[keyList[keyIndex]][1].push(cardlist[oldKeyList[oldkeyIndex]][1][tempdata]);
            // cardlist.keyList[keyIndex][1].push(tempdata);
         }
      }
    }
  }
  console.log(cardlist);
  return cardlist;
}



class TagMethod extends Component {
  state = {
    
    methodType:'体裁',
    cardList: TAG_CONTENT.text.a,
    menuData: TAG_MENU.text,
    menuType: 'text'
  }

  constructor(props) {
    super(props);
    TAG_CONTENT.text=TagContentadd(TAG_CONTENT.text);
  }

  handleTabChange = (key) => {
    TAG_CONTENT[key]=TagContentadd(TAG_CONTENT[key]);
    this.setState({
      menuData: TAG_MENU[key],
      menuType: key,
      cardList: TAG_CONTENT[key].a || [[], []],
      methodType:''
    })
   
  }

  handleTreeSelect = (selectKey, e) => {
    const key = e.node.key;
    {/*如果是要一级目录切换的话，那么长度就等于1;如果是二级目录切换的话，那么长度就等于2*/}
    if ((((key.length === 2)||(key.length===1))||(key.length === 3))||(key.length === 4)) {
      this.setState({
        methodType: e.node.title,
        cardList:(TAG_CONTENT[this.state.menuType] && TAG_CONTENT[this.state.menuType][key]) || [[], []]
      })
    }
  }

  render() {
    return (
      
      <Row justify='center' className='tag-method-wrapper'>
        
        {/*修改了宽度Span宽度*/}
        <Col span={5.1} className='tag-method-col'>
          <Tabs defaultActiveKey='text' onChange={this.handleTabChange} >
            {
             
              Object.entries(TAG_NAME).map(entry => <TabPane tab={entry[1]} key={entry[0]}></TabPane>)
            }
          </Tabs>
          {/* <h1>
            <img src={methodIcon} width={30} height={30} />
            {`��ǩ${this.state.menuType}����`}
          </h1> */}
          <Tree
            treeData={this.state.menuData}
            showLine={true}
            defaultExpandedKeys={['a']}
            defaultSelectedKeys={['a']}
            onSelect={this.handleTreeSelect}
          />
        </Col>
        <Col span={12} style={{ marginLeft: '20px' }}>
          <MethodContent methodType={this.state.methodType} cardList={this.state.cardList} menuType={this.state.menuType} />
        </Col>
      </Row>
    )
  }
}

export default TagMethod;