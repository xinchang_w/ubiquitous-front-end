import React, { Component } from "react";
import { Card, Row, Col, Empty,Pagination,Tabs,Tag, Modal} from 'antd'
import DownloadModal from './downloadModal'

import '@/assets/style/home.css'
import semanticIcon from '@/assets/image/dataTagMethodSemantic.png'
import toolSetIcon from '@/assets/image/dataTagMethodToolSet.png'
import EntranceIcon from '@/assets/image/GitLab.png'
import contentIcon from '@/assets/image/详细信息.png'
import cardCover from '@/assets/image/工具.png'

import axios from "axios";




const { Meta } = Card;
const TabPane = Tabs.TabPane; 



const NumberList=(props)=>{  
  if(!props.numbers){
    return null;
  }
  const listItems = props.numbers.map((number) =>
    <Tag>{number}</Tag>
  );
  return (
    <div>{listItems}</div>
  );
}




const genCard = (cardDataListall,page, setModalValues, type,showModal1,showModal2,showModal3) => {
    if (cardDataListall.length) {
      if(cardDataListall.length>=3){
        var cardDataList=cardDataListall.slice(page*3-3, page*3)
      }else
      {
        var cardDataList=cardDataListall
      }   
    const rawDomList = [];
    //const rowNum = Math.ceil(cardDataList.length / 3);
    const rowNum = Math.ceil(cardDataList.length / 3);
    

    for (let i = 0; i < rowNum; i++) {
      const cardDomList = [];
      for (let j = 0; j < 3; j++) {
        const tempCard = cardDataList[i * 3 + j]
        if(j===0){
          tempCard && cardDomList.push(
            <row span={8} style={{display:'flex'}}>
              <Card
                // className='tag-method-card-col'
                key={tempCard.cardId}
      
                style={{width:910,height:195,marginLeft:10,marginTop:10}}
                // cover={<img alt='cardCover' src={tempCard.imagePath} />}  
                // <a href={`${IFRAME}/${tempCard.imagePath}`} 
              >
            
              <div style={{fontSize:20}} >{tempCard.cardTitle}</div>
              <div className="custom-image" style={{display:'flex'}}>
                    <img alt="example" width="15%" src={cardCover}/> 
                     <div style={{ height: '100%', width: '80%',marginLeft:10}}>
                       <p style={{marginTop:20,width:'95%',whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>{tempCard.cardcontent}</p>
                      <NumberList numbers={tempCard.cardTag}/>
                    </div>
                    <div style={{ height: '100%', width: '5%',marginLeft:5}}>
                      
                      <img src={contentIcon} width={30} height={30} padding={20} onClick={showModal1}/>
        
                      <br/><br/><br/>
                      <img src={EntranceIcon} width={30} height={30} padding={20}/>
                    </div>
              </div>
              </Card>
            </row>
          )
        }
        if(j===1){
          tempCard && cardDomList.push(
            <row span={8} style={{display:'flex'}}>
              <Card
                // className='tag-method-card-col'
                key={tempCard.cardId}
      
                style={{width:910,height:195,marginLeft:10,marginTop:20}}
                // cover={<img alt='cardCover' src={tempCard.imagePath} />}  
                // <a href={`${IFRAME}/${tempCard.imagePath}`} 
              >
            
              <div style={{fontSize:20}} >{tempCard.cardTitle}</div>
              <div className="custom-image" style={{display:'flex'}}>
                    <img alt="example" width="15%" src={cardCover}/> 
                    <div style={{ height: '100%', width: '80%',marginLeft:10}}>
                       <p style={{marginTop:20,width:'95%',whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>{tempCard.cardcontent}</p>
                      <NumberList numbers={tempCard.cardTag}/>
                    </div>
                    <div style={{ height: '100%', width: '5%',marginLeft:5}}>
                      
                      <img src={contentIcon} width={30} height={30} padding={20} onClick={showModal2}/>
        
                      <br/><br/><br/>
                      <img src={EntranceIcon} width={30} height={30} padding={20}/>
                    </div>
              </div>
              </Card>
            </row>
          )
        }
        if(j===2){
          tempCard && cardDomList.push(
            <row span={8} style={{display:'flex'}}>
              <Card
                // className='tag-method-card-col'
                key={tempCard.cardId}
      
                style={{width:910,height:195,marginLeft:10,marginTop:20}}
                // cover={<img alt='cardCover' src={tempCard.imagePath} />}  
                // <a href={`${IFRAME}/${tempCard.imagePath}`} 
              >
            
              <div style={{fontSize:20}} >{tempCard.cardTitle}</div>
              <div className="custom-image" style={{display:'flex'}}>
                    <img alt="example" width="15%" src={cardCover}/> 
                    {/* <div style={{ height: '100%', width: '95%',marginLeft:10,display: 'block',overflowWrap:'break-word'}}> */}
                    <div style={{ height: '100%', width: '80%',marginLeft:10}}>
                       <div style={{ marginTop:20,width:'95%',whiteSpace:'nowrap',overflow:'hidden',textOverflow:'ellipsis'}}>{tempCard.cardcontent}</div>
                      <NumberList numbers={tempCard.cardTag}/>
                    </div>
                    <div style={{ height: '100%', width: '5%',marginLeft:5}}>
                      
                      <img src={contentIcon} width={30} height={30} padding={20} onClick={showModal3}/>
        
                      <br/><br/><br/>
                      <img src={EntranceIcon} width={30} height={30} padding={20}/>
                    </div>
              </div>
              </Card>
            </row>
          )
        }

        !tempCard && cardDomList.push(<row span={8} style={{display:'flex'}}>
          <Card
           style={{width:910,height:195,marginLeft:10,marginTop:20}}
           bordered={false}
           >

          </Card>
        </row>)
      }
      rawDomList.push(
        <Row justify='space-around' gutter={16} className='tag-method-card-raw'>
          {cardDomList}
        </Row>
      )
    }
    return rawDomList;
  } else {
    return <div style={{ height: '100%', width: '100%' }}>
      <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
    </div>;
  }

}


class MethodContent extends Component {
  constructor(props) {
    super(props);

    this.setModalValues = this.setModalValues.bind(this);
  }
  state = {
    current1: 1,
    current2: 1,

    //控制对话框展开属性
    visible1:false,
    visible2:false,
    visible3:false,
    visible4:false,
    visible5:false,
    visible6:false,
    //控制对话框传入的变量

    // downloadModal 的属性
    modalVisible: false,
    modalName: '',  
    content: '',

    
  };

  setModalValues(name, type) {
    console.log('set modal values')
    console.log('type: ', type)
    if(name && type) {
      this.setState({
        modalName: name,
      })

      const baseUrl = 'http://172.21.213.50:5050'
      let that = this
      axios({
        url: baseUrl + '/view/readme',
        method: 'POST',
        data: {
            type: type,
            name: name
        }
        }).then((res) => {
            that.setState({
              content: res.data,
              modalVisible: !this.state.modalVisible,
              type: type
            })
        })
    }
    else{
      this.setState({
        modalVisible: !this.state.modalVisible,
        type: type
      })
    }
  }

  showdata = (page,)=>
  {
    console.log(page);

  }

  onChange1 = (page)=> {
  
    this.setState({
      current1: page
    });
    
    this.showdata(page)
  };

  onChange2 = (page)=> {
  
    this.setState({
      current2: page
    });
    this.showdata(page)
  };

  //控制对话框展开事件
  showModal1=()=>
  {
    this.setState({
      visible1:true,
    })
  };
  showModal2=()=>
  {
    this.setState({
      visible2:true,
    })
  };
  showModal3=()=>
  {
    this.setState({
      visible3:true,
    })
  };
  showModal4=()=>
  {
    this.setState({
      visible4:true,
    })
  };
  showModal5=()=>
  {
    this.setState({
      visible5:true,
    })
  };
  showModal6=()=>
  {
    this.setState({
      visible6:true,
    })
  };
  //控制对话框关闭事件
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible1: false,
      visible2: false,
      visible3: false,
      visible4: false,
      visible5: false,
      visible6: false,
    });
  };
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible1: false,
      visible2: false,
      visible3: false,
      visible4: false,
      visible5: false,
      visible6: false,
    });
  }
  

  render() {
    return (
      <div style={{ height: '100%', width: '100%' }}>
        <DownloadModal name={this.state.modalName} visible={this.state.modalVisible} changeModalVisible={this.setModalValues} content={this.state.content} type={this.state.type}/>
         <Tabs defaultActiveKey="1" type="card">
           <TabPane tab={<span> <img src={toolSetIcon} width={30} height={30} />{`${this.props.methodType}工具集`}</span>} key="1">
              <Row className='tag-method-panel'>
              {genCard(this.props.cardList[0],this.state.current1, this.setModalValues, 'tool',this.showModal1,this.showModal2,this.showModal3)}
                <Modal
                   title={(Number(this.state.current1*3-3)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-3].cardTitle:"模型"}
                   visible={this.state.visible1} 
                   onOk={this.handleOk}
                   onCancel={this.handleCancel}
                   >
                   <p>内容:{(Number(this.state.current1*3-3)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-3].cardcontent:"内容"}</p>
                   <p>来源:<a href={(Number(this.state.current1*3-3)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-3].cardsource:"来源"}>{(Number(this.state.current1*3-3)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-3].cardsource:"来源"}</a></p>
                  </Modal>
                <Modal
                   title={(Number(this.state.current1*3-2)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-2].cardTitle:"模型"}
                   visible={this.state.visible2} 
                   onOk={this.handleOk}
                   onCancel={this.handleCancel}
                   >
                   <p>内容:{(Number(this.state.current1*3-2)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-2].cardcontent:"内容"}</p>
                   <p>来源:<a href={(Number(this.state.current1*3-2)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-2].cardsource:"来源"}>{(Number(this.state.current1*3-2)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-2].cardsource:"来源"}</a></p>
                  </Modal>
                <Modal
                   title={(Number(this.state.current1*3-1)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-1].cardTitle:"模型"}
                   visible={this.state.visible3} 
                   onOk={this.handleOk}
                   onCancel={this.handleCancel}
                   >
                   <p>内容:{(Number(this.state.current1*3-1)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-1].cardcontent:"内容"}</p>
                   <p>来源:<a href={(Number(this.state.current1*3-1)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-1].cardsource:"来源"}>{(Number(this.state.current1*3-1)<Number(this.props.cardList[0].length))?this.props.cardList[0][this.state.current1*3-1].cardsource:"来源"}</a></p>
                  </Modal>
            
              <Pagination current={this.state.current1} pageSize={3} onChange={this.onChange1} total={this.props.cardList[0].length} />
               </Row>
           </TabPane>
           <TabPane tab={<span><img src={semanticIcon} width={30} height={30} />{(`${this.props.menuType}`===`text`)? `${this.props.methodType}语料库`:`${this.props.methodType}样本集`}</span>} key="2">
              <Row className='tag-method-panel'>
               {genCard(this.props.cardList[1],this.state.current2, this.setModalValues, 'corpus',this.showModal4,this.showModal5,this.showModal6)}
               <Modal
                   title={(Number(this.state.current2*3-3)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-3].cardTitle:"模型"}
                   visible={this.state.visible4} 
                   onOk={this.handleOk}
                   onCancel={this.handleCancel}
                   >
                   <p>内容:{(Number(this.state.current2*3-3)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-3].cardcontent:"内容"}</p>
                   <p>来源:<a href={(Number(this.state.current2*3-3)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-3].cardsource:"来源"}>{(Number(this.state.current2*3-3)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-3].cardsource:"来源"}</a></p>
                  </Modal>
                  <Modal
                   title={(Number(this.state.current2*3-2)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-2].cardTitle:"模型"}
                   visible={this.state.visible5} 
                   onOk={this.handleOk}
                   onCancel={this.handleCancel}
                   >
                   <p>内容:{(Number(this.state.current2*3-2)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-2].cardcontent:"内容"}</p>
                   <p>来源:<a href={(Number(this.state.current2*3-2)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-2].cardsource:"来源"}>{(Number(this.state.current2*3-2)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-2].cardsource:"来源"}</a></p>
                  </Modal>
                  <Modal
                   title={(Number(this.state.current2*3-1)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-1].cardTitle:"模型"}
                   visible={this.state.visible6} 
                   onOk={this.handleOk}
                   onCancel={this.handleCancel}
                   >
                   <p>内容:{(Number(this.state.current2*3-1)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-1].cardcontent:"内容"}</p>
                   <p>来源:<a href={(Number(this.state.current2*3-1)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-1].cardsource:"来源"}>{(Number(this.state.current2*3-1)<Number(this.props.cardList[1].length))?this.props.cardList[1][this.state.current2*3-1].cardsource:"来源"}</a></p>
                  </Modal>
               <Pagination current={this.state.current2} pageSize={3} onChange={this.onChange2} total={this.props.cardList[1].length} />
              </Row>
        </TabPane>
        </Tabs>
        
  
      </div>
    )
  }
}

export default MethodContent;