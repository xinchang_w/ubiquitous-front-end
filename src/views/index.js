import Loadable from 'react-loadable'
import PageLoading from './PageLoading'

const Home = Loadable({
  loader: () => import('./Home/index'),
  loading: PageLoading
})

// const DataResources = Loadable({
//   loader: () => import('./DataResources'),
//   loading: PageLoading
// })
const DataCollect = Loadable({
  loader: () => import('./DataCollect'),
  loading: PageLoading
});

const DataQuery = Loadable({
  loader: () => import('./DataQuery'),
  loading: PageLoading
});

const DataSource = Loadable({
  loader: () => import("./DataSource"),
  loading: PageLoading
});

const TagMethod = Loadable({
  loader: () => import('./TagMethod'),
  loading: PageLoading
})

const TagResult = Loadable({
  loader: () => import('./TagResult'),
  loading: PageLoading
})

const TagTool = Loadable({
  loader: () => import('./TagTool'),
  loading: PageLoading
})

// const DataLookup = Loadable({
//   loader: () => import('./DataLookup'),
//   loading: PageLoading
// })

const ContentList = Loadable({
  loader: () => import('./ContentList'),
  loading: PageLoading
})

const SubjectCus = Loadable({
  loader: () => import('./SubjectCus'),
  loading: PageLoading
})

//单向溯源
const OneSideSrc = Loadable({
  loader: () => import('./OneSideSrc'),
  loading: PageLoading
})

//过程溯源
const ProcessSrc = Loadable({
  loader: () => import('./ProcessSrc'),
  loading: PageLoading
})

// const LocationAggregation = Loadable({
//   loader: () => import('./LocationAggregation'),
//   loading: PageLoading
// })

//聚合模板
const AggregationTemplate = Loadable({
  loader: () => import('./AggregationTemplate'),
  loading: PageLoading
})

//聚合任务
// const AggregationTask = Loadable({
//   loader: () => import('./AggregationTask'),
//   loading: PageLoading
// })

//聚合任务
const AggregationTask = Loadable({
  loader: () => import('./AggreTask'),
  loading: PageLoading
})

// const DynamicUpdate = Loadable({
//   loader: () => import('./DynamicUpdate'),
//   loading: PageLoading
// })
//更新算子
const UpdateOpr = Loadable({
  loader: () => import('./UpdateOpr'),
  loading: PageLoading
})

//更新任务
const UpdateTask = Loadable({
  loader: () => import('./UpdateTask'),
  loading: PageLoading
})


//南京市城市交通态势聚合任务
const UpdateTaskNanJingTrafficAggregation = Loadable({
  loader: () => import('./NanJingCityTrafficAggregation'),
  loading: PageLoading
})





//平台帮助
const PlatformHelp = Loadable({
  loader: () => import('./PlatformHelp'),
  loading: PageLoading
})

//地图工具
const MapTool = Loadable({
  loader: () => import("./MapTool"),
  loading: PageLoading
});

//视频工具
const VideoTool = Loadable({
  loader: () => import("./VideoTool"),
  loading: PageLoading
});

//声频工具
const VoiceTool = Loadable({
  loader: () => import("./VoiceTool"),
  loading: PageLoading
});

//文字工具
const TextTool = Loadable({
  loader: () => import("./TextTool"),
  loading: PageLoading
});

//图片工具
const ImageTool = Loadable({
  loader: () => import("./ImageTool"),
  loading: 
  PageLoading
});

//图表工具
const ChartTool = Loadable({
  loader: () => import("./ChartTool"),
  loading: PageLoading
});

// 图像列表
const FileList = Loadable({
  loader: () => import("./FileList"),
  loading: PageLoading
})

// 图像列表
const SoundFileList = Loadable({
  loader: () => import("./SoundFileList"),
  loading: PageLoading
})


export {
  Home,
  DataCollect,
  DataQuery,
  TagMethod,
  TagResult,
  TagTool,
  ContentList,
  SubjectCus,
  // DataTracing,
  OneSideSrc,
  ProcessSrc,
  // LocationAggregation,
  AggregationTask,
  AggregationTemplate,
  // DynamicUpdate
  UpdateOpr,
  UpdateTask,
  //南京城市交通聚合
  UpdateTaskNanJingTrafficAggregation,

  PlatformHelp,
  MapTool,
  ChartTool,
  ImageTool,
  VideoTool,
  VoiceTool,
	TextTool,
  FileList,
  SoundFileList,
  DataSource
}
