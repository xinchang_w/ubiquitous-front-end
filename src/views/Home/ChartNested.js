import React, { Component } from "react";
import * as echarts from "echarts";

class ChartNested extends Component{
    constructor(props){
        super(props);
        this.chart = null;
    }
    componentDidMount(){
        this.chart = echarts.init(document.getElementById(this.props.id));
        this.updateChart();
    }
    componentDidUpdate(){
        this.updateChart();
    }
    componentWillUnmount(){
        this.chart && echarts.dispose(this.chart);
    }
    render(){
        return (
            <div id={this.props.id} className={this.props.className} ></div>
        );
    }
    updateChart(){
        let dataIn = this.props.dataIn;
        let dataOut = this.props.dataOut;
        let option = setOption(this.props.type,dataIn,dataOut);
        this.chart.setOption(option);
    }
}


function setOption(type, dataIn,dataOut){
    return {
        tooltip: {
            trigger: 'item'
        },
        "visualMap":{
            show: false,
            min: 0,
            max: 1500,
            text:[ 'High', 'Low' ],
            realtime: false,
            calculable: true,
            inRange:{color:['#34699a','#408ab4','#65c6c4','#776d8a','#ad6989']}
        },
        series: [
            {
                name: '',
                type: 'pie',
                radius: ['45%','60%'],
                data:dataOut,
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            },
            {
                name: '',
                type: 'pie',
                radius: ['0','28%'],
                data: dataIn,
                label: {
                    position: 'inner',
                    fontSize: 10,
                },
                labelLine: {
                    show: false
                },
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            },

        ],
        "textStyle":{
            color: 'white'
        }
    }
}

export default ChartNested;