import React, { Component } from "react";
import * as echarts from "echarts";
import 'echarts/map/js/china.js';
import 'echarts/map/js/world.js';
import * as THREE from "three";
//import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
const OrbitControls = require('three-orbitcontrols')

class Map extends Component{
    constructor(props){
        super(props);
        this.map = null;
    }
    componentDidMount(){
        init(this.props.data);
    }
    componentDidUpdate(){
        //init(this.props.data);
    }
    render(){
        return (
            <div id={this.props.id} className="map"></div>
        );
    }

    componentWillUnmount(){
        
    }
}
var camera, controls, scene, renderer;
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var earth;
var mapSize = {
    width: 4096,
    height: 2048
};
var mapCanvas, mapTexture;
var option;

// 初始化echarts地图
function initMap(data) {
    mapCanvas = document.createElement( 'canvas' );
    mapCanvas.width = mapSize.width;
    mapCanvas.height = mapSize.height;

    mapTexture = new THREE.Texture( mapCanvas );

    var chart = echarts.init ( mapCanvas );


    option = {
        visualMap: {
            show: false,
            min: 0,
            max: 1500,
            text:[ 'High', 'Low' ],
            realtime: false,
            calculable: true,
            //inRange:{color:['#A5CAD2','#758EB7','#6F5F90','#8A5082','#FF7B89']}
            //inRange:{color:['#33539E','#7FACD6','#BFB8DA','#E8B7D4','#A5678E']}
            //inRange:{color:['#113f67','#096386','#13abc4','#7efaff']}
            inRange:{color:['#34699a','#408ab4','#65c6c4','#776d8a','#ad6989']}
},
        //backgroundColor: 'rgb( 0, 95, 151)',//地球海洋部分颜色
        backgroundColor: 'rgb(4, 34, 72)',//地球海洋部分颜色
        series: [
            {
                type: 'map',
                mapType: 'world',
                roam: true,
                aspectScale: 1,
                //layoutCenter: [ '50%', '50%' ],
                //layoutSize: 4096,
                itemStyle:{
                    normal:{
                        borderWidth: 2,//边际线大小
                        borderColor:'#00ffff',//边界线颜色
                    },
                    emphasis:{
                        label: {
                            show: true
                        }
                    }
                },
                data: data
            }
        ]
    };

    chart.setOption( option );
    mapTexture.needsUpdate = true;

    // 选中或移出时才更新贴图
    // 内存向显存上传数据很慢，应该尽量减少贴图更新
    chart.on( 'mouseover', function () {
        mapTexture.needsUpdate = true;
    } );

    chart.on( 'mouseout', function () {
        mapTexture.needsUpdate = true;
    } );

}

var container;

// 初始化three.js场景
function initScene() {

    container = document.getElementById( 'container' );

    // 场景
    scene = new THREE.Scene();

    // 相机
    camera = new THREE.PerspectiveCamera( 60, container.clientWidth / container.clientHeight, 1, 1000 );
    camera.position.z = -500;

    // 渲染器
    renderer = new THREE.WebGLRenderer({alpha: true});//这里要设置为true才能允许背景设置为透明
    //设置canvas背景色(clearColor)和背景色透明度（clearAlpha）
    renderer.setClearColor( 0x333333,0 );
    renderer.setPixelRatio( window.devicePixelRatio );

    renderer.setSize( container.clientWidth, container.clientHeight );//three的大小，重点调整的

    renderer.domElement.style.outline = "none";
    container.appendChild( renderer.domElement );

    // 控制器
    controls = new OrbitControls( camera, renderer.domElement );
    controls.enableDamping = true;
    controls.dampingFactor = 0.1;
    controls.enablePan = false;
    //controls.enableRotate = true;
    //controls.rotateSpeed = 0.4;

    controls.autoRotate = true;
    controls.autoRotateSpeed = -2.5;
    console.log(controls.autoRotateSpeed);



    // 设置光线
    scene.add(new THREE.HemisphereLight('#ffffff', '#ffffff', 1));

    // 地球
    var earthGeometry = new THREE.SphereBufferGeometry( 220, 36, 36 );//调整地球半径
    var earthMaterial = new THREE.MeshLambertMaterial( { map: mapTexture, color: 0xffffff } );
    earth = new THREE.Mesh( earthGeometry, earthMaterial );

    scene.add( earth );

    //渲染
    render();

}


function init(data) {

    initMap(data);

    initScene();


    //window.addEventListener( 'resize', onWindowResize, false );

    container.addEventListener( 'mousemove', onMouseMove, false );

}

function onMouseMove( event ) {

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

    raycaster.setFromCamera( mouse, camera );       // 通过鼠标坐标和相机位置构造射线

    var intersected = raycaster.intersectObject( earth );   // 获取射线和地球的相交点

    if ( intersected && intersected.length > 0 ) {

        // 根据射线相交点的uv反算出在canvas上的坐标
        var x = intersected[ 0 ].uv.x * mapSize.width;
        var y = mapSize.height - intersected[ 0 ].uv.y * mapSize.height;

        // 在mapCanvas上模拟鼠标事件，这里或许有更好的方法
        var virtualEvent = document.createEvent( 'MouseEvents' );
        virtualEvent.initMouseEvent( 'mousemove', false, true, document.defaultView, 1, x, y, x, y,false, false, false, false, 0, null );
        mapCanvas.dispatchEvent(virtualEvent);

    }


}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( container.clientWidth, container.clientHeight );

}

function render() {

    //设置地球旋转
    //scene.rotation.y -= 0.008;
    renderer.render( scene, camera );
    controls.update();
    requestAnimationFrame(render);
}



export default Map;