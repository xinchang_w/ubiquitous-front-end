import React, { Component } from "react";
import * as echarts from "echarts";

class Chart extends Component{
    constructor(props){
        super(props);
        this.chart = null;
    }
    componentDidMount(){
        this.chart = echarts.init(document.getElementById(this.props.id));
        this.updateChart();
    }
    componentDidUpdate(){
        this.updateChart();
    }
    componentWillUnmount(){
        this.chart && echarts.dispose(this.chart);
    }
    render(){
        return (
            <div id={this.props.id} className={this.props.className} ></div>
        );
    }
    updateChart(){
        //展示30条数据
        let data = this.props.data.slice(0, 30);
        let option = setOption(this.props.type, data, this.props.radius);
        this.chart.setOption(option);
    }
}

function setOption(type, data, radius){
    return {
        "yAxis":{
            "show": type === "bar",
            "type": "value",
        },
        "xAxis":{
            "boundaryGap": false,
            "show": type === "bar",
            "type": "category",
            "axisLabel": {
                "interval": 0,
                "rotate": 90
            },
            "data": data.map(item => item.name)
        },
        "tooltip": {
            "trigger": 'item'
        },
        "visualMap":{
            show: false,
            min: 0,
            max: 1500,
            text:[ 'High', 'Low' ],
            realtime: false,
            calculable: true,
            inRange:{color:['#34699a','#408ab4','#65c6c4','#776d8a','#ad6989']}
        },
        "series": [
            {
                "type": type,
                "radius": radius,
                "data": data,
                "itemStyle":{color:'rgb(18,150,219)'},
            }
        ],
        "textStyle":{
            color: 'white'
        }
    }
}

export default Chart;