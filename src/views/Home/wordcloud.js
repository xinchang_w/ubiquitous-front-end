import React, { Component } from "react";
import ReactEcharts from "echarts-for-react";
import * as echarts from "echarts";
import "echarts-wordcloud";
import sp from "@/assets/image/shape.png"

class WordCloud extends Component {

    constructor(props){
        super(props);
        this.chart = null;
    }
    componentDidMount(){
        this.createChart(this.props.year);
    }
    componentDidUpdate(){
        this.createChart(this.props.year);
    }
    componentWillUnmount(){

    }

    render(){
        return (
            <div id={this.props.id} className={this.props.className} ></div>
        );
    }

    createChart(){
        //this.chart && echarts.dispose(this.chart);
        this.chart = echarts.init(document.getElementById(this.props.id));
        this.chart.setOption(setWordOption(this.props.year));
    }
}

function setWordOption(year){
    let wordData_2010 = [
        {
            "name": "给力",
            "value": 30
        },
        {
            'name': "淡定",
            'value': 29
        },
        {
            'name': "世界杯",
            'value': 28
        },
        {
            'name': "世博会",
            'value': 27
        },
        {
            'name': "犀利",
            'value': 26
        },
        {
            'name': "靠谱",
            'value': 25
        },
        {
            'name': "地震",
            'value': 24
        },
        {
            'name': "偷菜",
            'value': 23
        },
        {
            'name': "任性",
            'value': 22
        },
        {
            'name': "悲剧",
            'value': 21
        },
        {
            'name': "蒜你狠",
            'value': 20
        },
        {
            'name': "微博",
            'value': 19
        },
        {
            'name': "浮云",
            'value': 18
        },
        {
            'name': "炫父",
            'value': 17
        },
        {
            "name":" Chatroulette",
            "value":1
        },
        {"name":" iPad","value":2},
        {"name":" Justin Bieber","value":3},
        {"name":" Nicki Minaj","value":4},
        {"name":" Friv","value":5},
        {"name":" Myxer","value":6},
        {"name":" Katy Perry","value":7},
        {"name":" Twitter","value":8},
        {"name":" Gamezer","value":9},
        {"name":"Facebook","value":10},
    ];
    let wordData_2011 = [
        {
            "name": "hold住",
            "value": 1
        },
        {
            'name': "乔布斯",
            'value': 2
        },
        {
            'name': "地沟油",
            'value': 3
        },
        {
            'name': "校车",
            'value': 4
        },
        {
            'name': "郭美美",
            'value': 5
        },
        {
            'name': "伤不起",
            'value': 6
        },
        {
            'name': "PM2.5",
            'value': 7
        },
        {
            'name': "谣盐",
            'value': 8
        },
        {
            'name': "占领华尔街",
            'value': 9
        },
        {
            'name': "悲剧",
            'value': 10
        },
        {"name":" Fukushima","value":1},
        {"name":" iPhone 4s","value":2},
        {"name":" Melania Rea","value":3},
        {"name":" Salvatore Parolisi","value":4},
        {"name":" Lamberto Sposini","value":5},
        {"name":" Battlefield 3","value":6},
        {"name":" DSK","value":7},
        {"name":" iPhone 5","value":8},
        {"name":" Gaddafi","value":9},
        {"name":"Libya","value":10},
    ];
    let wordData_2012 = [
        {
            "name": "十八大",
            "value": 20
        },
        {
            'name': "钓鱼岛",
            'value': 19
        },
        {
            'name': "伦敦奥运",
            'value': 18
        },
        {
            'name': "航母style",
            'value': 17
        },
        {
            'name': "异地高考",
            'value':16
        },
        {
            'name': "北京暴雨",
            'value':15
        },
        {
            'name': "11·11",
            'value': 14
        },
        {
            'name': "逆袭",
            'value': 13
        },
        {
            'name': "世界末日",
            'value': 12
        },
        {
            'name': "微信",
            'value': 11
        },
        {"name":" Hurricane Sandy","value":1},
        {"name":" Kate Middleton Pictures Released","value":2},
        {"name":" Olympics 2012","value":3},
        {"name":" SOPA Debate","value":4},
        {"name":" Costa Concordia crash","value":5},
        {"name":" Presidential Debate","value":6},
        {"name":" Stratosphere Jump","value":7},
        {"name":" Penn State Scandal","value":8},
        {"name":" Trayvon Martin shooting","value":9},
        {"name":"Pussy Riots","value":10},
    ];
    let wordData_2013 = [
        {
            "name": "中国梦",
            "value": 20
        },
        {
            'name': "土豪",
            'value': 19
        },
        {
            'name': "雾霾",
            'value': 18
        },
        {
            'name': "中国大妈",
            'value': 17
        },
        {
            'name': "单独二孩",
            'value':16
        },
        {
            'name': "比特币",
            'value':15
        },
        {
            'name': "嫦娥三号",
            'value': 14
        },
        {
            'name': "房姐",
            'value': 13
        },
        {
            'name': "大黄鸭",
            'value': 12
        },
        {
            'name': "斯诺登",
            'value': 11
        },
        {"name":" Nelson Mandela","value":1},
        {"name":" Paul Walker","value":2},
        {"name":" iPhone 5s","value":3},
        {"name":" Cory Monteith","value":4},
        {"name":" Harlem Shake","value":5},
        {"name":" Boston Marathon","value":6},
        {"name":" Royal Baby","value":7},
        {"name":" Samsung Galaxy S4","value":8},
        {"name":" PlayStation 4","value":9},
        {"name":"North Korea","value":10},
    ];
    let wordData_2014 = [
        {
            "name": "依法治国",
            "value": 20
        },
        {
            'name': "马航",
            'value': 19
        },
        {
            'name': "APEC蓝",
            'value': 18
        },
        {
            'name': "埃博拉",
            'value': 17
        },
        {
            'name': "冰桶挑战",
            'value': 16
        },
        {
            'name': "监狱风云",
            'value': 15
        },
        {
            'name': "克里米亚",
            'value': 14
        },
        {
            'name': "《小苹果》",
            'value': 13
        },
        {
            'name': "奶茶恋",
            'value':12
        },
        {
            'name': "玉兔号",
            'value': 11
        },
        {"name":" Ebola","value":1},
        {"name":" ISIS","value":2},
        {"name":" Malaysia Airlines","value":3},
        {"name":" Crimea / Ukraine","value":4},
        {"name":" Ferguson","value":5},
        {"name":" Gaza and Israel","value":6},
        {"name":" Scotish Referendum","value":7},
        {"name":" Oscar Pistorius trial","value":8},
    ];
    let wordData_2015 = [
        {
            "name": "抗战胜利70周年阅兵",
            "value": 30
        },
        {
            'name': "2022年北京冬奥会",
            'value': 29
        },
        {
            'name': "中美互联网论坛",
            'value': 28
        },
        {
            'name': "开普勒-452b",
            'value': 27
        },
        {
            'name': "屠呦呦",
            'value': 26
        },
        {
            'name': "扶老人险",
            'value': 25
        },
        {
            'name': "天津爆炸案",
            'value': 24
        },
        {
            'name': "第五套人民币",
            'value': 23
        },
        {
            'name': "雾霾",
            'value': 22
        },
        {
            'name': "全面二孩",
            'value': 21
        },
        {"name":" Charlie Hebdo","value":1},
        {"name":" Paris","value":2},
        {"name":" Hurricane Patricia","value":3},
        {"name":" Isis","value":4},
        {"name":" Nepal","value":5},
        {"name":" El Chapo","value":6},
        {"name":" Greece","value":7},
        {"name":" Baltimore Riots","value":8},
        {"name":" San Bernardino","value":9},
        {"name":"Hurricane Joaquin","value":10},
    ];
    let wordData_2016 = [
        {
            "name": "里约热内卢奥运会",
            "value": 1
        },
        {
            'name': "特朗普",
            'value': 2
        },
        {
            'name': "神舟十一号",
            'value': 3
        },
        {
            'name': "崔顺实门",
            'value': 4
        },
        {
            'name': "人工智能",
            'value': 5
        },
        {
            'name': "南海仲裁案",
            'value': 6
        },
        {
            'name': "引力波",
            'value': 7
        },
        {
            'name': "G20峰会",
            'value': 8
        },
        {
            'name': "蓝瘦香菇",
            'value': 9
        },
        {
            'name': "裸条",
            'value': 10
        },
        {"name":" US Election","value":1},
        {"name":" Olympics","value":2},
        {"name":" Brexit","value":3},
        {"name":" Orlando Shooting","value":4},
        {"name":" Zika Virus","value":5},
        {"name":" Panama Papers","value":6},
        {"name":" Nice","value":7},
        {"name":" Brussels","value":8},
        {"name":" Dallas Shooting","value":9},
        {"name":"Kumamoto Earthquake","value":10},
    ];
    let wordData_2017 = [
        {
            "name": "新时代",
            "value": 1
        },
        {
            'name': "共享",
            'value': 2
        },
        {
            'name': "雄安新区",
            'value': 3
        },
        {
            'name': "金砖国家",
            'value': 4
        },
        {
            'name': "人工智能",
            'value': 5
        },
        {
            'name': "人类命运共同体",
            'value': 6
        },
        {
            'name': "天舟一号",
            'value': 7
        },
        {
            'name': "不忘初心",
            'value': 8
        },
        {
            'name': "牢记使命",
            'value': 9
        },
        {
            'name': "撸起袖子加油干",
            'value': 10
        },
        {"name":" Hurricane Irma","value":1},
        {"name":" Bitcoin","value":2},
        {"name":" Las Vegas Shooting","value":3},
        {"name":" North Korea","value":4},
        {"name":" Solar Eclipse","value":5},
        {"name":" Hurricane Harvey","value":6},
        {"name":" Manchester","value":7},
        {"name":" Hurricane Jose","value":8},
        {"name":" Hurricane Maria","value":9},
        {"name":"April the Giraffe","value":10},
    ];
    let wordData_2018 = [
        {
            "name": "世界杯",
            "value": 20
        },
        {
            'name': "重庆万州公交车坠江",
            'value': 10
        },
        {
            'name': "滴滴顺风车事件",
            'value': 9
        },
        {
            'name': "改革开放四十周年",
            'value': 20
        },
        {
            'name': "高铁霸座",
            'value': 15
        },
        {
            'name': "锦鲤",
            'value': 16
        },
        {
            'name': "杠精",
            'value': 14
        },
        {
            'name': "真香",
            'value': 8
        },
        {
            'name': "安排",
            'value': 9
        },
        {
            'name': "佛系",
            'value': 10
        },
        {"name":"World Cup","value":1},
        {"name":" Hurricane Florence","value":2},
        {"name":"Mega Millions Result","value":3},
        {"name":" Royal Wedding","value":4},
        {"name":" Election Results","value":5},
        {"name":" Hurricane Michael","value":6},
        {"name":" Kavanaugh Confirmation","value":7},
        {"name":" Florida Shooting","value":8},
        {"name":" Greve dos caminhoneiros","value":8},
        {"name":" Government Shutdown","value":8},
    ];
    let wordData_2019 = [
        {
            "name": "嫦娥四号",
            "value": 1
        },
        {
            'name': "苏炳添",
            'value': 2
        },
        {
            'name': "少年的你",
            'value': 3
        },
        {
            'name': "我和我的祖国",
            'value': 4
        },
        {
            'name': "流浪地球",
            'value': 5
        },
        {
            'name': "垃圾分类",
            'value': 6
        },
        {
            'name': "网红产品",
            'value': 7
        },
        {
            'name': "超前消费",
            'value': 8
        },
        {
            'name': "网络短视频",
            'value': 9
        },
        {
            'name': "学术造假",
            'value': 10
        },
        {
            'name': "英国脱欧",
            'value': 11
        },
        {"name":"Copa America","value":1},
        {"name":"Notre Dame","value":2},
        {"name":"ICC Cricket World Cup","value":3},
        {"name":"Hurricane Dorian","value":4},
        {"name":"Rugby World Cup","value":5},
        {"name":" Sri Lanka","value":6},
        {"name":"Area 51","value":7},
        {"name":"India election results","value":8},
        {"name":"台風 19 号","value":9},
        {"name":"Fall of Berlin Wall","value":10},
    ];
    let wordData_2020 = [
        {
            'name': "中美关系",
            'value': 8
        },
        {
            "name": "新冠肺炎病毒",
            "value": 30
        },
        {
            "name": "疫情",
            "value": 28
        },
        {
            'name': "抗议英雄",
            'value': 25
        },
        {
            'name': "钟南山",
            'value': 20
        },
        {
            'name': "口罩",
            'value': 15
        },
        {
            'name': "火神山",
            'value': 10
        },
        {
            'name': "美国大选",
            'value': 8
        },
        {
            'name': "隐秘的角落",
            'value': 8
        },
        {
            'name': "脱贫攻坚",
            'value': 15
        },
        {
            'name': "乘风破浪",
            'value': 6
        },
        {
            'name': "凡尔赛",
            'value': 6
        },
        {
            'name': "直播带货",
            'value': 8
        },
        {
            'name': "网课",
            'value': 6
        },

        {
            'name': "民法典",
            'value': 5
        },
        {
            'name': "逆行者",
            'value': 18
        },
        {
            'name': "武汉",
            'value': 20
        },
        {
            'name': "打工人",
            'value': 8
        },
        {
            'name': "网抑云",
            'value': 6
        },
        {"name":" Coronavirus","value":1},
        {"name":"Election results","value":2},
        {"name":" Iran","value":3},
        {"name":"Beirut","value":4},
        {"name":"Hantavirus","value":5},
        {"name":"Stimulus checks","value":6},
        {"name":"Unemployment","value":7},
        {"name":"Tesla stock","value":8},
        {"name":" Bihar election result","value":9},
        {"name":"Black Lives Matter","value":10},

    ];
    
    let wordData = {wordData_2010, wordData_2011, wordData_2012, wordData_2013, wordData_2014, wordData_2015, 
        wordData_2016, wordData_2017, wordData_2018, wordData_2019, wordData_2020 };

    let maskImage = new Image();
    maskImage.src = sp;

    let option = {
        backgroundColor: "rgba(41,46,60,0)",
        // tooltip: {
        //     pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
        // },
        series: [
            {
                type: "wordCloud",
                gridSize: 1,
                // Text size range which the value in data will be mapped to.
                // Default to have minimum 12px and maximum 60px size.
                sizeRange: [15, 30],
                // Text rotation range and step in degree. Text will be rotated randomly in range [-90,                                                                             90] by rotationStep 45

                shape: 'square',
                //maskImage:maskImage,
                //rotationRange: [-45,0,0,45],
                rotationStep: 45,
                textStyle: {
                    normal: {
                        color: function() {
                            return (
                                "rgb(" +
                                Math.round(Math.random() * 255) +
                                ", " +
                                Math.round(Math.random() * 255) +
                                ", " +
                                Math.round(Math.random() * 255) +
                                ")"
                            );
                        }
                    }
                },
                // Folllowing left/top/width/height/right/bottom are used for positioning the word cloud
                // Default to be put in the center and has 75% x 80% size.
                left: "center",
                top: "center",
                right: null,
                bottom: null,
                width: "100%",
                height: "100%",
                data: wordData["wordData_" + year]
            }
        ]
    };
    return option;
}

export default WordCloud;
