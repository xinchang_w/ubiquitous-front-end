import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Layout, Row, Col, Card } from "antd";
import { WORLD_COUNTRIES, CHINA_PROVINCES } from "@/assets/global/index";
import { FIRST_NAV } from '@/routes'
import WorldMap from "./worldMap";
import ChinaMap from "./chinaMap";
import Chart from "./chart";
import ChartNested from  "./ChartNested";
import ChartLevel from "./ChartLevel";
import WordCloud from "./wordcloud";
import { Radio, Table } from "antd";
import "@/assets/style/home.css";
// import "@/assets/style/grid.css";


import aliyun from "@/assets/image/aliyun.png";
import amazon from "@/assets/image/amazon.png";
import boson from "@/assets/image/boson.png";
import dby from "@/assets/image/dby.png";
import es from "@/assets/image/es.png";
// import bg from "@/assets/image/banner.png"
import world from "@/assets/image/World_Icon.png";
import china from "@/assets/image/China_Icon.png";
import navy from "@/assets/image/navy.png";
import tagImage1 from "@/assets/image/信息标签.png";
import tagImage2 from "@/assets/image/位置聚合.png";

import infoPic1 from "@/assets/image/中国新闻网.png";
import infoPic2 from "@/assets/image/光明日报.png";
import infoPic3 from "@/assets/image/中国经济网.png";
import infoPic4 from "@/assets/image/中央气象台.png";
import infoPic5 from "@/assets/image/人民日报.png";
import infoPic6 from "@/assets/image/人民网.png";
import infoPic7 from "@/assets/image/今日头条.png";
import infoPic8 from "@/assets/image/凤凰网.png";
import infoPic9 from "@/assets/image/宁夏新闻网.png";
import infoPic10 from "@/assets/image/央视新闻.png";
import infoPic11 from "@/assets/image/微博.png";
import infoPic12 from "@/assets/image/新华网.png";
import infoPic13 from "@/assets/image/海外网.png";
import infoPic14 from "@/assets/image/环球网.png";
import infoPic15 from "@/assets/image/百度地图.png";
import infoPic16 from "@/assets/image/百度新闻.png";
import infoPic17 from "@/assets/image/纽约时报中文网.png";
import infoPic18 from "@/assets/image/网易新闻.png";
import infoPic19 from "@/assets/image/联商网.png";
import infoPic20 from "@/assets/image/腾讯地图.png";
import infoPic22 from "@/assets/image/长沙晚报网.png";
import infoPic23 from "@/assets/image/闽南网.png";
import infoPic24 from "@/assets/image/高德地图.png";
import infoPic25 from "@/assets/image/胶东在线.png";
import infoPic26 from "@/assets/image/中国江苏网.png";
import infoPic27 from "@/assets/image/资阳网.png";
import infoPic28 from "@/assets/image/华尔街日报.png";


import {color} from "echarts/src/export";

const { Header, Content, Footer } = Layout;
//let chinaData = createMapData(CHINA_PROVINCES);
//let worldData = createMapData(WORLD_COUNTRIES);
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "mapType": "world",
            "year": 2010,
            "chinaData1": createMapData(CHINA_PROVINCES),
            "worldData1": createMapData(WORLD_COUNTRIES),
            "chinaData2": createMapData(tagName),
            "worldData2": createMapData(tagName),
            "chinaData3": createMapData(tagClass),
            "worldData3": createMapData(tagClass),
            "chinaData4": createMapData(tagNameClass),
            "worldData4": createMapData(tagNameClass),
        };
        //保存图表和地图组件的引用
        this.barChart = React.createRef();
        this.pieChart = React.createRef();
        this.map = React.createRef();
        this.timer = 0;
        this.timer_year = 0;

        this.changeMapType = this.changeMapType.bind(this);
        this.forceRenderChart = this.forceRenderChart.bind(this);
    }

    componentDidMount() {
        this.changeYear();
    }
    componentWillUnmount() {
        this.timer && clearTimeout(this.timer);
        this.timer_year = clearInterval(this.timer_year);
    }
    render() {
        let data1 = this.state.mapType === "china" ? this.state.chinaData1 : this.state.worldData1;
        let data2 = this.state.mapType === "china" ? this.state.chinaData2 : this.state.worldData2;
        let data3 = this.state.mapType === "china" ? this.state.chinaData3 : this.state.worldData3;
        let data4 = this.state.mapType === "china" ? this.state.chinaData4 : this.state.worldData4;

        return (
            <div className="wrapper wrapper-grid">
            <Layout className="layout" style={{height:"100%"}}>
                <Header style={{ backgroundColor: "transparent" }}>
                    <h1 style={{ fontSize: '36px', color: "white", textAlign: 'center', fontWeight: 'bold',letterSpacing:'10px' }}>泛在数据信息建模平台</h1>
                </Header>
                <Content style={{height:"calc(100% - 110px)"}}>
                    <Row gutter={[{ md: 8, xxl: 24 }, 0]} style={{height:"44%"}}>
                        <Col md={6} xxl={6}>
                            <WordCloud className="chart" id="wordcloud" year={this.state.year}></WordCloud>
                        </Col>
                        <Col md={12} xxl={12} style={{ position: "relative" }}>{/* style={{"height": "400px"}}*/}
                            {
                                this.state.mapType == "world" ?
                                    <WorldMap className="chart" type={this.state.mapType} id="container" data={data1} /> :
                                    <ChinaMap className="chart" type={this.state.mapType} id="container" data={data1} />
                            }
                        </Col>
                        <Col md={6} xxl={6}>
                            <Chart className="chart" id="bar1" type="bar" data={data1} ref={this.barChart} />
                        </Col>
                    </Row>
                    <Row gutter={[{ md: 8, xxl: 24 }, 0]} style={{height:"55%"}}>
                        <Col md={6} xxl={6}>
                            <ChartLevel className="chart" id="bar2" type="bar" radius={'60%'} data={data4} ref={this.pieChart} />
                        </Col>
                        <Col md={12} xxl={12} style={{ fontSize: '17px' }}>
                            <div className="tagBox" >
                                <div className="tagName" style={{transform:"translateY(60px)"}}>文本</div>
                                <div className="tagName" style={{transform:"translateY(24px)"}} >音频</div>
                                <div className="tagName3" style={{transform:"translateY(14px)"}} >视频</div>
                                <div className="tagName3" style={{transform:"translateY(14px)"}} >图像</div>
                                <div className="tagName" style={{transform:"translateY(24px)"}} >地图</div>
                                <div className="tagName" style={{transform:"translateY(60px)"}}>图表</div>
                            </div>
                            <div style={{ marginTop: "15px" }}>
                                <p style={{ fontSize: "30px", color: '#be9fe1', fontWeight: '10px', textAlign: "center" }}><strong>{this.state.year}</strong></p>
                            </div>
                            <div className="buttonBox">
                                <span className="tagName2" style={{float:"left",marginLeft:"120px",transform:"translateY(-26px) rotate(8deg)"}} >
                                    <img src={tagImage1} style={{width:"180px",opacity:""}} />
                                </span>
                                <span className="changeButton" style={{marginLeft:"80px"}} onClick={() => this.changeMapType("world")} >
                                    <img src={world} style={{height:"40px", width:"50px"}}/>
                                </span>
                                <span className="changeButton" style={{}} onClick={() => this.changeMapType("china")}>
                                    <img src={china} style={{height:"50px"}}/>
                                </span>
                                <span className="tagName2" style={{float:"right",marginRight:"180px",transform:"translateY(-15px) rotate(-10deg)"}} >
                                    <img src={tagImage2} style={{width:"180px",opacity:""}} />
                                </span>

                            </div>
                            <div style={{ color: "white", paddingTop: '50px', textIndent: '2em' }}>
                                本平台运用互联网+，人工智能，地理信息系统等技术方法，针对文本、图表、图像、地图、视频、
                                音频等不同形式的泛在数据进行收集整理，并对其中包含的时间、地点、人物、事物、事件、现象与场景
                                等信息要素进行处理解析，同时提供资源管理、信息标签、信息溯源、信息聚合、动态更新等应用
                                服务，实现人文社会场景中泛在数据的深度挖掘与有序组织。
                                <Link to={FIRST_NAV}>
                                    <img className="navy-img" src={navy} style={{height:"45px"}} />
                                </Link>
                            </div>
                        </Col>
                        <Col md={6} xxl={6}>
                            <ChartNested className="chart" id="pie1" type="pie" dataIn={data2} dataOut={data3}  ref={this.pieChart} />
                        </Col>
                    </Row>
                </Content>
                <Footer className="infoSrcBox">
                    <img className="infoPic" src={infoPic1} alt="aliyun" />
                    <img className="infoPic" src={infoPic2} alt="aliyun" />
                    <img className="infoPic" src={infoPic3} alt="aliyun" />
                    <img className="infoPic" src={infoPic4} alt="aliyun" />
                    <img className="infoPic" src={infoPic5} alt="aliyun" />
                    <img className="infoPic" src={infoPic6} alt="aliyun" />
                    <img className="infoPic" src={infoPic7} alt="aliyun" />
                    <img className="infoPic" src={infoPic8} alt="aliyun" />
                    <img className="infoPic" src={infoPic9} alt="aliyun" />
                    <img className="infoPic" src={infoPic10} alt="aliyun" />
                    <img className="infoPic" src={infoPic11} alt="aliyun" />
                    <img className="infoPic" src={infoPic12} alt="aliyun" />
                    <img className="infoPic" src={infoPic13} alt="aliyun" />
                    <img className="infoPic" src={infoPic14} alt="aliyun" />
                    <img className="infoPic" src={infoPic15} alt="aliyun" />
                    <img className="infoPic" src={infoPic16} alt="aliyun" />
                    <img className="infoPic" src={infoPic17} alt="aliyun" />
                    <img className="infoPic" src={infoPic18} alt="aliyun" />
                    <img className="infoPic" src={infoPic19} alt="aliyun" />
                    <img className="infoPic" src={infoPic20} alt="aliyun" />
                    <img className="infoPic" src={infoPic22} alt="aliyun" />
                    <img className="infoPic" src={infoPic23} alt="aliyun" />
                    <img className="infoPic" src={infoPic24} alt="aliyun" />
                    <img className="infoPic" src={infoPic25} alt="aliyun" />
                    <img className="infoPic" src={infoPic26} alt="aliyun" />
                    <img className="infoPic" src={infoPic27} alt="aliyun" />
                    <img className="infoPic" src={infoPic28} alt="aliyun" />
                </Footer>
            </Layout >
            </div>
        );
    }
    changeMapType(type) {
        this.setState({
            "mapType": type
        });
    }

    //调整页面大小后，强制重新渲染图表组件，使其自适应窗口大小
    forceRenderChart() {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.barChart.current.forceUpdate();
            this.pieChart.current.forceUpdate();
            this.map.current.forceUpdate();
        }, 500);
    }

    //动态加载不同年份数据
    changeYear() {
        this.timer_year = setInterval(() => {
            let year = this.state.year === 2020 ? 2010 : this.state.year + 1;
            let worldData1 = createMapData(WORLD_COUNTRIES);
            let chinaData1 = createMapData(CHINA_PROVINCES);
            let worldData2 = createMapData(tagName);
            let chinaData2 = createMapData(tagName);
            let worldData3 = createMapData(tagClass);
            let chinaData3 = createMapData(tagClass);
            let worldData4 = createMapData(tagNameClass);
            let chinaData4 = createMapData(tagNameClass);
            this.setState({ year, worldData1, chinaData1,worldData2,chinaData2,worldData3,chinaData3,worldData4,chinaData4});

        }, 3000);
    }

    

}
const tagName = ["文本","音频","视频","图像","地图","图表"];
const tagClass = ["时间","地点","人物","事物","事件","现象","场景"];
const tagNameClass = ["文本","音频","视频","图像","地图","图表","时间","地点","人物","事物","事件","现象","场景"];

function createMapData(names) {
    return names.map(name => {
        return {
            "name": name,
            "value": Math.round(Math.random() * 1500)
        }
    });
}



function getDescriptions() {
    return `本平台运用“互联网+”、人工智能、地理信息系统等技术方法，针对文本、图表、图像、地图、音频、视频等不同形式的泛在数据进行收集整理，并对其中包含\
        的时间地点、人物、事物、事件、现象与场景等信息要素进行处理解析，同时提供资源管理、信息标签、信息溯源、信息聚合、动态更新等应用服务，实现人文社会\
        场景中泛在数据的深度挖掘与有序组织。`
}

export default Home;
