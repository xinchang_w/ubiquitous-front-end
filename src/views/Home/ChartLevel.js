import React, { Component } from "react";
import * as echarts from "echarts";

class ChartLevel extends Component{
    constructor(props){
        super(props);
        this.chart = null;
    }
    componentDidMount(){
        this.chart = echarts.init(document.getElementById(this.props.id));
        this.updateChart();
    }
    componentDidUpdate(){
        this.updateChart();
    }
    componentWillUnmount(){
        this.chart && echarts.dispose(this.chart);
    }
    render(){
        return (
            <div id={this.props.id} className={this.props.className} ></div>
        );
    }
    updateChart(){
        //展示30条数据
        let data = this.props.data.slice(0, 30);
        let option = setOption(this.props.type, data, this.props.radius);
        this.chart.setOption(option);
    }
}

function setOption(type, data, radius){
    return {
        "yAxis":{
            "type": "category",
            "data": data.map(item => item.name),
            // "type": "value",
            "show": type === "bar",
            "axisLine":{       //y轴
                "show":false

            },
            "axisTick":{       //y轴刻度线
                "show":false
            },
            "splitLine": {     //网格线
                "show": false
            },

        },
        "xAxis":{
            "type": "value",
            // "type": "category",
            // "data": data.map(item => item.name),
            "boundaryGap": false,
            "show": type === "bar",

            "axisLabel": false,
            "splitLine": {
                "show": false
            },
            "axisTick":{       //y轴刻度线
                "show":false
            },
            "axisLine":{       //y轴
                "show":false

            },

        },
        "tooltip": {
            "trigger": 'item'
        },
        "visualMap":{
            show:false,
            orient: 'horizontal',
            left: 'center',
            min: 10,
            max: 1500,
            text: ['High', 'Low'],
            dimension: 0,
            inRange:{color:['#34699a','#408ab4','#65c6c4','#776d8a','#ad6989']}
        },
        "series": [
            {
                "type": type,
                "radius": radius,
                "data": data,
                //"itemStyle":{color:'rgb(18,150,219)'},
            }
        ],
        "textStyle":{
            color: 'white'
        }
    }
}

export default ChartLevel;