import React, { Component } from "react";
import * as echarts from "echarts";
import 'echarts/map/js/china.js';
import 'echarts/map/js/world.js';

class ChinaMap extends Component{
    constructor(props){
        super(props);
        this.map = null;
    }
    componentDidMount(){
        this.createMap();
    }
    componentDidUpdate(){
        this.createMap();
    }
    render(){
        return (
            <div id={this.props.id} className="map"></div>
        );
    }
    createMap(){
        this.map && echarts.dispose(this.map);
        this.map = echarts.init(document.getElementById(this.props.id));
        this.map.setOption(getMapOption(this.props.type, this.props.data));
    }
}

function getMapOption(mapType, data) {
    return {
        //提示框浮层
        tooltip: {
            trigger: 'item',
            "formatter": "{b}: {c}"
        },
        //左侧图例
        visualMap: {
            min: 0,
            max: 1500,
            left: 'left',
            top: 'bottom',
            text: ['高', '低'],
            textStyle:{color: 'white'},
            calculable: true,
            show:false,//是否显示色彩渐变条图例
            //inRange:{color:['#A5CAD2','#758EB7','#6F5F90','#8A5082','#FF7B89']}
            //inRange:{color:['#33539E','#7FACD6','#BFB8DA','#E8B7D4','#A5678E']}
            //inRange:{color:['#113f67','#096386','#13abc4','#7efaff']}
            inRange:{color:['#34699a','#408ab4','#65c6c4','#776d8a','#ad6989']}

        },
        series: [
            {
                name: 'value',
                type: 'map',
                map: mapType,
                // 是否支持缩放平移：true, false, 'scale', 'move'
                roam: false,
                label: {
                    normal: {
                        show: false
                    },
                    emphasis: {
                        show: true
                    }
                },
                itemStyle:{
                    normal:{
                        borderWidth: 1,//边际线大小
                        borderColor:'#00ffff',//边界线颜色
                    },
                },
                data: data
            }
        ]
    };
}

export default ChinaMap;