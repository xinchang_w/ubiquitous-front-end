import React from "react";
import { Breadcrumb, Card } from "antd";

import semanteme from "@/assets/image/update-opr-semanteme.png";
import attribute from "@/assets/image/update-opr-attribute.png";
import geometry from "@/assets/image/update-opr-geometry.png";
import location from "@/assets/image/update-opr-location.png";
import relation from "@/assets/image/update-opr-relation.png";
import evolution from "@/assets/image/update-opr-evolution.png";

const items = [
    {
        "name": "语义描述更新",
        "icon": semanteme
    },
    {
        "name": "空间位置更新",
        "icon": location
    },
    {
        "name": "几何形态更新",
        "icon": geometry
    },
    {
        "name": "演化过程更新",
        "icon": evolution
    },
    {
        "name": "要素关系更新",
        "icon": relation
    },
    {
        "name": "作用机制更新",
        "icon": attribute
    },
    {
        "name": "属性特征更新",
        "icon": attribute
    }
];

function updateOpr(props){
    return (
        <div className="container">
            <Breadcrumb className="breadcrumb">
                <Breadcrumb.Item>动态更新</Breadcrumb.Item>
                <Breadcrumb.Item className="breadcrumb-item">更新算子</Breadcrumb.Item>
            </Breadcrumb>
            <div className="cardBox" style={{display:"flex", justifyContent:"space-around", flexWrap:"wrap"}}>
                {
                    items.map((i, ind) => (
                        <Card 
                            key={ind}
                            hoverable={true} 
                            style={{width:"30%",textAlign:"center", overflow:"hidden",margin:"5px"}}
                        >
                            <img src={i.icon} style={{height:200, width:200}} />
                            <p>{i.name}</p>
                        </Card>
                    ))
                }
            </div>
        </div>
    );
}

export default updateOpr;