import React from 'react'
import { Layout, Menu, Divider, Row, Col, Typography} from 'antd';
import DataServerTool from '../../components/DataServerTool/Tool'
import MapDiv from '../../components/MapJs/Map.js'

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;
const { Title } = Typography;

class MapTool extends React.Component{
  constructor(props){
    super(props);
    this.state = {

    }
  }

  render(){
    return (
    <Layout id='layout' style={{height: '100vh',  minWidth: '1000px', minHeight: '600px',}}>
      <Row style={{height: '100%', marginTop: '10px'}}>
        <Col span={4}>
        <div style={{maxHeight: '70%', minHeight: '40%', width: '100%', }}>
        <Title level={3} style={{textAlign: 'center'}}>模型列表</Title>
        <DataServerTool type='Image' isWindow='true'/>
        </div>
        <div style={{height: '30%'}}>
        <Title level={3} style={{textAlign: 'center'}}>七要素</Title>
        </div>
        </Col>
        <Col span={20}>
          <MapDiv dataType='image' />
        </Col>
      </Row>
    </Layout>
    );
  }
}


export default MapTool;