import { RightOutlined } from "@ant-design/icons";
import style from "./index.module.css";

function CustomizeTable(props){
    let { caption, columns, dataSource } = props;

    return (
        <div className={style["tableBox"]}>
            <div className={Array.isArray(dataSource) ? `${style["transfer"]} ${style["on"]}` : style["transfer"]}>
                <RightOutlined />
            </div>
            <table className={style["cusTable"]}>
                <caption className={style["cusTable-caption"]}>{caption}</caption>
                <thead className={style["cusTable-thead"]}>
                    <tr>
                        {
                            columns.map((col, ind) => (
                                <th key={ind}>
                                    {col.title}
                                </th>
                            ))
                        }
                    </tr>
                </thead>
                <tbody className={style["cusTable-tbody"]}>
                    {Array.isArray(dataSource) && dataSource.length > 0
                        ? createTr(dataSource, columns)
                        : emptyTr(columns.length)
                    }
                </tbody>
            </table>
        </div>
    );
}

function emptyTr(totalCols){
    return (
        <tr>
            <td colSpan={totalCols}>暂无数据</td>
        </tr>
    );
}

function createTr(dataSource, cols){
    let arr = [];
    let tr, data;
    for(data of dataSource){
        tr = (
            <tr key={data.id}>
                {
                    cols.map((col, ind) => {
                        let dataIndex = col.dataIndex;
                        let val = typeof dataIndex === "string" ? data[dataIndex] : getVal(data, dataIndex);
                        let td;
                        if("render" in col){
                           td = <td key={ind}>{col.render(val)}</td>;
                        }
                        else{
                            td = <td key={ind}>{val}</td>;
                        }
                        return td;
                    })
                }
            </tr>
        );
        arr.push(tr);
    }
    return arr;
}

function getVal(s, dataIndex){
    let temp = s[dataIndex[0]];
    let i;
    for(i = 1; i < dataIndex.length; i++){
        temp = temp[dataIndex[i]];
    }
    return temp;
}

export default CustomizeTable;