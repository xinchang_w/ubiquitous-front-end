import { colorMap, Title, colorMap_image, colorMap_text} from "./Title";
import Paragraph from "./Paragraph";
import TableRendering from "./TableRendering";
import CustomizeModal  from "./customizeModal";
import CusImage from "./CusImage";
import Card from "./customizeCard";
import CusMap from "./customizeMap";
export { default as PageFrame } from './PageFrame';
export { default as Loading } from './Loading';

export {
    Chart,
    BarChart,
    LineChart,
    PieChart
} from './Charts';
export {
    colorMap,
    colorMap_image,
    colorMap_text,
    Title,
    Paragraph,
    TableRendering,
    CustomizeModal,
    CusImage,
    Card,
    CusMap
}