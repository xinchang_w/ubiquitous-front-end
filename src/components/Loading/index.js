import React, { Component } from 'react';
import '@/assets/style/loading.less';

export default class Loading extends Component {
    render() {
        const { masker, size, loading } = this.props;
        const maskerStyle = masker ? { backgroundColor: 'rgb(0, 0, 0, 0.1)' } : {};
        const loadingClass = loading ? 'loading' : '';
        const spinSize = size === 'small' ? '40px' :
            size === 'large' ? '100px' : '70px';
        const spinStyle = { width: spinSize, height: spinSize };

        return (
            <div className='loading-container'>
                <div className={`loading-icon ${loadingClass}`} style={maskerStyle}>
                    <div className='loading-icon-wrapper'>
                        <div className='loading-spin' style={spinStyle}></div>
                        <div className='loading-text-wrapper'>
                            <p className='loading-text'>加载中...</p>
                        </div>
                    </div>
                </div>
                <div className='loading-content'>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

