import { Modal } from "antd";
/*
@param visible {Boolean}
@param data {Object}
@param close {Function}    关闭弹框的函数
@param width {Number}    弹框宽度
*/
function CustomizeModal(props){
    return (
        <Modal
            visible={props.visible}
            title={props.title}
            onOk={props.close}
            onCancel={props.close}
            width={props.width || 800}
        >
            {props.children}
        </Modal>
    );
}

export default CustomizeModal;