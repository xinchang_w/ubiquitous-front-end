import { Card, Tooltip, Modal, Descriptions, Form, Input, Button, Divider, message, Upload, List, Avatar, Spin } from 'antd'
import { ConsoleSqlOutlined, UploadOutlined } from '@ant-design/icons'
import React from 'react'
import axios from 'axios'
import ToolImage from './modelIcon.jpg'
// import './Tool.css'
import { ParametricBufferGeometry } from 'three'

/*
 * @param type {string} 模型的类型
 * @param isWindow {boolean} 数据容器的类型：window或者linux
 */

const { Meta } = Card;

// 加拦截器，get里面设置content-type
const options = {
    headers: {
        'Content-Type': 'application/json'
    }
}
axios.interceptors.request.use(
    config => {
        config.headers['Content-Type'] = 'application/json'
        return config
    }
)
const request = axios.create(options)
request.interceptors.request.use(config => {
    if (config.method === 'get') {
        config.data = true
    }
    config.headers['Content-Type'] = 'application/json'
    return config
}, err => Promise.reject(err))


function ToolItem(props) {
    return (
        <div className='toolItem'>
            <Tooltip title={props.description}>
                <Card
                    bodyStyle={{ padding: 5 }}
                    hoverable
                    col-2
                    cover={
                        <img src={ToolImage} size='small' />
                    }
                >
                    <Meta title={props.name} />
                </Card>
            </Tooltip>

        </div>
    )
}

function ImgModal(props) {
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 32,
        },
    };

    const [form] = Form.useForm();

    const onReset = () => {
        props.initDataServer()
        form.resetFields();
    };

    return (
        <Modal
            title={props.item.data.name || ''}
            visible={props.visible}
            onOk={props.handleOk}
            // confirmLoading={props.confirmLoading}
            onCancel={props.handleCancel}
        >
            <Form {...layout} form={form} onFinish={props.onFinish}>
                <h2>&nbsp;&nbsp;&nbsp;&nbsp;{props.item.data.description || ''}</h2>
                <Divider orientation="left">Input:</Divider>
                {
                    (props.data.input ? props.data.input : []).map(input => {
                        return (
                            <Tooltip title={input.description}>
                                <Form.Item
                                    name={input.name}
                                    label={input.name}
                                    rules={[{ required: true }]}
                                >
                                    <Upload
                                        name='file'
                                        beforeUpload={props.beforeUploadInput}
                                    // afterRead={afterRead}
                                    >
                                        <Button icon={<UploadOutlined />}>上传文件（1）</Button>
                                    </Upload>
                                </Form.Item>
                            </Tooltip>

                        )
                    })
                }
                <Divider orientation="left">Parameter:</Divider>
                {
                    (props.data.parameter ? props.data.parameter : []).map(parameter => {
                        return (
                            <Tooltip title={parameter.description}>
                                <Form.Item
                                    name={parameter.name}
                                    label={parameter.name}
                                    rules={[{ required: true }]}
                                ><Input /></Form.Item>
                            </Tooltip>

                        )
                    })
                }
                <Divider orientation="left">Output:</Divider>
                {
                    <Tooltip title={props.data.output[0].description}>
                        {/* <Form.Item
                                name={props.data.output[0].name}
                                label={props.data.output[0].name}
                                > */}
                        <div style={{ display: props.displayVisual, border: '1px solid gray;', borderRadius: '5px', width: '100%' }}>
                            <iframe src={props.visualFileUrl} style={{ width: '100%' }}>

                            </iframe>
                        </div>
                        {/* </Form.Item> */}
                    </Tooltip>
                }
                <div style={{ width: '100%', textAlign: 'center' }}>
                    <Button type="primary" htmlType="submit">
                        运行模型
                    </Button>
                    <Button type="primary" style={{ display: props.displayVisual }} >
                        结果入库
                    </Button>
                    <Button type="primary" style={{ display: props.displayVisual }} onClick={props.downloadFile}>
                        下载结果
                    </Button>
                    <Button id='resetImgModal' htmlType="button" onClick={onReset}>
                        重置
                    </Button>
                </div>


            </Form>
        </Modal>
    )
}

class Tool extends React.Component {
    constructor(props) {
        super(props);
        let DATASERVER_WINDOW = '0QItKcP0/Qg7e688yB/imQWOztNpZAlf1+JlR4DeQsE='
        let DATASERVER_LINUX = ''
        let token = this.props.isWindow ? DATASERVER_WINDOW : DATASERVER_LINUX;
        this.state = {
            type: this.props.type,

            nowItem: JSON.parse(JSON.stringify(
                {
                    "code": 0,
                    "capability": {
                        "msg": "capability",
                        "data": {
                            "name": "图片水印",
                            "date": "2021-03-23 11:28",
                            "description": "输入两幅图片，小图片作为图片水印添加到大图片右上角",
                            "dataTemplateOid": "0500b6b6-7d6d-4c5c-9102-1caae76c05e2",
                            "paramsCount": "0",
                            "metaDetail": {
                                "description": "\r\n        Add an image watermark to the target image\r\n    ",
                                "dependency": [{
                                    "name": "PIL",
                                    "url": ""
                                }, {
                                    "name": "os",
                                    "url": ""
                                }, {
                                    "name": "sys",
                                    "url": ""
                                }],
                                "input": [{
                                    "name": "test.png",
                                    "type": "path",
                                    "description": "original png or jpg file"
                                }, {
                                    "name": "test2.png",
                                    "type": "path",
                                    "description": "watermark png or jpg file"
                                }],
                                "output": [{
                                    "name": "res.png",
                                    "type": "path",
                                    "description": "result, an png or jpg file"
                                }],
                                "parameter": []
                            },
                            "authorship": "1344591831@qq.com"
                        }
                    }
                }
            )),
            items: [],
            baseUrl: 'http://111.229.14.128:8898',
            token: token,
            serviceUrl: 'http://111.229.14.128:8898/onlineNodesAllPcs?token=' + token + '&type=Processing',
            loadService: 'http://111.229.14.128:8898/capability?token=' + encodeURIComponent(token) + '&type=Processing&id=',
            uploadDataUrl: 'http://221.226.60.2:8082/data',
            runSeriveUrl: 'http://111.229.14.128:8898/invokeUrlsDataPcsWithKey',
            pcsId: '',
            seviceSuccess: false,

            visible: false,
            testData: '',

            resDataURL: '',
            visualFileUrl: '',
            displayVisual: 'none',
            uploadFiles: [],
            uploadFilesUrl: [],
            parameters: [],
            modelIsRunned: false
        }

        this.handleOk = this.handleOk.bind(this)
        this.confirmLoading = this.confirmLoading.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
        this.xml2json = this.xml2json.bind(this)
        this.xmlStr2XmlObj = this.xmlStr2XmlObj.bind(this)
        this.xmlStr2json = this.xmlStr2json.bind(this)
        this.loadNowMethod = this.loadNowMethod.bind(this)
        this.initDataServer = this.initDataServer.bind(this)
        this.beforeUploadInput = this.beforeUploadInput.bind(this)
        this.storeData = this.storeData.bind(this)
        this.storeUploadFile = this.storeUploadFile.bind(this)

        this.onFinish = this.onFinish.bind(this)
        this.visualResult = this.visualResult.bind(this)
        this.downloadFile = this.downloadFile.bind(this)
    }

    componentDidMount() {
        let that = this
        try {
            axios.get(this.state.baseUrl + '/state?token=' + encodeURIComponent(this.state.token)).then(res => {
                if (res.status != 200 || res.data.code && res.data.code === -1) {
                    message.error('tool server isn\'t running.')
                    return
                }
            })
        } catch (error) {
            message.error('tool server isn\'t running.')
            return
        }

        axios.post(this.state.baseUrl + '/findWorkSpace', {
            name: this.state.type,
            token: this.state.token
        }).then(res => {
            console.log('workSpace res: ', res)
            if (res.data.code === 0) {
                that.setState({
                    workSpace: res.data.data
                })
                axios.post(that.state.baseUrl + '/queryList', {
                    token: that.state.token,
                    parentLevel: "-1",
                    type: "Processing",
                    name: "",
                    listType: 'Processing',
                    workSpace: res.data.data.uid
                }).then((res) => {
                    console.log('query collectoin res: ', res)
                    if (res.data.code === 0) {
                        that.setState({
                            items: res.data.data[0]
                        })
                    }
                })
            } else {
                message.error('find tools\' workspace error')
            }
        })
    }

    xmlStr2XmlObj(xmlStr) {
        var xmlObj = {};
        xmlObj = new DOMParser().parseFromString(xmlStr, "text/xml");
        return xmlObj;
    }

    xmlStr2json(xml) {
        var xmlObj = this.xmlStr2XmlObj(xml);
        var jsonObj = {};
        if (xmlObj.childNodes.length > 0) {
            jsonObj = this.xml2json(xmlObj);
        }
        return jsonObj;
    }

    xml2json(xml) {
        try {
            var obj = {};
            if (xml.children.length > 0) {
                for (var i = 0; i < xml.children.length; i++) {
                    var item = xml.children.item(i);
                    var nodeName = item.nodeName;
                    if (typeof (obj[nodeName]) == "undefined") {
                        obj[nodeName] = this.xml2json(item);
                    } else {
                        if (typeof (obj[nodeName].push) == "undefined") {
                            var old = obj[nodeName];
                            obj[nodeName] = [];
                            obj[nodeName].push(old);
                        }
                        obj[nodeName].push(this.xml2json(item));
                    }
                }
            } else {
                obj = xml.textContent;
            }
            return obj;
        } catch (e) {
            message.info(e.message);
        }
    }

    loadTestServices() {
        // if(this.state.items instanceof Array) {
        //     return this.state.items.map(item => {
        //         return(
        //             <div onClick={() => this.loadNowMethod(item)}>
        //                 <ToolItem name = {item.name} description={item.desc} />
        //             </div>
        //         )
        //       })
        // }

        // return(
        // <List
        //     itemLayout="horizontal"
        //     dataSource={this.state.items}
        //     renderItem={item => (
        //         <List.Item style={{width: '100%'}}>
        //           <div onClick={() => this.loadNowMethod(item)} style={{width: '100%'}}>
        //           <List.Item.Meta
        //             avatar={<Avatar src='./modelIcon.jpg' />}
        //             title={<a>{item.name}</a>}
        //             description={item.desc}
        //           />
        //           </div>
        //         </List.Item>
        //     )}
        // />)

        return (
            <div className='demo-infinite-container'>
                {/* <InfiniteScroll> */}
                <List
                    dataSource={this.state.items}
                    itemLayout='horizontal'
                    renderItem={item => (
                        <List.Item style={{ width: '100%' }} onClick={() => this.loadNowMethod(item)} styel={{ width: '100%' }}>
                            <List.Item.Meta
                                avatar={<Avatar src="https://img2.baidu.com/it/u=2652576963,4108228020&fm=26&fmt=auto&gp=0.jpg" />}
                                title={<a>{item.name}</a>}
                                description={item.desc} />
                        </List.Item>
                    )}
                />
                {/* </InfiniteScroll> */}
            </div>
        )
    }

    // 数据容器的相关接口
    loadNowMethod(item) {
        let that = this
        that.setState({
            pcsId: item.id
        })
        try {
            request.get(this.state.loadService + item.id).then(res => {
                if (res.data.code === 0) {
                    that.setState({
                        nowItem: res.data,
                        visible: true,
                    })
                    document.getElementById('resetImgModal').click()
                }
            })

        } catch (error) {
            message.error('该服务读取失败: ')
        }
    }

    handleCancel() {
        this.setState({
            visible: false
        })
        this.initDataServer()
    }

    handleOk() {
        this.initDataServer()
    }

    confirmLoading() {
        console.log('read')
    }

    loadData() {
        console.log('load')
    }

    initDataServer() {
        let resDataURL = ''
        let displayVisual = 'none'
        let uploadFiles = []
        let uploadFilesUrl = []
        let parameters = []
        this.setState({
            resDataURL: resDataURL,
            displayVisual: displayVisual,
            uploadFiles: uploadFiles,
            uploadFilesUrl: uploadFilesUrl,
            parameters: parameters,
            parameters: ParametricBufferGeometry
        })
    }

    beforeUploadInput(file) {
        this.state.uploadFiles.push(file)
        this.setState({
            modelIsRunned: false
        })
        return true
    }

    async storeData(file) {
        let formData = new FormData();
        formData.append("name", file.name.split('.')[0])
        formData.append("userId", '1')
        formData.append("serverNode", 'china')
        formData.append('origination', 'developer')
        formData.append("datafile", file)
        let that = this;
        try {
            await axios({
                method: 'POST',
                url: that.state.uploadDataUrl,
                data: formData,
                headers: { 'Content-Type': 'multipart/form-data; boundary=<calculated when request is sent>' }
            }).then(res => {
                if (res.data.code === 1) {
                    that.state.uploadFilesUrl.push(res.data.data.id)
                }
            })
        } catch (error) {
            message.error('上传图片失败: ', error)
        }
    }

    async storeUploadFile() {
        for (let i = 0; i < this.state.uploadFiles.length; ++i) {
            await this.storeData(this.state.uploadFiles[i])
        }
    }

    async onFinish(values) {
        if (this.state.modelIsRunned) {
            message.info('模型已经运行，请进行其它操作')
            return
        }
        await this.storeUploadFile();
        let urls = {}
        let data = this.state.nowItem.capability.data.metaDetail
        let input = data.input
        let parameter = data.parameter
        let valuesData = Object.values(values)
        for (let i = 0; i < valuesData.length; ++i) {
            if (typeof valuesData[i] === 'string') {
                this.state.parameters.push(valuesData[i])
            }
        }

        if (this.state.uploadFilesUrl.length < input.length) {
            message.info('input 数据尚未上传完')
            return
        }

        if (this.state.parameters.length < parameter.length) {
            message.info('parameter 数据尚未上传完')
            return
        }

        let uploadFileUrlLength = this.state.uploadFilesUrl.length, inputLength = input.length

        for (let i = 0; i < input.length; ++i) {
            urls[input[i].name] = 'http://221.226.60.2:8082/data/' + this.state.uploadFilesUrl[uploadFileUrlLength - input.length + i]
        }
        let config = {
            'token': this.state.token,
            'pcsId': this.state.pcsId,
            'urls': urls,
            'params': this.state.parameters
        }
        let that = this
        try {
            axios({
                method: 'POST',
                url: this.state.runSeriveUrl,
                data: config,
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then(res => {
                // 调用完返回的结构是uid,也就是结果网址
                if (res.data.code === 0) {
                    let resDataURL = Object.values(res.data.urls)[0];
                    that.visualResult(resDataURL)
                    message.success('服务调用成功!')
                } else {
                    message.info('服务调用失败')
                }
            })
        } catch (error) {
            message.info('服务调用失败: ', error)
        }
    }

    visualResult(resDataURL) {
        let type = this.state.type, res
        if (type === 'Text' || type === 'Sound') {
            res = resDataURL + '?type=html'
        } else {
            res = resDataURL + '?type=png'
        }
        this.setState({
            resDataURL: resDataURL,
            displayVisual: '',
            visualFileUrl: res,
            modelIsRunned: true,
        })
    }

    downloadFile() {
        if (this.state.resDataURL != '') {
            window.location.href = this.state.resDataURL
            message.info('开始下载输出结果')
        } else {
            message.info('没有结果，请先运行该服务！')
        }
    }

    render() {
        return (
            <div className='tool'>
                {
                    this.loadTestServices()
                }
                <ImgModal visible={this.state.visible} displayVisual={this.state.displayVisual} visualFileUrl={this.state.visualFileUrl} item={this.state.nowItem.capability} data={this.state.nowItem.capability.data.metaDetail} initDataServer={this.initDataServer} handleOk={this.handleOk} handleCancel={this.handleCancel} onFinish={this.onFinish} beforeUploadInput={this.beforeUploadInput} downloadFile={this.downloadFile} />
            </div>
        )
    }
}

export default Tool;