import React, { PureComponent } from "react";
import axios from "axios";
import { Table, message, Pagination } from "antd";

/*
* 这个react组件不会自动更新，只有调用search或clearTable方法时才会更新
*
* @param cols {Array} 表格列
* @param url {String} 查询路径
* @param queryCon {Object} 查询条件
* @param pageSize {Number} 分页时，每页显示的数据条数
* @param selectRow {Function} 选中行后的回调
* @param clickRow {Function} 点击行后的回调
*/

message.config({ "top": 300 });
class TableRendering extends PureComponent{
    constructor(props){
        super(props);
        this.state = {
            url: this.props.url,
            cols: this.props.cols,
            queryCon: this.props.queryCon,
            tableLoading: Boolean(this.props.url),                       //表格是否处于加载中
            tableData: null,                                             //表格数据
            total: 0,                                                    //表格数据总条目数
            curPage: 1,                                                  //当前页数
            pageSize: this.props.pageSize || 10                          //每页展示条数
        };

        this.renderTable = this.renderTable.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
        this.search = this.search.bind(this);
    }

    componentDidMount(){
        if(!this.state.url){
            return;
        }

        getTableData(`${this.state.url}/1/${this.state.pageSize}`, this.state.queryCon, this.renderTable);
    }

    render() {
        let { selectRow, clickRow } = this.props;
        let { cols, tableData, tableLoading, curPage, total, pageSize } = this.state;
        let style = {
            "margin": "16px 0",
            "float": "right"
        };
        return (
            <div>
                <Pagination
                    style={style}
                    current={curPage}
                    total={total}
                    showQuickJumper={true}
                    showSizeChanger={false}
                    size="small"
                    pageSize={pageSize}
                    onChange={this.onPageChange}
                />
                <Table
                    rowKey="_id"
                    dataSource={tableData}
                    columns={cols}
                    bordered={true}
                    loading={tableLoading}
                    pagination={{
                        size: "small",
                        current: curPage,
                        total: total,
                        pageSize: pageSize,
                        showQuickJumper: true,
                        showSizeChanger: false,
                        onChange: this.onPageChange
                    }}
                    rowSelection={
                        selectRow ? {
                            "type": "radio",
                            "preserveSelectedRowKeys": false,
                            "onSelect": selectRow
                        } : null
                    }
                    onRow={record => {
                        return {
                            onClick: () => typeof clickRow === "function" && clickRow(record)
                        };
                    }}
                />
            </div>
        );
    }

    onPageChange(page, pageSize){
        this.setState({
            "curPage": page,
            "tableLoading": true
        });
        let url = `${this.state.url}/${page}/${pageSize}`;
        getTableData(url, this.state.queryCon, this.renderTable);
    }

    renderTable(err, response){
        if(err){
            getTbDataError();
            this.setState({ "tableLoading": false, "tableData": null, "total": 0 });
            console.log(err);
            return;
        }

        if(response.data.code === 0){
            getTbDataError();
            this.setState({ "tableLoading": false, "tableData": null, "total": 0 });
            console.log(response.data.msg);
            return;
        }

        let data = response.data;
        this.setState({
            "tableData": data.data.textData,
            "tableLoading": false,
            "total": data.data.count
        });
    }

    search(url, cols, queryCon, pageSize){
        this.setState(prevState => {
            return {
                url: url,
                queryCon: queryCon,
                tableLoading: true,
                pageSize: pageSize || prevState.pageSize
            };
        });

        getTableData(`${url}/1/${pageSize || this.state.pageSize}`, queryCon, (err, response) => {
            if(err){
                getTbDataError();
                this.setState({ "tableLoading": false, "tableData": null, "total": 0 });
                console.log(err);
                return;
            }

            if(response.data.code === 0){
                getTbDataError();
                this.setState({ "tableLoading": false, "tableData": null, "total": 0 });
                console.log(response.data.msg);
                return;
            }

            let data = response.data;
            this.setState({
                "cols": cols,
                "tableData": data.data.textData,
                "tableLoading": false,
                "curPage": 1,
                "total": data.data.count
            });
        });
    }

    clearTable(cols){
        this.setState(prevState => {
            return {
                url: "",
                cols: cols ? cols : prevState.cols,
                queryCon: null,
                tableData: null,
                total: 0,
                curPage: 1
            };
        });
    }
}

function getTableData(url, queryCon, callback){
    axios.get(url, { params: queryCon ? queryCon : {} }).then(res => {
        callback(null, res);
    }).catch(err => {
        callback(err);
    })
}

//加载表格数据失败
function getTbDataError(){
    message.error({
        "content": "加载数据时出错",
    });
}

export default TableRendering;
