import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Echarts from 'echarts-for-react'
import 'echarts-gl'

export default class BaseChart extends PureComponent {
  // 进行类型检查和是否必需
  static propTypes = {
    option: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    getOption: PropTypes.func.isRequired,
    style: PropTypes.object
  };

  static defaultProps = {
    style: {}
  };

  componentDidMount() {
    const { runAction } = this.props;
    if (this.chartRef && runAction) {
      const chartIns = this.chartRef.getEchartsInstance();
      window.setTimeout(() => {
        runAction(chartIns);
      }, 300);
    }
  }

  render() {
    const { option, data, getOption, style } = this.props;

    const finalOption = getOption(option, data);
    const finalStyle = getStyle(style);

    return (
      <Echarts
        ref={ref => {
          this.chartRef = ref;
        }}
        style={finalStyle}
        option={finalOption}
        notMerge={true}
        lazyUpdate={true}
      />
    );
  }
}


// 添加统一的style样式
const getStyle = (style) => {
  return Object.assign(
    {
      position: 'relative'
    },
    style
  );
}