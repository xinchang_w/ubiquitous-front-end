/*eslint-disable */
// 将输入的option和data，结合默认option转换为完整的option
function seriesCreator(series) {
  return series.map(e => ({
    type: 'line',
    symbol: 'circle',
    smooth: true,
    lineStyle: {
      normal: {
        width: 3,
      },
    },
    ...e,
  }));
}

export default function(option, data) {
  // 初始化时使用赋值语句（如series）给定没有的默认值
  // 使用...可以取出剩余部分
  const { tooltip, xAxis, yAxis, yCategory, series = [], ...rest } = data;
  return {
    ...option,
    xAxis: {
      ...option.xAxis,
      ...xAxis,
    },
    tooltip: {
      ...option.tooltip,
      ...tooltip,
    },
    yAxis: {
      ...option.yAxis,
      ...yAxis,
      data: yCategory || [],
    },
    series: seriesCreator(series),
    ...rest,
  };
}
