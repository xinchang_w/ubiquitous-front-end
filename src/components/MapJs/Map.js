import {Button, Modal, Form, List, Input,Space, Divider, Upload, message, Table, Tabs, Popconfirm,Select, Row, Col} from 'antd'
import {  UploadOutlined  } from '@ant-design/icons'
import axios from 'axios'
import React from 'react'
import Map from 'ol/Map';
import View from 'ol/View'
import * as olProj from 'ol/proj';
import Source from 'ol/source/Source';
import Layer from 'ol/layer/Layer';
import extent from 'ol/extent'
import Interaction from 'ol/interaction/Interaction'
import Point from 'ol/geom/Point';
import Polygon, {circular, fromExtent, fromCircle} from 'ol/geom/Polygon';
import Feature from 'ol/Feature'
import Static from 'ol/source/ImageStatic';
import ImageLayer from 'ol/layer/Image';    
import {getCenter} from 'ol/extent';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Style from 'ol/style/Style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Draw from 'ol/interaction/Draw';
import {
    createBox,
    createRegularPolygon,
  } from 'ol/interaction/Draw';
  
import 'ol/ol.css'
import TypeTag from './TypeTag'
import { BASE_PATH } from '@/assets/global';
// import OSM from 'ol/source/OSM';
// // import proj4 from 'proj4';
// import {Tile as TileLayer} from 'ol/layer';
// import {register} from 'ol/proj/proj4';
// import {transform} from 'ol/proj';


const { TabPane } = Tabs;
const { TextArea } = Input;
const {Option} = Select

function StructureTag(props) {
    const layout = {
        labelCol: {
          span: 8,
        },
        wrapperCol: {
          span: 16,
        },
      };
      const tailLayout = {
        wrapperCol: {
          offset: 8,
          span: 16,
        },
      };
    
      const [form] = Form.useForm();
      
      const onFinish = (values) => {
        if(props.type != 'Box') {
            message.info('结构标签只能存储矩形标签,请重画！')
            return
        }
        values.metaId = props.metaId
        values.metaSource = "ULA_Meta_RasterMap"
        let start = props.coords[0];
        let end = props.coords[1];
        values.height = parseInt(start[1] - end[1])
        values.width = parseInt(end[0] - start[0])
        values.top = props.imageHeight - parseInt(start[1])
        values.left = parseInt(start[0])
        values.element = document.getElementById('textarea1').value
        console.log(values)
        try {
            axios({
                url: BASE_PATH + '/GenericMap' + props.structureTagUrl,
                method: 'POST',
                data: values,
                headers: { 'Content-Type': 'application/json;charset=UTF-8'}
            }).then(res =>{
                if(res.data.code === 0) {
                    message.success('提交成功')
                }
            })
          } catch (error) {
              message.error('提交失败')
          }
        props.getTagWithImageId()
      }

      const onReset = () => {
        form.resetFields();
      };

    return (
        <Form {...layout} form={form} onFinish={onFinish} encType="multipart/form-data">
        <Form.Item 
            name='tagType'
            label='标签类型'
            rules={[{required: true}]}
            ><Input placeholder='eg: 注记'/>
        </Form.Item>
        <Form.Item 
            name='tagValue'
            label='特征'
            ><Input placeholder='eg: 14时05分'/>
        </Form.Item>
        <Form.Item 
            name='element'
            label='特征构成(json)'
            > <TextArea id='textarea1' placeholder="eg：
            {
            ID:7,
            Symbol:{
                    Type:shape,
                Topleft: [22,45],
                Height:20,
                Width:40
            }
            Name:'7~8级大风'
            }" />
        </Form.Item>
        <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
            Submit
            </Button>
            <Button htmlType="button" onClick={onReset}>
            Reset
            </Button>
        </Form.Item>
    </Form>
    );
}
function ContentTag(props) {
    const layout = {
        labelCol: {
          span: 8,
        },
        wrapperCol: {
          span: 16,
        },
      };
      const tailLayout = {
        wrapperCol: {
          offset: 8,
          span: 16,
        },
      };
    
      const [form] = Form.useForm();
      
      const onFinish = (values) => {
          values.metaId = props.metaId;
          values.metaSource = 'ULA_Meta_RasterMap'
          console.log('coords: ',props.coords)
          console.log('type: ',props.type)
          if(props.type === 'Box') {
            let start = props.coords[0];
            let end = props.coords[1];
            let height = parseInt(start[1] - end[1])
            let width = parseInt(end[0] - start[0])
            values.patch = {
                Bbox: [start[0], props.imageHeight - start[1], start[0]+width, props.imageHeight - start[1], end[0], props.imageHeight - end[1], start[0], props.imageHeight - (start[1]-height)]
            } 
          } else {
              let temp = []
              let dataArray = props.coords[0]
              for(let i=0;i<dataArray.length;++i) {
                  temp.push(dataArray[i][0])
                  temp.push(props.imageHeight - dataArray[i][1])
              }
              values.patch = {
                Bbox: temp
            } 
          }

          values.geography = document.getElementById('textarea2').value
          console.log('Bbox: ', values.patch.Bbox)
          console.log('values:' ,values)
          try {
            axios({
                url: BASE_PATH + '/GenericMap' + props.contentTagUrl,
                method: 'POST',
                data: values,
                headers: { 'Content-Type': 'application/json;charset=UTF-8'}
            }).then(res =>{
                if(res.data.code === 0) {
                    message.success('提交成功')
                }
            })
          } catch (error) {
              message.error('提交失败')
          }
          props.getTagWithImageId();
          props.cancleModal()
      }

      const onReset = () => {
        form.resetFields();
      };

    return (
        <Form {...layout} form={form} onFinish={onFinish} encType="multipart/form-data">
        <Form.Item 
            name='tagType'
            label='内容媒体类型'
            rules={[{required: true}]}
            ><Input placeholder='eg: map, chart'/>
        </Form.Item>
        <Form.Item 
            name='tagName'
            label='内容数据类型'
            ><Input placeholder='eg: 专题要素'/>
        </Form.Item>
        <Form.Item 
            name='legend'
            label='图例结构标签标识'
            ><Input placeholder='eg: 大雨'/>
        </Form.Item>
        <Form.Item 
            name='symbol'
            label='图例符号标识'
            ><Input placeholder='eg: 5'/>
        </Form.Item>
        <Form.Item 
            name='geography'
            label='内容标签地理实体'
            > <TextArea id='textarea2' placeholder="eg：
            {
                Type: Point,
                coordinates: [
                    [long, lat]
               ] 
            }
          " />
        </Form.Item>
        <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
            Submit
            </Button>
            <Button htmlType="button" onClick={onReset}>
            Reset
            </Button>
        </Form.Item>
    </Form>
    );
}
function AddImageList(props){
  let imageFile = {};
  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };

  const [form] = Form.useForm();
  
  const onFinish = (values) => {
    let formData =  new FormData();
    formData.append("file", imageFile)
    formData.append("source", values.source)
    formData.append("description", values.description)
    formData.append(props.dataType + "Name", values.mapName)
    try {
        axios({
            url: BASE_PATH + '/GenericMap' + props.metaUrl,
            method: 'POST',
            data: formData,
            headers: { 'Content-Type': 'multipart/form-data', }
        }).then(res =>{
            console.log(res)
            if(res.data.code === 0) {
                message.success('提交成功')
                let item = res.data.data;
                props.getRasterChart(item.imagePath, item.id, item.width, item.height, item);
            }
        })
    } catch (error) {
        message.error('提交失败')
    }
    props.getTagWithImageId()

  };

  const onReset = () => {
    form.resetFields();
  };

  function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('仅支持 JPG/PNG 格式的' + props.dataName + '!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error(props.dataName + '大小不能超过 2MB!');
    }
    if(isJpgOrPng && isLt2M){
        imageFile = file;
        return true
    } else {
        message.error('上传失败')
        return false
    }
  }
    return (
        <Form {...layout} form={form} onFinish={onFinish} encType="multipart/form-data">
        <Form.Item 
            name='mapName'
            label='名字'
            rules={[{required: true}]}
            ><Input />
        </Form.Item>
        <Form.Item 
            name='source'
            label='来源'
            rules={[{required: true}]}
            ><Input />
        </Form.Item>
        <Form.Item 
            name='description'
            label='描述'
         ><Input />
        </Form.Item>
        <Form.Item 
            label='文件(1)：'
            // rules={[{required: true}]}
            >
            <>
              <Upload 
                name='file'
                beforeUpload={beforeUpload}
                // afterRead={afterRead}
                onRemove={file => {
                    imageFile = {}
                    file = {}
                }}
              >
                <Button icon={<UploadOutlined />}>Select File</Button>
              </Upload>
            </>
        </Form.Item>
        <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
            Submit
            </Button>
            <Button htmlType="button" onClick={onReset}>
            Reset
            </Button>
        </Form.Item>
    </Form>
    );
}
function AddImageModal (props) {
    const {Search} = Input

    const handleDelete = (id) => {
    try {
      axios({
          url: BASE_PATH + '/GenericMap' + props.metaUrl + '/' + id,
          method: 'DELETE',
      }).then(res => {
          if(res.data.code === 0){
              message.info('删除成功')
          }else{
              message.info('删除失败')
          }
      })
    } catch (error) {
        message.error('删除失败: ' + error)
    }
    }

    const columns = [
        {
          title: '名字',
          dataIndex: props.dataType + 'Name',
          key: 'mapName',
          align:'center'          
        },
        {
          title: '操作',
          dataIndex: 'Action',
          key: 'Action',
          align:'center',
          render: (_, record) => {
            return(
             <div className='centerClass'>
              <Button className='centerClass' type="primary" onClick={() => {props.getRasterChart(record.imagePath, record.id, record.width, record.height, record)}}>
                加载
              </Button>
              <Popconfirm title="确认删除吗?" onConfirm={() => handleDelete(record.id)}>
              <Button>删除</Button>
                </Popconfirm>
             </div>

            );
        },
        },
      ];

      return(
          <Modal
            className='imageModal'
            title='添加已有数据'
            visible={props.visible}
            onOk={props.handleOk}
            onCancel={props.handleCancel}>
                <Space direction="vertical" id='searchImg'>
                    <Search
                    placeholder="input search text"
                    allowClear
                    enterButton="Search"
                    size="large"
                    onSearch={props.searchImage}
                    />
                </Space>
                <Divider />
                    <Table dataSource= {props.data} columns={columns}
                    pagination= {{
                        pageSize: 5,
                    }}
                    />;
            </Modal>
      )
}
function AddNewImageModal (props) {

      return(
          <Modal
            title='添加新数据'
            visible={props.visible}
            onOk={props.handleOk}
            onCancel={props.handleCancel}>
                <AddImageList getRasterChart = {props.getRasterChart} metaUrl = {props.metaUrl} dataType={props.dataType}/>
        </Modal>
      )
}
function AddTagModal(props) {
    let tagClassName = props.tagClassName;
    
    const layout = {
        labelCol: {
          span: 8,
        },
        wrapperCol: {
          span: 16,
        },
      };
      const tailLayout = {
        wrapperCol: {
          offset: 8,
          span: 16,
        },
      };

      const [form] = Form.useForm();
      
      const onFinish = (values) => {
        console.log(values);
      };
      const handleChange = (value) => {
        if(value === 'type') {
            message.info('类型标签只有全图标签')
        }
        if(props.type != 'Box' && value === 'structure') {
            message.info('结构标签只有矩形标签')
        }
        tagClassName = value
      };

      return(
          <Modal
            title='添加标签'
            visible={props.visible}
            onOk={props.handleOk}
            onCancel={props.handleCancel}>
                <div className="card-container">
                  <Tabs type="card">
                    <TabPane tab="添加类型标签" key="type" >
                        <TypeTag metaId={props.metaId} getTagWithImageId={props.getTagWithImageId} cancleModal = {props.handleCancel} typeTagUrl={props.typeTagUrl}/>
                    </TabPane>
                    <TabPane tab="添加结构标签" key="structure">
                    <StructureTag metaId={props.metaId} coords = {props.coords} getTagWithImageId={props.getTagWithImageId} cancleModal = {props.handleCancel} imageHeight = {props.imageHeight} type = {props.type} structureTagUrl={props.structureTagUrl}/>
                    </TabPane>
                    <TabPane tab="添加内容标签" key="content">
                      <ContentTag metaId={props.metaId} coords = {props.coords} getTagWithImageId={props.getTagWithImageId} cancleModal = {props.handleCancel} imageHeight = {props.imageHeight}
                      type = {props.type} contentTagUrl={props.contentTagUrl}/>
                    </TabPane>
                  </Tabs>
                </div>
            </Modal>
      )
}

/**
 * param dataType 四种类型中的一种：chart、map、image、video
 */
class MapDiv extends React.Component{
    constructor(props) {
        super(props);
        let dataType = this.props.dataType
        let elementUrl, metaUrl, structureTagUrl, contentTagUrl, typeTagUrl, dataName
        // 因为后台接口不规范，只能这么写了
        if(dataType === 'chart') {
            dataName = '图表'
            elementUrl = '/chartElement'
            metaUrl = '/chartMeta'
            structureTagUrl = '/chartStructure'
            contentTagUrl = '/chatContent'
            typeTagUrl = '/chartType'
        } else if (dataType === 'map') {
            dataName = '地图'
            elementUrl = '/rasterMapElement'
            metaUrl = '/rasterMapMeta'
            structureTagUrl = '/rasterMapStructureTag'
            contentTagUrl = '/rasterMapContentTag'
            typeTagUrl = '/rasterMapTypeTag'
        } else if (dataType === 'image') {
            dataName = '图片'
            elementUrl = '/imageElement'
            metaUrl = '/imgMeta'
            structureTagUrl = '/structureTagImg'
            contentTagUrl = '/contentTagImg'
            typeTagUrl = '/typeTagImage'
        } else if (dataType === 'video') {
            dataName = '视频'
            elementUrl = '/videoElement'
            metaUrl = '/videoMeta'
            structureTagUrl = '/videoStructure'
            contentTagUrl = '/videoContent'
            typeTagUrl = '/videoType'
        } else {
            console.log('dataType fot map\.js is wrong.')
            return
        }
        this.state = {
            dataType: dataType,
            dataName: dataName,
            elementUrl: elementUrl,
            metaUrl: metaUrl,
            structureTagUrl: structureTagUrl,
            contentTagUrl: contentTagUrl,
            typeTagUrl: typeTagUrl,

            tagClassName: 'type',
            imgDataUrl: BASE_PATH + '/GenericMap' + metaUrl + '/all/1/500',            
            imageData:[],
            nowImageData:JSON.parse(JSON.stringify({
                "oid": "601c001c50db401f8c9cc4c9",
                "id": "96b2c466-c2a1-11e9-8edb-9801a7af21cb",
                "crawler": "",
                "description": "081020-24-预报",
                "source": "http://e.weather.com.cn/mtqzt/3225013.shtml",
                "recordDatetime": "2019-08-20 00:51:28",
                "mapName": "081020-24-预报",
                "imagePath": "/Lekima/强降水/081020-24-预报.jpg",
                "height": "450",
                "width": "554",
                "channel": "RGB",
                "format": "jpg"
            })),
            imageMetaId:'',
            imageModalVisible: false,
            newImageModalVisible: false,

            canvasHasMap: false,
            map:'',
            nowImage:'',

            draw:'',
            type:'None',
            // drawStyle: new Style.Style({
            //     fill: new Fill({
            //         color: 'rgba(0, 255, 0, 0.3)'
            //     }),
            //     stroke: new Style.Stroke({
            //         color: 'green',
            //         width: 1
            //     }),
            // }),
            drawSource: '',
            drawLayer: '',
            interAction:'',
            coordinates:[],

            typeTagModalVisible:false,

            typeTag:[],
            structureTag:[],
            contentTag:[],

            typeColumns: [
                { title: '标签', dataIndex: 'typeTag', key: 'typeTag' , align:'center'},
                { title: '类型数量', dataIndex: 'typeNum', key: 'typeNum', align:'center' },
                {
                  title: '操作',
                  dataIndex: '',
                  key: 'x',
                  render: (_, record) => {
                      return(
                        <Popconfirm title="确认删除吗?" onConfirm={() => this.deleteTag(record.key)}>
                        <Button>删除</Button>
                        </Popconfirm>
                      );
                  },
                  align:'center'
                },
              ],
            
            typeData : [],

            structureColumns: [
                { title: '结构标签类型', dataIndex: 'tagType', key: 'tagType' , align:'center'},
                { title: '特征', dataIndex: 'tagValue', key: 'tagValue', align:'center' },
                {
                  title: '操作',
                  dataIndex: '',
                  key: 'x',
                  render: (_, record) => {
                    return(
                        <Popconfirm title="确认删除吗?" onConfirm={() => this.deleteTag(record.key)}>
                        <Button>删除</Button>
                        </Popconfirm>
                      );
                  }
                  , align:'center'
                },
              ],
            
            structureData : [],

            contentColumns: [
                { title: '内容媒体类型', dataIndex: 'tagType', key: 'tagType' , align:'center'},
                { title: '内容数据类型', dataIndex: 'tagName', key: 'tagName', align:'center' },
                {
                  title: '操作',
                  dataIndex: '',
                  key: 'x',
                  render: (_, record) => {
                    return(
                        <Popconfirm title="确认删除吗?" onConfirm={() => this.deleteTag(record.key)}>
                        <Button>删除</Button>
                        </Popconfirm>
                      );
                  }
                  , align:'center'
                },
              ],
            
            contentData : [],

            activeIndex: '',

            graph: '',
        }

        this.handleCancel = this.handleCancel.bind(this)
        this.handleNewCancel = this.handleNewCancel.bind(this)
        this.handleOk = this.handleOk.bind(this)
        this.getRasterChart = this.getRasterChart.bind(this)
        this.getTag = this.getTag.bind(this)
        this.getTagWithId = this.getTagWithId.bind(this)
        this.getTagWithImageId = this.getTagWithImageId.bind(this)
        this.addRect = this.addRect.bind(this)
        this.showRect = this.showRect.bind(this)
        this.handleTypeTagCancel = this.handleTypeTagCancel.bind(this)
        this.showTypeTagModal = this.showTypeTagModal.bind(this)
        this.searchImage = this.searchImage.bind(this)
        this.deleteTag = this.deleteTag.bind(this)
        this.updateDataSource = this.updateDataSource.bind(this)
    }

    componentDidMount(){
        console.log('imageDataUrl: ', this.state.imgDataUrl)
        axios.get(this.state.imgDataUrl).then((res)=>{
            if(res.data.code === 0){
                console.log('imageDataUrl res: ', res)
                let imageMaps
                if(this.state.dataType === 'map')
                    imageMaps = res.data.data.rasterMapData;
                else if(this.state.dataType === 'chart')
                    imageMaps = res.data.data.rasterChartData
                else if(this.state.dataType === 'image') 
                    imageMaps = res.data.data.textData
                let result = []
                for (var i = 0; i < imageMaps.length; i++) {
                    // 有四张图片不可以
                    if(this.state.dataType === 'map' && JSON.stringify(imageMaps[i].imagePath).search('080820')!= -1 || JSON.stringify(imageMaps[i].imagePath).search('081514')!= -1 || JSON.stringify(imageMaps[i].imagePath).search('081408')!= -1 || JSON.stringify(imageMaps[i].imagePath).search('081214')!= -1){
                        continue;
                    }
                    if(this.state.dataType === 'image') {
                        imageMaps[i]['imageName'] = imageMaps[i]['caption'].substring(0, 10)
                    }
                    else if(this.state.dataType === 'chart') {
                        imageMaps[i]['chartName'] = imageMaps[i]['imagePath'].substring(7)
                    }
                    result.push(JSON.parse(JSON.stringify(imageMaps[i])))
                }
                this.setState({
                    imageData: result,
                })
            }
        })
    }

    showImgModal(){
        this.setState({
            imageModalVisible: true
        })
    }

    showNewImgModal(){
        this.setState({
            newImageModalVisible: true
        })
    }

    showTypeTagModal(){
        if(this.state.map === ''){
            message.info("请先添加" + this.state.dataName + "！");
            return;
        }
        if(this.state.draw === '') {
            message.info('请先在' + this.state.dataName + '上画一个标签!');
            return;
        }
        this.setState({
            typeTagModalVisible: true
        })
    }

    handleCancel(){
        this.setState({
            imageModalVisible: false
        })
    }

    handleTypeTagCancel(){
        this.setState({
            typeTagModalVisible: false
        })
    }

    handleNewCancel(){
        this.setState({
            newImageModalVisible: false
        })
    }

    handleOk() {
        console.log('ok')
    }

    // 与ol有关的函数
    getRasterChart(path, id, width, height, item) {
        this.setState({
            nowImageData: item
        })
        let map = this.state.map;
        if(map != ''){
            map.removeLayer(map.getLayers().getArray()[0])
            map.removeLayer(map.getLayers().getArray()[0])
        }
        let extent = [0, 0, width, height];//图片图层四至
        let projection = new olProj.Projection({//定义坐标系
          code: 'xkcd-image',
          units: 'pixels',
          extent: extent
        });

        let img = new ImageLayer({
            source: new Static({
              url: BASE_PATH + '/GenericMap' + path,//地址
              projection: projection,
              imageExtent: extent
            })
          })
  
        let vectorSource = new VectorSource();
        let vectorLayer = new VectorLayer({
              source: vectorSource
          });
        this.setState({
            drawSource: vectorSource,
            drawLayer: vectorLayer
        })
        if(this.state.map === '') {
            let map = new Map({
            layers: [
                img,
                vectorLayer
            ],
            view: new View({
                projection: projection,
                center: getCenter(extent),
                zoom: 1,
                maxZoom: 8
            }),
            target: 'canvas',
            });
            this.setState({
                map: map,
            })
        } else {
            this.state.map.addLayer(img)
            this.state.map.addLayer(vectorLayer)
        }

        this.setState({
            nowImage: img,
            canvasHasMap: true,
            imageMetaId: id
        })  
        this.getTagWithId(id)           
    }

    addInteraction() {
        var value = this.state.type;
        let map  = this.state.map;
        let source = this.state.drawSource
        let geometryFunction;
        if (value !== 'None') {
          if (value === 'Square') {
            value = 'Circle';
            geometryFunction = createRegularPolygon(4);
          } else if (value === 'Box') {
            value = 'Circle';
            geometryFunction = createBox();
        //     geometryFunction = function (coordinates, geometry) {
        //         if (!geometry) {
        //             geometry = new Polygon(null);       //多边形
        //         }
        //         start = coordinates[0];
        //         end = coordinates[1];
        //         geometry.setCoordinates([
        //             [
        //                 start,
        //                 [start[0], end[1]],
        //                 end,
        //                 [end[0], start[1]],
        //                 start
        //             ]
        //         ]);
        //         return geometry;
        //   }
        //   draw = new Draw({
        //     source: source,
        //     type: value,
        //     geometryFunction: geometryFunction,
        //   });
        //   map.addInteraction(draw);
        } else if(value === 'Square') {
            value = 'Circle';
            geometryFunction = createRegularPolygon(4);

        }
        // 初始化Draw绘图控件
        let newDraw = new Draw({
            source: this.state.drawSource,
            type: value,
            geometryFunction: geometryFunction
        });
        
        newDraw.on('drawend', function (event) {
            map.removeInteraction(this);
        });
        // 将Draw绘图控件加入Map对象
        map.addInteraction(newDraw);
        this.setState({
            draw: newDraw,
        }) 
      }
    }
      
    addRect() {
        if(this.state.map === '') {
            message.info('请先添加'+ this.state.dataName + '.');
            return;
        }
        if(this.state.drawSource != ''){
            this.state.drawSource.clear()
        }
        if(this.state.type === 'None') {
            return;
        }
        this.addInteraction()
      }

      resetRect(){
        if(this.state.drawSource != ''){
            this.state.drawSource.clear()
        }
      }
    // addRect() {
    //     if(this.state.map === '') {
    //         message.info('请先添加图片');
    //         return;
    //     }
    //     if(this.state.drawSource != ''){
    //         this.state.drawSource.clear()
    //     }
    //     let type = this.state.type
    //     let geometryFunction;
    //     let map = this.state.map
    //     let start, end;
    //     switch(type){   
    //         case "Square": 
    //             type = 'Circle';
    //             // 生成规则的四边形的图形函数
    //             geometryFunction = Draw.createRegularPolygon(4);
    //             break;
    //         case 'Box':
    //             type = 'Circle';
    //             // 生成盒形状的图形函数
    //             // geometryFunction = Interaction.Draw.createBox();
    //             geometryFunction = function (coordinates, geometry) {
    //                 if (!geometry) {
    //                     geometry = new fromCircle(null);       //多边形
    //                 }
    //                 start = coordinates[0];
    //                 end = coordinates[1];
    //                 geometry.setCoordinates([
    //                     [
    //                         start,
    //                         [start[0], end[1]],
    //                         end,
    //                         [end[0], start[1]],
    //                         start
    //                     ]
    //                 ]);
    //                 return geometry;
    //             };
    //             break;
    //         default:break;
    //     }
                
    //     // 初始化Draw绘图控件
    //     let newDraw = new Draw({
    //         source: this.state.drawSource,
    //         type: type,
    //         geometryFunction: geometryFunction
    //     });
        
    //     newDraw.on('drawend', function (event) {
    //         map.removeInteraction(this);
    //     });
    //     // 将Draw绘图控件加入Map对象
    //     map.addInteraction(newDraw);
    //     this.setState({
    //         draw: newDraw,
    //     }) 
    // }
    showRect(record) {
        let coords;
        let type = '';
        let width = parseInt(this.state.nowImageData.width);
        let height = parseInt(this.state.nowImageData.height);
        try {
            if(this.state.tagClassName === 'type'){
            message.info('类型标签只有全图标签')
            coords = [[0, height], [width, height], [width, 0], [0, 0]];
        } else if( this.state.tagClassName === 'structure'){
            let data = this.state.structureTag[record.key-1];
            let left = parseInt(data.left)
            let top = parseInt(this.state.nowImageData.height) - parseInt(data.top)
            let width = parseInt(data.width)
            let height = parseInt(data.height)
            coords = [[left, top],[left + width, top],[left + width, top - height], [left, top - height]]
        } else if(this.state.tagClassName === 'content'){
            let data = this.state.contentTag[record.key-1].patch
            let Bbox = data.Bbox;
            coords = []
            for(let i=0;i<Bbox.length;) {
                coords.push([Bbox[i], height - Bbox[i+1]]);
                i = i+2;
            }

            // let data = this.state.contentTag[record.key-1].geography.geometry.coordinates
            // type = this.state.contentTag[record.key-1].geography.geometry.type
            // console.log('coords: ', data)
            // coords = data
        }
        } catch (error) {
            message.error('获取坐标失败: ' + error);
            return;
        }

        this.showData(coords, type)
    }

    showData(coords, type){
        let map = this.state.map
        let source = this.state.drawSource;
        this.clearAllRect();
        //矢量图层
        var vectorLayer = new VectorLayer({
            source: source,
            style: new Style({
                fill: new Fill({
                    color: 'rgba(0, 255, 0, 0.3)'
                }),
                stroke: new Stroke({
                    color: 'green',
                    width: 1
                }),
            })
        });

        // let polygon;

        // if(this.state.tagClassName === 'content'){
        //     if(type === 'Polygon'){
        //         polygon = new Polygon([coords])
        //     } else if(type === 'Point') {
        //         polygon = new Point([coords])
        //     }
        // } else {
            //多边形此处注意一定要是[坐标数组]
        let polygon = new Polygon([coords])
        // }

        //多边形要素类
        var feature = new Feature({
            geometry: polygon,
        });
        source.addFeature(feature);
        map.addLayer(vectorLayer);
        map.getView().setCenter(getCenter(feature.getGeometry().getExtent()))
    }

    getTagWithId(id){
        this.getTag(id)        
    }

    getTagWithImageId(){
        this.getTag(this.state.imageMetaId)
    }

    getTag(id){
        let typeTag,contentTag,structureTag;
        let path = BASE_PATH + '/GenericMap' + this.state.metaUrl + '/getTag/' + id;
        try {
            axios({
                url: path,
                method: 'GET',
            }).then(res =>{
                if(res.data.code === 0) {
                    let data = res.data.data
                    typeTag = data.typeTag;
                    contentTag = data.contentTag;
                    structureTag = data.structureTag;
                    this.initTag(typeTag, structureTag, contentTag)
                }
            })
        } catch (error) {
            message.info('获取标签失败')
        }
    }

    clearAllRect() {
        let map = this.state.map;
        let vectorLayers = [];
        for (var i = 0; i < map.getLayers().getArray().length; i++) {
            var layer = map.getLayers().getArray()[i];
            if (layer instanceof VectorLayer) {
                vectorLayers.push(layer);
            }
        }
        for (var i = 0; i < vectorLayers.length; i++) {
            vectorLayers[i].getSource().clear();
            map.removeLayer(vectorLayers[i]);
        }
    }

    initTag(typeTag, structureTag, contentTag){
        let typeData = [];
        let structureData = [];
        let contentData = [];
        if(this.state.dataType === 'map') {
            for(let i=0; i<typeTag.length; ++i) {
                let dataSource = []
                for(let j=0;j<typeTag[i].categoryList.length; ++j){
                    let key = Object.keys(typeTag[i].categoryList[j])[0];
                    let value = Object.values(typeTag[i].categoryList[j])[0];
                    dataSource.push('类型名: ' + key + '------------- 类型内容: ' + value);
                }
                typeData.push({
                    key: i+1,
                    typeTag: '标签' + (i+1),
                    typeNum:typeTag[i].categoryList.length, 
                    dataSource: dataSource
                })
            }
    
            for(let i=0; i<structureTag.length; ++i) {
                structureData.push({
                    key: i+1,
                    tagType: structureTag[i].tagType,
                    tagValue : structureTag[i].tagValue,
                    element: structureTag[i].element,
                })
            }
    
            for(let i=0; i<contentTag.length; ++i) {
                contentData.push({
                    key: i+1,
                    tagType: contentTag[i].tagType,
                    tagName : contentTag[i].tagName,
                    geography: contentTag[i].geography,
                })
            }
        }

        else if(this.state.dataType === 'image') {

        }


        this.setState({
            typeTag: typeTag,
            structureTag: structureTag,
            contentTag: contentTag,
            typeData: typeData,
            structureData: structureData,
            contentData: contentData,
        })
    }

    searchImage(value){
        if(value != ''){
            try{
            axios({
                url:BASE_PATH + '/GenericMap' + this.state.metaUrl + '/mapName/' + value + '/1/500' ,
                method:'GET',
            }).then(res => {
                if(res.data.code === 0) {
                    this.setState({
                        imageData : res.data.data.rasterMapData
                    })
                    message.info('搜索成功')
                }else {
                    message.info('搜索失败')
                }
            })
        } catch(error) {
            message.error('搜索失败: ' + error)
        }
        }
        else {
            try{
                axios({
                    url: this.state.imgDataUrl,
                    method: 'GET',
                }).then(res => {
                    if(res.data.code === 0){
                        let imageMaps = res.data.data.rasterMapData;
                        let result = []
                        for (var i = 0; i < imageMaps.length; i++) {
                            // 有四张图片不可以
                            if(JSON.stringify(imageMaps[i].imagePath).search('080820')!= -1 || JSON.stringify(imageMaps[i].imagePath).search('081514')!= -1 || JSON.stringify(imageMaps[i].imagePath).search('081408')!= -1 || JSON.stringify(imageMaps[i].imagePath).search('081214')!= -1){
                                continue;
                            }
                            result.push(JSON.parse(JSON.stringify(imageMaps[i])))
                        }
                        this.setState({
                            imageData: result,
                        })
                    }
                })
            } catch(error) {
                message.error('获取'+ this.state.dataName + '错误: ' + error )
            }
        }

    }

    deleteTag(key) {
        let tagClassName = this.state.tagClassName;
        try {
            if(tagClassName === 'type'){
                axios({
                    url: BASE_PATH + '/GenericMap' + this.state.typeTagUrl + '/' + this.state.typeTag[key-1].id,
                    method: 'DELETE',
                }).then(res => {
                    if(res.data.code === 0){
                        this.updateDataSource(key)
                    } else {
                        message.info('删除失败')
                    }
                })
            } else if(tagClassName === 'structure') {
                axios({
                    url: BASE_PATH + '/GenericMap'+ this.state.structureTagUrl + '/' + this.state.structureTag[key-1].id,
                    method: 'DELETE',
                }).then(res => {
                    if(res.data.code === 0){
                        this.updateDataSource(key)
                    } else {
                        message.info('删除失败')
                    }
                })
            } else {
                axios({
                    url: BASE_PATH + '/GenericMap' + this.state.contentTagUrl + '/' + this.state.contentTag[key-1].id,
                    method: 'DELETE',
                }).then(res => {
                    if(res.data.code === 0){
                        this.updateDataSource(key)
                    } else {
                        message.info('删除失败')
                    }
                })
            }
        } catch (error) {
            message.error('删除失败: ' + error);
        }
    }

    updateDataSource(key) {
        let tagClassName = this.state.tagClassName;
        let dataSource;
        if(tagClassName === 'type') {
            dataSource = [...this.state.typeData]
            this.setState({
                typeData: dataSource.filter((item) => item.key != key)
            })
        } else if(tagClassName === 'structure'){
            dataSource = [...this.state.structureData]
            this.setState({
                structureData: dataSource.filter((item) => item.key != key)
            })
        } else {
            dataSource = [...this.state.contentData]
            this.setState({
                contentData: dataSource.filter((item) => item.key != key)
            })
        }
        message.info('删除成功')
    }

    test(){
        axios({
            url:'http://172.21.212.100:8086/GenericMap'+ this.state.typeTagUrl + '/source/新浪微博/1/1',
            method: 'GET'
        }).then(res => {
            console.log(res)
            let temp = res.data.data[0].tagInfo[0]._id
            console.log(temp)
            console.log(temp.str());
        })
    }

    handleChangeType(value) {
        this.setState({
            type: value
        })
    }

    render() {
        return (
            <Row style={{height: '100%', width: '100%'}}>
                <Col span={16}>
                <div id='drawDiv' style={{height: '100%',}}>
                <div id='canvas' style={{height: '80%',border: '1px solid gray', borderRadius: '5px'}}></div>
                <div>
                <Button id='addNewImage' type='primary' onClick={()=>this.showNewImgModal()}>添加新{this.state.dataName} </Button>
                <Button id='addImage' type='primary' onClick={()=> this.showImgModal()}>添加已有{this.state.dataName} </Button>
                <Select id='selectType' defaultValue="None" style={{ width: 120 }} onChange={(value) => this.handleChangeType(value)} >
                  <Option value="Point">点</Option>
                  <Option value="Box">矩形</Option>
                  <Option value="Polygon">多边形</Option>
                  <Option value="Circle">圆</Option>
                  <Option value="None" selected>无</Option>
                </Select>
                 <Button id='drawManus' type='primary' onClick={()=>this.addRect()}>绘制</Button>  
                 <Button onClick={() => this.resetRect()}>清空</Button>  


                <Button id='save' type='primary' onClick={() => this.showTypeTagModal()}>保存该标签</Button>
                <AddImageModal visible={this.state.imageModalVisible} data={this.state.imageData} handleOk={this.handleOk} handleCancel={this.handleCancel} getRasterChart={this.getRasterChart} searchImage={this.searchImage} metaUrl={this.state.metaUrl} dataType={this.state.dataType} dataName={this.state.dataName}/>

                <AddNewImageModal visible={this.state.newImageModalVisible} handleOk={this.handleOk} handleCancel={this.handleNewCancel} getRasterChart = {this.getRasterChart} dataType={this.state.dataType} metaUrl={this.state.metaUrl}/>
                
                <AddTagModal visible={this.state.typeTagModalVisible} handleOk={this.handleOk} handleCancel={this.handleTypeTagCancel} metaId = {this.state.imageMetaId} tagClassName = {this.state.tagClassName} type = {this.state.type}
                 coords={this.state.draw.sketchCoords_} getTagWithImageId = {this.getTagWithImageId} imageHeight={parseInt(this.state.nowImageData.height)} structureTagUrl={this.state.structureTagUrl} contentTagUrl={this.state.contentTagUrl} typeTagUrl={this.state.typeTagUrl} dataName={this.state.dataName}/>
                </div>
            </div>
                </Col>
                <Col span={8}>

                <div id='tags' >
            <Tabs type="card" defaultActiveKey="type"  onChange={(key) => this.setState({tagClassName: key})} centered>
                    <TabPane tab="类型标签" key="type">
                        <Table
                        pagination= {{pageSize: 7}}
                        columns={this.state.typeColumns}
                        // expandable={{
                        //     expandedRowRender: record =>
                        //         <List size='small' bordered 
                        //         dataSource={record.dataSource} 
                        //         renderItem={item => (
                        //             <List.Item className='centerClass'>
                        //                 {item}
                        //             </List.Item>
                        //         )}
                        //         />
                        //     ,
                        //     rowExpandable: record => true,
                        //   }}
                          dataSource={this.state.typeData}
                          onRow={(record) => {//表格行点击事件
                            return {
                                onClick: this.showRect.bind(this,record)
                            };
                          }}
                        />,
                    </TabPane>
                    <TabPane tab="结构标签" key="structure">
                        <Table
                        pagination= {{pageSize: 7}}
                          columns={this.state.structureColumns}
                        //   expandable={{
                        //     expandedRowRender: record => <p style={{ margin: 0 }}>{record.element}</p>,
                        //     rowExpandable: record => record.name !== 'Not Expandable',
                        //   }}
                          dataSource={this.state.structureData}
                          onRow={(record) => {//表格行点击事件
                            return {
                                onClick: this.showRect.bind(this,record)
                            };
                          }}
                        />,
                    </TabPane>
                    <TabPane tab="内容标签" key="content">
                        <Table
                        pagination= {{pageSize: 7}}
                          columns={this.state.contentColumns}
                        //   expandable={{
                        //     expandedRowRender: record => <p style={{ margin: 0 }}>{record.geography}</p>,
                        //     rowExpandable: record => record.name !== 'Not Expandable',
                        //   }}
                          dataSource={this.state.contentData}
                          onRow={(record) => {//表格行点击事件
                            return {
                                onClick: this.showRect.bind(this,record)
                            };
                          }}
                        />,
                    </TabPane>
                  </Tabs>
            </div>
            
                </Col>
            </Row>
  
        )
    }
}


export default MapDiv;