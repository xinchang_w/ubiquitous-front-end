import React, { Component } from "react";
import * as echarts from "echarts";
import 'echarts/extension/bmap/bmap';
/*
* props.id        地图容器id
* props.style     地图容器样式
* props.option     地图配置项
*/
class EchartsMap extends Component{
    constructor(props){
        super(props);
        this.echartsMap = null;
        this.baseOption = {
            bmap: {
                // center: [118.86667, 31.91667],
                center: [116.46, 39.92],
                zoom: 14,
                roam: true
            }
        };
    }

    componentDidMount(){
        this.echartsMap = echarts.init(document.getElementById(this.props.id));
        this.echartsMap.setOption({
            ...this.baseOption,
            ...this.props.option
        });
    }

    componentDidUpdate(){
        this.echartsMap.setOption({
            ...this.baseOption,
            ...this.props.option
        });
    }

    render(){
        return (
            <div id={this.props.id} style={this.props.style}></div>
        );
    }
}

export default EchartsMap;