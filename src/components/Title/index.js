import { Fragment } from "react";
import { Badge } from "antd";

//颜色表
const colorMap_text = new Map([
    //文本
    ["体裁", "#f50"],
    ["功能", "#2db7f5"],
    ["规范性", "#87d068"],
    ["语种", "#108ee9"],
    ["领域", "#ccc"],
]);

const colorMap_image = new Map([
    //图片
    ["储存格式", "#f50"],
    ["分辨率", "#2db7f5"],
    ["应用", "#87d068"],
    ["波段数", "#108ee9"],
    ["色彩", "#ccc"]
]);

const colorMap_map = new Map([
    //地图
    ["专题", "#f50"],
    ["图表类型", "#2db7f5"]
]);

const colorMap_chart = new Map([
    //图表
    ["专题", "#f50"]
]);

const colorMap_audio = new Map([
    //音频
    ["专题", "#f50"],
    ["数据格式", "#2db7f5"]
]);

const colorMap_video = new Map([
    //视频
    ["视频编码", "#f50"],
    ["专题", "#2db7f5"],
    ["数据格式", "#87d068"]
]);

//标签列组件
function Title(props){
    let colorMap = [];
    switch(props.type){
        case "text":
            colorMap = colorMap_text;
            break;
        case "image":
            colorMap = colorMap_image;
            break;
        case "map":
            colorMap = colorMap_map;
            break;
        case "chart":
            colorMap = colorMap_chart;
            break;
        case "audio":
            colorMap = colorMap_audio;
            break;
        case "video":
            colorMap = colorMap_video;
            break;
        default: 
            break;
    }

    return (
        <Fragment>
            <span style={{ marginRight: "20px" }}>标签</span>
            {
                Array.from(colorMap.keys()).map((k, ind) => (
                    <Badge 
                        key={ind} 
                        color={colorMap.get(k)} 
                        text={k} 
                        style={{ marginRight: "10px" }} 
                    />
                ))
            }
        </Fragment>
    );
}

export {
    colorMap_text as colorMap,
    colorMap_text,
    colorMap_image,
    colorMap_map,
    colorMap_chart,
    colorMap_audio,
    colorMap_video,
    Title
};