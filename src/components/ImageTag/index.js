import React from 'react'
import Map from 'ol/Map'
import View from 'ol/View'
import ImageLayer from 'ol/layer/Image'  
import Static from 'ol/source/ImageStatic'
import {fromExtent, fromCircle} from 'ol/geom/Polygon'
import { Point, LineString, Circle as circle, Polygon } from 'ol/geom'
import Feature from 'ol/Feature'  
import {getCenter} from 'ol/extent'
import { Vector as vec } from 'ol/source'
import { Vector } from 'ol/layer'
import { Style, Fill, Stroke, Circle } from 'ol/style'
import * as olProj from 'ol/proj'
import 'ol/ol.css'

/*
* imagePath 图片地址
* imageWidth 图片宽度
* imageHeight 图片长度
* tagShape 标签形状：点、圆、矩形、多边形、、、
* coords 形状的点的坐标： 
point: [x, y], 
line: [[x_1, y_1] ··· [x_n, y_n]]
rect: [[[x_1, y_1]···[x_4, y_4]]]  // 四个点的坐标，矩形用多边形的绘制
square: {coords: [x, y], length: length}
circle: {center: [x, y], radius: radius}，
polygon: [[[x_1, y_1]···[x_n, y_n]]]，
* 参考： https://blog.csdn.net/gis_zzu/article/details/90896646
*/

class ImageTag extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            imagePath: this.props.imagePath,
            tagShape: this.props.tagShape,
            coords: this.props.coords,
            imageWidth: this.props.imageWidth,
            imageHeight: this.props.imageHeight,

            map: '',      // 目标地图
        }
    }

    componentDidMount() {
        let extent = [0, 0, this.state.imageWidth, this.state.imageHeight]
        let projection = new olProj.Projection({//定义坐标系
            code: 'xkcd-image',
            units: 'pixels',
            extent: extent
          });
        let img = new ImageLayer({
            source: new Static({
              url: this.state.imagePath,
              projection: projection,
              imageExtent: extent
            })
          })
        let graph = this.addGraph()
        //实例化一个矢量图层Vector作为绘制层
        var source = new vec({
            features: [graph],
          });
          //创建一个图层
        var vector = new Vector({
          source: source,
        });
        let map = new Map({
            layers: [
                img,
                vector
            ],
            view: new View({
                projection: projection,
                center: getCenter(extent),
                zoom: 2,
                maxZoom: 8
            }),
            target: 'canvas',
        });
        this.setState({map: map})
    }

    addGraph = () => {
        let graph = '';
        switch (this.state.tagShape) {
          case 'point':
            graph = this.addPoint();
            break;
          case 'line':
            graph = this.addLine();
            break;
          case 'circle':
            graph = this.addCricle();
            break;
          case 'square':
            graph = this.addSquare();
            break;
          case 'rect':
            graph = this.addPolygon();
            break;
          case 'polygon':
            graph = this.addPolygon();
            break;
          default:
            break;
        }
        return graph
      };

    //添加点
    addPoint = () => {
    let point = new Feature({
      geometry: new Point(this.state.coords),
    });
    //设置点1的样式信息
    point.setStyle(
      new Style({
        //填充色
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        //边线颜色
        stroke: new Stroke({
          color: '#8bc4da',
          width: 2,
        }),
        //形状
        image: new Circle({
          radius: 5,
          fill: new Fill({
            color: '#c25748',
          }),
        }),
      }),
    );
    return point;
  };
  //   添加线
  addLine = () => {
    var Line = new Feature({
      geometry: new LineString(this.state.coords),
    });
 
    //设置线的样式
    Line.setStyle(
      new Style({
        //填充色
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        //边线颜色
        stroke: new Stroke({
          color: '#8bc4da',
          width: 5,
        }),
        //形状
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#ffcc33',
          }),
        }),
      }),
    );
    return Line;
  };
 
  //   添加圆形
  addCricle = () => {
    var Cir = new Feature({
      geometry: new circle(this.state.coords.coords, this.state.coords.radius),
    });
 
    Cir.setStyle(
      new Style({
        //填充色
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.5)',
        }),
        //边线颜色
        stroke: new Stroke({
          color: '#8bc4da',
          width: 6,
        }),
        //形状
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#ffcc33',
          }),
        }),
      }),
    );
    return Cir;
  };
 
  //   添加正方形
  addSquare = () => {
    //创建一个圆
    var Cir = new circle(this.state.coords.coords, this.state.coords.length);
 
    //根据圆获取多边形
    var Square = new Feature({
      geometry: new fromCircle(Cir, 4, 150),
    });
 
    Square.setStyle(
      new Style({
        //填充色
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.8)',
        }),
        //边线颜色
        stroke: new Stroke({
          color: '#8bc4da',
          width: 2,
        }),
        //形状
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#ffcc33',
          }),
        }),
      }),
    );
    return Square;
  };
  //   添加矩形
  addRectangle = () => {
    var Rectangle = new Feature({
      geometry: new fromExtent(this.state.coords),
    });
 
    Rectangle.setStyle(
      new Style({
        fill: new Fill({
          color: 'rgba(33,33,33,0.5)',
        }),
        stroke: new Stroke({
          color: '#8bc4da',
          width: 4,
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#ffcc33',
          }),
        }),
      }),
    );
    return Rectangle;
  };
 
  // 添加多边形
  addPolygon = () => {
    var polygon = new Feature({
      geometry: new Polygon(this.state.coords),
    });
    //设置区样式信息
    polygon.setStyle(
      new Style({
        //填充色
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.5)',
        }),
        //边线颜色
        stroke: new Stroke({
          color: '#8bc4da',
          width: 2,
        }),
        //形状
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#ffcc33',
          }),
        }),
      }),
    );
    return polygon;
  };

    render() {
        return (
            <div id='canvas' style={{
                height: this.state.imageHeight,
                width: this.state.imageWidth
            }}></div>
        )
    }
}

export default ImageTag
