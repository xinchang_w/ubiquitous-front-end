import { React, Component } from "react";
import { Link } from "react-router-dom";
import { Menu, Layout } from "antd";
import { NAV_ITEMS, ROOT_ROUTE } from "@/routes";
import styles from "./index.module.css";
import logo from "@/assets/image/logo.png";

const { Header, Content, Footer } = Layout;
const { SubMenu, Item } = Menu;
class Nav extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <Layout className={styles.layout}>
                <Header>
                    <div className={styles["header-logo"]}>
                        <img src={logo} alt="网站图标" style={ { height: "75%" } } />
                    </div>
                    <h1 className={styles["header-h1"]}>泛在数据信息建模平台</h1>
                    <Menu 
                        mode="horizontal" 
                        className={styles["header-menu"]} 
                        theme="dark" 
                        defaultSelectedKeys={[this.props.page]}
                    >
                        {createMenu(NAV_ITEMS)}
                    </Menu>
                </Header>
                <Content className={styles["content"]}>
                    {this.props.children}
                </Content>
                <Footer className={styles.footer}>
                    Copyright &copy; 2020-2021 南京师范大学-信息工程大学-中南大学. All Rights Reserved.
                </Footer>
            </Layout>
        );
    }
}

function createMenu(menuList){
    let arr = [];
    menuList.forEach(item => {
        if(item.children){
            arr.push(<SubMenu key={item.key} title={item.title} popupOffset={[0, 2]}>{createMenu(item.children)}</SubMenu>);
        }
        else{
            arr.push(
                <Item key={item.key}>
                    <Link to={`${ROOT_ROUTE}/${item.key}`}>{item.title}</Link>
                </Item>);
        }
    });
    return arr;
}

export default Nav;