import React,{ useState, useEffect, useRef, PureComponent } from "react";
import loadingImg from "@/assets/image/loading.gif";
import notFound from "@/assets/image/notFound_image.png";
import CanOpr from "@/utility/canvasOpr";
import style from "./index.module.css";
//用canvas绘制的图像
//@param props.width {Number|String} 不能为相对值，只能为固定值，如："500"、"500px"
//@param props.height {Number|String}  
//@param props.url {String} 需要加载的图片的路径
//@param props.rect {Object|undefind} 包含x-矩形左上角x坐标, y-矩形左上角y坐标, w-矩形宽度, h-矩形高度四个属性
function CusImage(props){
    let [isLoaded, setLoadState] = useState(false);
    let canvasRef = useRef();
    let canOprRef = useRef();
    let imageRef = useRef();

    useEffect(() => {
        canOprRef.current = new CanOpr(canvasRef.current);
        imageRef.current = createImg(() => {
            setLoadState(true);
            canOprRef.current.drawImage(imageRef.current);
            imageRef.current.fillRect && imageRef.current.fillRect();
        }, () => {
            imageRef.current.src = notFound;
        });
    }, []);

    useEffect(() => {
        if(!props.url){
            setLoadState(false);
            return;
        }
        imageRef.current.src = props.url;
        imageRef.current.fillRect = () => {
            canOprRef.current.fillRect(props.rect);
        };
        return () => {
            canOprRef.current.clear();
        }
    },[props.url, props.rect]);
    
    return (
        <div className={style.wrapper} style={{ width: props.width, height: props.height }} >
            <canvas 
                ref={canvasRef}
                width={parseInt(props.width)}
                height={props.height ? parseInt(props.height) : 300}
            >
                你的浏览器不支持canvas!
            </canvas>
            <img
                src={loadingImg}
                alt="加载中"
                className={style.loading}
                style={{
                    visibility: isLoaded ? "hidden" : "visible"
                }}
            />
        </div>
    );
}

function createImg(onload, onerror){
    let image = new Image();
    image.onload = onload;
    image.onerror = onerror;
    return image;
}

export default CusImage;