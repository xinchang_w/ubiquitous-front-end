import React, { useEffect, useRef } from "react";
import css from "./index.module.css";
/*
@param title
@param closable {Boolean} 是否显示关闭按钮，默认为false
@param resizable {Boolean} 是否可以调整大小，默认为false
@param movable {Boolean} 是否可以移动，默认为false
@param onClose {Function} 点击关闭按钮回调
@param style {Ojbect} 应用在卡片中的自定义样式
*/
function Card(props){
    let { title, id, style, onClose, closable = false, resizable = false, movable = false } = props;
    let maxZIndex = "9999";
    let headElem = useRef(null);
    useEffect(() => {
        if(!movable){
            return;
        }

        let elem = headElem.current;
        let elemParent = elem.parentNode;
        let left, top, startX, startY, zIndex;
        elem.addEventListener("mousedown", e => {
            left = elemParent.offsetLeft;
            top = elemParent.offsetTop;
            zIndex = elemParent.style.zIndex;
            startX = e.clientX;
            startY = e.clientY;
            elemParent.style.zIndex = maxZIndex;
            elemParent.offsetParent.addEventListener("mousemove", moveMouse);
        });

        elem.addEventListener("mouseup", e => {
            elemParent.offsetParent.removeEventListener("mousemove", moveMouse);
            if(zIndex){
                elemParent.style.zIndex = zIndex;
            }
            else{
                elemParent.style.removeProperty("z-index");
            }
        });

        function moveMouse(e){
            let distanceX = e.clientX - startX;
            let distanceY = e.clientY - startY;
            let left_ = left + distanceX;
            let top_ = top + distanceY;
            elemParent.style.left = left_ >= 0 ? `${left_}px` : "0px";
            elemParent.style.top = top_ >= 0 ? `${top_}px` : "0px";
        }
    }, []);

    return (
        <div
            className={`${css.card} ${resizable ? css["card-resize"] : ""}`}
            style={style}
            id = {id}
        >
            <div
                className={`${css["card-head"]} ${movable ? css["card-move"] : ""}`}
                ref={headElem}
            >
                <span className={css["card-head-title"]}>{title}</span>
                {
                    closable ? <span className={css["card-head-closeBtn"]} onClick={onClose}>x</span> : ""
                }
            </div>
            <div className={css["card-body"]}>
                {props.children}
            </div>
        </div>
    );
}

export default Card;
