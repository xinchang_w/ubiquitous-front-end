import React, { useRef, useEffect } from "react";
import Map from "ol/Map";
import View from "ol/View";
import * as Control from "ol/control"; 
import Feature from "ol/Feature";
import GeoJSON from "ol/format/GeoJSON";
import { OSM, Vector as VectorSource} from "ol/source";
import { Tile as TileLayer, Vector as VectorLayer } from "ol/layer";
/* 
用于展示geoJSON类型的数据
    @param width {Number} 地图组件宽度，默认为300px
    @param height {Number} 地图组件高度，默认为200px
@param center {Array} 地图中心经纬度
@param zoom {Number} 地图初始缩放等级
@param geoJSON {geoJson Object} geoJSON数据
*/

function CusMap(props){
    let { width = "100%", height = "100%", center, zoom, geoJSON } = props;
    let oMap = useRef();
    let map = new MapOpr();
    useEffect(() => {
        map.init(oMap.current, center, zoom);
    }, null);

    return (
        <div ref={oMap} style={{width, height}}></div>
    );
}

class MapOpr{
    constructor(){
        this._map = null;
    }
    init(elem, center, zoom){
        this._map = new Map({
            target: elem,
            layers : [
                new TileLayer({source: new OSM()})
            ],
            view: new View({ center, zoom, projection: "EPSG:4326" }),
            controls: Control.defaults().extend([
                new Control.MousePosition()      // 实例化坐标拾取控件，并加入地图
            ])
        });
        this._map.on()
    }
    loadGeoJsonData(data){

    }
}

export default CusMap;