import { useState, useRef, Fragment } from "react";
import Icon, { PlayCircleOutlined, PauseCircleOutlined } from "@ant-design/icons";
import loading from "@/assets/image/loading.gif";
import style from "./index.module.css";
import { ReactComponent as MusicSvg } from "@/assets/image/music.svg";
/*
* @param {title} String
* @param {content} String
* @param {image} String
*/
function Paragraph(props){
    if(props.content){
        return (
            <div className={style.line}>
                <span style={{"fontWeight": "bold"}}>{props.title}</span>
                <span>{props.content}</span>
            </div>
        );
    }

    if(props.image){
        return (
            <div className={style.line}>
                <Img image={props.image} />
            </div>
        );
    }

    if(props.audio){
        return (
            <div className={style.line}>
                <span style={{"fontWeight": "bold"}}>{props.title}</span>
                <Audio audio={props.audio} />
            </div>
        );
    }

    if(props.video){
        return (
            <div className={style.line}>
                <Video video={props.video} />
            </div>
        );
    }
    
    return (
        <div className={style.line}>
            <span style={{"fontWeight": "bold"}}>{props.title}</span>
            <span></span>
        </div>
    );
}

/*
@param image {String} 图片路径
*/
function Img(props){
    function load(e){
        e.target.src = props.image;
    }

    return (
        <img 
            src={loading} 
            className={style.image} 
            onLoad={load} 
            alt="图片加载失败" 
        />
    );
}

/*
@param audio {String} 音频路径
*/
function Audio(props){
    let [playState, setPlayState] = useState("play");   //音频播放状态
    let [loadingState, setLoadingState] = useState(1);  //音频加载状态，1 - 加载中，2 - 加载失败，3 - 加载成功
    let audioRef = useRef(null);
    let com = null;
 
    switch(loadingState){
        case 1:
            com = (<span className={style["playState-span"]}>加载中……</span>);
            break;
        case 2:
            com = (<span className={style["playState-span"]} style={{color:"red"}}>加载失败</span>);  
            break;
        case 3:
            com = (<Icon component={MusicSvg} spin={playState === "pause"} className={style["playState-icon"]} />);
            break;
        default:
            break;
    }

    function changePlayState(){
        //音频未成功加载，不允许播放或暂停
        if(loadingState !== 3){
            return;
        }

        if(playState === "play"){
            audioRef.current.play();
            setPlayState("pause");
        }
        if(playState === "pause"){
            audioRef.current.pause();
            setPlayState("play");
        }
    }

    return (
        <Fragment>
            <div className={`${style.playBtn} ${loadingState === 3 ? "" : style["playBtn-disable"]}`} onClick={changePlayState}>
                {
                    playState === "play"
                        ? <PlayCircleOutlined className={style["playBtn-icon"]} />
                        : <PauseCircleOutlined className={style["playBtn-icon"]} />
                }
                <span className={style["playBtn-text"]}>{playState === "play" ? "播放" : "暂停"}</span>
                <audio 
                    src={props.audio} 
                    className={style["playBtn-audio"]} 
                    ref={audioRef}
                    onCanPlayThrough={() => setLoadingState(3)}
                    onError={() => setLoadingState(2)}
                    onEnded={() => setPlayState("play")}
                >
                    你的浏览器不支持audio标签
                </audio>
            </div>
            <p className={style.playState}>{com}</p>
        </Fragment>
    );
}

function Video(props){
    return (
        <video controls src={props.video} width="480" height="360" className={style.video}>
            你的浏览器不支持video标签
        </video>
    );
}

export default Paragraph;