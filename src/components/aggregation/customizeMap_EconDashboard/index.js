import React, { Component } from "react";

import './EconDashboard.css'
import EconDashboardRelation from "@/components/aggregation/customizeMap_EconDashboard_Relation"
import NetworkDashboard from "@/components/aggregation/customizeMap_NetworkDashboard"
import ChinaNetworkDashboard from "@/components/aggregation/customizeMap_ChinaNetworkDashboard"
import GlobalNetworkDashboard from "@/components/aggregation/customizeMap_GlobalNetworkDashboard"
import TreeMap from "@/components/aggregation/customizeMap_TreeMap"
import Card from "@/components/customizeCard"
import EconDashboardComposite from '@/components/aggregation/customizeMap_EconDashboard_Composite'
import EconDashboardParallel from '@/components/aggregation/customizeMap_EconDashboard_Parallel'
import EconDashboardGroup from '@/components/aggregation/customizeMap_EconDashboard_Group'
class SelectMap extends Component {
    constructor(props) {
        super(props)
        this.cate = "enco";
        this.state = {
            type: "经济活力",
        }
    };
    componentDidMount() {

    }
    onRefEconComposite(ref) {
        // console.log(ref)
        this.EconDashboardComposite = ref;
    }
    onRefEconRelation(ref) {
        // console.log(ref)
        this.EconDashboardRelation = ref;
    }
    onRefEconParallel(ref) {
        // console.log(ref)
        this.EconDashboardParallel = ref;
    }
    onRefEconGroup(ref) {
        this.EconDashboardGroup = ref
    }

    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容
            <div id="SelectMap" className="mapContainer">
                <Card title="活力因子选择框" style={{ width: 400, height: 100 }} movable={true}>
                    <RadioGroup name="mvvm" onChange={e => this.onGroupChange(e)} defaultSelect={0}>
                        <Radio>经济活力</Radio>
                        <Radio>社会活力</Radio>
                        <Radio>环境活力</Radio>
                        <Radio>科技活力</Radio>
                    </RadioGroup>
                </Card>
                <Card title="工业创新环境综合指标分布图" style={{ width: "fit-content", height: 660 }} movable={true}>
                    <EconDashboardComposite id="EconDashboardComposite" cate="enco" onRef={this.onRefEconComposite.bind(this)} />
                </Card>
                <Card title={this.state.type + "因子相关性概览图"} style={{ width: "fit-content", height: "fit-content" }} movable={true}>
                    <EconDashboardRelation id="EconDashboardRelation" cate="enco" onRef={this.onRefEconRelation.bind(this)} ></EconDashboardRelation>
                </Card>
                <Card title={"各市" + this.state.type + "因子对比图"} style={{ width: 1000, height: "fit-content" }} movable={true}>
                    <EconDashboardParallel id="EconDashboard_Parallel" cate="enco" onRef={this.onRefEconParallel.bind(this)} ></EconDashboardParallel>
                </Card>

                <Card title={"江苏省" + this.state.type + "因子总值"} style={{ width: 280, height: "fit-content" }} movable={true}>
                    <EconDashboardGroup id="EconDashboard_Group" cate="enco" onRef={this.onRefEconGroup.bind(this)} ></EconDashboardGroup>
                </Card>

            </div>

        )
    }
    onGroupChange(e) {
        switch (e.target.value) {
            case "0":
                this.setState({
                    type: "经济活力",
                })
                this.cate = "enco";

                break;
            case "1":
                this.cate = "soci";
                this.setState({
                    type: "社会活力",
                })
                break;
            case "2":
                this.setState({
                    type: "环境活力",
                })
                this.cate = "envi";
                break;
            case "3":
                this.setState({
                    type: "科技活力",
                })
                this.cate = "tech";
                break;

            default:
                this.setState({
                    type: "经济活力",
                })
                this.cate = "enco";
                break;
        }


        this.EconDashboardComposite.dataChange(this.cate);
        this.EconDashboardRelation.dataChange(this.cate);
        this.EconDashboardParallel.dataChange(this.cate);
        this.EconDashboardGroup.dataChange(this.cate);
    }
}
// 父级组件
const RadioGroup = (props) => {
    let index = 0;
    let defaultSelect = props.defaultSelect;

    return (
        <div className="selectMapbox" onChange={e => typeof props.onChange === 'function' && props.onChange(e)}>
            {
                React.Children.map(props.children, child => {
                    return React.cloneElement(child, {
                        name: props.name,
                        // 设置index
                        value: index++,
                        //  设置默认勾选
                        defaultChecked: (index - 1) === defaultSelect
                    })
                })
            }
        </div>
    )
}

// 子组件
const Radio = ({ children, ...props }) => (
    <label className="selectLable">
        <input type="radio" {...props} data-name={children} /> {children}
    </label>
)

export default SelectMap
