import React, { useRef, useEffect, Component } from "react";
import * as d3 from 'd3';

import './EconDashboard_Parallel.css'


class EconDashboard_Composite extends Component {

    constructor(props) {
        super(props);
        this.cate = "enco"
        if (this.props.cate) {
            this.cate = this.props.cate
        }

    }
    dataChange(cate) {
        this.Initialize(cate)
    }
    componentDidMount() {
        this.props.onRef(this);
        this.Initialize(this.cate)

    }
    Initialize(cate) {
        document.getElementById('EconParallel').innerHTML = "";
        const margin = {
            top: 50,
            right: 30,
            bottom: 50,
            left: 50
        };
        const bounds = {
            width: 860 - margin.left - margin.right,
            height: 360 - margin.top - margin.bottom
        };



        var csvDataTitleOri = this.getTitle(cate)
        var csvDataTitle = csvDataTitleOri.slice(2, csvDataTitleOri.length);
        var cityName = ['南京市', '无锡市', '徐州市', '常州市', '苏州市', '南通市', '连云港市', '淮安市', '盐城市', '扬州市', '镇江市', '泰州市', '宿迁市']
        var Visualisation = d3.select("#EconParallel")
            .append('svg')
            .attr('width', 860)
            .attr('height', 320)
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`)


        // const xScale = d3.scaleBand().rangeRound([0, bounds.width]).domain(csvDataTitle);

        // const xAxisGenerator = d3.axisTop().scale(xScale);
        // const xAxis = Visualisation.append('g').attr('transform', `translate(0,${bounds.height})`)
        //                                        .call(xAxisGenerator)
        // const xAxis = Visualisation.append('g').call(xAxisGenerator)

        // const yScale = d3.scaleLinear().range([0, bounds.height]).domain([0, 500]);
        // const yAxisGenerator = d3.axisLeft().scale(yScale);
        // const yAxis = Visualisation.append('g').call(yAxisGenerator)


        const cityNameScale = d3.scaleBand().domain(cityName).rangeRound([0, bounds.height]);
        const cityNameAxisGenerator = d3.axisLeft().scale(cityNameScale).ticks(13);
        const cityNameAxis = Visualisation.append('g').call(cityNameAxisGenerator)
        // .attr('transform', 'translate(' + padding.left + ',' + padding.top + ')')
        Visualisation.append('text')
            .attr('dx', 0)
            .attr('dy', 0)
            .text('地级市名称')
            .attr('transform', 'rotate(-5) translate(' + -40 + ',' + -10 + ')')
            .style("fill", 'black')







        const _this = this;
        // console.log(cate);
        d3.csv("ecodata/" + cate + ".csv", function (csvdata) {

            return csvdata;

        }).then(function (csvdata1) {
            // console.log(csvdata1)
            var csvData = [];
            var data1 = [];
            var data2 = [];
            var data3 = [];
            var data4 = [];
            var data5 = [];
            var data6 = [];
            var data7 = [];
            var data8 = [];
            var csvLineData = [];
            for (let i = 0; i < csvdata1.length; i++) {
                const valueTemp = csvdata1[i];
                var tempLineData = [];
                // console.log('csvDataTitle', csvDataTitle)
                for (let j = 0; j < csvDataTitle.length; j++) {
                    tempLineData.push(valueTemp[csvDataTitle[j]])
                }

                data1.push(valueTemp[csvDataTitleOri[0]])
                data2.push(valueTemp[csvDataTitleOri[1]])
                data3.push(valueTemp[csvDataTitleOri[2]])
                data4.push(valueTemp[csvDataTitleOri[3]])
                data5.push(valueTemp[csvDataTitleOri[4]])
                data6.push(valueTemp[csvDataTitleOri[5]])
                data7.push(valueTemp[csvDataTitleOri[6]])
                data8.push(valueTemp[csvDataTitleOri[7]])

            }

            csvLineData.push(_this.fitRanger(data3, bounds.height));
            csvLineData.push(_this.fitRanger(data4, bounds.height));
            csvLineData.push(_this.fitRanger(data5, bounds.height));
            csvLineData.push(_this.fitRanger(data6, bounds.height));
            csvLineData.push(_this.fitRanger(data7, bounds.height));
            csvLineData.push(_this.fitRanger(data8, bounds.height));
            // console.log('csvLineData', csvLineData)

            csvData.push(data1)
            csvData.push(data2)
            csvData.push(data3)
            csvData.push(data4)
            csvData.push(data5)
            csvData.push(data6)
            csvData.push(data7)
            csvData.push(data8)

            for (let j = 2; j < csvData.length; j++) {
                const tempData = csvData[j];
                const Scale1 = d3.scaleBand().domain(tempData).rangeRound([0, bounds.height]);
                const AxisGenerator1 = d3.axisLeft().scale(Scale1);
                const Axis1 = Visualisation.append('g').call(AxisGenerator1)
                    .attr('transform', 'translate(' + bounds.width * (j - 1) / 6 + ',' + 0 + ')')

            }
            // 绘制坐标抽
            Visualisation.append('text')
                .attr('dx', bounds.width * (2 - 1) / 6)
                .attr('dy', 0)
                .text(csvDataTitleOri[2])
                .attr('transform', 'rotate(-5) translate(' + -60 + ',' + 0 + ')')
                .style("fill", 'black')
            Visualisation.append('text')
                .attr('dx', bounds.width * (3 - 1) / 6)
                .attr('dy', 0)
                .text(csvDataTitleOri[3])
                .attr('transform', 'rotate(-5) translate(' + -50 + ',' + 10 + ')')
                .style("fill", 'black')
            Visualisation.append('text')
                .attr('dx', bounds.width * (4 - 1) / 6)
                .attr('dy', 0)
                .text(csvDataTitleOri[4])
                .attr('transform', 'rotate(-5) translate(' + -50 + ',' + 20 + ')')
                .style("fill", 'black')
            Visualisation.append('text')
                .attr('dx', bounds.width * (5 - 1) / 6)
                .attr('dy', 0)
                .text(csvDataTitleOri[5])
                .attr('transform', 'rotate(-5) translate(' + -50 + ',' + 30 + ')')
                .style("fill", 'black')
            Visualisation.append('text')
                .attr('dx', bounds.width * (6 - 1) / 6)
                .attr('dy', 0)
                .text(csvDataTitleOri[6])
                .attr('transform', 'rotate(-5) translate(' + -50 + ',' + 44 + ')')
                .style("fill", 'black')
            Visualisation.append('text')
                .attr('dx', bounds.width * (7 - 1) / 6)
                .attr('dy', 0)
                .text(csvDataTitleOri[7])
                .attr('transform', 'translate(' + -50 + ',' + 60 + ') rotate(-5)')
                .style("fill", 'black')

            // 绘制线
            // console.log('csvLineData', csvLineData)
            var originalColorList = ['#8dd3c7', '#228B22', '#a999da', '#A52A2A', '#DA70D6', '#578fe1', '#c2de5c', '#E9967A', '#696969', '#a26ca3', '#32CD32', '#FFD700', '#DB7093'];

            for (let i = 0; i < 13; i++) {
                var lines = [[0, i * bounds.height / 12], [bounds.width / 6, csvLineData[0][i]],
                [2 * bounds.width / 6, csvLineData[1][i]], [3 * bounds.width / 6, csvLineData[2][i]],
                [4 * bounds.width / 6, csvLineData[3][i]], [5 * bounds.width / 6, csvLineData[4][i]],
                [6 * bounds.width / 6, csvLineData[5][i]]];
                // console.log('lines', lines)

                var linePath = d3.line();

                // 添加路径
                Visualisation.append('path')
                    .attr('d', linePath(lines)) // 使用了线段生成器
                    .attr('stroke', originalColorList[i])
                    .attr('stroke-width', '1px')
                    .attr('fill', 'none');


            }
        })

    }
    getTitle(cate) {
        var csvDataTitle = [];
        switch (cate) {
            case 'enco':
                csvDataTitle = ['id', '地级市名称', '生产总值(亿元)', '第一产业产值(亿元)', '第二产业产值(亿元)', '第三产业产值(亿元)', '规模以上单位数(个)', '实际使用外资(亿美元)']
                break;
            case 'soci':
                csvDataTitle = ['id', '地级市名称', '常住人口数量(万人)', '公共预算收入(亿元)', '公共预算支出(亿元)', '年末就业人数(万人)', '居民人均可支配收入(元)', '居民人均住房建筑面积(平方米)']
                break;
            case 'envi':

                csvDataTitle = ['id', '地级市名称', '公共汽车数(辆)', '公路里程(公里)', '绿地面积(公顷)', '供水量(万吨)', '供电量(亿千瓦时)', '全年供天然气总量(万立方米)']
                break;
            case 'tech':
                csvDataTitle = ['id', '地级市名称', '专利申请授权数(件)', '在校大学生数量(万人)', '公共图书馆藏书量(千册)', '高等学校数量(所)', '科学技术公共预算支出(亿元)', '各类专业技术人员数(万人)']
                break;

            default:
                csvDataTitle = ['id', '地级市名称', '生产总值(亿元)', '第一产业产值(亿元)', '第二产业产值(亿元)', '第三产业产值(亿元)', '规模以上单位数(个)', '实际使用外资(亿美元)']
                break;
        }





        return csvDataTitle
    }
    fitRanger(arraydata, number) {

        var returnData = new Array(arraydata.length);

        var max = Number(arraydata[0])
        var min = Number(arraydata[0])
        for (let i = 0; i < arraydata.length; i++) {

            const numberArray = Number(arraydata[i])

            // console.log('dataori min max 前', [numberArray, min, max])


            if (numberArray > max) {
                max = numberArray
            }
            if (numberArray < min) {
                min = numberArray
            }
            // console.log('datao min max后', [numberArray, min, max])

        }



        for (let i = 0; i < arraydata.length; i++) {
            returnData[i] = (arraydata[i] - min) / ((max - min) / number)
        }
        // console.log('arraydata', arraydata)
        // console.log('returnData', returnData)
        return returnData

    }
    getMax(data) {
        var max = Number(data[0]);
        for (let i = 1; i < data.length; i++) {
            const element = Number(data[i]);
            if (element > max) {
                max = element;
            }
        }
        return max
    }
    arrayTurn(data) {
        // console.log("调换顺序前", data)
        var arrayReturn = [];
        for (let i = 0; i < data.length; i++) {
            const element = data[i];
            var tempData = [];
            for (let j = (element.length - 1); j > -1; j--) {
                tempData.push(element[j]);
            }
            arrayReturn.push(tempData)
        }
        // console.log("调换顺序后", arrayReturn)
        return arrayReturn

    }
    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容
            <div id="EconParallel" className="EconDashboard_Parallel">

            </div>
        )
    }
}


export default EconDashboard_Composite
