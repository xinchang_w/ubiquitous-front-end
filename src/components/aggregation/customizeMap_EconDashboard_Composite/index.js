import React, { useRef, useEffect, Component } from "react";

import L from "leaflet";
import './EconDashboard_Composite.css'
import mapboxgl from 'mapbox-gl'

import * as d3 from 'd3';
import city_geojson from "./data/city_boundary_simplified.js"
import city_points from "./data/city_geocenter.js"
class EconDashboard_Composite extends Component {

    constructor(props) {
        super(props);
        this.cate = "enco"
        if (this.props.cate) {
            this.cate = this.props.cate
        }

    }
    dataChange(cate) {

        console.log(cate)
        this.Initialize(this.address, cate, this.map, this.image)

    }
    componentDidMount() {
        this.props.onRef(this);
        mapboxgl.accessToken = 'pk.eyJ1IjoibWFvcmV5IiwiYSI6ImNqNWhrenIwcDFvbXUyd3I2bTJxYzZ4em8ifQ.KHZIehQuWW9AsMaGtATdwA';
        this.address = "http://localhost:3000/ubiquitous-geoinformation/ecodata/"

        this.image = document.getElementById('CompositeImg')
        this.image.src = 'ecodata/' + this.cate + "-legend.png"
        this.Initialize(this.address, this.cate, this.map, this.image)

    }
    Initialize(address, cate, map, image) {

        document.getElementById('CompositeMap').innerHTML = "";
        map = new mapboxgl.Map({
            container: 'CompositeMap',
            center: [119.2, 33], // starting position [lng, lat]
            zoom: 5.9, // starting zoom
            style: {
                "version": 8,
                "sprite": address + "mapbox_resource/sprite/" + cate,
                "glyphs": address + "mapbox_resource/fonts/{fontstack}/{range}.pbf",
                "sources": {
                    'boundary': {
                        type: 'geojson',
                        data: city_geojson,
                    },
                    'city': {
                        type: 'geojson',
                        data: city_points,
                    }
                },
                "layers": [
                    {
                        'id': 'boundary',
                        'type': 'fill',
                        'source': 'boundary',
                        'layout': {},
                        "paint": {
                            "fill-color": "hsl(0, 0%, 86%)",
                            "fill-outline-color": "#696969"
                        }
                    },
                    {
                        "id": "highlight",
                        "type": "line",
                        "source": "boundary",
                        "layout": { "visibility": "none" },
                        "paint": { "line-color": "#696969", "line-width": 1.5 }
                    },
                    {
                        "id": "cityname",
                        "type": "symbol",
                        "source": "city",
                        "layout": {
                            "text-field": ["to-string", ["get", "city_cn"]],
                            "text-size": 10,
                            "text-allow-overlap": true,
                            "text-offset": [
                                "match",
                                ["get", "city_cn"],
                                ["淮安市"],
                                ["literal", [2, -2]],
                                ["徐州市"],
                                ["literal", [3.2, 0]],
                                ["镇江市"],
                                ["literal", [1.7, -2.2]],
                                ["无锡市"],
                                ["literal", [1.5, -2.3]],
                                ["苏州市"],
                                ["literal", [2, -2.3]],
                                ["扬州市"],
                                ["literal", [0.4, -2.3]],
                                ["泰州市"],
                                ["literal", [-0.4, -2.5]],
                                ["常州市"],
                                ["literal", [-2.5, 2]],
                                ["literal", [0, -2.3]]
                            ]
                        },
                        "paint": { "text-color": "#696969" }
                    }
                ]
            },
        });

        var radar_size = 0.09
        if (cate == "enco") {
            map.on('load', function () {
                map.addLayer({
                    "id": "icon",
                    "type": "symbol",
                    "source": "city",
                    "layout": {
                        "icon-offset": [0, 0],
                        "icon-image": [
                            "match",
                            ["get", "city_cn"],
                            ["南京市"],
                            "nanjing_enco",
                            ["无锡市"],
                            "wuxi_enco",
                            ["徐州市"],
                            "xuzhou_enco",
                            ["常州市"],
                            "changzhou",
                            ["苏州市"],
                            "suzhou_enco",
                            ["南通市"],
                            "nantong_enco",
                            ["连云港市"],
                            "lianyungang_enco",
                            ["淮安市"],
                            "huaian_enco",
                            ["盐城市"],
                            "yancheng_enco",
                            ["扬州市"],
                            "yangzhou_enco",
                            ["镇江市"],
                            "zhenjiang_enco",
                            ["泰州市"],
                            "taizhou_enco",
                            ["宿迁市"],
                            "suqian_enco",
                            "%E5%9B%BE%E7%89%871"
                        ],
                        "icon-size": radar_size
                    },
                    "paint": { "icon-translate": [0, 0] }
                })
            })
        } else if (cate == "soci") {
            map.on('load', function () {
                map.addLayer({
                    "id": "icon",
                    "type": "symbol",
                    "source": "city",
                    "layout": {
                        "icon-offset": [0, 0],
                        "icon-image": [
                            "match",
                            ["get", "city_cn"],
                            ["南京市"],
                            "nanjing_soci",
                            ["无锡市"],
                            "wuxi_soci",
                            ["徐州市"],
                            "xuzhou_soci",
                            ["常州市"],
                            "changzhou_soci",
                            ["苏州市"],
                            "suzhou_soci",
                            ["南通市"],
                            "nantong_soci",
                            ["连云港市"],
                            "lianyungang_soci",
                            ["淮安市"],
                            "huaian_soci",
                            ["盐城市"],
                            "yancheng_soci",
                            ["扬州市"],
                            "yangzhou_soci",
                            ["镇江市"],
                            "zhenjiang_soci",
                            ["泰州市"],
                            "taizhou_soci",
                            ["宿迁市"],
                            "suqian_soci",
                            "%E5%9B%BE%E7%89%871"
                        ],
                        "icon-size": radar_size
                    },
                    "paint": { "icon-translate": [0, 0] }
                })
            })
        } else if (cate == "envi") {
            map.on('load', function () {
                map.addLayer({
                    "id": "icon",
                    "type": "symbol",
                    "source": "city",
                    "layout": {
                        "icon-offset": [0, 0],
                        "icon-image": [
                            "match",
                            ["get", "city_cn"],
                            ["南京市"],
                            "nanjing_envi",
                            ["无锡市"],
                            "wuxi_envi",
                            ["徐州市"],
                            "xuzhou_envi",
                            ["常州市"],
                            "changhzou_envi",
                            ["苏州市"],
                            "suzhou_envi",
                            ["南通市"],
                            "nantong_envi",
                            ["连云港市"],
                            "lianyungang_envi",
                            ["淮安市"],
                            "huaian_envi",
                            ["盐城市"],
                            "yancheng_envi",
                            ["扬州市"],
                            "yangzhou_envi",
                            ["镇江市"],
                            "zhenjiang_envi",
                            ["泰州市"],
                            "taizhou_envi",
                            ["宿迁市"],
                            "suqian_envi",
                            "%E5%9B%BE%E7%89%871"
                        ],
                        "icon-size": radar_size
                    },
                    "paint": { "icon-translate": [0, 0] }
                })
            })
        } else if (cate == "tech") {
            map.on('load', function () {
                map.addLayer({
                    "id": "icon",
                    "type": "symbol",
                    "source": "city",
                    "layout": {
                        "icon-offset": [0, 0],
                        "icon-image": [
                            "match",
                            ["get", "city_cn"],
                            ["南京市"],
                            "nanjing_tech",
                            ["无锡市"],
                            "wuxi_tech",
                            ["徐州市"],
                            "xuzhou_tech",
                            ["常州市"],
                            "changzhou_tech",
                            ["苏州市"],
                            "suzhou_tech",
                            ["南通市"],
                            "nantong_tech",
                            ["连云港市"],
                            "lianyungang_tech",
                            ["淮安市"],
                            "huaiai_tech",
                            ["盐城市"],
                            "yancheng_tech",
                            ["扬州市"],
                            "yangzhou_tech",
                            ["镇江市"],
                            "zhenjiang_tech",
                            ["泰州市"],
                            "taizhou_tech",
                            ["宿迁市"],
                            "suqian_tech",
                            "%E5%9B%BE%E7%89%871"
                        ],
                        "icon-size": radar_size
                    },
                    "paint": { "icon-translate": [0, 0] }
                })
            })
        }
        image.src = 'ecodata/' + cate + "-legend.png"
    }
    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容
            <div className="EconDashboard_Composite_container">
                <div id='CompositeMap' className="EconDashboard_CompositeMap" ></div>
                <img id="CompositeImg" className="EconDashboard_CompositeImg" src="" alt="我是一张图片"></img>
            </div>
        )
    }
}


export default EconDashboard_Composite
