import React, { useRef, useEffect, Component } from "react";

import './EconDashboard_Relation.css'


class EconDashboard_Composite extends Component {

    constructor(props) {
        super(props);
        this.cate = "enco"
        if (this.props.cate) {
            this.cate = this.props.cate
        }

    }
    dataChange(cate) {
        this.Initialize(cate, this.CompositeImgMain)
    }
    componentDidMount() {
        this.props.onRef(this);

        this.CompositeImgMain = document.getElementById('CompositeImgMain')

        this.CompositeImgMain.src = 'ecodata/' + this.cate + ".png"
        console.log("this.CompositeImgMain", this.CompositeImgMain.src)

        this.Initialize(this.cate, this.CompositeImgMain)

    }
    Initialize(cate, image) {

        image.src = 'ecodata/' + cate + ".png"
    }
    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容
            <div className="EconDashboard_Composite_container">
                <img id='CompositeImgMain' className="EconDashboard_CompositeImgMain" src="" alt="Legend"></img>
                <img id="CompositeImgLegend" className="EconDashboard_CompositeImgLegend" src="ecodata/legend.png" alt="Legend"></img>
            </div>
        )
    }
}


export default EconDashboard_Composite
