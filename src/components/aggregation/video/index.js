import React, { Fragment, useState, useEffect } from "react";
import { Spin, message } from "antd";
import axios from "axios";
import { FILE_SERVER } from "@/assets/global";

/*
显示一条或最多两条视频数据
如果传递了data，则直接显示data中的数据，否则显示根据queryCon查询到的数据
@param data { Object Array } 视频数据数组
@param url { String } 查询路径
@param queryCon { Object } 查询条件
*/
const videoStyle = {
    display: "block",
    margin: "10px auto"
};

function Video(props) {
    let { data, url, queryCon } = props;
    let [videoItems, setVideoItems] = useState([]);
    let [spinning, setSpinning] = useState(true);

    useEffect(() => {
        if(data){
            setVideoItems(data.silce(0, 2));
            return;
        }
        axios.get(url, {
            params: queryCon
        }).then(res => {
            if(res.data.code){
                setVideoItems(res.data.data.textData.slice(0, 2));
                setSpinning(false);
            }
            else{
                throw new Error(res.data.msg);
            }
        }).catch(err => {
            console.log(err);
            setSpinning(false);
            message.error("加载视频数据失败");
        });
 
    }, []);

    return (
        <Spin spinning={spinning}>
            {
                videoItems.map(i => (
                    <video key={i._id} controls src={`${FILE_SERVER}/${i.video}`} width="350" height="200" style={videoStyle}>
                        你的浏览器不支持video标签
                    </video>
                ))
            }
        </Spin>
    );
}

export default Video;