import React, { useRef, useEffect, Component } from "react";
import * as d3 from 'd3'
import './EconDashboard_Group.css'


class EconDashboard_Group extends Component {

    constructor(props) {
        super(props);
        this.cate = "enco"
        if (this.props.cate) {
            this.cate = this.props.cate
        }

    }
    dataChange(cate) {
        this.Initialize(cate)
    }
    componentDidMount() {
        this.props.onRef(this);
        this.Initialize(this.cate)



    }
    Initialize(cate) {

        d3.csv("ecodata/" + cate + "-jiangsu.csv", function (csvdata) {

            return csvdata;

        }).then(function (csvdata) {
            // console.log('csvdata1', csvdata);

            const titleID = ['title1', 'title2', 'title3', 'title4', 'title5', 'title6']
            const contentID = ['content1', 'content2', 'content3', 'content4', 'content5', 'content6']
            for (let i = 0; i < csvdata.length; i++) {
                document.getElementById(titleID[i]).innerHTML = csvdata[i].name
                document.getElementById(contentID[i]).innerHTML = csvdata[i].value + "&nbsp;" + csvdata[i].unit

            }
        })
    }
    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容
            <div id="EconGroup" className="EconGroup">
                <div className="lable">
                    <span id='title1' className="title">标题</span>
                    <div id='content1' className="content">内容</div>
                </div>
                <div className="lable">
                    <span id='title2' className="title">标题</span>
                    <div id='content2' className="content">内容</div>
                </div>
                <div className="lable">
                    <span id='title3' className="title">标题</span>
                    <div id='content3' className="content"> 内容</div>
                </div>
                <div className="lable">
                    <span id='title4' className="title">标题</span>
                    <div id='content4' className="content"> 内容</div>
                </div>
                <div className="lable">
                    <span id='title5' className="title">标题</span>
                    <div id='content5' className="content"> 内容</div>
                </div>
                <div className="lable">
                    <span id='title6' className="title">标题</span>
                    <div id='content6' className="content"> 内容</div>
                </div>
            </div>
        )
    }
}


export default EconDashboard_Group
