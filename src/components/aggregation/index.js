import Loadable from "react-loadable";
import Loading from "./loading";

function generateLoadable(path){
    return Loadable({
        loader: () => import(`@/components/aggregation/${path}`),
        loading: Loading
    });
}

//数据库的组件记录与前端组件之间的关系
//组件记录中的allias字段对应前端展示的具体组件
//组件记录中的dataType字段表示该组件中的数据该从哪个接口获取，若dataType字段的值为undefind则，表示该组件不需要从服务器获取数据
const Allias_Com_Map = {
    "text": generateLoadable("text"),
    "video": generateLoadable("video"),
    "audio": generateLoadable("audio"),
    //动态地图
    "dynamicMap": generateLoadable("customizeMap_dynamicMap"),
    //全景地图
    "allMap": generateLoadable("customizeMap_allmap"),
    //香港暴力事件图表
    "HongKong_apexCharts": generateLoadable("HongKong/ApexCharts"),    //条形图
    "HongKong_echartsCore": generateLoadable("HongKong/EchartsCore"),    //折线图
    "HongKong_reactApexChart": generateLoadable("HongKong/ReactApexChart"),    //扇形图
    "HongKong_reactEchartsCore": generateLoadable("HongKong/ReactEchartsCores")    //环形
}; 

//数据类型与查询路径之间的映射
const DataType_Url_Map = {
    "text": "aggregation/text/query/1/10",
    "video": "aggregation/video/query/1/10",
    "audio": "aggregation/audio/query/1/10"
};

export {Allias_Com_Map, DataType_Url_Map};
