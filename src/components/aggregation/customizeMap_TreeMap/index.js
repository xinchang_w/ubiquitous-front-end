import { div } from "echarts-gl";
import React, { Component } from "react";

import './TreeMap.css'

class TreeMap extends Component {
    constructor(props) {
        super(props);

        this.state = {
            area: "YRD"
        }
        if (this.props.area) {
            this.state.area = this.props.area
        }
    }


    componentDidMount() {
        console.log("TreeMap", this.props)
        this.props.onRef(this);
    }
    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容

            <img id="treemap" className="treeMapbox" src={"data/" + this.state.area + "/" + this.state.area + "_treemap.svg"} alt="" />


        )
    }
}

export default TreeMap
