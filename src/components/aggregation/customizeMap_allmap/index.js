import React, { useRef, useEffect } from "react";

import style from './allmap.module.css'
/* 

展示百度街景

*/
const BMap = window.BMap

export default class myMap extends React.Component {
    componentDidMount() {
        // 创建百度地图对象
        // 参数：表示地图容器的id值
        const map = new BMap.Map('container')
        map.centerAndZoom(new BMap.Point(120.305456, 31.570037), 12);
        map.enableScrollWheelZoom(true);
        // 覆盖区域图层测试




        // //添加全景控件

        map.addTileLayer(new BMap.PanoramaCoverageLayer());
        var panorama = new BMap.Panorama('container1');
        panorama.setPov({ heading: -40, pitch: 6 });
        panorama.setPosition(new BMap.Point(120.305456, 31.570037));
        //监听地图点击事件
        map.addEventListener('click', function (e) {
            panorama.setPosition(new BMap.Point(e.point.lng, e.point.lat));
        });

    }

    render() {
        return (

            <div className={style.mapContainer}>
                {/* 地图容器： */}
                <div id="container" className={style.mapBox} />
                <div id="container1" className={style.mapBox} />
            </div>
        )
    }
}
