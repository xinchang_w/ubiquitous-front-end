import { Loading } from '@/components'
function Load(props){
    return (
        <Loading size='large' masker={false} loading={true} />
    )
}

export default Load;