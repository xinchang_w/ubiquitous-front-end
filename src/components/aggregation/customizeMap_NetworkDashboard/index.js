import React, { useRef, useEffect, Component } from "react";

import L from "leaflet";
import './NetworkDashboard.css'
import yrd_cities from './data/YRD_cities_projection.json'
import yrd_districts from './data/YRD_districts.json'

import * as d3 from 'd3';
import CanvasFlowmapLayer from "./CanvasFlowmapLayer";
class NetworkMap extends Component {
    constructor(props) {
        super(props);
        this.area = "YRD"
        if (this.props.area) {
            this.area = this.props.area
        }

    }
    dataChange(areaData) {

        regionalMap(areaData, this.yrdLinksLayer, this.jsonLayer, this.mymap, false);
    }
    componentDidMount() {

        this.props.onRef(this);

        this.mymap = L.map('mapDiv').setView([31.298, 119], 6);
        L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/light_all/{z}/{x}/{y}{r}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
            maxZoom: 18
        }).addTo(this.mymap);

        this.yrdLinksLayer = L.layerGroup();


        this.jsonLayer = L.geoJSON(yrd_cities, {
            "color": "#696969",
            "weight": 0.5,
            "fillOpacity": 0,
            onEachFeature: onEachFeature1
        })
        this.jsonLayer.addTo(this.mymap)


        regionalMap(this.area, this.yrdLinksLayer, this.jsonLayer, this.mymap, true);
    }
    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容
            <div id="mapDiv" className="mapBoxNetWork"> </div>
        )
    }
}

function onEachFeature1(feature, layer) {
    // does this feature have a property named popupContent?
    if (feature.properties && feature.properties.CityNameC) {
        layer.bindPopup(feature.properties.CityNameC);
    }
}
function regionalMap(area, yrdLinksLayer, jsonLayer, mymap, truetag) {

    yrdLinksLayer.clearLayers();

    if (truetag) {
        jsonLayer.remove(mymap)
        if (area == "YRD") {
            jsonLayer = L.geoJSON(yrd_cities, {
                "color": "#696969",
                "weight": 0.5,
                "fillOpacity": 0,
                onEachFeature: onEachFeature1
            })
            jsonLayer.addTo(mymap)
        } else {
            jsonLayer = L.geoJSON(yrd_districts, {
                "color": "#696969",
                "weight": 0.5,
                "fillOpacity": 0,
                onEachFeature: onEachFeature2
            })
            jsonLayer.addTo(mymap)
        }
    }


    d3.csv("data/" + area + "/" + area + "_" + "YRD_nodes.csv", function (nodes) {
        return nodes;
    }).then(function (nodes2) {

        for (var i = 0; i < nodes2.length; i++) {
            L.circle([nodes2[i].geoY, nodes2[i].geoX], {
                radius: getRadius(nodes2[i].weight, nodes2[0].weight, 'regional'),
                weight: 1,
                color: '#ff7706',
                fillColor: '#ff7706',
                fillOpacity: 0.5,
            }).addTo(yrdLinksLayer)
        }
    })



    d3.csv("data/" + area + "/" + area + "_" + "YRD_edges.csv", function (edges) {
        return edges;
    }).then(function (edges1) {
        var geoJsonFeatureCollection = {}
        geoJsonFeatureCollection.type = 'FeatureCollection'
        geoJsonFeatureCollection.features = []
        for (var i = 0; i < edges1.length; i++) {
            var point =
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [edges1[i].node1_geoX, edges1[i].node1_geoY]
                },
                'properties': {
                    'weight': getWidth(edges1[i].weight, edges1[0].weight),
                    'original_ID': edges1[i].node1,
                    'original_lon': edges1[i].node1_geoX,
                    'original_lat': edges1[i].node1_geoY,
                    'destination_ID': edges1[i].node2,
                    'destination_lon': edges1[i].node2_geoX,
                    'destination_lat': edges1[i].node2_geoY
                }
            }
            geoJsonFeatureCollection.features.push(point)
        }

        //var flowmapLayer = L.canvasFlowmapLayer(geoJsonFeatureCollection, {
        var flowmapLayer = new CanvasFlowmapLayer(geoJsonFeatureCollection, {
            originAndDestinationFieldIds: {
                weight: 'weight',
                originUniqueIdField: 'original_ID',
                originGeometry: {
                    x: 'original_lon',
                    y: 'original_lat'
                },
                destinationUniqueIdField: 'destination_ID',
                destinationGeometry: {
                    x: 'destination_lon',
                    y: 'destination_lat'
                }
            },
            animationStarted: false,
        }).addTo(yrdLinksLayer);
    });

    yrdLinksLayer.addTo(mymap);
}

function onEachFeature2(feature, layer) {
    // does this feature have a property named popupContent?
    if (feature.properties && feature.properties.NAME) {
        layer.bindPopup(feature.properties.NAME);
    }
}
function getRadius(weight, max, type) {
    if (type == 'regional')
        return (((weight - 1) / (max - 1) * (13 - 1) + 2) * 7000);
    else if (type == 'china')
        return (((weight - 1) / (max - 1) * (13 - 1) + 2) * 7000) * 4;
    else if (type == 'global')
        return (((weight - 1) / (max - 1) * (13 - 1) + 2) * 7000) * 10;
}
function getWidth(weight, max) {
    return ((weight - 1) / (max - 1) * (8 - 1) + 1);
}
export default NetworkMap
