import React, { useState, useEffect } from "react";
import { Spin, message } from "antd";
import axios from "axios";
import { FILE_SERVER } from "@/assets/global";

/*
显示一条或最多两条音频数据
如果传递了data，则直接显示data中的数据，否则显示根据queryCon查询到的数据
@param data { Object Array } 音频数据数组
@param url { String } 查询路径
@param queryCon { Object } 查询条件
*/

const audioStyle = {
    display: "block",
    margin: "10px auto"
};

function Audio(props) {
    let { data, url, queryCon } = props;
    let [audioItems, setAudioItems] = useState([]);
    let [spinning, setSpinning] = useState(true);

    useEffect(() => {
        if(data){
            setAudioItems(data.silce(0, 2));
            return;
        }
        axios.get(url, {
            params: queryCon
        }).then(res => {
            if(res.data.code){
                setAudioItems(res.data.data.textData.slice(0, 2));
                setSpinning(false);
            }
            else{
                throw new Error(res.data.msg);
            }
        }).catch(err => {
            console.log(err);
            setSpinning(false);
            message.error("加载音频数据失败");
        });
 
    }, []);

    return (
        <Spin spinning={spinning}>
            {
                audioItems.map(i => (
                    <audio key={i._id} controls src={`${FILE_SERVER}/${i.audio}`} style={audioStyle}>
                        你的浏览器不支持audio标签
                    </audio>
                ))
            }
        </Spin>
    );
}

export default Audio;