import React, { Component } from "react";

import './SelectMap.css'

import NetworkDashboard from "@/components/aggregation/customizeMap_NetworkDashboard"
import ChinaNetworkDashboard from "@/components/aggregation/customizeMap_ChinaNetworkDashboard"
import GlobalNetworkDashboard from "@/components/aggregation/customizeMap_GlobalNetworkDashboard"
import TreeMap from "@/components/aggregation/customizeMap_TreeMap"
import Card from "@/components/customizeCard"
class SelectMap extends Component {
    constructor(props) {
        super(props)
        this.area = "YRD";
        this.state = {
            in: "长三角区域内部合作形态",
            china: "长三角区域与国内其它地区（不含港澳台）合作形态",
            international: "长三角区域与国际及港澳台地区合作形态",
        }
    };
    componentDidMount() {

    }


    onRef(ref) {
        this.TreeMap = ref;
    }

    onRefNetwork(ref) {
        this.NetworkDashboard = ref;
    }
    onRefChinaNetwork(ref) {
        this.ChinaNetworkDashboard = ref;
    }
    onRefGlobalNetwork(ref) {
        this.GlobalNetworkDashboard = ref;
    }

    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容
            <div id="SelectMap" className="mapContainer">

                <Card title="地域选择工具栏" style={{ width: 400, height: 100 }} movable={true}>
                    <RadioGroup name="mvvm" onChange={e => this.onGroupChange(e)} defaultSelect={0}>
                        <Radio>长三角区域</Radio>
                        <Radio>上海市</Radio>
                        <Radio>江苏省</Radio>
                        <Radio>浙江省</Radio>
                        <Radio>安徽省</Radio>
                    </RadioGroup>
                </Card>



                <Card title="创新领域信息概览" style={{ width: 280, height: 500 }} movable={true}>
                    <TreeMap onRef={this.onRef.bind(this)} id="TreeMap" area={this.area} />
                </Card>


                <Card title={this.state.in} style={{ width: 566, height: 486 }} movable={true}>
                    <NetworkDashboard onRef={this.onRefNetwork.bind(this)} id="NetworkDashboard" area={this.area} />
                </Card>
                <Card title={this.state.china} style={{ width: 711, height: 316 }} movable={true}>
                    <ChinaNetworkDashboard id="ChinaNetworkDashboard" onRef={this.onRefChinaNetwork.bind(this)} id="ChinaNetworkDashboard" area={this.area} />
                </Card>

                <Card title={this.state.international} style={{ width: 711, height: 316 }} movable={true}>
                    <GlobalNetworkDashboard id="GlobalNetworkDashboard" onRef={this.onRefGlobalNetwork.bind(this)} id="ChinaNetworkDashboard" area={this.area} />
                </Card>







            </div>

        )
    }
    onGroupChange(e) {
        // var card1 = document.getElementById('card1')
        // card1.title = 'card1'
        switch (e.target.value) {
            case "0":

                this.setState({
                    in: "长三角区域内部合作形态",
                    china: "长三角区域与国内其它地区（不含港澳台）合作形态",
                    international: "长三角区域与国际及港澳台地区合作形态",
                })
                this.area = "YRD"

                break;
            case "1":
                this.area = "Shanghai"
                this.setState({
                    in: "上海市在长三角区域内部合作形态",
                    china: "上海市与国内其它地区（不含港澳台）合作形态",
                    international: "上海市与国际及港澳台地区合作形态",
                })
                break;
            case "2":
                this.setState({
                    in: "江苏省在长三角区域内部合作形态",
                    china: "江苏省与国内其它地区（不含港澳台）合作形态",
                    international: "江苏省与国际及港澳台地区合作形态",
                })
                this.area = "Jiangsu"
                break;
            case "3":
                this.setState({
                    in: "浙江省在长三角区域内部合作形态",
                    china: "浙江省与国内其它地区（不含港澳台）合作形态",
                    international: "浙江省与国际及港澳台地区合作形态",
                })
                this.area = "Zhejiang"
                break;
            case "4":
                this.setState({
                    in: "安徽省在长三角区域内部合作形态",
                    china: "安徽省与国内其它地区（不含港澳台）合作形态",
                    international: "安徽省与国际及港澳台地区合作形态",
                })
                this.area = "Anhui"
                break;
            default:
                this.area = "YRD"
                break;
        }

        // console.log("select", this.area)

        this.TreeMap.setState({ area: this.area })
        this.NetworkDashboard.dataChange(this.area)
        this.ChinaNetworkDashboard.dataChange(this.area)
        this.GlobalNetworkDashboard.dataChange(this.area)
    }
}
// 父级组件
const RadioGroup = (props) => {
    let index = 0;
    let defaultSelect = props.defaultSelect;

    return (
        <div className="selectMapbox" onChange={e => typeof props.onChange === 'function' && props.onChange(e)}>
            {
                React.Children.map(props.children, child => {
                    return React.cloneElement(child, {
                        name: props.name,
                        // 设置index
                        value: index++,
                        //  设置默认勾选
                        defaultChecked: (index - 1) === defaultSelect
                    })
                })
            }
        </div>
    )
}

// 子组件
const Radio = ({ children, ...props }) => (
    <label className="selectLable">
        <input type="radio" {...props} data-name={children} /> {children}
    </label>
)

export default SelectMap
