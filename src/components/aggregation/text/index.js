import React, { useState, useEffect } from "react";
import { Spin, message } from "antd";
import axios from "axios";
import { NODE_PATH } from "@/assets/global";
import style from "./index.module.css";

/*
显示一条或多条文本数据
如果传递了data，则直接显示data中的数据，否则显示根据queryCon查询到的数据
@param data { Object Array } 文本数据数组
@param url { String } 查询路径
@param queryCon { Object } 查询条件
*/
function Text(props){
    let { data, url, queryCon } = props;
    let [textItems, setTextItems] = useState([]);
    let [spinning, setSpining] = useState(true);
    
    useEffect(() => {
        if(data){
            setTextItems(data);
            return;
        }
        axios.get(url, {
            params: queryCon
        }).then(res => {
            if(res.data.code){
                setSpining(false);
                setTextItems(res.data.data.textData);
            }
            else{
                throw new Error(res.data.msg);
            }
        }).catch(err => {
            setSpining(false);
            console.log(err);
            message.error("查询文本数据失败！");
        });
    }, []);

    return (
        <Spin spinning={spinning} style={{height: "100%"}}>
            <div className={style.textBox}>
                {
                    textItems.map(item => (
                        <div className={style.item} key={item._id}>
                            <h5 className={style["item-h5"]}>
                                <span>{item.author}</span>
                                <span>{item.time}</span>
                            </h5>
                            <p className={style["item-p"]}>{item.content}</p>
                        </div>
                    ))
                }
            </div>
        </Spin>
    );
}

export default Text;