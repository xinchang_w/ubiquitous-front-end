import React, { useRef, useEffect, Component } from "react";
import L from "leaflet";
import './GlobalNetworkDashboard.css'
import china_basemap from './data/China_basemap.json'
import * as d3 from 'd3';
import CanvasFlowmapLayer from "./CanvasFlowmapLayer";
class GlobalNetworkMap extends Component {
    constructor(props) {
        super(props);
        this.area = "YRD"
        if (this.props.area) {
            this.area = this.props.area
        }

    }
    dataChange(areaData) {
        regionalMap(areaData, this.globalLinksLayer, this.NetworkMap);
    }
    componentDidMount() {
        this.props.onRef(this);

        this.NetworkMap = L.map('globalNetworDiv').setView([25, 100], 1);
        L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/light_all/{z}/{x}/{y}{r}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
            subdomains: 'abcd',
            maxZoom: 19
        }).addTo(this.NetworkMap);

        this.globalLinksLayer = L.layerGroup();


        regionalMap(this.area, this.globalLinksLayer, this.NetworkMap);

    }
    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容

            <div id="globalNetworDiv" className="mapBoxChinaNetWork"> </div>
        )
    }
}

function regionalMap(area, globalLinksLayer, NetworkMap) {

    globalLinksLayer.clearLayers();
    d3.csv("data/" + area + "/" + area + "_global_nodes.csv", function (nodes) {
        return nodes;
    }).then(function (nodes2) {

        for (var i = 0; i < nodes2.length; i++) {
            L.circle([nodes2[i].geoY, nodes2[i].geoX], {
                radius: getRadius(nodes2[i].weight, nodes2[0].weight, 'global'),
                weight: 1,
                color: '#ff7706',
                fillColor: '#ff7706',
                fillOpacity: 0.5,
            }).addTo(globalLinksLayer)
        }
    });
    d3.csv("data/" + area + "/" + area + "_global_edges.csv", function (edges) {
        return edges;
    }).then(function (edges2) {

        var geoJsonFeatureCollection = {}
        geoJsonFeatureCollection.type = 'FeatureCollection'
        geoJsonFeatureCollection.features = []
        for (var i = 0; i < edges2.length; i++) {
            var point =
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [edges2[i].node1_geoX, edges2[i].node1_geoY]
                },
                'properties': {
                    'weight': getWidth(edges2[i].weight, edges2[0].weight),
                    'original_ID': edges2[i].node1,
                    'original_lon': edges2[i].node1_geoX,
                    'original_lat': edges2[i].node1_geoY,
                    'destination_ID': edges2[i].node2,
                    'destination_lon': edges2[i].node2_geoX,
                    'destination_lat': edges2[i].node2_geoY
                }
            }
            geoJsonFeatureCollection.features.push(point)
        }
        var flowmapLayer = new CanvasFlowmapLayer(geoJsonFeatureCollection, {
            originAndDestinationFieldIds: {
                weight: 'weight',
                originUniqueIdField: 'original_ID',
                originGeometry: {
                    x: 'original_lon',
                    y: 'original_lat'
                },
                destinationUniqueIdField: 'destination_ID',
                destinationGeometry: {
                    x: 'destination_lon',
                    y: 'destination_lat'
                }
            },
            animationStarted: false,
        }).addTo(globalLinksLayer);
    });

    globalLinksLayer.addTo(NetworkMap);

}


function getRadius(weight, max, type) {
    if (type == 'regional')
        return (((weight - 1) / (max - 1) * (13 - 1) + 2) * 7000);
    else if (type == 'china')
        return (((weight - 1) / (max - 1) * (13 - 1) + 2) * 7000) * 4;
    else if (type == 'global')
        return (((weight - 1) / (max - 1) * (13 - 1) + 2) * 7000) * 10;
}
function getWidth(weight, max) {
    return ((weight - 1) / (max - 1) * (8 - 1) + 1);
}


export default GlobalNetworkMap
