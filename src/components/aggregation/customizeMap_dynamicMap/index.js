import React, { useRef, useEffect } from "react";
import L from 'leaflet'

function CusMap(props){
    let { width = "100%", height = "100%", center, zoom, geoJSON } = props;
    let map = new MapOpr();
    let ref = useRef();
    useEffect(() => {
        map.init(ref.current, center, zoom);
        map.loadGeoJsonData()
        return () => {
            map.destory();
        };
    }, []);

    return (

        <div ref={ref} style={{ width, height }}></div>

    );
}

class MapOpr {
    constructor() {
        this._map = null;
        this.interval = null;
    }

    init(elem, center, zoom) {

        let map = L.map(elem).setView([32.11458, 118.903702], 15);
        map.options.crs = L.CRS.EPSG3857;

        let baseLayer = L.tileLayer("http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}", {
            attribution: '&copy; 高德地图',
            maxZoom: 20,
            minZoom: 6,
            subdomains: "1234"
        });
        map.addLayer(baseLayer);
        this._map = map;
    }

    // 请求 GeoJson
    loadGeoJsonData() {
        fetch('./geojson/sum.json')
            .then((res) => { return res.json(); })
            .then((data) => {

                let counter = 0;
                let that = this;
                let lastLayers = [];
                let currentLayers = [];

                this.interval = setInterval(function () {
                    if (counter >= data.length) {
                        counter = 0;
                    }

                    lastLayers = currentLayers;
                    currentLayers = [];

                    fetch('./geojson/' + data[counter++])
                        .then((res) => { return res.json(); })
                        .then((gjson) => {

                            for (var i = 0; i < gjson.length; i++) {
                                let ss = L.geoJson(gjson[i], {
                                    style: {
                                        "color": gjson[i]['properties']['color'],
                                        "weight": 2
                                    },
                                }).addTo(that._map);

                                currentLayers.push(ss)
                            }


                            if (lastLayers.length > 0) {
                                for (var i = 0; i < lastLayers.length; i++) {
                                    that._map.removeLayer(lastLayers[i])
                                }
                                lastLayers.length = 0;
                            }
                        });

                }, 1000)
            })

    }

    destory(){
        clearInterval(this.interval);
    }

}

export default CusMap;
