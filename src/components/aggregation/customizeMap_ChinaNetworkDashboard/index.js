import React, { useRef, useEffect, Component } from "react";
import L from "leaflet";
import './ChinaNetworkDashboard.css'
import china_basemap from './data/China_basemap.json'
import * as d3 from 'd3';
import CanvasFlowmapLayer from "./CanvasFlowmapLayer";
class ChinaNetworkMap extends Component {
    constructor(props) {
        super(props);
        this.area = "YRD"
        if (this.props.area) {
            this.area = this.props.area
        }

    }
    dataChange(areaData) {


        regionalMap(areaData, this.chinaLinksLayer, this.NetworkMap);
    }
    componentDidMount() {
        this.props.onRef(this);
        this.NetworkMap = L.map('chinaNetworDiv').setView([38, 104], 3);
        L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/light_all/{z}/{x}/{y}{r}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
            subdomains: 'abcd',
            maxZoom: 19
        }).addTo(this.NetworkMap);

        L.geoJSON(china_basemap, {
            "color": "#696969",
            "weight": 0.5,
            "fillOpacity": 0,
            onEachFeature: onEachFeature3
        }).addTo(this.NetworkMap)

        this.chinaLinksLayer = L.layerGroup();
        regionalMap(this.area, this.chinaLinksLayer, this.NetworkMap);
        // regionalMap(chinaLinksLayer, jsonLayer, NetworkMap);
    }
    render() {
        return ( //这里(不能写到下一行，否则react认为render中没有返回任何内容

            <div id="chinaNetworDiv" className="mapBoxChinaNetWork"> </div>


        )
    }
}

function regionalMap(area, chinaLinksLayer, NetworkMap) {
    chinaLinksLayer.clearLayers();


    d3.csv("data/" + area + "/" + area + "_China_nodes.csv", function (nodes) {
        return nodes;
    }).then(function (nodes2) {

        for (var i = 0; i < nodes2.length; i++) {
            L.circle([nodes2[i].geoY, nodes2[i].geoX], {
                radius: getRadius(nodes2[i].weight, nodes2[0].weight, 'china'),
                weight: 1,
                color: '#ff7706',
                fillColor: '#ff7706',
                fillOpacity: 0.5,
            }).addTo(chinaLinksLayer)
        }
    });
    d3.csv("data/" + area + "/" + area + "_China_edges.csv", function (edges) {
        return edges;
    }).then(function (edges2) {

        var geoJsonFeatureCollection = {}
        geoJsonFeatureCollection.type = 'FeatureCollection'
        geoJsonFeatureCollection.features = []
        for (var i = 0; i < edges2.length; i++) {
            var point =
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [edges2[i].node1_geoX, edges2[i].node1_geoY]
                },
                'properties': {
                    'weight': getWidth(edges2[i].weight, edges2[0].weight),
                    'original_ID': edges2[i].node1,
                    'original_lon': edges2[i].node1_geoX,
                    'original_lat': edges2[i].node1_geoY,
                    'destination_ID': edges2[i].node2,
                    'destination_lon': edges2[i].node2_geoX,
                    'destination_lat': edges2[i].node2_geoY
                }
            }
            geoJsonFeatureCollection.features.push(point)
        }
        var flowmapLayer = new CanvasFlowmapLayer(geoJsonFeatureCollection, {
            originAndDestinationFieldIds: {
                weight: 'weight',
                originUniqueIdField: 'original_ID',
                originGeometry: {
                    x: 'original_lon',
                    y: 'original_lat'
                },
                destinationUniqueIdField: 'destination_ID',
                destinationGeometry: {
                    x: 'destination_lon',
                    y: 'destination_lat'
                }
            },
            animationStarted: false,
        }).addTo(chinaLinksLayer);
    });

    chinaLinksLayer.addTo(NetworkMap);

}


function getRadius(weight, max, type) {
    if (type == 'regional')
        return (((weight - 1) / (max - 1) * (13 - 1) + 2) * 7000);
    else if (type == 'china')
        return (((weight - 1) / (max - 1) * (13 - 1) + 2) * 7000) * 4;
    else if (type == 'global')
        return (((weight - 1) / (max - 1) * (13 - 1) + 2) * 7000) * 10;
}
function getWidth(weight, max) {
    return ((weight - 1) / (max - 1) * (8 - 1) + 1);
}

function onEachFeature3(feature, layer) {
    // does this feature have a property named popupContent?
    if (feature.properties && feature.properties.NAME) {
        layer.bindPopup(feature.properties.NAME);
    }
}
export default ChinaNetworkMap
