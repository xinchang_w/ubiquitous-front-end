import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { mainRoutes, HOME_PAGE, ROOT_ROUTE } from '@/routes';
import { PageFrame } from '@/components';
import "@/assets/style/common.css";

const SITE_TITLE = '泛在数据信息建模平台';
export default class App extends Component {
	render() {
		return (
			<Switch>
				{
					mainRoutes.map(route => {
						const { homePage } = route;
						return (
							<Route key={route.key} path={`${ROOT_ROUTE}/${route.key}`} render={({match, history}) => {
								return (
								    homePage
									?
								    <route.component match={match} />
								    :
								    <PageFrame page={route.key}>
								 		<route.component pageTitle={`${route.title}-${SITE_TITLE}`} match={match} history={history} />
								 	</PageFrame>
								)
								// return (
								// 	<PageFrame page={route.key}>
								// 		<route.component pageTitle={`${route.title}-${SITE_TITLE}`} />
								// 	</PageFrame>
								// );
							}} />
						)
					})
				}
				<Redirect to={HOME_PAGE} from="/" exact />
				<Redirect to={HOME_PAGE} from={ROOT_ROUTE} exact />
				<Redirect to={`${ROOT_ROUTE}/404`} />
			</Switch>
		)
	}
}

