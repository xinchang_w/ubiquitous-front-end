const WORLD_COUNTRIES = ["Somalia", "Liechtenstein", "Morocco", "W. Sahara", "Serbia", "Afghanistan", "Angola", "Albania", "Aland", "Andorra", "United Arab Emirates", "Argentina", "Armenia", "American Samoa", "Fr. S. Antarctic Lands", "Antigua and Barb.", "Australia", "Austria", "Azerbaijan", "Burundi", "Belgium", "Benin", "Burkina Faso", "Bangladesh", "Bulgaria", "Bahrain", "Bahamas", "Bosnia and Herz.", "Belarus", "Belize", "Bermuda", "Bolivia", "Brazil", "Barbados", "Brunei", "Bhutan", "Botswana", "Central African Rep.", "Canada", "Switzerland", "Chile", "China", "Côte d'Ivoire", "Cameroon", "Dem. Rep. Congo", "Congo", "Colombia", "Comoros", "Cape Verde", "Costa Rica", "Cuba", "Curaçao", "Cayman Is.", "N. Cyprus", "Cyprus", "Czech Rep.", "Germany", "Djibouti", "Dominica", "Denmark", "Dominican Rep.", "Algeria", "Ecuador", "Egypt", "Eritrea", "Spain", "Estonia", "Ethiopia", "Finland", "Fiji", "Falkland Is.", "France", "Faeroe Is.", "Micronesia", "Gabon", "United Kingdom", "Georgia", "Ghana", "Guinea", "Gambia", "Guinea-Bissau", "Eq. Guinea", "Greece", "Grenada", "Greenland", "Guatemala", "Guam", "Guyana", "Heard I. and McDonald Is.", "Honduras", "Croatia", "Haiti", "Hungary", "Indonesia", "Isle of Man", "India", "Br. Indian Ocean Ter.", "Ireland", "Iran", "Iraq", "Iceland", "Israel", "Italy", "Jamaica", "Jersey", "Jordan", "Japan", "Siachen Glacier", "Kazakhstan", "Kenya", "Kyrgyzstan", "Cambodia", "Kiribati", "Korea", "Kuwait", "Lao PDR", "Lebanon", "Liberia", "Libya", "Saint Lucia", "Sri Lanka", "Lesotho", "Lithuania", "Luxembourg", "Latvia", "Moldova", "Madagascar", "Mexico", "Macedonia", "Mali", "Malta", "Myanmar", "Montenegro", "Mongolia", "N. Mariana Is.", "Mozambique", "Mauritania", "Montserrat", "Mauritius", "Malawi", "Malaysia", "Namibia", "New Caledonia", "Niger", "Nigeria", "Nicaragua", "Niue", "Netherlands", "Norway", "Nepal", "New Zealand", "Oman", "Pakistan", "Panama", "Peru", "Philippines", "Palau", "Papua New Guinea", "Poland", "Puerto Rico", "Dem. Rep. Korea", "Portugal", "Paraguay", "Palestine", "Fr. Polynesia", "Qatar", "Romania", "Russia", "Rwanda", "Saudi Arabia", "Sudan", "S. Sudan", "Senegal", "Singapore", "S. Geo. and S. Sandw. Is.", "Saint Helena", "Solomon Is.", "Sierra Leone", "El Salvador", "St. Pierre and Miquelon", "São Tomé and Principe", "Suriname", "Slovakia", "Slovenia", "Sweden", "Swaziland", "Seychelles", "Syria", "Turks and Caicos Is.", "Chad", "Togo", "Thailand", "Tajikistan", "Turkmenistan", "Timor-Leste", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Tanzania", "Uganda", "Ukraine", "Uruguay", "United States", "Uzbekistan", "St. Vin. and Gren.", "Venezuela", "U.S. Virgin Is.", "Vietnam", "Vanuatu", "Samoa", "Yemen", "South Africa", "Zambia", "Zimbabwe"];
const CHINA_PROVINCES = ["台湾","河北","山西","内蒙古","辽宁","吉林","黑龙江","江苏","浙江","安徽","福建","江西","山东","河南","湖北","湖南","广东","广西","海南","四川","贵州","云南","西藏","陕西","甘肃","青海","宁夏","新疆","北京","天津","上海","重庆","香港","澳门","南海诸岛"];


const HOME_CHART_TIME = [1946, 2020];

// const BASE_PATH = "http://172.21.213.105:8086";
const BASE_PATH = "http://106.14.78.235:8086";
const NODE_PATH = "http://94.191.49.160:8081/api";
const FILE_SERVER = "http://94.191.49.160/resource";    //文件服务器

const IFRAME = "http://www.geofuturelab.com/ubiquitous-geoinformation";
export {
  WORLD_COUNTRIES,
  CHINA_PROVINCES,
  HOME_CHART_TIME,
  BASE_PATH,
  NODE_PATH,
  FILE_SERVER,
  IFRAME
}