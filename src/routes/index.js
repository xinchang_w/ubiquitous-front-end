import {
	Home,
	DataCollect,
	DataQuery,
	TagMethod,
    TagResult,
	TagTool,
	ContentList,
	SubjectCus,
	// DataTracing,
	OneSideSrc,
	ProcessSrc,
	// LocationAggregation,
	AggregationTemplate,
	AggregationTask,
	// DynamicUpdate
	UpdateOpr,
	UpdateTask,
	UpdateTaskNanJingTrafficAggregation,
	PlatformHelp,
	MapTool,
	ChartTool,
	ImageTool,
	VideoTool,
	VoiceTool,
	TextTool,
	FileList,
	SoundFileList,
	DataSource
} from '@/views'


//根目录
const ROOT_ROUTE = '/ubiquitous-geoinformation';
//首页路由
const HOME_PAGE = `${ROOT_ROUTE}/home`;
//点击首页中的导航图标后，打开的页面
const FIRST_NAV = `${ROOT_ROUTE}/data-collect`;
//页面路由
const mainRoutes = [
	{
		key: "home",
		component: Home,
		title: '首页',
		homePage: true
	},
	{
		key: "data-collect",
		component: DataCollect,
		title: '数据采集'
	},
	{
		key: "data-query",
		component: DataQuery,
		title: '数据查询'
	},
	{
		key: "data-source",
		component: DataSource,
		title: "数据来源"
	},
	{
		key: "tag-result",
		component: TagResult,
		title: '标签结果'
	},
	{
		key: "tag-method",
		component: TagMethod,
		title: '标签方法'
	},
	{
		key: "tag-tool",
		component: TagTool,
		title: '标签工具'
	},
	{
		key: "content-list",
		component: ContentList,
		title: "内容列表"
	},
	{
        key: "subject-cus",
		component: SubjectCus,
		title: "专题定制"
	},
	// {
	// 	key: "data-tracing",
	// 	component: DataTracing,
	// 	title: '信息溯源'
	// },
	{
        key: "oneSide-src",
		component: OneSideSrc,
		title: "单向溯源"
	},
	{
        key: "process-src",
		component: ProcessSrc,
		title: "过程溯源"
	},
	// {
	// 	key: "location-aggregation",
	// 	component: LocationAggregation,
	// 	title: '位置聚合'
	// },
	{
        key: "aggregation-template",
		component: AggregationTemplate,
		title: "聚合模板"
	},
	{
        key: "aggregation-Task",
		component: AggregationTask,
		title: "聚合任务"
	},
	// {
	// 	key: "dynamic-update",
	// 	component: DynamicUpdate,
	// 	title: '动态更新'
	// }
	{
        key: "update-opr",
		component: UpdateOpr,
		title: "更新算子"
	},
	{
        key: "update-Task",
		component: UpdateTask,
		title: "更新任务"
	},


	//南京市城市交通态势聚合任务
	{
        key: "update-Task-nanjing-city-traffic-aggregation",
		component: UpdateTaskNanJingTrafficAggregation,
		title: "南京市城市交通态势聚合任务",
		params:''
	},


	{
		key: "platform-help",
		component: PlatformHelp,
		title: "平台帮助"
	},

	// 标签工具和方法
	{
		key: "map-tool",
		component: MapTool,
		title: "地图工具"
	},
	{
		key: "chart-tool",
		component: ChartTool,
		title: "地图工具"
	},
	{
		key: "image-tool",
		component: ImageTool,
		title: "地图工具"
	},
	{
		key: "video-tool",
		component: VideoTool,
		title: "视频工具"
	},
	{
		key: "chart-tool",
		component: ChartTool,
		title: "图表工具"
	},
	{
		key: "voice-tool",
		component: VoiceTool,
		title: "声频工具"
	},
	{
		key: "text-tool",
		component: TextTool,
		title: "文字工具"
	},
	{
		key: "image-tool",
		component: ImageTool,
		title: "图片工具"
	},
	{
		key: 'fileList',
		component: FileList,
		tiltle: '图像列表'
	},
	{
		key: 'soundfileList',
		component: SoundFileList,
		tiltle: '图像列表'
	}
];
//导航项
const NAV_ITEMS = [
	{
		key: 'home',
		title: '首页'
	},
	{
		key: 'data-resource',
		title: '数据资源',
		children: [
			{
				key: "data-source",
				title: '数据来源'
			},
			{
				key: 'data-collect',
				title: '数据采集'
			},
			{
				key: 'data-query',
				title: '数据查询'
			}
		]
	},
	{
		key: 'data-tag',
		title: '信息标签',
		children: [
			{
				key: 'tag-result',
				title: '标签结果'
			},
			{
				key: 'tag-method',
				title: '标签方法'
			},
			{
				key: 'tag-tool',
				title: '标签工具'
			}
		]
	},
	{
		key: 'data-lookup',
		title: '信息检索',
		children: [
			{
				key: 'content-list',
				title: '内容列表'
			},
			{
				key: 'subject-cus',
				title: '内容服务'
			},
		]
	},
	{
		key: 'data-tracing',
		title: '信息溯源',
		children: [
			{
				key: 'oneSide-src',
				title: '单向溯源'
			},
			{
				key: 'process-src',
				title: '过程溯源'
			},
		]
	},
	{
		key: 'location-aggregation',
		title: '位置聚合',
		children: [
			{
				key: 'aggregation-template',
				title: '聚合模板'
			},
			{
				key: 'aggregation-Task',
				title: '聚合任务'
			},
		]
	},
	{
		key: 'dynamic-update',
		title: '动态更新',
		children: [
			{
				key: 'update-opr',
				title: '更新算子'
			},
			{
				key: 'update-Task',
				title: '更新任务'
			},
		]
	},
	{
		key: "platform-help",
		title: "平台帮助"
	}
];


export {
	mainRoutes,
	ROOT_ROUTE,
	HOME_PAGE,
	NAV_ITEMS,
	FIRST_NAV
}
