import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom'
import { ConfigProvider } from 'antd'
import reportWebVitals from './reportWebVitals';

import zhCN from 'antd/lib/locale-provider/zh_CN'
import '@/assets/style/style.less'
import App from './App';

ReactDOM.render(
    <ConfigProvider locale={zhCN}>
        <Router>
          <App />
        </Router>
    </ConfigProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
