/*
为字符串片段设置高亮
@param str {String} 字符串
@param startPos {Number|String} 字符串片段起始位置
@param endPos {Number|String} 字符串片段终止位置
*/
function setHighlight(str, startPos, endPos){
    let s = Number(startPos);
    let e = Number(endPos);
    if(isNaN(s) || isNaN(e)){
        return str;
    }

    str = typeof str === "string" ? str : String(str);
    let subStr = str.slice(s - 1, e);
    let resStr = str.replace(subStr, `<span style="background-color: rgb(135, 208, 104)">${subStr}</span>`);
    return (
        <span dangerouslySetInnerHTML={{__html:resStr}}></span>
    );
}

/*
antd表格，点击表格中的列时，阻止继续冒泡
@param e {Object} react事件
*/
function stopPropagation(record, rowIndex){
    return {
        onClick: e => e.stopPropagation()
    };
}

export {
    setHighlight,
    stopPropagation
}