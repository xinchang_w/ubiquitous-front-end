class Canvas{
    constructor(elem){
        if(elem.nodeName.toLocaleLowerCase() !== "canvas" || !elem.getContext){
            this.isSupported = false;
            return ;
        }

        this.canvas = elem;
        this.ctx = elem.getContext("2d");
        this.isSupported = true;
        this.canvasHeight = this.canvas.height;
        this.canvasWidth = this.canvas.width;

        this.ctx.save();    //保存画布初始状态    
    }

    //绘制图像
    //@param img {DOM Ojbect} 图片DOM对象
    drawImage(img){
        if(typeof img !== "object" || img.nodeName.toLocaleLowerCase() !== "img"){
            return;
        }
        if(!this.check()){
            return;
        }

        let w = img.width;
        let h = img.height;
        let x, scale;
        if(w <= this.canvasWidth){
            x = Math.floor((this.canvasWidth - w) / 2);
            this.canvasHeight = h;
            this.canvas.height = h;
            this.ctx.drawImage(img, x, 0, w, h);
            this.ctx.translate(x, 0);
        }
        else{
            scale = this.canvasWidth / w;
            this.canvasHeight = h * scale;
            this.canvas.height = this.canvasHeight;
            this.ctx.scale(scale, scale);
            this.ctx.drawImage(img, 0, 0, w, h);
        }
    }

    //绘制填充矩形
    fillRect({color = "#fff", x = 0, y = 0, w = 0, h = 0} = {}) {
        if(!this.check()){
            return;
        }
        this.ctx.fillStyle = color;
        this.ctx.fillRect(parseInt(x), parseInt(y), parseInt(w), parseInt(h));
        this.ctx.lineWidth = 3;
        this.ctx.strokeStyle = "red";
        this.ctx.strokeRect(x, y, w, h);
    }

    //绘制路径
    //@param arr {Array} 多边形节点坐标数组
    drawPolygon(arr){

    }

    //清空画布
    clear() {
        this.ctx.restore();
        this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
    }

    //检查浏览器是否支持canvas
    check(){
        return this.isSupported
    }
}

export default Canvas;