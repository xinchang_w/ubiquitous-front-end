const { override, fixBabelImports, addLessLoader, addDecoratorsLegacy, addWebpackAlias, addWebpackPlugin } = require('customize-cra');
// const webpack = require("webpack");
// process.env.GENERATE_SOURCEMAP = "false";
module.exports = override(
  addDecoratorsLegacy(),
  // fixBabelImports('import', {
  //   libraryName: 'antd',
  //   libraryDirectory: 'es',
  //   style: true
  // }),
  addLessLoader({
    javascriptEnabled: true,
  }),
  addWebpackAlias({
    ['@']: require('path').resolve(__dirname, 'src')
  }),
  // addWebpackPlugin(new webpack.DefinePlugin({
  //   test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
  //   use: [
  //     {
  //       loader: 'babel-loader',
  //     },
  //     {
  //       loader: '@svgr/webpack',
  //       options: {
  //         babel: false,
  //         icon: true,
  //       },
  //     },
  //   ],
  // }))
)